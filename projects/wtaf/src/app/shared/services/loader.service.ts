import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable()
export class LoaderService {
    isLoading = new Subject<boolean>();
    show() {
        this.isLoading.next(true);
        // console.log("show yesssssssssssss");
    }
    hide() {
        setTimeout(() => {
            this.isLoading.next(false);
            // console.log("show noooooooooo");
		}, 500);
    }
}