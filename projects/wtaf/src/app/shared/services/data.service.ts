import { Injectable, ChangeDetectorRef } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { AuthenticationService } from '../services/authentication.service';
//import {  AuthenticationService } from '../../../shared/services/authentication.service';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { MatSidenav } from '@angular/material';
import { DatePipe } from '@angular/common';
import { Type } from '../../features/models/type';
import { Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from './loader.service';
const jenkinsapi = require('jenkins-api');
import { InterceptorSkipLoader } from '../../core/loader-interceptors/loader.interceptor';
import { InterceptorSkipLoader_fromWidget } from '../../core/loader-interceptors/loader.interceptor';
import { headSpinToken } from './../../core/auth/jwt.interceptor';
import { env } from 'process';
import { Constvar } from '../../models/constVar';

const jenkins = jenkinsapi.init(environment.urlConfig.jenkinsUrl);
declare var $: any;

@Injectable({
  providedIn: 'root'
})
export class DataService {
  errorWhenInUse: String = '';
  errorUnauthorized: string = '';
  errorUsedregionBrand: string = '';
  isDevicesLoading: boolean = false;
  error_500_status: string = '';
  //for Mobile and Web
  commonJobName = environment.framework_Exe;
  interval: any;
  IsjobName_error = '';
  arrJenkinsSlave: any = [];
  user = JSON.stringify(localStorage.getItem('currentUser'));
  token = `Bearer ` + this.user['token'];
  customFilter = {};
  userName: string;
  platform: string;
  isconsoleOut: boolean = false;
  slaveOS: string;
  serverDevices: any = [];
  isfromWidget: boolean = false;
  consoleOut_datares: any;
  loadarFlag: boolean = false;
  androidGrepCommand ='pm list packages | grep';  
  constructor(
    private http: HttpClient,
    private router: Router,
    private authenticationService: AuthenticationService,
    public authService: AuthenticationService, // public ref: ChangeDetectorRef,
    public deviceService: DeviceDetectorService,
    public toastr: ToastrManager,
    public loaderService: LoaderService
  ) {}

  //Number Validation
  onlyNumberValidation(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 12 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  // =====================================Region API's==============================================================
  saveRegion(details) {
    const apiURL = environment.urlConfig.baseUrl + 'region/addRegion';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllRegion() {
    const url = environment.urlConfig.baseUrl + 'region/getAllRegion';
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getAllRegionByProjectType(typeId) {
    let httpOptions;
    const url =
      environment.urlConfig.baseUrl +
      'region/getAllRegionByProjectType?projectTypeId=' +
      typeId;
    if (this.isfromWidget) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8'
        }).set(InterceptorSkipLoader_fromWidget, '')
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8'
        })
      };
    }

    return this.http.get(url, httpOptions);
  }

  getRegionById(regionId) {
    const url =
      environment.urlConfig.baseUrl +
      'region/getRegionById?regionId=' +
      regionId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteRegion(regionId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'region/deleteRegion?regionId=' +
      regionId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  // =====================================Appliance Category API's==============================================================

  saveApplianceCategory(details) {
    const apiURL =
      environment.urlConfig.baseUrl + 'appliance/addApplianceCategory';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllApplianceCategory(sortColumn, sortOrder) {
    const url =
      environment.urlConfig.baseUrl +
      'appliance/getAllApplianceCategory?sortColumn=' +
      sortColumn +
      '&sortOrder=' +
      sortOrder;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getApplianceCategoryById(applianceCategoryId) {
    const url =
      environment.urlConfig.baseUrl +
      'appliance/getApplianceCategoryById?applianceCategoryId=' +
      applianceCategoryId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteapplianceCategory(applianceCategoryId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'appliance/deleteApplianceCategory?applianceCategoryId=' +
      applianceCategoryId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }
  // =====================================Brand API's==============================================================

  saveBrand(details) {
    const apiURL = environment.urlConfig.baseUrl + 'brand/addBrand';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllBrand() {
    const url = environment.urlConfig.baseUrl + 'brand/getAllBrand';
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getBrandsByRegionId(regionId) {
    let httpOptions;
    const url =
      environment.urlConfig.baseUrl + 'brand/getAllBrand?regionId=' + regionId;
    if (this.isfromWidget) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8',
          authToken: `${this.authService.authToken}`
        }).set(InterceptorSkipLoader_fromWidget, '')
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8',
          authToken: `${this.authService.authToken}`
        })
      };
    }
    return this.http.get(url, httpOptions);
  }

  getByBrandId(brandId) {
    const url =
      environment.urlConfig.baseUrl + 'brand/getBrandById?barndId=' + brandId;

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteBrandById(brandId) {
    const apiURL =
      environment.urlConfig.baseUrl + 'brand/deleteBrand?barndId=' + brandId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }
  // =====================================Brandwise Capability API's==============================================================
  saveBrandWiseCapability(details) {
    const apiURL = environment.urlConfig.baseUrl + 'capability/addBrandWiseCapability';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllBrandWiseCapability() {
    const url = environment.urlConfig.baseUrl + 'capability/getAllBrandWiseCapability';
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  deleteBrandwisecapabilityById(brandCapId) {
    const apiURL =
      environment.urlConfig.baseUrl + 'capability/deleteBrandWiseCapability?brandCapId=' + brandCapId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }
  getByBrandCapId(brandCapId) {
    const url =
      environment.urlConfig.baseUrl + 'capability/getBrandWiseCapabilityById?brandCapId=' + brandCapId;

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  checkForDuplicateInbrandWiseCapability(details) {
    const url =
      environment.urlConfig.baseUrl +
      'capability/checkForDuplicateInbrandWiseCapability?brandId=' +
      details.brandId +
      '&capabilityId=' +
      details.capabilityId +
      '&platformId=' +
      details.platformId +
      '&env=' +
      details.env 
     ;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  
  getAppCapabilitiesByBrand(brandId, env, platformId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'capability/getBrandWiseCapabilityBybrandIdPlatformIdAndEnvironment?brandId=' + brandId + '&environment=' + env + '&platformId='+ platformId
      brandId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }
  // =====================================Appliance API's==============================================================

  saveAppliance(details) {
    const apiURL = environment.urlConfig.baseUrl + 'appliance/addAppliance';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllAppliance() {
    const url = environment.urlConfig.baseUrl + 'appliance/getAllAppliance';

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions).toPromise();
  }

  getByApplianceId(applianceId) {
    const url =
      environment.urlConfig.baseUrl +
      'appliance/getApplianceById?applianceId=' +
      applianceId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteApplianceById(applianceId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'appliance/deleteAppliance?applianceId=' +
      applianceId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  // =====================================Role API's==============================================================
  saveRole(details) {
    const apiURL = environment.urlConfig.baseUrl + 'user/addUserRole';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllRole(sortColumn, sortOrder) {
    const url =
      environment.urlConfig.baseUrl +
      'user/getAllUserRole?sortColumn=' +
      sortColumn +
      '&sortOrder=' +
      sortOrder;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getRoleById(roleId) {
    const url =
      environment.urlConfig.baseUrl + 'user/getUserRoleById?roleId=' + roleId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteRole(roleId) {
    const apiURL =
      environment.urlConfig.baseUrl + 'user/deleteRole?roleId=' + roleId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  // =====================================User API's==============================================================
  saveUser(details) {
    console.log('details', details);
    const apiURL =
      environment.urlConfig.baseUrl +
      'user/addUser?loggedInUserId=' +
      details.loggedInUserId +
      '&profileId=' +
      details.profileId +
      '&userId=' +
      details.userId +
      '&userName=' +
      details.userName +
      '&userRoleId=' +
      details.userRoleId;

    const httpOptions = {
      headers: new HttpHeaders({
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, httpOptions);
  }

  getAllUser() {
    const url = environment.urlConfig.baseUrl + 'user/getAllUser';
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getUserById(profileId) {
    const url =
      environment.urlConfig.baseUrl + 'user/getUserById?profileId=' + profileId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteUser(profileId) {
    const apiURL =
      environment.urlConfig.baseUrl + 'user/deleteUser?profileId=' + profileId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  downloadImage(profileId) {
    var time = Date.now();
    let authToken = 1 + '&timeStamp=' + time;
    const url =
      environment.urlConfig.baseUrl +
      'user/downloadUserImg?profileId=' +
      profileId +
      '&authToken=' +
      authToken;
    let httpOptions = {
      headers: new HttpHeaders({
        responseType: 'blob'
      })
    };
    return url;
  }

  // =====================================Keyword API's==============================================================

  saveKeyword(details) {
    console.log('saveKeyword-->', details);
    const apiURL = environment.urlConfig.baseUrl + 'keyword/addKeyword';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllKeyword() {
    const url = environment.urlConfig.baseUrl + 'keyword/getAllKeywords';
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions).toPromise();
  }

  //Get all keyword by projectTypeId

  getKeywordByProjectTypeId(projectTypeId) {
    const url =
      environment.urlConfig.baseUrl +
      'keyword/getAllKeywords?projectTypeId=' +
      projectTypeId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions).toPromise();
  }

  getKeywordById(keywordId) {
    const url =
      environment.urlConfig.baseUrl +
      'keyword/getKeywordById?keywordId=' +
      keywordId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteKeyword(keywordId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'keyword/deleteKeyword?keywordId=' +
      keywordId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  // =====================================Document API's==============================================================

  saveDocument(details) {
    console.log('saveDocument-->', details);
    const apiURL = environment.urlConfig.baseUrl + 'document/addDocument';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllDocument() {
    const url = environment.urlConfig.baseUrl + 'document/getAllDocuments';
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions).toPromise();
  }

  getDocumentById(documentId) {
    const url =
      environment.urlConfig.baseUrl +
      'document/getDocumentById?documentId=' +
      documentId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteDocument(documentId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'document/deleteDocument?documentId=' +
      documentId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  downloadGoggleDriveDocument(fileId): Observable<HttpResponse<Blob>> {
    const url =
      environment.urlConfig.baseUrl +
      'document/downloadFileFromGoogleDrive?fileId=' +
      fileId;
    return this.http.get<Blob>(url, {
      observe: 'response',
      responseType: 'blob' as 'json'
    });
  }

  downloadConfluenceDocument(title): Observable<HttpResponse<Blob>> {
    const url =
      environment.urlConfig.baseUrl +
      'document/downloadFileFromConfluence?title=' +
      title;
    return this.http.get<Blob>(url, {
      observe: 'response',
      responseType: 'blob' as 'json'
    });
  }

  // =====================================Element API's==============================================================
  saveElement(details) {
    const apiURL = environment.urlConfig.baseUrl + 'element/addElement';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllElement(sortColumn, sortOrder) {
    const url =
      environment.urlConfig.baseUrl +
      'element/getAllElement?sortColumn=' +
      sortColumn +
      '&sortOrder=' +
      sortOrder;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions).toPromise();
  }

  getElementById(elementId) {
    const url =
      environment.urlConfig.baseUrl +
      'element/getElementById?elementId=' +
      elementId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteElement(elementId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'element/deleteElement?elementId=' +
      elementId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  // =====================================Test Suite API's==============================================================
  saveTestSuite(details) {
    const apiURL = environment.urlConfig.baseUrl + 'testsuite/addTestSuite';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  saveTestSuiteAfterImport(details) {
    const apiURL = environment.urlConfig.baseUrl + 'testsuite/importTestSuite';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllTestSuite(
    sortColumn,
    sortOrder,
    regionId,
    brandId,
    projectTypeId,
    projectName,
    pageNo,
    pageSize
  ) {
    let url = '';
    if (projectName != '') {
      url =
        environment.urlConfig.baseUrl +
        'testsuite/getAllTestSuite?regionId=' +
        regionId +
        '&brandId=' +
        brandId +
        '&projectTypeId=' +
        projectTypeId +
        '&projectName=' +
        projectName +
        '&pageNo=' +
        pageNo +
        '&pageSize=' +
        pageSize +
        '&sortColumn=' +
        sortColumn +
        '&sortOrder=' +
        sortOrder;
    } else {
      url =
        environment.urlConfig.baseUrl +
        'testsuite/getAllTestSuite?regionId=' +
        regionId +
        '&brandId=' +
        brandId +
        '&projectTypeId=' +
        projectTypeId +
        '&pageNo=' +
        pageNo +
        '&pageSize=' +
        pageSize +
        '&sortColumn=' +
        sortColumn +
        '&sortOrder=' +
        sortOrder;
    }

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  getAllTestSuiteBySearch(serachValue, pageNo, pageSize) {
    let url = '';
    url =
      environment.urlConfig.baseUrl +
      'testsuite/getAllTestSuiteBySearch?searchBy=' +
      serachValue +
      '&pageNo=' +
      pageNo +
      '&pageSize=' +
      pageSize;

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  getAllTestSuiteByPlatformId(
    sortColumn,
    sortOrder,
    regionId,
    brandId,
    projectTypeId,
    projectName,
    platformId
  ) {
    let url =
      environment.urlConfig.baseUrl +
      'testsuite/getAllTestSuite?regionId=' +
      regionId +
      '&brandId=' +
      brandId +
      '&projectTypeId=' +
      projectTypeId +
      '&projectName=' +
      projectName +
      '&platformId=' +
      platformId +
      '&sortColumn=' +
      sortColumn +
      '&sortOrder=' +
      sortOrder;

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getAllTestSuite_withoutProject(
    regionId,
    brandId,
    projectTypeId,
    projectName
  ) {
    let url = '';

    url =
      environment.urlConfig.baseUrl +
      'testsuite/getTestSuiteNameAndId?brandId=' +
      brandId;

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getTestSuiteById(testSuiteId) {
    const url =
      environment.urlConfig.baseUrl +
      'testsuite/getTestSuiteById?testSuiteId=' +
      testSuiteId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteTestSuite(testSuiteId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'testsuite/deleteTestSuite?testSuiteId=' +
      testSuiteId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  getProjectTypeByBrandId(brandId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'testsuite/getProjectType?brandId=' +
      brandId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }

  getProjectNameByTypeId(brandId, projectTypeId, regionId) {
    let httpOptions;
    const apiURL =
      environment.urlConfig.baseUrl +
      'testsuite/getProjectName?brandId=' +
      brandId +
      '&projectTypeId=' +
      projectTypeId +
      '&regionId=' +
      regionId;
    if (this.isfromWidget) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8'
        }).set(InterceptorSkipLoader_fromWidget, '')
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8'
        })
      };
    }

    return this.http.get(apiURL, httpOptions);
  }

  getTestSuiteNameByProjectName(projectName) {
    let httpOptions;
    const apiURL =
      environment.urlConfig.baseUrl +
      'testsuite/getTestSuiteNameByProjectName?projectName=' +
      projectName;
    if (this.isfromWidget) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8',
          authToken: `${this.authService.authToken}`
        }).set(InterceptorSkipLoader_fromWidget, '')
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8',
          authToken: `${this.authService.authToken}`
        })
      };
    }

    return this.http.get(apiURL, httpOptions);
  }

  // =====================================Test Case API's==============================================================
  saveTestCase(details) {
    const apiURL = environment.urlConfig.baseUrl + 'testcase/addTestCase';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllTestCase(sortColumn, sortOrder, regionId, brandId, testSuiteId) {
    const url =
      environment.urlConfig.baseUrl +
      'testcase/getAllTestCases?regionId=' +
      regionId +
      '&brandId=' +
      brandId +
      '&sortColumn=' +
      sortColumn +
      '&sortOrder=' +
      sortOrder +
      '&testSuiteId=' +
      testSuiteId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getTestCaseByTestSuiteId(sortColumn, sortOrder, testSuiteId) {
    const url =
      environment.urlConfig.baseUrl +
      'testcase/getTestCaseByTestSuiteId?testSuiteId=' +
      testSuiteId +
      '&sortColumn=' +
      sortColumn +
      '&sortOrder=' +
      sortOrder;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getAllTestCasebyProjects(
    sortColumn,
    sortOrder,
    regionId,
    brandId,
    projectTypeId,
    projectName,
    testSuiteId
  ) {
    const url =
      environment.urlConfig.baseUrl +
      'testcase/getAllTestCases?regionId=' +
      regionId +
      '&brandId=' +
      brandId +
      '&projectTypeId=' +
      projectTypeId +
      '&projectName=' +
      projectName +
      '&testSuiteId=' +
      testSuiteId +
      '&sortColumn=' +
      sortColumn +
      '&sortOrder=' +
      sortOrder;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      }).set(InterceptorSkipLoader_fromWidget, this.loadarFlag.toString())
    };
    return this.http.get(url, httpOptions);
  }

  getTestCaseById(testCaseId) {
    const url =
      environment.urlConfig.baseUrl +
      'testcase/getTestCaseById?testCaseId=' +
      testCaseId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteTestCase(testCaseId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'testcase/deleteTestCase?testCaseId=' +
      testCaseId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }
  // =====================================End Test Case API's==============================================================
  // =====================================All Shared Test Case API's ==============================================================
  saveSharedTestCase(details) {
    const apiURL = environment.urlConfig.baseUrl + 'testcase/testCaseSave';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllSharedTestCasebyProjects(
    sortColumn,
    sortOrder,
    regionId,
    brandId,
    projectTypeId,
    projectName,
    isShared,
    testSuiteId
  ) {
    const url =
      environment.urlConfig.baseUrl +
      'testcase/getAllTestCases?regionId=' +
      regionId +
      '&brandId=' +
      brandId +
      '&projectTypeId=' +
      projectTypeId +
      '&projectName=' +
      projectName +
      '&isShared=' +
      isShared +
      '&testSuiteId=' +
      testSuiteId +
      '&sortColumn=' +
      sortColumn +
      '&sortOrder=' +
      sortOrder;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  getSharedTestCaseById(testCaseId) {
    const url =
      environment.urlConfig.baseUrl +
      'testcase/getTestCaseById?testCaseId=' +
      testCaseId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  deleteShareTestCase(testCaseId, testSuiteId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'testcase/deleteTestCase?testCaseId=' +
      testCaseId +
      '&testSuiteId=' +
      testSuiteId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }
  addSharedTestcaseToSuite(details) {
    const apiURL =
      environment.urlConfig.baseUrl + 'testcase/addSharedTestCaseToSuite';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }
  clonesharedTestCase(details) {
    const apiURL =
      environment.urlConfig.baseUrl + 'testcase/cloneSharedTestCase';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }
  //Import File Data
  addImportSharedCaseFileData(caseDetails) {
    const apiURL =
      environment.urlConfig.baseUrl + 'util/importSharedCaseFileData';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http
      .post(apiURL, JSON.stringify(caseDetails), httpOptions)
      .toPromise();
  }
  // =====================================End shared Test Case API's==============================================================
  saveTestStep(details) {
    const apiURL = environment.urlConfig.baseUrl + 'teststep/addTestStep';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllTestStep(regionId, brandId, testSuiteId, testCaseId) {
    const url =
      environment.urlConfig.baseUrl +
      'teststep/getAllTestStep?regionId=' +
      regionId +
      '&brandId=' +
      brandId +
      '&testSuiteId=' +
      testSuiteId +
      '&testCaseId=' +
      testCaseId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getTestStepById(testStepId) {
    const url =
      environment.urlConfig.baseUrl +
      'teststep/getTestStepById?testStepId=' +
      testStepId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteTestStep(testStepId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'teststep/deleteTestStep?testStepId=' +
      testStepId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }
  deleteSharedTestStep(testStepId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'teststep/deleteTestStep?testStepId=' +
      testStepId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }
  // =====================================Utilities API's==============================================================

  checkDuplication(details) {
    const url =
      environment.urlConfig.baseUrl +
      'util/checkForDuplicate?id=' +
      details.id +
      '&resource=' +
      encodeURIComponent(details.resource) +
      '&type=' +
      details.type +
      (details.type == Type.Appliance ? '&optionalId=' + details.brandId : '');
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  checkDuplicationwithOptionalId(details) {
    const url =
      environment.urlConfig.baseUrl +
      'util/checkForDuplicate?id=' +
      details.id +
      '&optionalId=' +
      details.optionalId +
      '&resource=' +
      encodeURIComponent(details.resource) +
      '&type=' +
      details.type +
      (details.type == Type.Appliance
        ? '&optionalId=' + details.platformId
        : '');
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      }).set(InterceptorSkipLoader, '')
    };
    return this.http.get(url, httpOptions);
  }

  getAllProjectType() {
    console.log(
      'this.isfromWidget_ from monthly execution status',
      this.isfromWidget
    );

    let httpOptions;
    const url = environment.urlConfig.baseUrl + 'util/getAllProjectType';

    if (this.isfromWidget) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8',
          authToken: `${this.authService.authToken}`
        }).set(InterceptorSkipLoader_fromWidget, '')
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8',
          authToken: `${this.authService.authToken}`
        })
      };
    }

    console.log('tokennnnnnnnnnnn', this.token);
    return this.http.get(url, httpOptions);
  }

  getPlatformByProjectTypeId(projectTypeId) {
    let httpOptions;
    const url =
      environment.urlConfig.baseUrl +
      'util/getAllPlatform?projectTypeId=' +
      projectTypeId;
    if (this.isfromWidget) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8',
          authToken: `${this.authService.authToken}`
        }).set(InterceptorSkipLoader_fromWidget, '')
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=UTF-8',
          authToken: `${this.authService.authToken}`
        })
      };
    }
    return this.http.get(url, httpOptions);
  }

  exportTestSuiteData(testSuiteId) {
    const url =
      environment.urlConfig.baseUrl +
      'util/exportTestSuiteData?testSuiteId=' +
      testSuiteId;
    return this.http.get(url, { responseType: 'arraybuffer' });
  }

  syncWithJira(testSuiteId) {
    const url =
      environment.urlConfig.baseUrl +
      'util/synchWithJira?testSuiteId=' +
      testSuiteId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'text/plain;charset=UTF-8 ',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, { responseType: 'text' });
  }

  // ===================== Capability API's=============================

  saveCapability(details) {
    const apiURL =
      environment.urlConfig.baseUrl + 'capability/addCapabilityMaster';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllCapability() {
    const url = environment.urlConfig.baseUrl + 'capability/getAllCapability';
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }


  //get capability by Project type Id

  getCapabilitiesByProjectTypeId(platformId) {
    const url =
      environment.urlConfig.baseUrl +
      'capability/getAllCapability?platformId=' +
      platformId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      }).set(InterceptorSkipLoader, '')
    };
    return this.http.get(url, httpOptions);
  }

  getCapabilityById(capabilityId) {
    const url =
      environment.urlConfig.baseUrl +
      'capability/getCapabilityById?capabilityId=' +
      capabilityId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteCapability(capabilityId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'capability/deleteCapability?capabilityId=' +
      capabilityId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  // ===================== Test Data API's=============================
  getTestStepByCase(testCaseId) {
    const url =
      environment.urlConfig.baseUrl +
      '/teststep/getTestStepByCase?testCaseId=' +
      testCaseId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  getTestStepBysuite(testsuiteId) {
    const url =
      environment.urlConfig.baseUrl +
      'teststep/getTestStepBySuite?testSuiteId=' +
      testsuiteId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  saveDataset(details) {
    const apiURL = environment.urlConfig.baseUrl + 'teststep/addDataSet';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  DeleteIterations(dataSetName, iterationId, testsuiteId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'teststep/deleteDataSet?dataSetName=' +
      dataSetName +
      '&iterationId=' +
      iterationId +
      '&testSuiteId=' +
      testsuiteId +
      '';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  getAllDataSetByCaseId(testCaseId) {
    const url =
      environment.urlConfig.baseUrl +
      'teststep/getDataSetName?testCaseId=' +
      testCaseId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getAllDataSetBySuiteId(testSuiteId) {
    const url =
      environment.urlConfig.baseUrl +
      'teststep/getDataSetName?testSuiteId=' +
      testSuiteId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      }).set(InterceptorSkipLoader_fromWidget, this.loadarFlag.toString())
    };
    return this.http.get(url, httpOptions);
  }

  getAllDataSetIterationsByDsName(dsName, testSuiteId) {
    const url =
      environment.urlConfig.baseUrl +
      'teststep/getAllDataSet?dataSetName=' +
      dsName +
      '&testSuiteId=' +
      testSuiteId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  // API for Get list of dataset for selected suites

  getDataSetNameList(suitedetails) {
    const apiURL =
      environment.urlConfig.baseUrl + 'teststep/getDataSetNameList';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, suitedetails, httpOptions);
  }

  getAllTestCasesByMultipleSuite(suitedetails) {
    const apiURL =
      environment.urlConfig.baseUrl + 'testcase/getAllTestCasesByMultipleSuite';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, suitedetails, httpOptions);
  }
  // End of API's for get Testcases and datasets for selected suites
  getcapabilityTemplateByName(templateName) {
    const url =
      environment.urlConfig.baseUrl +
      'exection/getCapabilityTemplates?templateName=' +
      encodeURIComponent(templateName);

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      }).set(InterceptorSkipLoader, '')
    };
    return this.http.get(url, httpOptions);
  }
  getcapabilityTemplateByNameHeadspin(templateType, usrTemplateName, userId) {
    const url =
      environment.urlConfig.baseUrl +
      'exection/getHsCapabilityTemplates?templateType=' +
      templateType +
      '&usrTemplateName=' +
      usrTemplateName +
      '&userId=' +
      userId;

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      }).set(InterceptorSkipLoader, '')
    };
    return this.http.get(url, httpOptions);
  }
  getAllHeadspinDevicesAppiumUrl() {
    const apiURL =
      environment.urlConfig.headspinBaseUrl + 'devices/automation-config';
    const httpOptions = {
      headers: new HttpHeaders({}).set(headSpinToken, 'true')
    };
    return this.http.get(apiURL, httpOptions);
  }
  //Add  Device Capability
  addDeviceCapability(details) {
    const apiURL =
      environment.urlConfig.baseUrl + 'exection/addDeviceCapability';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details[0]), httpOptions);
    // JSON.stringify(details)
  }

  // ===============================================Capability Template===================================================
  AddCapabilityTemplate(details) {
    const apiURL =
      environment.urlConfig.baseUrl + 'exection/addCapabilityTemplate';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  addDeviceFarmCapabilityTemplate(details) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/addDeviceFarmCapabilityTemplate';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getcapabilityTemplateName(userId) {
    const url =
      environment.urlConfig.baseUrl +
      'exection/getCapabilityTemplateName?userId=' +
      userId;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteCapabilityTemplateById(capTemplateId, userId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/deleteCapabilityTemplate?capTemplateId=' +
      capTemplateId +
      '&userId=' +
      userId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  deleteEntireCapabilityTemplate(capabilityTemplateName, userId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/deleteEntireCapabilityTemplate?usrTemplateName=' +
      capabilityTemplateName +
      '&userId=' +
      userId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  // ========================= Save Execution Details ========================================
  addExecutionHeader(details) {
    const apiURL =
      environment.urlConfig.baseUrl + 'exection/addExecutionHeader';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  addExecutionDetails(details) {
    const apiURL =
      environment.urlConfig.baseUrl + 'exection/addExecutionDetails';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }
  //=================================Execution_Schedule_list_parent===================================
  getExecutionSchedule() {
    //
    const apiURL = environment.urlConfig.baseUrl + 'exection/getRunningJobs';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }
  getScheduleListDetails(exeHeaderId, exeHistoryId) {
    const apiURL1 =
      environment.urlConfig.baseUrl +
      'exection/getScheduleListDeatils?exeHeaderId=' +
      exeHeaderId +
      '&exeHistoryId=' +
      exeHistoryId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL1, httpOptions);
  }

  jobReExecution(exeHeaderId, exeHistoryId, userId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/reExecuteJob?exeHeaderId=' +
      exeHeaderId +
      '&exeHistoryId=' +
      exeHistoryId +
      '&userId=' +
      userId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }
  // Prashant Chnages - getScheduleDetails function added
  getScheduleDetails(searchBy, pageNo, pageSize) {
    // get all scheduled data         'Accept':'text/plain',
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/getScheduleList?pageNo=' +
      pageNo +
      '&pageSize=' +
      pageSize +
      '&searchBy=' +
      searchBy;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }
  updateScheduleDetails(details) {
    const apiURL = environment.urlConfig.baseUrl + 'exection/updateSchedule';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }
  deleteSchedule(exeHeaderId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/deleteSchedule?exeHeaderId=' +
      exeHeaderId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }
  //===========================================History Api======================================
  getHistoryDetails() {
    const getUrl = environment.urlConfig.baseUrl + 'exection/getHistoryDetails';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8,'
      })
    };
    return this.http.get(getUrl, httpOptions);
  }

  //===========================================PiaChartStatus_Details Api======================================
  getStatusCountsForPieCharts() {
    const getUrl =
      environment.urlConfig.baseUrl + 'exection/getStatusCountsForPieCharts';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8,'
      })
    };
    return this.http.get(getUrl, httpOptions);
  }

  jira(details) {
    const apiURL1 = 'https://siliconstack.atlassian.net/rest/api/2/issue';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization:
          'Basic QW1iYWRhcy5rZWRhckBzaWxpY29uc3RhY2suY29tLmF1OlRxYkJ0NkR6Y1lRRTZ1UGJtRVFaM0Q5Mg=='
      })
    };
    return this.http.post(apiURL1, httpOptions);
  }
  exportElementsDataAsObject(testSuiteId) {
    const urlasDataObject =
      environment.urlConfig.baseUrl +
      'util/exportElementsDataAsObject?testSuiteId=' +
      testSuiteId;
    return this.http.get(urlasDataObject, { responseType: 'blob' });
  }

  //Import File Data
  addImportFileData(caseDetails) {
    const apiURL = environment.urlConfig.baseUrl + 'util/addImportFileData';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http
      .post(apiURL, JSON.stringify(caseDetails), httpOptions)
      .toPromise();
  }

  //Get All Jira Name
  getAllJiraProjectsName() {
    const exportURL = environment.urlConfig.baseUrl + 'util/getJiraProject';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(exportURL, httpOptions);
  }
  readFile(path) {
    return this.http.get(path, { responseType: 'text' });
  }

  exportData(executionHeaderId, HistoryID, languageId) {
    const serverIP = environment.urlConfig.baseUrl;
    const exportURL =
      environment.urlConfig.baseUrl +
      'util/exportData?executionHeaderId=' +
      executionHeaderId +
      '&executionHistoryId=' +
      HistoryID +
      '&serverIP=' +
      serverIP +
      '&languageId=' +
      languageId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(exportURL, httpOptions);
  }

  getJenkinsBuildLog(jenkinsJobId, strJobName) {
    const url = environment.urlConfig.jenkinsUrl;
    const apiURL1 =
      environment.urlConfig.baseUrl +
      'util/getJenkinsBuildLog?jenkinsJobId=' +
      jenkinsJobId +
      '&url=' +
      url +
      '&jobType=' +
      strJobName;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL1, httpOptions);
  }

  updateHistoryWithArn(historyId, jobarn, mobileAppFileName) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/updateHistoryDetailsARN?exeHistoryId=' +
      historyId +
      '&jobArn=' +
      jobarn +
      '&mobileAppFileName=' +
      mobileAppFileName;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8'
      })
    };
    return this.http.post(apiURL, httpOptions);
  }
  updateHistorystatus(exeHistoryId, isStartTime, jobStatus, runStatus) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/updateHistoryStatus?exeHistoryId=' +
      exeHistoryId +
      '&isStartTime=' +
      isStartTime +
      '&jobStatus=' +
      jobStatus +
      '&runStatus=' +
      runStatus;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, httpOptions);
  }

  addDeviceFarmExtentedReport(exeHeaderId, exeHistoryId, fileUrl, testSuiteId) {
    let details = {
      exeHeaderId: exeHeaderId,
      exeHistoryId: exeHistoryId,
      urls: fileUrl,
      testSuiteId: testSuiteId
    };
    const apiURL =
      environment.urlConfig.baseUrl + 'util/addDeviceFarmExtentedReport';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http
      .post(apiURL, JSON.stringify(details), httpOptions)
      .toPromise();
  }

  //updated exended report
  updateDeviceFarmExtentedReport(exeHeaderId, exeHistoryId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'util/updateReportPathForExtentReport?exeHeaderId=' +
      exeHeaderId +
      '&exeHistoryId=' +
      exeHistoryId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, httpOptions);
  }

  getProjectNameByTypeId_exc(regionId, brandId, projectTypeId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'testsuite/getProjectName?brandId=' +
      brandId +
      '&projectTypeId=' +
      projectTypeId +
      '&regionId=' +
      regionId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }

  filter(key, value) {
    this.customFilter = {};
    this.customFilter[key] = value;
    return this.customFilter;
  }

  getApplianceByBrand(brandId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'appliance/getApplianceByBrand?brandId=' +
      brandId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions).toPromise();
  }

  exportElementFile(brandId, platformId, projectTypeId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'util/exportElementFile?brandId=' +
      brandId +
      '&platformId=' +
      platformId +
      '&projectTypeId=' +
      projectTypeId;
    return this.http.get(apiURL, { responseType: 'blob' });
  }

  cloneTestSuite(details) {
    const apiURL = environment.urlConfig.baseUrl + 'testsuite/cloneTestSuite';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  cloneTestCase(details) {
    const apiURL = environment.urlConfig.baseUrl + 'testcase/cloneTestCase';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  // Added by akash===

  getExtendedReportListById(exeHeaderId, exeHistoryId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/getExtendedReportList?exeHeaderId=' +
      exeHeaderId +
      '&exeHistoryId=' +
      exeHistoryId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions).toPromise();
  }

  getDashboardCountByRegion() {
    const apiURL =
      environment.urlConfig.baseUrl + 'dashboard/getDashboardCounts';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8'
      })
    };
    return this.http.get(apiURL, httpOptions);
  }

  getDashboardCountForTestSuiteTrends(
    endTime,
    platformId,
    projectId,
    projectTypeId,
    regionId,
    startDate,
    suiteId
  ) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/getDashboardCountsForTestExecutionTrends?endDate=' +
      endTime +
      'platformId=' +
      platformId +
      'projectId=' +
      projectId +
      'projectTypeId=' +
      projectTypeId +
      'regionId=' +
      regionId +
      'startDate=' +
      startDate +
      'suiteId=' +
      suiteId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }
  /*---------add jenkins Job id Api-----------------*/
  addJenkinsJobId(exeHistoryId, jenkinsJobId) {
    let details = {
      exeHistoryId: exeHistoryId,
      jenkinsJobId: jenkinsJobId
    };
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/addJenkinsJobId?exeHistoryId=' +
      exeHistoryId +
      '&jenkinsJobId=' +
      jenkinsJobId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  downloadAppiumLog(exeHeaderId, exeHistoryId) {
    const url =
      environment.urlConfig.baseUrl +
      'util/downloadAppiumLogFile?exeHeaderId=' +
      exeHeaderId +
      '&exeHisotryId=' +
      exeHistoryId;
    return this.http.get(url, { responseType: 'text' });
  }

  findIsExists(sourceArr, destinationArr) {
    var result = sourceArr.filter(function(o1) {
      // filter out (!) items in result2
      return destinationArr.some(function(o2) {
        return (
          o1.name === o2.name &&
          o1.os === o2.os &&
          o1.formFactor === o2.formFactor &&
          o1.availability === o2.availability &&
          o1.platform === o2.platform &&
          o1.udid === o2.udid &&
          o1.deviceType === o2.deviceType &&
          o1.selected === o2.selected &&
          o1.id === o2.id
        ); // assumes unique id
      });
    });
    return result.length == destinationArr.length;
  }

  getJobHistory(searchBy, pageNo, pageSize) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/getJobHistory?pageNo=' +
      pageNo +
      '&pageSize=' +
      pageSize +
      '&searchBy=' +
      searchBy;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }

  getJobHistoryDetails(exeHeaderId, exeHistoryId, seeMore) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/getHistoryDetails?exeHeaderId=' +
      exeHeaderId +
      '&exeHistoryId=' +
      exeHistoryId +
      '&seeMore=' +
      seeMore;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }
  addReorderTestcases(details) {
    const apiURL = environment.urlConfig.baseUrl + 'testcase/re_orderTestCase';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    console.log('response of reorder ---', JSON.stringify(details));
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  addReorderTestStep(details) {
    const apiURL = environment.urlConfig.baseUrl + 'teststep/re_orderTestStep';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    console.log('response of reorder ---', JSON.stringify(details));
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  saveJenkinsJob(details) {
    const apiURL = environment.urlConfig.baseUrl + 'util/saveJenkinsLog';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    console.log('response of reorder ---', JSON.stringify(details));
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  downloadJenkinsLog(jenkinsLogFilePath) {
    const url =
      environment.urlConfig.baseUrl +
      'util/downloadJenkinsLog?fileurl=' +
      jenkinsLogFilePath;

    return this.http.get(url, { responseType: 'text' });
  }

  /*login API*/
  login(password, username) {
    let details = {
      password: password,
      username: username
    };
    const apiURL = environment.urlConfig.baseUrl + 'authenticate';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8'
      })
    };
    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  showExtendReport(path) {
    const url =
      environment.urlConfig.baseUrl +
      'util/downloadExtendReportFile?fileurl=' +
      encodeURIComponent(path);
    return this.http.get(url);
  }

  //<============================get template name by header id=====================================>
  getTemplateNamebyHeaderId(headerId) {
    const baseUrl =
      environment.urlConfig.baseUrl +
      'exection/getTemplateNameByHeaderId?exeHeaderId=' +
      headerId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(baseUrl, httpOptions);
  }

  checkTestSuiteUsedInExecution(testSuiteId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'testsuite/checkTestSuiteUsedInExecution?testSuiteId=' +
      testSuiteId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }

  // =====================================Language API's==============================================================

  saveLanguage(details) {
    const apiURL = environment.urlConfig.baseUrl + 'language/addLanguage';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllLanguage() {
    const url = environment.urlConfig.baseUrl + 'language/getAllLanguage';
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getByLanguageId(languageId) {
    const url =
      environment.urlConfig.baseUrl +
      'language/getLanguageById?languageId=' +
      languageId;

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteLanguageById(languageId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'language/deleteLanguage?languageId=' +
      languageId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  // =====================================Label API's==============================================================

  saveLabel(details) {
    const apiURL =
      environment.urlConfig.baseUrl + 'languageLabel/addlanguageLabel';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(apiURL, JSON.stringify(details), httpOptions);
  }

  getAllLabel() {
    const url = environment.urlConfig.baseUrl + 'languageLabel/getAllLabel';
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  getByLabelId(labelId) {
    const url =
      environment.urlConfig.baseUrl +
      'languageLabel/getLabelById?labelId=' +
      labelId;

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(url, httpOptions);
  }

  deleteLabelById(labelId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'languageLabel/deleteLabel?labelId=' +
      labelId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  exportLabelFile() {
    const apiURL = environment.urlConfig.baseUrl + 'util/exportLabelFile';
    return this.http.get(apiURL, { responseType: 'blob' });
  }

  // =============================Get Template List by HistoryId==============================
  getTemplateNameByHistoryId(historyId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/getTemplateNameByHistoryId?exeHistoryId=' +
      historyId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }

  // =============================Get Selected Suite cases data set List by HeaderId HistoryId==============================
  getSelectedSuitedata(headerId, historyId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/getSelectedTestSuites?exeHeaderId=' +
      headerId +
      '&exeHistoryId=' +
      historyId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }

  // =============================Get Environment details by HeaderId HistoryId==============================

  getEnvironmentdetails(headerId, historyId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/getEnvDetailsForTestSuites?exeHeaderId=' +
      headerId +
      '&exeHistoryId=' +
      historyId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.get(apiURL, httpOptions);
  }

  //  ===============================Update Execution Name ===========================
  updateExecutionName(exeHeaderId, executionName) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/changeTestExecutionName?exeHeaderId=' +
      exeHeaderId +
      '&executionName=' +
      executionName;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.put(apiURL, httpOptions);
  }

  //====================================== Delete All Job History By HeaderID=================
  DeleteAllJob_by_HeaderID(headerID) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/deleteJobHistoryDetails?exeHeaderId=' +
      headerID;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(apiURL, JSON.stringify(headerID), httpOptions);
  }
  //====================================== Delete  Job History By HistoryID===================
  DeleteAllJob_by_historyID(historyID) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/deleteHistoryById?exeHistoryId=' +
      historyID;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(apiURL, JSON.stringify(historyID), httpOptions);
  }
  //==================================Test suite execution trend (widget 1)========================
  testExeTrend(dashboardDetails) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'dashboard/getDashboardCountsForTestExecutionTrends';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(
      apiURL,
      JSON.stringify(dashboardDetails),
      httpOptions
    );
  }

  //==================================Test suite execution time trend (widget 2)========================

  testExeTimeTrend(dashboardDetails) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'dashboard/getDashboardCountsForTestExecutionTimeTrends';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(
      apiURL,
      JSON.stringify(dashboardDetails),
      httpOptions
    );
  }
  //==================================Test suite execution count trend (widget 3)========================

  testSuiteExeStatusTrend(dashboardSuiteStatusDetails) {
    const apiURL =
      environment.urlConfig.baseUrl + 'dashboard/getDashboardSuiteCountTrend';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };

    return this.http.post(
      apiURL,
      JSON.stringify(dashboardSuiteStatusDetails),
      httpOptions
    );
  }

  //==================================Monthly execution status (widget 4)========================

  monthlyExestatus(dashboardSuiteStatusDetails) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'dashboard/getDashboardSuiteMonthlyExeStatus';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(
      apiURL,
      JSON.stringify(dashboardSuiteStatusDetails),
      httpOptions
    );
  }

  //==================================Count for Test suite execution time trend (widget 6)========================

  getDashboardSuiteExeTimeProjectWise(dashboardSuiteStatusDetails) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'dashboard/getDashboardSuiteExeTimeProjectWise';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(
      apiURL,
      JSON.stringify(dashboardSuiteStatusDetails),
      httpOptions
    );
  }
  //==================================delete device farm capability ========================
  deleted_dfCapability(capabilityId, userTemplateName, userId) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'exection/deleteDeviceFarmCapability?capabilityId=' +
      capabilityId +
      '&usrTemplateName=' +
      userTemplateName +
      '&userId=' +
      userId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.delete(apiURL, httpOptions);
  }

  getAllHeadspinDevices() {
    const apiURL = environment.urlConfig.headspinBaseUrl + 'devices';
    const httpOptions = {
      headers: new HttpHeaders({}).set(headSpinToken, 'true')
    };
    return this.http.get(apiURL, httpOptions);
  }

  getAllDevicesAvailability() {
    let postText = {};
    const apiURL = environment.urlConfig.headspinBaseUrl + 'devices/unlock';
    const httpOptions = {
      headers: new HttpHeaders({}).set(headSpinToken, 'true')
    };
    return this.http.post(apiURL, postText, httpOptions);
  }

  getUploadedAppFile(platform) {
    let apiURL;
    if (platform == Constvar.Android) {
      apiURL = environment.urlConfig.headspinBaseUrl + 'apps/apks';
    } else {
      apiURL = environment.urlConfig.headspinBaseUrl + 'apps/ipas';
    }
    const httpOptions = {
      headers: new HttpHeaders({}).set(headSpinToken, 'true')
    };
    return this.http.get(apiURL, httpOptions);
  }

  getAllUploadedAppListOnDevices(deviceId)
  {
      let apiURL =  environment.urlConfig.headspinBaseUrl + 'idevice/' + deviceId + '/installer/list?json';
    const httpOptions = {
      headers: new HttpHeaders({}).set(headSpinToken, 'true')
    };
    return this.http.get(apiURL, httpOptions);
  }

  getAllUploadedAppListOnAndroidDevices(deviceId, appPackage)
  {
    let reqData = this.androidGrepCommand +' '+ appPackage;
      let apiURL =  environment.urlConfig.headspinBaseUrl + 'adb/' + deviceId + '/shell';
    const httpOptions = {
      headers: new HttpHeaders({
      }).set(headSpinToken, 'true')
    };
    return this.http.post(
      apiURL,
      reqData,
      httpOptions
    );
  }


 

  uploadImg_for_Comparison(testsuiteId, testCaseId, testStepId, imageFile) {
    const apiURL =
      environment.urlConfig.baseUrl +
      'teststep/uploadCompareImage?testCaseId=' +
      testCaseId +
      '&testStepId=' +
      testStepId +
      '&testSuiteId=' +
      testsuiteId;
    const httpOptions = {
      headers: new HttpHeaders({
        authToken: `${this.authService.authToken}`
      })
    };
    return this.http.post(apiURL, imageFile, httpOptions);
  }

  //======================================================================================================================
  async getLocalDevices(
    slaveOS,
    selectedSlave,
    platform,
    isFromStartExe
  ): Promise<any> {
    if (!isFromStartExe) {
      this.loaderService.show();
    } else {
      this.loaderService.hide();
    }

    this.platform = platform;
    this.slaveOS = slaveOS;

    let jobName = '';
    if (slaveOS != null && slaveOS != undefined && slaveOS != '') {
      console.log('slaveOS in if', slaveOS);

      if (slaveOS.toString().indexOf('Windows') > -1) {
        //  For Android
        jobName = 'getandroiddevices_node';
      }
      // For IOS
      else {
        jobName = 'getiosdevices';
      }
    } else {
      this.loaderService.hide();
      console.log('slaveOS in else', slaveOS);
      return this.toastr.errorToastr('Something went wrong.Please try again.');
    }
    let doc = this;
    let statusCode;
    await new Promise(async (resolve, reject) => {
      await jenkins.build_with_params(
        jobName,
        // Akash added Platform
        {
          mode: 'no-cors',
          RunJobNode: selectedSlave,
          Platform: platform,
          delay: 0
        }, //Platform: doc.platform,
        async function(err, data) {
          if (err) {
            doc.IsjobName_error = err;
            doc.isDevicesLoading = false;
            $('#btnSelectDevices').prop('disabled', false);
            setTimeout(() => {
              doc.IsjobName_error = '';
            }, 2500);
            doc.loaderService.hide();
            doc.toastr.errorToastr(
              'Please check whether your node has internet connection, selected node is online'
            );
            return console.log(
              'jenkins build error----build_with_params -->' + err
            );
          }
          console.log(data);
          resolve(data);
          doc.loaderService.hide();
        }
      );
    });
    return jobName;
  }

  async getJobInfo_by_jobName(jobName): Promise<any> {
    let statusCode;
    let getlocalDevicesData;
    let doc = this;
    await new Promise((resolve, reject) => {
      jenkins.job_info(jobName, async function(err, data) {
        if (err) {
          doc.IsjobName_error = err;
          doc.isDevicesLoading = false;
          $('#btnSelectDevices').prop('disabled', false);
          setTimeout(() => {
            doc.IsjobName_error = '';
            doc.toastr.errorToastr(
              'Please check whether your node has internet connection, selected node is online'
            );
          }, 2500);
          return console.log(err);
        }
        console.log(data);
        resolve(data);
        getlocalDevicesData = data;
        statusCode = data.statusCode;
      });
    });
    return getlocalDevicesData;
  }

  async readConsoleOutPut(jobName, data, jobType): Promise<any> {
    let doc = this;
    let consoleOutdata_res;
    await new Promise(async (resolve, reject) => {
      jenkins.console_output(jobName, data.builds[0].number, async function(
        err,
        data
      ) {
        if (err) {
          reject(err);
          doc.IsjobName_error = err;
          doc.isDevicesLoading = false;
          $('#btnSelectDevices').prop('disabled', false);
          setTimeout(() => {
            doc.IsjobName_error = '';
          }, 2500);
          return console.log(
            'jenkins console output error for job ' + jobName + ' -->' + err
          );
        } else {
          console.log('data============1', data);
          consoleOutdata_res = data;
          doc.consoleOut_datares = data;
          resolve(data);
        }
      });
    });
    if (consoleOutdata_res) {
      return { consoleOutdata_res, doc, jobType };
    }
  }

  async parseAndroidFile(data, doc, active_step): Promise<any> {
    let file = data.body;
    if (file != undefined) {
      try {
        file = file.split(': ').join(':');

        var startIndex,
          arrStartIndex = [];
        var endIndex,
          arrEndIndex = [];

        while (
          (startIndex = file.indexOf('[ro.boot.serialno]', startIndex + 1)) !=
          -1
        ) {
          arrStartIndex.push(startIndex);
        }
        while ((endIndex = file.indexOf('Next Device', endIndex + 1)) != -1) {
          arrEndIndex.push(endIndex + 1);
        }
        this.serverDevices = [];
        console.log('Start - ', arrStartIndex);
        console.log('End - ', arrEndIndex);

        for (let i = 0; i <= arrStartIndex.length - 1; i++) {
          let deviceProperty = {
            name: '',
            os: '',
            formFactor: '',
            availability: '',
            platform: '',
            udid: '',
            deviceType: ''
          };

          let fileValue = file.substring(arrStartIndex[i], arrEndIndex[i]);
          console.log('file value--------?', fileValue);

          let objFile = fileValue.split('\n');

          objFile.forEach(element => {
            let objVal = element.split(':');

            if (
              objVal[0]
                .toString()
                .replace('[', '')
                .replace(']', '') == 'ro.build.version.release'
            ) {
              console.log('objVal----version?', objVal);
              deviceProperty.os = objVal[1]
                .toString()
                .replace('[', '')
                .replace(']', '')
                .trim();
            } else if (
              objVal[0]
                .toString()
                .replace('[', '')
                .replace(']', '') == 'ro.product.model'
            ) {
              console.log('objVal-------model', objVal);
              deviceProperty.name =
                deviceProperty.name.toString().trim() +
                '_' +
                objVal[1]
                  .toString()
                  .replace('[', '')
                  .replace(']', '')
                  .toUpperCase()
                  .trim();
              console.log(
                'deviceProperty.name............>',
                deviceProperty.name
              );
            } else if (
              objVal[0]
                .toString()
                .replace('[', '')
                .replace(']', '') == 'ro.product.brand'
            ) {
              console.log('objVal------brand', objVal);
              deviceProperty.name = objVal[1]
                .toString()
                .replace('[', '')
                .replace(']', '')
                .toUpperCase()
                .trim();
              deviceProperty.formFactor = 'PHONE';
              deviceProperty.availability = 'AVAILABLE';
              deviceProperty.platform = 'ANDROID';
              deviceProperty.deviceType = 'Physical';
            } else if (
              objVal[0]
                .toString()
                .replace('[', '')
                .replace(']', '') == 'DEVICEUDID'
            ) {
              console.log('objVal------udid', objVal);
              deviceProperty.name =
                deviceProperty.name.toString().trim() +
                '_' +
                objVal[1].toString().toUpperCase();
              console.log('checkkkkkkkkkkkkkkkkkkkkkkkk', deviceProperty.name);

              deviceProperty.udid = objVal[1]
                .toString()
                .replace('[', '')
                .replace(']', '')
                .trim();
            }
          });
          await this.serverDevices.push(deviceProperty);
        }
        let index = 1;
        this.serverDevices.forEach(function(element) {
          element.selected = false;
          element.id = index;
          index = index + 1;
        });

        setTimeout(() => {
          //to open pop up only for select devices step(code by Mustayeed)
          if (active_step == 3) {
            $('#SelectDeviceTemplateModal').modal('show');
          } else {
            $('#SelectDeviceTemplateModal').modal('hide');
          }

          $('#btnSelectDevices').prop('disabled', false);
          $('#startExeBtnView').prop('disabled', false);
          this.isDevicesLoading = false;
        }, 1000);
        this.isconsoleOut = false;
      } catch (exception) {
        this.isconsoleOut = true;
        console.log('Fialed to load divices');
        $('#btnSelectDevices').prop('disabled', false);
        doc.isDevicesLoading = false;
        doc.IsjobName_error = exception;
        doc.ref.detectChanges();
        setTimeout(() => {
          doc.IsjobName_error = '';
          doc.ref.detectChanges();
        }, 2500);
      }
    }
    console.log('serverDevices length', this.serverDevices.length);
    return this.serverDevices;
  }

  async parseIosFile(data, doc, active_step): Promise<any> {
    this.serverDevices = [];
    let iOsFile = data.body;
    if (iOsFile != undefined) {
      try {
        var objIosFile = iOsFile.split('Known Devices:');

        var objData = objIosFile[1].split('\n');
        console.log('obj data=====>', objData);

        objData.forEach(element => {
          if (element != '' && element.toString().indexOf('Finished') == -1) {
            let deviceProperty = {
              name: '',
              os: '',
              formFactor: '',
              availability: '',
              platform: '',
              udid: '',
              deviceType: ''
            };

            let objIosVal = element.split(' [');
            if (
              objIosVal[0]
                .toString()
                .replace('\n', '')
                .toString()
                .indexOf('iPhone') > -1
            ) {
              if (
                objIosVal[0]
                  .toString()
                  .replace('\n', '')
                  .toString()
                  .indexOf('(') > -1
              ) {
                let val = objIosVal[0].split('(');
                deviceProperty.name =
                  val[0]
                    .replace('\n', '')
                    .toString()
                    .trim() +
                  '_' +
                  objIosVal[1]
                    .replace(']', '')
                    .toString()
                    .replace('(Simulator)', '')
                    .trim();

                if (val.length - 1 == 1) {
                  deviceProperty.os = val[1].replace(')', '');
                } else if (val.length - 1 == 2) {
                  deviceProperty.os = val[2].replace(')', '');
                } else if (val.length - 1 == 3) {
                  deviceProperty.os = val[3].replace(')', '');
                } else {
                  deviceProperty.os = val[1].replace(')', '');
                }

                deviceProperty.udid = objIosVal[1]
                  .replace(']', '')
                  .toString()
                  .replace('(Simulator)', '')
                  .trim();
              } else {
                deviceProperty.name =
                  objIosVal[0]
                    .replace('\n', '')
                    .toString()
                    .trim() +
                  '_' +
                  objIosVal[1]
                    .replace(']', '')
                    .toString()
                    .replace('(Simulator)', '')
                    .trim();
                deviceProperty.udid = objIosVal[1]
                  .replace(']', '')
                  .toString()
                  .replace('(Simulator)', '')
                  .trim();
                deviceProperty.os = '-';
              }
            }
            if (
              objIosVal[0]
                .toString()
                .replace('\n', '')
                .toString()
                .indexOf('iPhone') > -1
            ) {
              deviceProperty.formFactor = 'iPhone';
            }
            deviceProperty.availability = 'AVAILABLE';
            deviceProperty.platform = 'IOS';

            if (element.toString().indexOf('Simulator') > -1) {
              deviceProperty.deviceType = 'Simulator';
            } else {
              deviceProperty.deviceType = 'Physical';
            }

            if (objIosVal[0].toString().indexOf('iPhone') > -1) {
              this.serverDevices.push(deviceProperty);
            }
          }
        });

        console.log(this.serverDevices);
        let index = 1;
        this.serverDevices.forEach(function(element) {
          element.selected = false;
          element.id = index;
          index = index + 1;
        });
        setTimeout(() => {
          //to open pop up only for select devices step(code by Mustayeed)
          if (active_step == 3) {
            $('#SelectDeviceTemplateModal').modal('show');
          } else {
            $('#SelectDeviceTemplateModal').modal('hide');
          }
          $('#btnSelectDevices').prop('disabled', false);
          $('#startExeBtnView').prop('disabled', false);
          this.isconsoleOut = false;
          this.isDevicesLoading = false;
          doc.ref.detectChanges();
        }, 5000);
        this.isconsoleOut = false;
      } catch (exception) {
        //flag checked for is console out
        doc.isconsoleOut = true;
        console.log('Fialed to load divices');
        $('#btnSelectDevices').prop('disabled', false);
        doc.isDevicesLoading = false;
        doc.IsjobName_error = exception;
        doc.ref.detectChanges();
        setTimeout(() => {
          doc.IsjobName_error = '';
          doc.ref.detectChanges();
        }, 2500);
      }
    }
    return this.serverDevices;
  }

  validate(event: any) {
    if (event.which === 32 && !event.target.value.length)
      event.preventDefault();

    var regex = new RegExp('^[a-zA-Z0-9 ]*$');
    var key = String.fromCharCode(
      event.charCode == event.which ? event.which : event.charCode
    );
    if (!regex.test(key)) {
      event.preventDefault();
      return false;
    }
  }

  checkIfCapabilityTemplateHasDuplicates(
    capabilityIdValuesViews_res: any
  ): string {
    const status = capabilityIdValuesViews_res.some(capabilityIdValuesView => {
      let counter = 0;
      for (const iterator of capabilityIdValuesViews_res) {
        if (iterator.capabilityName === capabilityIdValuesView.capabilityName) {
          counter += 1;
        }
      }
      return counter > 1;
    });
    if (status) {
      return 'Duplicate capabilities are present in capability template, please use another capability template for execution.';
    }
  }

  checkIfCapabilityTemplateIsCorrputed(
    capabilityIdValuesViews_res: any
  ): string {
    let counter = 0;
    for (const iterator of capabilityIdValuesViews_res) {
      if (iterator.capabilityName.trim() === iterator.capabilityValue.trim()) {
        counter += 1;
      }
    }
    if (counter > 0) {
      return 'Capability Template is corrupted, please use another capability template for execution.';
    }
  }
}
