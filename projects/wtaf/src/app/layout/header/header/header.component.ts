import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthenticationService } from '../../../shared/services/authentication.service';
import { Router, RouterLinkActive, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../shared/services/data.service';
import { Role } from '../../../models/role';
@Component({
  selector: 'wtaf-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  routeLinks: any[];
  routeLinksRole: any[];
  username: string;
  role: string;
  Admin: string = '';
  Test_lead: string = '';
  Test_Mgr: string = '';
  Test_Engg: string = '';
  @ViewChild('menuIconDiv', { static: false }) menuIconDiv: ElementRef;
  @ViewChild('menuItemDiv', { static: false }) menuItemDiv: ElementRef;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private activatedRoute: ActivatedRoute,
    private dataservice: DataService
  ) {
    this.routeLinksRole = [
      {
        label: 'Dashboard',
        link: '/dashboard',
        //canActivate: [AuthGuardService],
        path: 'assets/images/dashboard.png'
      },
      {
        label: 'Test Suite',
        link: '/test_suite',
        //  canActivate: [AuthGuardService],
        path: 'assets/images/suite.png'
      },
      {
        label: 'Test Case',
        link: '/test_case',
        // canActivate: [AuthGuardService],
        path: 'assets/images/case.png'
      },
      {
        label: 'Test Step',
        link: '/test_step',
        //  canActivate: [AuthGuardService],
        path: 'assets/images/step.png'
      },
      {
        label: 'Test Data',
        link: '/test_data',
        // canActivate: [AuthGuardService],
        path: 'assets/images/test_data.png'
      },
      {
        label: 'Execution',
        link: '/execution',
        // canActivate: [AuthGuardService],
        path: 'assets/images/execution.png'
      },
      // {
      //   label: 'Reports',
      //   link: '/report',
      //   //canActivate: [AuthGuardService],
      //   path: 'assets/images/reports.png'
      // },
      {
        label: 'Documents',
        link: '/documents',
        //canActivate: [AuthGuardService],
        path: 'assets/images/test_data.png'
      }
    ];
    this.routeLinks = [
      {
        label: 'Dashboard',
        link: '/dashboard',
        //canActivate: [AuthGuardService],
        path: 'assets/images/dashboard.png'
      },
      {
        label: 'Test Suite',
        link: '/test_suite',
        path: 'assets/images/suite.png'
      },
      {
        label: 'Test Case',
        link: '/test_case',
        // canActivate: [AuthGuardService],
        path: 'assets/images/case.png'
      },
      {
        label: 'Test Step',
        link: '/test_step',
        //  canActivate: [AuthGuardService],
        path: 'assets/images/step.png'
      },
      {
        label: 'Test Data',
        link: '/test_data',
        path: 'assets/images/test_data.png'
      },
      {
        label: 'Execution',
        link: '/execution',
        // canActivate: [AuthGuardService],
        path: 'assets/images/execution.png'
      },
      {
        label: 'Configuration',
        link: '/configuration',
        // canActivate: [AuthGuardService],
        //data: {
        //roles: [
        //['Admin','Test Lead','Test Manager']
        // ]
        // },
        path: 'assets/images/config.png'
      },
      // {
      //   label: 'Reports',
      //   link: '/report',
      //   path: 'assets/images/reports.png'
      // },
      {
        label: 'Documents',
        link: '/documents',
        path: 'assets/images/test_data.png'
      }
    ];
  } 

  ngOnInit() {
    this.username = localStorage.getItem('userDisplayName');
    this.role = localStorage.getItem('userRole');
    this.Admin = Role.Admin;
    this.Test_lead = Role.Test_Lead;
    this.Test_Mgr = Role.Test_Manager;
    this.Test_Engg = Role.Test_Engineer;
    console.log('local usr -->', this.username, ' with role:', this.role);
  }

  showMenuItems() {
    this.menuItemDiv.nativeElement.style.display = 'block';
    this.menuIconDiv.nativeElement.style.display = 'none';
  }

  hideMenuItems() {
    console.log('sgdfgdgfd');
    this.menuItemDiv.nativeElement.style.display = 'none';
    this.menuIconDiv.nativeElement.style.display = 'block';
  }
  okLogout() {
    this.authenticationService.logout();
    this.username = '';
    sessionStorage.clear();
    this.router.navigate(['']);
  }
}
