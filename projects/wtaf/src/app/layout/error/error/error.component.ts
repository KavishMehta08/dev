import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'wtaf-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  statusCode:any;
  errMessage:any;

  constructor( private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location
    ) { }

  ngOnInit() {
    
   document.getElementById('parentDivId').style.opacity='1';
     this.statusCode = this.activatedRoute.snapshot.paramMap.get('errCode');
    //  this.errMessage=this.activatedRoute.snapshot.paramMap.get('errMsg');
    console.log("Eroor Msg",this.errMessage);
    console.log("Status COde",this.statusCode);
    if(this.statusCode == 404){
      
      this.errMessage = 'No Data Found';
    }
    else if(this.statusCode == 401){

      this.errMessage = 'Unauthorized ';

    }
    else if(this.statusCode==403){

      this.errMessage = 'Forbidden Error';

    }
    else if(this.statusCode==500){

      this.errMessage = 'Internal Server Error';

    }
    else if(this.statusCode==0){

      this.errMessage = 'Server Down';

    }
    else{
      this.statusCode = '';
        this.errMessage = 'Failed to Load'
    }
    
  }

  backToPreviousPage()
  {
    // this.location.back();
    this.router.navigate(['dashboard'])
  }

}
