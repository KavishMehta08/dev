import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

import { ForgotPasswordRoutingModule } from './forgot-password-routing.module';
@NgModule({
  declarations: [ForgotPasswordComponent],
  imports: [CommonModule, SharedModule,ForgotPasswordRoutingModule]
})
export class LoginModule {}
