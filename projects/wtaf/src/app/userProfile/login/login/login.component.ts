import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../shared/services/authentication.service';
import { AlertService } from '../../../shared/services/alert.service'
import { first } from 'rxjs/operators';
import { DataService } from '../../../shared/services/data.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'wtaf-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  isInvalid = false;
  fieldTextType: boolean;
  submitted = false;
  errorMsg: String = '';
  returnUrl: string;
  isRemember: boolean = false;
  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    // public authService: AuthenticationService,
     public authenticationService: AuthenticationService,
    private alertService: AlertService,
    private dataservice: DataService
) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) { 
      this.router.navigate(['']);
  }
 }

 ngOnInit() {
  //this.isInvalid=false;
  

  this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  //this.isInvalid=false;
  let checkRememberme = localStorage.getItem('isRemember'); 
  if(checkRememberme) {
     // this.loginForm.c
     this.loginForm.controls.username.setValue(localStorage.getItem('userName'));
    this.loginForm.controls.password.setValue(localStorage.getItem('password'));
      this.onSubmit();
   }
  // get return url from route parameters or default to '/'
  this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'dashboard';
}
  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }
  onSubmit() {
    
    this.submitted = true;
     var token;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
     this.isInvalid=true;
    return;
    }
    this.loading = true;
    this.dataservice
      .login(this.f.password.value, this.f.username.value)
      .subscribe(
        res => {
          
          console.log(
            'token------------------',
            res['token'],
            res['userName'],
            res['roleName'],
            res['userId']
          );
          this.authenticationService.login({
            id: res['userId'],
            username: res['userName'],
            name: res['userName'],
            role: res['roleName'],
            token: res['token'],
            roleName: res['roleName']
          });
          //to show the username on header with user image
          localStorage.setItem('userName', this.f.username.value);
          localStorage.setItem('userDisplayName', res['userName']);
          localStorage.setItem('userId', res['userId']);
          localStorage.setItem('userRole', res['roleName']);
          if(this.isRemember){
          localStorage.setItem('isRemember', 'true');
          localStorage.setItem('password', this.f.password.value);
          }

          this.router.navigate([this.returnUrl]);
        },
        error => {
          console.log(error.message);
          this.loading = false;
          if(error.status == 401)
          {
            this.toastr.errorToastr('Entered Username/Password is incorrect');
          }
          //this.errorMsg =ErroAuthEn.convertMessage(error['code']);
          // return error.message
        }
      );
    //   error => {
    //     this.alertService.error(error);
    //     this.loading = false;
    // }
    // );
  }
  //     this.authenticationService.login(this.f.username.value, this.f.password.value)
  //         .pipe(first())
  //         .subscribe(
  //             data => {
  //                 this.router.navigate([this.returnUrl]);
  //             },
  //             error => {
  //                 this.alertService.error(error);
  //                 this.loading = false;
  //             });
  // }
  // );

//     this.authenticationService.login(this.f.username.value, this.f.password.value)
//         .pipe(first())
//         .subscribe(
//             data => {
//                 this.router.navigate([this.returnUrl]);
//             },
//             error => {
//                 this.alertService.error(error);
//                 this.loading = false;
//             });
// }
checkValue(isRememberval) {
     if (isRememberval.srcElement.checked) {
        this.isRemember = true;
    } else {
       this.isRemember = false;
       
   
     }
    }
toggleFieldTextType() {
  this.fieldTextType = !this.fieldTextType;
}



}
