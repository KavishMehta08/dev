import { DragDropModule } from '@angular/cdk/drag-drop';
// import {DragDropModule} from '@angular/cdk/drag-drop';
// import { HighchartsChartComponent } from 'highcharts-angular';
import {
  DatePipe, HashLocationStrategy,
  LocationStrategy
} from '@angular/common';
import { NgModule } from '@angular/core';
// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
// import { ReportComponent } from './features/report_/report/report.component';
import { ToastrModule } from 'ng6-toastr-notifications';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app/app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';


@NgModule({
  imports: [
    // angular
    BrowserAnimationsModule,
    BrowserModule,
    Ng2SearchPipeModule,
    // AngularMultiSelectModule,

    // core & shared
    // HighchartsChartComponent,
    CoreModule,
    SharedModule,
    FormsModule,
    DragDropModule,
    // DragDropModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    // app
    AppRoutingModule,
    DeviceDetectorModule.forRoot()
  ],
  declarations: [AppComponent,],
  providers: [
    DatePipe,
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],

  bootstrap: [AppComponent]
})
export class AppModule {}
