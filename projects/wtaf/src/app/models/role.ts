export enum Role {
  Admin = 'Admin',
  Test_Manager = 'Test manager',
  Test_Lead = 'Test Lead',
  Test_Engineer = 'Test Engineer'
}
