import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'wtaf-execution',
  templateUrl: './execution.component.html',
  styleUrls: ['./execution.component.css']
})
export class ExecutionComponent implements OnInit {
  routeLinks: any = [];
  constructor() {
    this.routeLinks = [
      {
        label: 'Start Execution',
        link: 'start_execution'
      },
      {
        label: 'In Progress',
        link: 'inprogress'
      },
      {
        label: 'Scheduled',
        link: 'scheduled'
      },
      {
        label: 'History',
        link: 'history'
      }
    ];
  }

  ngOnInit() {}
}
