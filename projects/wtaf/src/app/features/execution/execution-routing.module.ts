import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExecutionComponent } from './execution/execution.component';
import { StartExecutionComponent } from '../start_execution/start-execution/start-execution.component';

const routes: Routes = [
  {
    path: '',
    component: ExecutionComponent,
    data: { title: '' },
    children: [
      {
        path: '',
        redirectTo: 'start_execution',
        pathMatch: 'full'
      },
      {
        path: 'start_execution',
        loadChildren: () =>
        import('../start_execution/start_execution.module').then(
          m => m.StartExecutionModule
        ),
      }, 
      {
        path: 'start_execution/:executionId/:exeHistoryId',
        loadChildren: () =>
        import('../start_execution/start_execution.module').then(
          m => m.StartExecutionModule
        ),
      },
       {
        path: 'inprogress',
        loadChildren: () =>
        import('../inprogress/inprogress.module').then(
          m => m.InprogressModule
        ),
      },
      {
        path: 'scheduled',
        loadChildren: () =>
        import('../scheduled/scheduled.module').then(
          m => m.ScheduledModule
        ),
      },
      {
        path: 'history',
        loadChildren: () =>
        import('../history/history.module').then(
          m => m.HistoryModule
        ),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExecutionRoutingModule {}
