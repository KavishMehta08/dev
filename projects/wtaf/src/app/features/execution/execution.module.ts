import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ExecutionComponent } from './execution/execution.component';
import { ExecutionRoutingModule } from './execution-routing.module';

@NgModule({
  declarations: [ExecutionComponent],
  imports: [CommonModule, SharedModule,ExecutionRoutingModule]
})
export class ExecutionModule {}
