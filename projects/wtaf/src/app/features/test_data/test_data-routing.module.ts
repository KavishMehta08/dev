import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestDataComponent } from './test-data/test-data.component';

const routes: Routes = [
  {
    path: '',
    component: TestDataComponent,
    data: { title: '' }
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestDataRoutingModule {}
