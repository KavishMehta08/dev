import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { TestDataComponent } from './test-data/test-data.component';
import { TestDataRoutingModule } from './test_data-routing.module';
@NgModule({
  declarations: [TestDataComponent],
  imports: [CommonModule, SharedModule, TestDataRoutingModule]
})
export class TestDataModule { }
