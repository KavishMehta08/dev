import { Component, OnInit, PipeTransform, Pipe } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { async } from 'rxjs/internal/scheduler/async';
import { Type } from '../../models/type';
import { Role } from '../../../models/role';
import { Constvar } from '../../../models/constVar';
declare var $: any;

@Pipe({ name: 'keys' })
@Component({
  selector: 'wtaf-test-data',
  templateUrl: './test-data.component.html',
  styleUrls: ['./test-data.component.css']
})
export class TestDataComponent implements OnInit {
  no_of_iteration_exist = 0;
  max_it_id = 0;
  objDataSet: any = [];
  columns: any = [];
  successMessage: String = '';
  errorMessage: String = '';
  regionId: Number = 0;
  brandId: Number = 0;
  testSuiteId: Number = 0;
  testCaseId: Number = 0;
  testCaseList: any = [];
  regionList: any = [];
  brandList: any = [];
  projectNameList: any = [];
  testSuiteList: any = [];
  dataset_list: any = [];
  details: any = {};
  NoOfIterations: 0;
  platformList: any = [];
  refinedList: any = [];
  itearationLength = 0;
  maxLength = 0;
  isIteration = true;
  isData = false;
  dataIndex = 0;

  dataSetName = '';
  dtName = '';
  testSuitName = '';
  TestCaseName = '';
  No_of_col: any = [];
  collen = 0;
  Test_steps_iterations: any = [];
  projectTypeName = '';
  testSteps: any = [];
  Iterations_keys: any;
  jsonDS_Iterations = [];
  IsData: boolean = false;
  isCopyData: boolean = false;
  deletedItern_Details: any = [];
  Issteps = false;
  IsFromTestCase = false;
  IsViewData = false;
  IsViewOnly = false;
  projectTypeId = 0;
  projectTypeList: any = [];
  projectName = '';
  platformId = 0;
  userId: number;
  role: string = '';
  Admin: string = '';
  Test_lead: string = '';
  Test_Mgr: string = '';
  testStepsList : any = [];
  VarDS_Iterations: any;
  testStepCounter : number =0;

  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }
  async ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log('local usr id', this.userId);
    this.role = localStorage.getItem('userRole');
    this.Admin = Role.Admin;
    this.Test_lead = Role.Test_Lead;
    this.Test_Mgr = Role.Test_Manager;
    await this.getAllProjectType();
    this.testCaseId = parseInt(
      this.activatedRoute.snapshot.paramMap.get('testCaseId')
    );

    if (this.testCaseId > 0) {
      this.IsFromTestCase = true;
      this.dataservice
        .getSharedTestCaseById(this.testCaseId)
        .subscribe((response: {}) => {
          console.log('getTestCaseById----------', response);
          this.projectTypeId = response['projectTypeId'];
          this.regionId = response['regionId'];
          this.brandId = response['brandId'];
          this.TestCaseName = response['testCaseName'];
          this.testSuitName = response['testSuiteName'];
          this.getAllRegion_byProjectTypeId(this.projectTypeId);
          this.getBrandsByRegionId(response['regionId']);
          this.getAllTestSuite(
            response['regionId'],
            response['brandId'],
            response['projectTypeId'],
            response['projectName']
          );
          this.getAllTestStepsById(parseInt(this.testCaseId.toString()));
          this.testSuiteId = response['testSuiteId'];
          this.dtName = this.testSuitName + '_' + this.TestCaseName;
        });
    } else {
      this.testCaseId = 0;
      this.IsFromTestCase = false;
    }
  }

  //new code bt mustaid
  async changeProjectType(event) {
    
    this.regionList = [];
    this.regionId = 0;
    this.testSteps = [];
    this.Test_steps_iterations = [];
    this.testCaseList = [];
    this.testSuiteList = [];
    this.jsonDS_Iterations = [];
    this.testCaseId = 0;
    this.brandId = 0;
    this.testSuiteId = 0;
    this.brandList = [];
    this.dataSetName = '';
    this.dataset_list = [];
    this.dtName = '';
    this.isCopyData = false;
    this.projectNameList = [];
    this.projectName = '';
    this.projectTypeId = parseInt(event.target.value);
    this.projectTypeName = event.target.options[event.target.selectedIndex].text;
    console.log('this.projectTypeName...', this.projectTypeName);

    if (this.projectTypeId != 0) {
      await this.getAllRegion_byProjectTypeId(this.projectTypeId);
      if (
        this.projectTypeId != null &&
        this.projectTypeId != undefined &&
        this.projectTypeId != 0
      ) {
        await this.getPlatformByProjectTypeId(this.projectTypeId);
      }
    }
  }

  /* change region and get brands*/
  changeRegion(e) {
    this.testSteps = [];
    this.Test_steps_iterations = [];
    this.testCaseList = [];
    this.clearTestSuiteArr();
    this.jsonDS_Iterations = [];
    this.projectNameList = [];
    this.brandId = 0;
    this.brandList =[];
    this.testCaseId = 0;
    this.dataSetName = '';
    this.dtName = '';
    this.isCopyData = false;
    this.onChangeSelectAllPlatfrom(false,false);
    this.getBrandsByRegionId(parseInt(e.target.value));
    this.regionId = parseInt(e.target.value);

  }

  // Clear test suite array if platformId greater than 0 ,
  clearTestSuiteArr() {
    this.testSuiteList = [];
  }
  changeBrand(e) {
    this.dataSetName = '';
    this.dtName = '';
    this.testCaseId = 0;
    this.isCopyData = false;
    this.testCaseId = 0;
    this.clearTestSuiteArr();
    this.onChangeSelectAllPlatfrom(false, false);
    this.getAllProjectNameByTypeId(
      this.brandId,
      this.projectTypeId,
      this.regionId
    );
    this.brandId = parseInt(e.target.value);
  }
  changeProjectName(e) {
    this.projectName = e.target.value;
    this.dataSetName = '';
    this.dtName = '';
    this.dataset_list = [];
    this.testSuiteList = [];
    this.onChangeSelectAllPlatfrom(false,true);
    this.getTestSuiteNameByPlatformId(this.platformId);
  }

  changeTestSuite(e) {
    this.testCaseId = 0;
    this.testSuitName = e.target.options[e.target.selectedIndex].text;
    this.dtName = this.testSuitName;
    this.testSuiteId = e.target.value;
    this.getAllTestStepsById(parseInt(e.target.value));
    this.getAllDataSetById(parseInt(e.target.value));
    this.testSuiteId = parseInt(e.target.value);
    this.isCopyData = false;
  }

  changePlatform(event) {
    let platformId = event.target.value;
    console.log('platformId', platformId);
    this.isCopyData = false;
    this.dataset_list = [];
    this.dataSetName = '';
    this.testSteps = [];
    this.Test_steps_iterations = [];
    this.dtName = '';
    this.getTestSuiteNameByPlatformId(platformId);
  }

  /* get all platforms by project type Id */
  async getPlatformByProjectTypeId(projectTypeId) {
    await this.dataservice
      .getPlatformByProjectTypeId(projectTypeId)
      .toPromise()
      .then((response: {}) => {
        this.platformList = response;
        if (response != null) {
          if (this.platformList.length == 1) {
            this.platformId = this.platformList[0].platformId;
          }
          this.onChangeSelectAllPlatfrom(true,true);
        } else {
          this.platformId = 0;
        }
      });
  }

  onChangeSelectAllPlatfrom(isFromPlatfrom, isfromProjectName) {
    if (this.projectTypeName == Constvar.Mobile) {
      if (isFromPlatfrom) {
        this.platformList.push({ platformId: 0, platformName: Constvar.AllPlatform })
      }
      this.platformList.unshift(
        this.platformList.splice(
          this.platformList.findIndex(item => item.platformName === Constvar.AllPlatform),
          1
        )[0]
      );
      this.platformId = 0;
      if(isfromProjectName)
      {
        this.getTestSuiteNameByPlatformId(this.platformId);

      }
    }
    else {

    }

  }

  async getTestSuiteNameByPlatformId(platformId) {
    this.platformId = platformId;
    this.testSuiteList = [];
    let TestSuiteId = Type.TestSuiteId;
    let desc = Type.descending;
    if(this.projectTypeId != 0 && this.regionId != 0 && this.brandId != 0
      && this.projectName != '')
    {
    await this.dataservice
      .getAllTestSuiteByPlatformId(
        TestSuiteId,
        desc,
        this.regionId,
        this.brandId,
        this.projectTypeId,
        this.projectName,
        this.platformId
      )
      .toPromise()
      .then((response: {}) => {
        this.testSuiteList = response;
        console.log('----------------->', this.testSuiteList);
        if (response != null) {
          if (this.testSuiteList.length == 1) {
            this.testSuiteId = this.testSuiteList[0].testSuiteId;
            this.testSuitName = this.testSuiteList[0].testSuiteName;
            console.log('this.testSuitName', this.testSuitName);
            this.dtName = this.testSuitName;
            this.getAllTestStepsById(this.testSuiteId);
            this.getAllDataSetById(this.testSuiteId);
          } else {
            this.testSuiteId = 0;
          }
        } else {
          this.testSuiteId = 0;
        }
      })
      .catch(err => {
        console.log(err);
      });
    }
  }
  async getAllTestStepsById(suiteId) {
    this.testSteps = [];
    this.Test_steps_iterations = [];
    await this.dataservice
      .getTestStepBysuite(suiteId)
      .toPromise()
      .then(async (response: {}) => {
        console.log('Steps-----', response);
        this.testSteps = response;
        if (response != null) {
          this.Issteps = false;
          this.testSuiteId = suiteId;
        } else {
          this.Issteps = true;
        }
      });
  }

  async getAllRegion_byProjectTypeId(typeId) {
    await this.dataservice
      .getAllRegionByProjectType(typeId)
      .toPromise()
      .then(async (response: {}) => {
        this.regionList = response;
        //to place NAR region at first position in frop down
        this.regionList.unshift(
          this.regionList.splice(
            this.regionList.findIndex(item => item.regionName === 'NAR'),
            1
          )[0]
        );
        console.log(this.regionList);
        if (response != null) {
          if (this.regionList.length == 1) {
            this.regionId = this.regionList[0].regionId;
          }

          if (this.regionId > 0) {
            this.getBrandsByRegionId(this.regionId);
          }
        }
      });
  }

  // --------Get All Project Types
  async getAllProjectType() {
    this.regionId = 0;
    this.brandId = 0;
    this.projectName = '';
    this.platformId = 0;
    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(async response => {
        // Success
        this.projectTypeList = response;
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.projectTypeId = this.projectTypeList[0].projectTypeId;
            this.getAllRegion_byProjectTypeId(this.projectTypeId);
          } else {
            this.projectTypeId = 0;
          }
        }
      });
  }

  /* get all brands by region Id */

  getBrandsByRegionId(regionId) {
    
    this.dataservice.getBrandsByRegionId(regionId).subscribe((response: {}) => {
      console.log('brands', this.brandList);
      this.brandList = response;
      if (response != null) {
        if (this.brandList.length == 1) {
          if(this.brandId == 0 ){
            this.brandId = this.brandList[0].brandId;
          }
        }else{
          if(this.brandId == 0 ){
            this.brandId = this.brandList[0].brandId;
          }
          
        }
        this.getAllProjectNameByTypeId(
          this.brandId,
          this.projectTypeId,
          this.regionId
        );
      } else {
        if (this.IsFromTestCase == false) {
          this.brandId = 0;
        }
      }
      if (this.brandId > 0) {
        if (this.IsFromTestCase == false) {
          this.brandId = this.brandList[0].brandId;
        }
      }
    });
  }

  /* get all test suite  */
  getAllTestSuite(regionId, brandId, projectTypeId, projectName) {
    this.testSuiteList = [];
    this.dataSetName = '';
    this.dtName = '';
    this.dataset_list = [];
    let TestSuiteName = Type.TestSuiteName;
    let asc = Type.ascending;
    this.dataservice
      .getAllTestSuite(
        TestSuiteName,
        asc,
        regionId,
        brandId,
        projectTypeId,
        projectName,
        0,0
      )
      .subscribe((response: {}) => {
        this.testSuiteList = response;
        if (response != null) {
          if (this.testSuiteList.length == 1) {
            this.testSuiteId = this.testSuiteList[0].testSuiteId;
            this.testSuitName = this.testSuiteList[0].testSuiteName;
            this.dtName = this.testSuitName;
            if (this.TestCaseName != '') {
              this.dtName = this.testSuitName + '_' + this.TestCaseName;
            }
            this.getAllTestStepsById(this.testSuiteId);
            this.getAllDataSetById(this.testSuiteId);
          } else {
            if (this.IsFromTestCase == false) {
              this.testSuiteId = 0;
            }
          }
        } else {
          this.testSuiteId = 0;
          this.dataSetName = '';
          this.dtName = '';
          this.dataset_list = [];
          this.testSuiteList = [];
        }

        if (this.testSuiteId > 0) {
          if (this.IsFromTestCase == false) {
            this.testSuiteId = this.testSuiteList[0].testSuiteId;
          }
          this.getTestCaseByID(this.testSuiteId);
        }
      });
  }
  /* get all project name by type Id */
  async getAllProjectNameByTypeId(brandId, projectTypeId, regionId) {
    
    await this.dataservice
      .getProjectNameByTypeId(brandId, projectTypeId, regionId)
      .toPromise()
      .then((response: {}) => {
        this.projectNameList = response;
        console.log('response-product name list', response);
        if (response != null) {
          if (this.projectNameList.length == 1) {
            this.projectName = this.projectNameList[0].projectTypeId;
          } else {
            this.projectName = '';
          }
        } else {
          this.projectName = '';
        }
        if (this.projectName != '' && this.platformId > 0) {
          this.getAllTestSuite(
            this.regionId,
            this.brandId,
            this.projectTypeId,
            this.projectName
          );
        }
      });
  }
  /* get all test case  */
  getTestCaseByID(testSuiteId) {
    this.testCaseList = [];
    let testCaseName = Type.TestCaseName;
    let asc = Type.ascending;
    this.dataservice
      .getTestCaseByTestSuiteId(testCaseName, asc, testSuiteId)
      .subscribe((response: {}) => {
        this.testCaseList = response;
        if (response != null) {
          if (this.testCaseList.length == 1) {
            this.testCaseId = this.testCaseList[0].testCaseId;
            this.TestCaseName = this.testCaseList[0].testCaseName;
            this.dtName = this.testSuitName + '_' + this.TestCaseName;
            this.getAllTestStepsById(parseInt(this.testCaseId.toString()));
            this.getAllDataSetById(this.testCaseId);
          } else {
            if (this.IsFromTestCase == false) {
              this.testCaseId = 0;
            }
          }
          if (this.testCaseId > 0) {
            if (this.IsFromTestCase == false) {
              this.testCaseId = this.testCaseList[0].testCaseId;
            }
            this.dataset_list = [];
            this.dataSetName = '';
            this.getAllDataSetById(this.testCaseId);
          }
        } else {
          this.testCaseId = 0;
        }
      });
  }

  getAllDataSetById(testsuiteId) {
    this.dataset_list = [];
    this.dataSetName = '';
    this.dataservice
      .getAllDataSetBySuiteId(testsuiteId)
      .subscribe((response: {}) => {
        if (response != null) {
          this.dataset_list = response;
        } else {
          this.dataset_list = [];
          this.dataSetName = '';
        }
      });
  }

  /* this method clear all messages and variables */
  isDatasetValid() {
    if (this.dataSetName.trim() === '') {
      this.IsData = true;
    } else {
      this.IsData = false;
    }
  }
  // ........op define operation either create / view

  hidemsg() {
    this.IsViewData = false;
  }

  createTestdata(op) {
    this.IsViewData = false;
    this.deletedItern_Details = [];
    this.jsonDS_Iterations = [];
    this.Test_steps_iterations = [];
    if (op === 'view') {
      this.IsViewOnly = true;
    } else {
      this.IsViewOnly = false;
    }
    if (this.dataSetName.trim() != '') {
      if (this.testSuiteId > 0) {
        this.dataservice
          .getAllDataSetIterationsByDsName(this.dataSetName, this.testSuiteId)
          .subscribe(async (ItRes: {}) => {
            console.log("ItRes",ItRes);
            if (ItRes != null) {
              if (ItRes['steps'] != null) {
                // this.testStepsList = {...ItRes};
                
                this.testStepsList = Object.assign({},{...ItRes['steps']})
                console.log(this.testStepsList);
                this.jsonDS_Iterations = ItRes['steps'];
                this.max_it_id = ItRes['maxItrId'];
                this.convert_jsTo_Tab();
              }
            } else {
              if (op == 'view') {
                this.IsViewData = true;
              } else {
                this.IsViewData = false;
                await this.getAllTestStepsById(this.testSuiteId);
                this.AddIteration();
              }
            }
          });
      }

      this.isCopyData = true;
      console.log('this.isCopyData  --> ', this.isCopyData);
      console.log('this.IsViewOnly  --> ', this.IsViewOnly);
    } else {
      this.IsData = true;
      this.isCopyData = false;
    }
    //convert copied data which is having HTML tags into plain text after paste
    setTimeout(() => {
      $(document).ready(function () {
        $('.iterationsteps').bind('paste', function (e) {
          var pastedData = e.originalEvent.clipboardData.getData('text');
          var originalData = pastedData.replace(/(\r\n|\n|\r)/gm, '');
          var a = document.execCommand('insertText', false, originalData);

          e.preventDefault();
        });
      });
    }, 2000);
  }

  async convert_jsTo_Tab() {
    let FormatedJson =[];

    if (this.jsonDS_Iterations != null && this.jsonDS_Iterations.length > 0) {
      this.Test_steps_iterations = [];
      this.Iterations_keys = [];
      
      var o;
      o = this.jsonDS_Iterations.reduce((a, b) => {
        a[b.testStepId] = a[b.testStepId] || [];
        a[b.testStepId].push({
          ['testStepName']: b.testStepName,
          [b.iterationId]: b.inputData,
          ['keyword']: b.keyword,
          ['element']: b.element,
          ['isdataRequired']: b.isdataRequired,
          ['testCaseName']: b.testCaseName,
          ['testCaseId']: b.testCaseId,
          ['caseNumber']:b.caseNumber,
          ['stepNumber']: b.stepNumber,
          ['testStepId']: b.testStepId,
          ['dataSetId']: b.dataSetId,
          ['testStepOrderNumber']: b.testStepOrderNumber,
          ['createdBy']: b.createdBy,
          ['createdOn']: b.createdOn,
          ['modifiedBy']: b.modifiedBy,
          ['modifiedOn']: b.modifiedOn
        });
        return a;
      }, {});
      let target = {};
      console.log('this.testStepsList..after reduce' , this.testStepsList);
      //Convert Array to Object and apply Map function
      let map = new Map();

      Object.keys(this.testStepsList).map(obj => {
        let mappedObj;
      
        mappedObj = this.testStepsList[obj];
        console.log('obj.testStepId', mappedObj.testStepId);
        let stepId_sorted = mappedObj.testStepId;
        console.log('o' , o[stepId_sorted]);
        map.set(stepId_sorted, o[stepId_sorted]); 
      })
      let testStepArr = [];
      for (let [key, value] of map) {
        console.log(key + " = " + value);
           testStepArr.push({
          step: value[0].testStepName,
          Number: value[0].testStepOrderNumber
        });
        }
      console.log('testStepArr------>', testStepArr);
      for (let [key, value] of map) {
        let keylen = value.length;
        let testStepId;
        let var_Keyword;
        let var_Element;
        let Var_isdataRequired;
        let var_testCaseName;
        let var_stepNumber;
        let var_testCaseId;
        let var_caseNumber;
        let var_testStepName;
        let var_testStepOrderNumber;
        let var_islabled;
        let var_createdBy;
        let var_createdOn;
        let var_modifiedBy;
        let var_modifiedOn;

        for (let l = 0; l < keylen; l++) {
          testStepId = o[key][l].testStepId;
          var_Keyword = o[key][l].keyword;
          var_Element = o[key][l].element;
          Var_isdataRequired = o[key][l].isdataRequired;
          var_stepNumber = o[key][l].stepNumber;
          var_testCaseName = o[key][l].testCaseName;
          var_testCaseId = o[key][l].testCaseId;
          var_caseNumber = o[key][l].caseNumber;
          var_testStepName = o[key][l].testStepName;
          var_testStepOrderNumber = o[key][l].testStepOrderNumber;
          var_createdBy = o[key][l].createdBy;
          var_createdOn = o[key][l].createdOn;
          var_modifiedBy = o[key][l].modifiedBy;
          var_modifiedOn = o[key][l].modifiedOn;
          delete o[key][l].testStepId;
          delete o[key][l].keyword;
          delete o[key][l].element;
          delete o[key][l].isdataRequired;
          delete o[key][l].stepNumber;
          delete o[key][l].testCaseName;
          delete o[key][l].testCaseId;
          delete o[key][l].testStepName;
          delete o[key][l].testStepOrderNumber;
          delete o[key][l].createdBy;
          delete o[key][l].createdOn;
          delete o[key][l].modifiedBy;
          delete o[key][l].modifiedOn;
        }
         FormatedJson.push({
          testStepName: var_testStepName,
          keyword: var_Keyword,
          element: var_Element,
          isdataRequired: Var_isdataRequired,
          testCaseName: var_testCaseName,
          testCaseId: var_testCaseId,
          caseNumber:var_caseNumber,
          stepNumber: var_stepNumber,
          TestStep: key,
          testStepId: testStepId,
          testStepOrderNumber: var_testStepOrderNumber,
          createdBy: var_createdBy,
          createdOn: var_createdOn,
          modifiedBy: var_modifiedBy,
          modifiedOn: var_modifiedOn,
          inputData: Object.assign.apply({}, o[key])
        });
      }
      console.log(FormatedJson);
      this.VarDS_Iterations = FormatedJson;
      this.collen = 0;
      let no_ofKeys: any = [];
      no_ofKeys = Object.keys(this.VarDS_Iterations[0].inputData);
      console.log(no_ofKeys);
      
      for (let j = 0; j < no_ofKeys.length; j++) {
        if (no_ofKeys[j] != 'dataSetId' && no_ofKeys[j] != 'caseNumber') {
          this.collen = this.collen + 1;
        }
      }


      this.No_of_col = Array(this.collen)
        .fill(0)
        .map((x, i) => i);
      this.Test_steps_iterations = [];
      for (let i = 0; i < this.VarDS_Iterations.length; i++) {
        let len = Object.keys(this.VarDS_Iterations[i].inputData).length;
        this.Test_steps_iterations.push({
          testStepName: this.VarDS_Iterations[i].testStepName,
          testStepId: this.VarDS_Iterations[i].testStepId,
          keyword: this.VarDS_Iterations[i].keyword,
          element: this.VarDS_Iterations[i].element,
          isdataRequired: this.VarDS_Iterations[i].isdataRequired,
          testCaseName: this.VarDS_Iterations[i].testCaseName,
          testCaseId: this.VarDS_Iterations[i].testCaseId,
          caseNumber : this.VarDS_Iterations[i].caseNumber,
          stepNumber: this.VarDS_Iterations[i].stepNumber,
          testStepOrderNumber: this.VarDS_Iterations[i].testStepOrderNumber,
          createdBy: this.VarDS_Iterations[i].createdBy,
          createdOn: this.VarDS_Iterations[i].createdOn,
          modifiedBy: this.VarDS_Iterations[i].modifiedBy,
          modifiedOn: this.VarDS_Iterations[i].modifiedOn
        });

        this.Iterations_keys = Object.keys(this.VarDS_Iterations[i].inputData);
        for (let k = 0; k < len; k++) {
          if (this.Iterations_keys[k] == 'dataSetId') {
            this.Test_steps_iterations[i][
              'dataSetId' + k
            ] = this.VarDS_Iterations[i].inputData['dataSetId'];
            delete this.Iterations_keys[k];
          } else {
            this.Test_steps_iterations[i][
              'Iteration' + this.Iterations_keys[k]
            ] = this.VarDS_Iterations[i].inputData[this.Iterations_keys[k]];
          }
        }
      }
    }
  }

  Create_Iteration() {
    
    let NoOfSteps = 0;
    let iterationId = 0;
    let sorted_steps =[];
    if (this.jsonDS_Iterations.length > 0) {
      this.saveExistingtbl();
      let iterationId_temp = Math.max.apply(
        Math,
        this.jsonDS_Iterations.map(function (o) {
          return o.iterationId;
        })
      );
      if (this.max_it_id > 0 && iterationId_temp + 1 == this.max_it_id) {
        iterationId = this.max_it_id;
      } else {
        iterationId = Math.max.apply(
          Math,
          this.jsonDS_Iterations.map(function (o) {
            return o.iterationId;
          })
        );
      }
    }
    if (this.testSteps != null) {
      if (this.testStepCounter == 0) {
        ++this.testStepCounter;
        console.log('++this.testStepCounter', ++this.testStepCounter);

        sorted_steps = [];
        this.jsonDS_Iterations.map(obj => {
          let index = this.testSteps.findIndex(x => x.testStepId === +obj.testStepId);
          console.log('index', index);
          sorted_steps.push(this.testSteps[index])

        })
        this.testSteps = [];
        this.testSteps = sorted_steps;
        console.log('test steps....', this.testSteps);
        console.log('sorted_steps steps....', sorted_steps);
      }
    

      this.Issteps = false;
      for (let m = 0; m < this.testSteps.length; m++) {
        let step = this.testSteps[m].testStepName;
        let keyword =
          this.testSteps[m].keyword == null
            ? ''
            : this.testSteps[m].keyword;
        let element =
          this.testSteps[m].element == null
            ? ''
            : this.testSteps[m].element;
        let testCaseName =
          this.testSteps[m].testCaseName == null
            ? ''
            : this.testSteps[m].testCaseName;
        let testCaseId =
          this.testSteps[m].testCaseId == null
            ? ''
            : this.testSteps[m].testCaseId;
            let caseNumber =
            this.testSteps[m].caseNumber == null
              ? ''
              : this.testSteps[m].caseNumber;
        let testStepOrderNumber =
          this.testSteps[m].testStepOrderNumber == null
            ? ''
            : this.testSteps[m].testStepOrderNumber;

        let isdataRequired = this.testSteps[m].isdataRequired;
        let stepNumber = this.testSteps[m].stepNumber;
        let filtered_iteration: any;
        console.log('before filter iteration' , this.jsonDS_Iterations);
        
        filtered_iteration = this.jsonDS_Iterations.filter(function (item) {
          return item.iterationId.toString() === '1';
        });
        console.log('filtered iteration --->', filtered_iteration);
        let NewIteration: any = {
          keyword: keyword,
          element: element,
          isdataRequired: isdataRequired,
          testCaseName: testCaseName,
          testCaseId: testCaseId,
          caseNumber:caseNumber,
          stepNumber: stepNumber,
          testStepId: this.testSteps[m].testStepId,
          testStepName: step.trim(),
          iterationId: iterationId + 1,
          inputData: filtered_iteration[m].inputData,
          dataSetId: '0',
          testStepOrderNumber: testStepOrderNumber,
          createdBy: filtered_iteration[m].createdBy,
          createdOn: filtered_iteration[m].createdOn,
          modifiedBy: filtered_iteration[m].modifiedBy,
          modifiedOn: filtered_iteration[m].modifiedOn
        };
        console.log('NewIteration...', NewIteration );
        
        this.jsonDS_Iterations.push(NewIteration);
      }
      this.convert_jsTo_Tab();
    } else {
      this.Issteps = true;
    }
  }

  AddIteration() {
    
    let NoOfSteps = 0;
    let iterationId = 0;
    if (this.jsonDS_Iterations.length > 0) {
      this.saveExistingtbl();
      let iterationId_temp = Math.max.apply(
        Math,
        this.jsonDS_Iterations.map(function (o) {
          return o.iterationId;
        })
      );
      if (this.max_it_id > 0 && iterationId_temp + 1 == this.max_it_id) {
        iterationId = this.max_it_id;
      } else {
        iterationId = Math.max.apply(
          Math,
          this.jsonDS_Iterations.map(function (o) {
            return o.iterationId;
          })
        );
      }
    }
    if (this.testSteps != null) {
      this.Issteps = false;
      for (let m = 0; m < this.testSteps.length; m++) {
        let step = this.testSteps[m].testStepName;
        let keyword =
          this.testSteps[m].keyword == null
            ? ''
            : this.testSteps[m].keyword.toLowerCase();
        let element =
          this.testSteps[m].element == null
            ? ''
            : this.testSteps[m].element.toLowerCase();
        let testCaseName =
          this.testSteps[m].testCaseName == null
            ? ''
            : this.testSteps[m].testCaseName;
        let testCaseId =
          this.testSteps[m].testCaseId == null
            ? ''
            : this.testSteps[m].testCaseId;
            let caseNumber =
            this.testSteps[m].caseNumber == null
              ? ''
              : this.testSteps[m].caseNumber;
        let testStepOrderNumber =
          this.testSteps[m].testStepOrderNumber == null
            ? ''
            : this.testSteps[m].testStepOrderNumber;

        let isdataRequired = this.testSteps[m].isdataRequired;
        let stepNumber = this.testSteps[m].stepNumber;
        let filtered_iteration: any;
        filtered_iteration = this.jsonDS_Iterations.filter(function (item) {
          return item.iterationId.toString() === '1';
        });
        console.log('filtered iteration --->', filtered_iteration);
        let NewIteration: any = {
          keyword: keyword,
          element: element,
          isdataRequired: isdataRequired,
          testCaseName: testCaseName,
          testCaseId: testCaseId,
          caseNumber:caseNumber,
          stepNumber: stepNumber,
          testStepId: this.testSteps[m].testStepId,
          testStepName: step.trim(),
          iterationId: iterationId + 1,
          inputData: '',
          dataSetId: '0',
          testStepOrderNumber: testStepOrderNumber,
          createdBy: this.testSteps[m].createdBy,
          createdOn: this.testSteps[m].createdOn,
          modifiedBy: this.testSteps[m].modifiedBy,
          modifiedOn: this.testSteps[m].modifiedOn
        };
        this.jsonDS_Iterations.push(NewIteration);
      }
      this.convert_jsTo_Tab();
    } else {
      this.Issteps = true;
    }
  }

  deleteIteration(id) {
    var isDelete = confirm(
      'Are you sure that you want to delete this iteration?'
    );
    if (isDelete) {
      if (this.jsonDS_Iterations.length > this.testSteps.length) {
        if (this.testSuiteId! > 0 && this.dataSetName != '') {
          this.saveExistingtbl();
          let IterationId = +id;
          if (IterationId > 0) {
            this.deletedItern_Details.push({
              dataSetName: this.dataSetName,
              iterationId: IterationId,
              testsuiteId: this.testSuiteId
            });
            let filtered: any;
            filtered = this.jsonDS_Iterations.filter(function (item) {
              return item.iterationId.toString() !== IterationId.toString();
            });
            this.jsonDS_Iterations = filtered;
            this.convert_jsTo_Tab();
          }
        } else {
          this.toastr.errorToastr(
            this.testCaseId <= 0
              ? 'Select test case'
              : 'Select / Enter dataset name'
          );
        }
      } else {
        this.toastr.errorToastr(
          "You can't delete this iteration, At least one iteration required..."
        );
        setTimeout(() => {
          this.errorMessage = '';
        }, 2000);
      }
    }
  }

  copyDataSet() {
    
    if (this.isCopyData) {
      var isCopy = confirm(
        'Are you sure that you want to copy this Test Data?'
      );
      if (isCopy) {
        let { dataSetName } = this;
        let datasetnameCopy;
        if (this.dataset_list.some(e => e.dataSetName === dataSetName)) {
          datasetnameCopy = true;
        }

        if (dataSetName.trim() != '' && !datasetnameCopy) {
          const tblData: any[] = [];
          const keys: string[] = [];
          //  ----------- tblIterationData is a table name
          const tableEl = document.getElementById('tblIterationData');
          [].forEach.call(
            tableEl.querySelectorAll('tr'),
            (lineElement: HTMLElement) => {
              const rows = lineElement.querySelectorAll('td,th');
              // console.log('rows....' , rows);
              
              /**
               * If is th tag, we store all items to keys array.
               */
              if (rows[0].nodeName === 'TH') {
                //We replace remove all whitespace char from key.
                rows.forEach((e: HTMLElement) =>
                  keys.push(e.innerText.replace(/\s*/g, ''))
                );
              } else {
                // We craft dynamically item object.
                let item: any = {};
                console.log('keys' ,keys);
                
                keys.forEach((key, index) => {
                  // We use keys array to populate it.
                  if (key != 'StepNo.') {
                    if (key == 'Steps') {
                      item[key] = rows[index].id;
                    } else if (key == 'TestCase') {
                      item['TestCase'] = rows[index]['innerText'];
                      item['testCaseId'] = rows[index].id;
                    } else {
                      let val = rows[index]['innerText'];
                      item[key] = val;
                      if(key != Constvar.CreatedBy && key != Constvar.CreatedOn && key !=  Constvar.ModifiedBy && key != Constvar.ModifiedOn)
                      {
                        item['dataSetId'] = rows[index].id;
                      }
                     
                    }
                  }
                });
                // Store it
                tblData.push(item);
              }
            }
          );
          console.log('tblData' ,tblData);

          let Iterations: any = [];
        
          for (let k = 0; k < tblData.length; k++) {
            let var_createdBy;
            let var_createdOn;
            let var_modifiedBy;
            let var_modifiedOn;
            let stepId = tblData[k].Steps;
            let varKeyword = tblData[k].Keyword.trim();
            let var_Element = tblData[k].Element.trim();
            let var_testCaseName = tblData[k].TestCase.trim();
            let var_testCaseId = tblData[k].testCaseId;
            let IsrequiredObj = this.jsonDS_Iterations.filter(
              o => o.keyword === varKeyword
            );
            let Var_isdataRequired = true;
            let var_stepNumber = 0;
            if(this.IsViewOnly == true)
            {
               var_createdBy = tblData[k].CreatedBy.trim().toLowerCase();
               var_createdOn = tblData[k].CreatedOn.trim().toLowerCase();
               var_modifiedBy = tblData[k].ModifiedBy.trim().toLowerCase();
               var_modifiedOn = tblData[k].ModifiedOn.trim().toLowerCase();
            }
        
            if (IsrequiredObj != null) {
              if (IsrequiredObj.length > 0) {
                if (IsrequiredObj[0].isdataRequired == undefined) {
                  console.log(k);
                  console.log('tblData', tblData);
                }
                Var_isdataRequired = IsrequiredObj[0].isdataRequired;
                var_stepNumber = IsrequiredObj[0].stepNumber;
              }
            }

            let stepKeys = Object.keys(tblData[k]);
            for (let l = 0; l < stepKeys.length; l++) {
              if (
                stepKeys[l] != 'Steps' &&
                stepKeys[l] != 'dataSetId' &&
                stepKeys[l] != 'testStepName' &&
                stepKeys[l].toLowerCase() != 'keyword' &&
                stepKeys[l].toLowerCase() != 'element' &&
                stepKeys[l] != 'TestCase' &&
                stepKeys[l] != 'CreatedBy' &&
                stepKeys[l] != 'CreatedOn' &&
                stepKeys[l] != 'ModifiedBy' &&
                stepKeys[l] != 'ModifiedOn' &&
                stepKeys[l] != 'testCaseId'
              ) {
                let keyname = stepKeys[l];
                let PushIndex = Iterations.length;
                
                Iterations.push({
                  testStepId: stepId,
                  keyword: varKeyword,
                  element: var_Element,
                  isdataRequired: Var_isdataRequired,
                  testCaseName: var_testCaseName,
                  testCaseId: var_testCaseId,
                  stepNumber: var_stepNumber,
                });
                var iterationLastIndex=Iterations.length-1;

                if(this.IsViewOnly== true)
                {
                      Iterations[iterationLastIndex].createdBy= var_createdBy
                     Iterations[iterationLastIndex].createdOn=var_createdOn
                     Iterations[iterationLastIndex].modifiedBy= var_modifiedBy
                     Iterations[iterationLastIndex].modifiedOn= var_modifiedOn
                }
               
                Iterations[PushIndex]['iterationId'] = keyname.replace(
                  'Iteration',
                  ''
                );
                Iterations[PushIndex]['inputData'] = tblData[k][keyname];
                Iterations[PushIndex]['testCaseId'] = tblData[k]['testCaseId'];
                Iterations[PushIndex]['languageId'] = 0;
                Iterations[PushIndex]['dataSetId'] = tblData[k][
                  'dataSetId'
                ].toString();
              }
            }
          }

          let jsnDataset: any = {},
            list: any = [],
            isInValidIteration = false;
          (jsnDataset.dataSetName = dataSetName.split(';').join('')),
            (jsnDataset.testSuiteId = this.testSuiteId),
            (jsnDataset.userId = this.userId);
          jsnDataset.steps = Iterations;
          var groupBy = function (xs, key) {
            return xs.reduce(function (rv, x) {
              (rv[x[key]] = rv[x[key]] || []).push(x);
              return rv;
            }, {});
          };

          let groupedIteration: any = {};
          groupedIteration = groupBy(Iterations, 'iterationId');
          for (let key in groupedIteration) {
            list = groupedIteration[key].filter(i => i.inputData.trim() == '');
            if (list != undefined) {
              if (list.length == groupedIteration[key].length) {
                isInValidIteration = true;
                break;
              }
            }
          }
          console.log('flag', isInValidIteration);
          if (!isInValidIteration) {
            console.log('jsnDataset' ,jsnDataset);
            
            this.dataservice.saveDataset(jsnDataset).subscribe(response => {
              if (response != null) {
                if (this.deletedItern_Details.length > 0) {
                  for (let k = 0; k < this.deletedItern_Details.length; k++) {
                    this.dataservice
                      .DeleteIterations(
                        this.deletedItern_Details[k].dataSetName,
                        this.deletedItern_Details[k].iterationId,
                        this.deletedItern_Details[k].testsuiteId
                      )
                      .subscribe(response => { });
                  }
                }
                this.toastr.successToastr('Dataset details saved successfully');
                this.Test_steps_iterations=[];
                this.isCopyData = false;
                this.getAllDataSetById(this.testSuiteId);
              }
            });
          } else {
            this.toastr.errorToastr(
              'Please add at least one value for each iteration'
            );
          }
        } else {
          if (datasetnameCopy) {
            this.toastr.errorToastr('Please add new data set name');
          } else {
            this.toastr.errorToastr('Please enter dataset name');
          }
        }
      }
    } else {
      this.toastr.errorToastr('Please select Data Set to Copy');
    }
  }

  addDataSet() {
    var isSave = confirm('Are you sure that you want to save this Test Data?');
    if (isSave) {
      let { dataSetName } = this;
      if (dataSetName.trim() != '') {
        const tblData: any[] = [];
        const keys: string[] = [];
        //  ----------- tblIterationData is a table name
        const tableEl = document.getElementById('tblIterationData');

        [].forEach.call(
          tableEl.querySelectorAll('tr'),
          (lineElement: HTMLElement) => {
            const rows = lineElement.querySelectorAll('td,th');
            /**
             * If is th tag, we store all items to keys array.
             */
            if (rows[0].nodeName === 'TH') {
              //We replace remove all whitespace char from key.
              rows.forEach((e: HTMLElement) =>
                keys.push(e.innerText.replace(/\s*/g, ''))
              );
            } else {
              // We craft dynamically item object.
              let item: any = {};
              keys.forEach((key, index) => {
                // We use keys array to populate it.
                if (
                  key != 'StepNo.' &&
                  key != 'CreatedBy' &&
                  key != 'CreatedOn' &&
                  key != 'ModifiedBy' &&
                  key != 'ModifiedOn'
                ) {
                  if (key == 'Steps') {
                    item[key] = rows[index].id;
                  } else if (key == 'TestCase') {
                    item['TestCase'] = rows[index]['innerText'].trim();
                    item['testCaseId'] = rows[index].id;
                  } else {
                    let val = rows[index]['innerText'].trim();
                    item[key] = val;
                    item['dataSetId'] = rows[index].id;
                  }
                }
              });
              //We Store it
              tblData.push(item);
            }
          }
        );
        let Iterations: any = [];

        for (let k = 0; k < tblData.length; k++) {
          let stepId = tblData[k].Steps;
          let varKeyword = tblData[k].Keyword.trim();
          let var_Element = tblData[k].Element.trim();
          let var_testCaseName = tblData[k].TestCase.trim();
          let var_testCaseId = tblData[k].testCaseId;
          let IsrequiredObj = this.jsonDS_Iterations.filter(
            o => o.keyword === varKeyword
          );
          let Var_isdataRequired = true;
          let var_stepNumber = 0;
          if (IsrequiredObj != null) {
            if (IsrequiredObj.length > 0) {
              if (IsrequiredObj[0].isdataRequired == undefined) {
                console.log(k);
                console.log('tblData', tblData);
              }
              Var_isdataRequired = IsrequiredObj[0].isdataRequired;
              var_stepNumber = IsrequiredObj[0].stepNumber;
            }
          }

          let stepKeys = Object.keys(tblData[k]);
          for (let l = 0; l < stepKeys.length; l++) {
            if (
              stepKeys[l] != 'Steps' &&
              stepKeys[l] != 'dataSetId' &&
              stepKeys[l] != 'testStepName' &&
              stepKeys[l].toLowerCase() != 'keyword' &&
              stepKeys[l].toLowerCase() != 'element' &&
              stepKeys[l] != 'TestCase' &&
              stepKeys[l] != 'CreatedBy' &&
              stepKeys[l] != 'CreatedOn' &&
              stepKeys[l] != 'ModifiedBy' &&
              stepKeys[l] != 'ModifiedOn' &&
              stepKeys[l] != 'testCaseId'
            ) {
              let keyname = stepKeys[l];
              let PushIndex = Iterations.length;
              Iterations.push({
                testStepId: stepId,
                keyword: varKeyword,
                element: var_Element,
                isdataRequired: Var_isdataRequired,
                testCaseName: var_testCaseName,
                testCaseId: var_testCaseId,
                stepNumber: var_stepNumber
              });
              Iterations[PushIndex]['iterationId'] = keyname.replace(
                'Iteration',
                ''
              );

              Iterations[PushIndex]['inputData'] = tblData[k][keyname];

              Iterations[PushIndex]['testCaseId'] = tblData[k]['testCaseId'];
              Iterations[PushIndex]['languageId'] = 0;
              Iterations[PushIndex]['dataSetId'] = tblData[k][
                'dataSetId'
              ].toString();
            }
          }
        }

        let jsnDataset: any = {},
          list: any = [],
          isInValidIteration = false;
        (jsnDataset.dataSetName = dataSetName.split(';').join('')),
          (jsnDataset.testSuiteId = this.testSuiteId),
          (jsnDataset.userId = this.userId);
        jsnDataset.steps = Iterations;
        var groupBy = function (xs, key) {
          return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
          }, {});
        };

        let groupedIteration: any = {};
        groupedIteration = groupBy(Iterations, 'iterationId');
        for (let key in groupedIteration) {
          list = groupedIteration[key].filter(i => i.inputData.trim() == '');
          if (list != undefined) {
            if (list.length == groupedIteration[key].length) {
              isInValidIteration = true;
              break;
            }
          }
        }
        console.log('flag', isInValidIteration);
        if (!isInValidIteration) {
          this.dataservice.saveDataset(jsnDataset).subscribe(response => {
            if (response != null) {
              if (this.deletedItern_Details.length > 0) {
                for (let k = 0; k < this.deletedItern_Details.length; k++) {
                  this.dataservice
                    .DeleteIterations(
                      this.deletedItern_Details[k].dataSetName,
                      this.deletedItern_Details[k].iterationId,
                      this.deletedItern_Details[k].testsuiteId
                    )
                    .subscribe(response => { });
                }
              }
              this.toastr.successToastr('Dataset details saved successfully');
              this.getAllDataSetById(this.testSuiteId);
            }
            this.createTestdata('view');
            //commented by mustaid 20-6-2020
            // this.clearData();
          });
        } else {
          this.toastr.errorToastr(
            'Please add at least one value for each iteration'
          );
        }
      } else {
        this.toastr.errorToastr('Enter Dataset Name');
      }
    }
  }
  
  clearData() {
    setTimeout(() => {
      this.successMessage = '';
    }, 2000);
    this.max_it_id = 0;
    this.errorMessage = '';
    this.dataSetName = '';
    this.No_of_col = [];
    this.collen = 0;
    this.deletedItern_Details = [];
    this.Test_steps_iterations = [];
    this.Iterations_keys = [];
    this.jsonDS_Iterations = [];
    if (this.testSuiteId > 0) {
      this.getAllTestStepsById(this.testSuiteId);
      this.getAllDataSetById(this.testSuiteId);
    }
  }

  saveExistingtbl() {
    let { dataSetName } = this;
    if (dataSetName != '' || dataSetName != undefined) {
      const tblData: any[] = [];
      const keys: string[] = [];
      //  ----------- tblIterationData is a table name
      const tableEl = document.getElementById('tblIterationData');

      [].forEach.call(
        tableEl.querySelectorAll('tr'),
        (lineElement: HTMLElement) => {
          const rows = lineElement.querySelectorAll('td,th');
          /**
           * If is th tag, we store all items to keys array.
           */
          if (rows[0].nodeName === 'TH') {
            //We replace remove all whitespace char from key.
            rows.forEach((e: HTMLElement) =>
              keys.push(e.innerText.replace(/\s*/g, ''))
            );
          } else {
            // We craft dynamically item object.
            let item: any = {};
            keys.forEach((key, index) => {
              if (key != 'StepNo.') {
                // We use keys array to populate it.

                if (key == 'Steps') {
                  item[key] = rows[index].id;

                  item['testStepName'] = rows[index]['innerText'];
                } else if (key == 'TestCase') {
                  item['TestCase'] = rows[index]['innerText'];
                  item['testCaseId'] = rows[index].id;
                } else {
                  let val = rows[index]['innerText'];
                  if (
                    key == 'CreatedBy' ||
                    key == 'CreatedOn' ||
                    key == 'ModifiedBy' ||
                    key == 'ModifiedOn' ||
                    key == 'Keyword' ||
                    key == 'Element'
                  ) {
                    item[key] = val;
                  } else {
                    item[key] = val;
                    item['dataSetId'] = rows[index].id;
                  }
                }
              }
            });
            //We Store it
            tblData.push(item);
          }
        }
      );
      let Iterations: any = [];
      for (let k = 0; k < tblData.length; k++) {
        let stepId = tblData[k].Steps;
        let varKeyword = tblData[k].Keyword.trim();
        let var_Element = tblData[k].Element.trim();
        let var_testCaseName = tblData[k].TestCase.trim();
        let var_testCaseId = tblData[k].testCaseId;
        // let var_caseNumber= tblData[k].caseNumber;
        let var_createdBy = tblData[k].CreatedBy;
        let var_createdOn = tblData[k].CreatedOn;
        let var_modifiedBy = tblData[k].ModifiedBy;
        let var_modifiedOn = tblData[k].ModifiedOn;
        let IsrequiredObj = this.jsonDS_Iterations.filter(
          o => o.keyword === varKeyword
        );
        let Var_isdataRequired = true;
        let var_stepNumber = 0;
        let var_testStepOrderNumber = 0;
        if (IsrequiredObj != null) {
          if (IsrequiredObj.length > 0) {
            if (IsrequiredObj[0].isdataRequired == undefined) {
              console.log(k);
              console.log('tblData', tblData);
            }
            Var_isdataRequired = IsrequiredObj[0].isdataRequired;
            var_stepNumber = IsrequiredObj[0].stepNumber;
            var_testStepOrderNumber = IsrequiredObj[0].testStepOrderNumber;
          }
        }

        let stepKeys = Object.keys(tblData[k]);
        for (let l = 0; l < stepKeys.length; l++) {
          if (
            stepKeys[l] != 'Steps' &&
            stepKeys[l] != 'dataSetId' &&
            stepKeys[l] != 'testStepName' &&
            stepKeys[l].toLowerCase() != 'keyword' &&
            stepKeys[l].toLowerCase() != 'element' &&
            stepKeys[l] != 'TestCase' &&
            stepKeys[l] != 'testCaseId' &&
            stepKeys[l] != 'CreatedBy' &&
            stepKeys[l] != 'CreatedOn' &&
            stepKeys[l] != 'ModifiedBy' &&
            stepKeys[l] != 'ModifiedOn'
          ) {
            let keyname = stepKeys[l];
            let PushIndex = Iterations.length;

            Iterations.push({
              testStepId: stepId,
              keyword: varKeyword,
              element: var_Element,
              isdataRequired: Var_isdataRequired,
              testCaseName: var_testCaseName,
              testCaseId: var_testCaseId,
              stepNumber: var_stepNumber,
              // caseNumber: var_caseNumber,
              testStepOrderNumber: var_testStepOrderNumber,
              createdBy: var_createdBy,
              createdOn: var_createdOn,
              modifiedBy: var_modifiedBy,
              modifiedOn: var_modifiedOn
            });
            Iterations[PushIndex]['iterationId'] = keyname.replace(
              'Iteration',
              ''
            );
            Iterations[PushIndex]['inputData'] = tblData[k][keyname];
            Iterations[PushIndex]['testStepName'] = tblData[k][
              'testStepName'
            ].trim();
            Iterations[PushIndex]['dataSetId'] = tblData[k][
              'dataSetId'
            ].toString();
          }
        }
      }

      this.jsonDS_Iterations = Iterations;
     console.log('in save table' , this.jsonDS_Iterations);
     
    }
  }
}
