import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { CreateTestStepComponent } from './create-test-step/create-test-step.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgPipesModule} from 'ngx-pipes';
import { CreateTestStepRoutingModule } from './create_test_step-routing.module';
@NgModule({
  declarations: [CreateTestStepComponent],
  imports: [CommonModule, SharedModule,CreateTestStepRoutingModule,Ng2SearchPipeModule,NgPipesModule]
})
export class CreateTestStepModule {}
