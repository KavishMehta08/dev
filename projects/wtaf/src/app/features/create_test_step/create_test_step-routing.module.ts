import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateTestStepComponent } from './create-test-step/create-test-step.component';

const routes: Routes = [
  {
    path: '',
    component: CreateTestStepComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateTestStepRoutingModule {}
