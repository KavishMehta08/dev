import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from '../../../shared/services/loader.service';
import { Type } from '../../models';
import { error } from 'protractor';
declare var $: any;
@Component({
  selector: 'wtaf-create-test-step',
  templateUrl: './create-test-step.component.html',
  styleUrls: ['./create-test-step.component.css']
})

export class CreateTestStepComponent implements OnInit {
  submitted = false;
  successMessage: String = '';
  errorMessage: String = '';
  brandList: any = [];
  regionList: any = [];
  testSuiteList: any = [];
  testCaseList: any = [];
  testStepList: any = [];
  keywordList: any = [];
  elementList: any = [];
  regionId: Number = 0;
  brandId: Number = 0;
  projectTypeList: any = [];
  testCaseId: Number = 0;
  testSuiteId: Number = 0;
  testStepId: number = 0;
  projectTypeid: Number = 0;
  projectTypeId: Number = 0;
  isFromTestCase: Boolean = false;
  isElementRequired: Boolean = false;
  isElementVisible = true;
  IsStepValid: Boolean = false;
  isTextChanged = false;
  txtSearch = '';
  ImageSelect = ['imagecompare', 'gifcompare'];
  selected_keywordName: string;
  stepSequence: Number = 0;
  userId: number;
  isSharedParaFlag: boolean;
  // soerting tables required variables
  sequenceNum: any;
  sortedStepId: any;
  sortedSteps: any = [];
  isDraggable = false;
  isSort = false;
  dataOrder: any = [];
  stepOrderJson: [];
  updatedStepOrderNum: any;
  testStepReorderFlag: boolean = false;
  testStepForm = this.formBuilder.group({
    projectTypeId: ['', [Validators.required]],
    brandId: ['', [Validators.required]],
    elementId: [''],
    keywordId: ['', [Validators.required]],
    regionId: ['', [Validators.required]],
    testSuiteId: ['', [Validators.required]],
    testCaseId: ['', [Validators.required]],
    testStepName: ['', [Validators.required]],
    testStepDesc: ['']
  });
  public imagePath;
  imgURL: any;
  public message: string;
  isImgUploaded: boolean = false;
  @ViewChild('imgfile', { static: false }) imgfile: ElementRef;

  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public loaderService: LoaderService

  ) { }

  async ngOnInit() {
    console.log('image list', this.ImageSelect);

    this.userId = parseInt(localStorage.getItem('userId'));
    this.isElementVisible = true;
    this.testStepId =
      parseInt(this.activatedRoute.snapshot.paramMap.get('id')) == null
        ? 0
        : parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    this.testCaseId =
      parseInt(this.activatedRoute.snapshot.paramMap.get('testCaseId')) == null
        ? 0
        : parseInt(this.activatedRoute.snapshot.paramMap.get('testCaseId'));
    this.projectTypeid = parseInt(
      this.activatedRoute.snapshot.paramMap.get('projectTypeid')
    );
    this.isSharedParaFlag = this.activatedRoute.snapshot.paramMap.get('isShared') == null
      ? false
      : Boolean(this.activatedRoute.snapshot.paramMap.get('isShared') == 'true');
    if (isNaN(this.testStepId)) {
      this.testStepId = 0;
    }

    await this.getAllProjectType();
    await this.getAllRegion();
    await this.getKeywordByProjectTypeId();
    await this.getAllElement();
    if (this.testStepId > 0) {
      this.isFromTestCase = false;
      this.getTestStepById(this.testStepId);
    }
    if (this.testCaseId > 0) {

      this.isFromTestCase = true;
      this.dataservice
        .getSharedTestCaseById(this.testCaseId)
        .subscribe(async (response: {}) => {
          this.testStepForm.controls['regionId'].setValue(response['regionId']);
          this.regionId = response['regionId'];
          this.brandId = response['brandId'];
          await this.getBrandsByRegionId(response['regionId']);

          this.testSuiteId = response['testSuiteId'];
          this.testStepForm.get('regionId').disable();
          this.testStepForm.get('brandId').disable();
          this.testStepForm.get('testSuiteId').disable();
          this.testStepForm.get('testCaseId').disable();
          this.testStepForm.get('projectTypeId').disable();
          this.testStepForm.controls['projectTypeId'].setValue(
            this.projectTypeid
          );
          await this.getAllTestStep(
            this.regionId,
            this.brandId,
            this.testSuiteId,
            this.testCaseId
          );
          if (this.isSharedParaFlag) {
            this.testCaseList = [response];
            if (response != null) {
              if (this.testCaseList.length == 1) {
                this.testStepForm.controls['testCaseId'].setValue(
                  this.testCaseList[0].testCaseId
                );
              } else {
                this.testStepForm.controls['testCaseId'].setValue('');
              }
            } else {
              this.testCaseId = 0;
              this.testStepForm.controls['testCaseId'].setValue('');
            }
            let formControl: FormControl = this.testStepForm.get(
              'testSuiteId'
            ) as FormControl;
            formControl.clearValidators();
            formControl.setValidators(null);
            formControl.updateValueAndValidity();
          }
        });

    }
    //Re-order Intialization
    function fixWidthHelper(e, ui) {
      ui.children().each(function () {
        $(this).width($(this).width());
      });
      return ui;
    }
    let jQueryInstance = this;
    $('#createTestStep').sortable({
      //   connectWith: 'table',
      items: 'tbody tr',
      //  items: 'tr',
      cursor: 'pointer',
      axis: 'y',
      opacity: 0.35,
      dropOnEmpty: true,
      helper: fixWidthHelper,
      containment: '#createTestStep',

      start: function (e, ui) {
        jQueryInstance.isSort = true;
        console.log('UI----', ui);
        jQueryInstance.sortedSteps = [];
        jQueryInstance.dataOrder = [];
        $('.testcasetr').remove();
        ui.item.addClass('selected');
        jQueryInstance.testStepReorderFlag = true;
      },
      stop: function (e, ui) {
        jQueryInstance.isSort = true;
        ui.item.removeClass('selected');
        var listValues = [];
        $(this)
          .find('tr')
          .each(function (index) {
            if (index > 0) {
              var rowDetails = $(this)
                .find('td')
                .eq(0)
                .html(index);
              jQueryInstance.updatedStepOrderNum = index;
              var sortstepId = $(this)
                .find('td')
                .eq(1)
                .html();
              console.log(sortstepId);
              jQueryInstance.sortedStepId = sortstepId;
              console.log('New sequence==', jQueryInstance.updatedStepOrderNum);
              console.log('New sequence==', jQueryInstance.sortedStepId);
              listValues.push(jQueryInstance.updatedStepOrderNum);
              jQueryInstance.sortedSteps.push(jQueryInstance.sortedStepId);

              jQueryInstance.dataOrder = listValues;
              console.log('steps', jQueryInstance.dataOrder);
              console.log('ID', jQueryInstance.sortedSteps);
              var sortedIDs = $(this).sortable('toArray');
              console.log('sortedIds----', sortedIDs);
              jQueryInstance.testStepReorderFlag = true;
            }
            var widget = $('.selector').sortable('widget');
          });


      }
    });
  }
  get f() {
    return this.testStepForm.controls;
  }

  //  Added by Akash - Sorting Table Asc Desc
  orderedArray = [{
    projectTypeName: "ASC",
    testStepName: "ASC",
    module: "ASC",
    element: "ASC",
    keyword: "ASC",
    modifiedOn: "ASC",
    modifiedBy: "ASC"
  }]
  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
    this.testStepList = this.sort_by_key(this.testStepList, key, order);
    this.loaderService.hide();
  }
  sort_by_key(array, key, order) {

    if (order === "ASC") {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];

        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
    else {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
  }
  async checkDuplication() {
    if (this.testStepForm.value.testStepName != '') {
      const StepDetails = this.testStepForm.value;
      StepDetails.resource = this.testStepForm.value.testStepName
        .toString()
        .trim();
      StepDetails.type = Type.TestStep;
      StepDetails.id = this.testCaseId;
      await this.dataservice
        .checkDuplication(StepDetails)
        .toPromise()
        .then(async (response: Boolean) => {
          this.IsStepValid = response;
          if (this.IsStepValid) {
            this.toastr.errorToastr(' Test Step "' + StepDetails.resource + '" is Already exists');

          } else {
            this.errorMessage = '';
          }
        });
    }
  }



  preview(uploadFile) {

    console.log('uploaded file ', uploadFile);
    if (uploadFile.length === 0) {
      return;
    }
    else {
      var mimeType = uploadFile[0].type;
      console.log("mimeType....", mimeType);

      if (mimeType.match(/image\/*/) == null) {
        this.message = "Only images are supported.";
        return;
      }
    }
    this.message = '';
    var reader = new FileReader();
    this.imagePath = uploadFile[0];
    console.log("image path----", this.imagePath[0]);

    reader.readAsDataURL(uploadFile[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
      console.log('image url-----========', this.imgURL);
    }
  }

  openBrowsePopUp() {
    this.imgURL = "assets/images/avatar.png";
    console.log('this.image', this.imgfile);
    this.imgfile.nativeElement.value = '';
    this.imagePath = null;
    $('#selectImgModal').modal('show');
  }

  /* get all regions  */
  async getAllRegion() {
    await this.dataservice.getAllRegion().subscribe((response: {}) => {
      this.regionList = response;
      //to place NAR region at first position in frop down
      //  this.regionList.unshift(this.regionList.splice(this.regionList.findIndex(item => item.regionName === 'NAR'), 1)[0])
      //  console.log(this.regionList);
      if (response != null) {
        if (this.regionList.length == 1) {
          this.testStepForm.controls['regionId'].setValue(
            this.regionList[0].regionId
          );
          this.getBrandsByRegionId(this.regionList[0].regionId);
        } else {
          this.testStepForm.controls['regionId'].setValue(
            this.regionList[0].regionId
          );
        }
      }
    });
  }

  getAllProjectType() {

    this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        // Success
        this.projectTypeList = response;
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.projectTypeId = this.projectTypeList[0].projectTypeId;
            this.testStepForm.controls['projectTypeId'].setValue(
              this.projectTypeList[0].projectTypeId
            );
          } else {
            this.testStepForm.controls['projectTypeId'].setValue(
              this.projectTypeList[0].projectTypeId
            );
          }
        }
      });
  }
  /* get all keyword  */
  async getKeywordByProjectTypeId() {
    await this.dataservice
      .getKeywordByProjectTypeId(this.projectTypeid)
      .then(response => {
        // Success
        this.keywordList = response;
        this.keywordList = this.keywordList.filter((ele) => {
          return ele.keywordName.trim().length != 0;
        });
        console.log('this.keywordList.....?', this.keywordList);
        if (response != null) {
          if (this.keywordList.length == 1) {
            this.testStepForm.controls['keywordId'].setValue(
              this.keywordList[0].keywordId
            );
          } else {
            this.testStepForm.controls['keywordId'].setValue('');
          }
        } else {
          this.testStepForm.controls['keywordId'].setValue('');
        }
      });
  }

  /* get all elements  */
  getAllElement() {
    let ElementName = Type.ElementName;
    let asc = Type.ascending;
    this.dataservice.getAllElement(ElementName, asc).then(async (response: {}) => {
      this.elementList = response;
      if (response != null) {
        if (this.elementList.length == 1) {
          this.testStepForm.controls['elementId'].setValue(
            this.elementList[0].elementId
          );
        } else {
          this.testStepForm.controls['elementId'].setValue('');
        }
      } else {
        this.testStepForm.controls['elementId'].setValue('');
      }
    });
  }

  /* change region and get brands*/
  changeRegion(e) {
    this.brandList = [];
    this.brandId = 0;
    this.testSuiteList = [];
    this.testSuiteId = 0;
    this.testStepForm.controls['testSuiteId'].setValue('');
    this.testCaseList = [];
    this.testCaseId = 0;
    this.testStepForm.controls['testCaseId'].setValue('');
    this.regionId = parseInt(e.target.value);
    this.getBrandsByRegionId(this.regionId);
  }

  /* get all brands by region Id */
  async getBrandsByRegionId(regionId) {
    await this.dataservice
      .getBrandsByRegionId(regionId)
      .subscribe((response: {}) => {
        console.log('brands', this.brandList);
        this.brandList = response;
        if (response != null) {
          if (this.brandList.length == 1) {
            this.brandId = this.brandList[0].brandId;
            this.testStepForm.controls['brandId'].setValue(
              this.brandList[0].brandId
            );
          } else {
            this.testStepForm.controls['brandId'].setValue('');
          }
        } else {
          this.brandId = 0;
          this.testStepForm.controls['brandId'].setValue('');
        }
        if (this.brandId > 0) {
          this.testStepForm.controls['brandId'].setValue(this.brandId);
          if (!this.isSharedParaFlag) {
            this.getAllTestSuite(this.regionId, this.brandId);
          }
        }
      });
  }

  changeKeyword(e) {

    this.isImgUploaded = false;
    let keywordName = e.target.options[e.target.options.selectedIndex].text;
    this.selected_keywordName = keywordName;
    console.log('e.target.value', e.target.options[e.target.options.selectedIndex].text);
    // for(let i = 0; this.ImageSelect.length > 0; i++)
    // {
    //   this.ImageSelect[i]
    // }
    // this.ImageSelect.filter(element =>{
    //   keywordName === element.
    // })


    if (this.ImageSelect.some(e => e.toUpperCase() === keywordName.trim().toUpperCase())) {
      this.isImgUploaded = true;
    }
    else {
      this.imgfile.nativeElement.value = '';
      this.imagePath = null;
      this.isImgUploaded = false;

    }

    this.isElementVisible = true;
    this.dataservice.getKeywordById(e.target.value).subscribe(response => {
      if (response['elementRequired']) {
        this.isElementVisible = false;
        this.testStepForm.controls['elementId'].setValue('');
      } else {
        this.isElementVisible = true;
        this.testStepForm.controls['elementId'].setValue(0);
      }
    });
  }

  changeBrand(e) {
    this.testSuiteList = [];
    this.testSuiteId = 0;
    this.testStepForm.controls['testSuiteId'].setValue('');
    this.testCaseList = [];
    this.testCaseId = 0;
    this.testStepForm.controls['testCaseId'].setValue('');
    this.getAllTestSuite(this.regionId, parseInt(e.target.value));
  }

  /* add and update method for test step */
  async saveTestStep() {
    this.isElementRequired = false;
    this.submitted = true;
    if (this.testStepForm.valid && this.IsStepValid == false) {
      const testStepDetails = this.testStepForm.value;
      this.dataservice
        .getKeywordById(testStepDetails.keywordId)
        .subscribe(response => {
          if (response['elementRequired']) {
            let elementId = this.testStepForm.value.elementId;
            if (elementId == '') {
              this.isElementRequired = true;
            } else {
              this.isElementRequired = false;
            }
          }
          if (!this.isElementRequired) {
            if (this.testStepForm.get('regionId').disabled) {
              testStepDetails.regionId = this.regionId;
            }
            if (this.testStepForm.get('brandId').disabled) {
              testStepDetails.brandId = this.brandId;
            }
            if (this.testStepForm.get('testSuiteId').disabled) {
              testStepDetails.testSuiteId = this.testSuiteId;
            }
            if (this.testStepForm.get('testCaseId').disabled) {
              testStepDetails.testCaseId = this.testCaseId;
            }
            let stepName = testStepDetails['testStepName'];
            testStepDetails.testStepId = this.testStepId;
            testStepDetails.userId = this.userId;
            testStepDetails.stepSequence = this.stepSequence;
            if ((this.ImageSelect.some(e => e.toUpperCase() === this.selected_keywordName.trim().toUpperCase()))
              && this.imagePath == null) {
              this.toastr.errorToastr('Please select image');
            }
            else {
              this.dataservice
                .saveTestStep(testStepDetails)
                .subscribe((response: any) => {

                  let updated_testStepId = response.savedTestStepId;
                  if (this.imagePath != null) {
                    console.log('image path', this.imagePath);
                    let file: File = this.imagePath;
                    let formData: FormData = new FormData();
                    formData.append('image', file);
                    formData.append('reportProgress', 'true');
                    console.log('formData...........', formData);
                    this.dataservice.uploadImg_for_Comparison(this.testSuiteId, this.testCaseId, updated_testStepId, formData).subscribe(imgRes => {
                      console.log('image response-------', imgRes);
                      if (imgRes != null) {
                        this.toastr.infoToastr('Image saved successfully')
                      }
                    },
                      error => {
                        console.log(error);

                      })
                  }
                  console.log('response1.............', updated_testStepId);
                  console.log('response2.............', this.testStepId);

                  if (
                    this.testStepId != 0 &&
                    this.testStepId != null &&
                    this.testStepId != undefined
                  ) {
                    this.toastr.successToastr(
                      'Test step ' + stepName + 'successfully updated.'
                    );
                    //added flag
                    this.router.navigate(['test_step/search_test_step', { isFromCreateStep: true }]);
                  } else {
                    this.toastr.successToastr(
                      'Test step  ' + stepName + ' successfully created.'
                    );
                  }
                  this.getAllTestStep(
                    this.regionId,
                    this.brandId,
                    this.testSuiteId,
                    this.testCaseId
                  );
                  this.clearData(1);
                });
            }

          }
        });
    } else {
      if (this.IsStepValid == true) {
        this.toastr.errorToastr('Test Step "' + this.testStepForm.value.testStepName + '" is Already exists');
      }
    }
  }

  /* get test case by test case Id */
  async getTestStepById(testStepId) {
    this.testStepId = testStepId;
    await this.dataservice
      .getTestStepById(testStepId)
      .subscribe(async response => {
        this.testStepForm.controls['regionId'].setValue(response['regionId']);
        if (response['elementId'] == 0) {
          response['elementId'] = '';
        }
        this.regionId = response['regionId'];
        await this.getBrandsByRegionId(response['regionId']);
        this.brandId = response['brandId'];
        await this.getAllTestSuite(
          parseInt(response['regionId']),
          this.brandId
        );
        this.testSuiteId = response['testSuiteId'];
        await this.getAllTestCase(
          this.regionId,
          this.brandId,
          this.testSuiteId
        );
        this.stepSequence = response['stepSequence'];
        this.testCaseId = response['testCaseId'];
        this.testStepForm.controls['testStepName'].setValue(
          response['testStepName']
        );
        this.testStepForm.controls['testStepDesc'].setValue(
          response['testStepDesc']
        );
        this.testStepForm.get('regionId').disable();
        this.testStepForm.get('brandId').disable();
        this.testStepForm.get('projectTypeId').disable();
        this.testStepForm.get('testSuiteId').disable();
        this.testStepForm.get('testCaseId').disable();
        this.testStepForm.controls['elementId'].setValue(response['elementId']);
        this.testStepForm.controls['projectTypeId'].setValue(
          this.projectTypeid
        );
        await this.getAllTestStep(
          this.regionId,
          this.brandId,
          this.testSuiteId,
          this.testCaseId
        );
        await this.dataservice
          .getKeywordById(response['keywordId'])
          .toPromise()
          .then(response => {
            this.testStepForm.controls['keywordId'].setValue(
              response['keywordId']
            );
            let keywordob = this.keywordList.filter(data => {
              return data.keywordId == response['keywordId'];
            });
            console.log('keywordobkeywordob', keywordob);
            this.selected_keywordName = keywordob[0].keywordName;
            if (this.ImageSelect.some(e => e.toUpperCase() === keywordob[0].keywordName.toUpperCase())) {
              this.isImgUploaded = true;
            }
            else {
              this.isImgUploaded = false;
            }

            if (response['elementRequired']) {
              this.isElementVisible = false;
            } else {
              this.isElementVisible = true;
            }
          });
      });
  }

  /* get all test suite  */
  async getAllTestSuite(regionId, brandId) {
    let TestSuiteName = Type.TestSuiteName;
    let asc = Type.ascending;
    await this.dataservice
      .getAllTestSuite(TestSuiteName, asc, regionId, brandId, 0, '',0,0)
      .subscribe((response: {}) => {
        this.testSuiteList = response;
        if (response != null) {
          if (this.testSuiteList.length == 1) {
            this.testStepForm.controls['testSuiteId'].setValue(
              this.testSuiteList[0].testSuiteId
            );
          } else {
            this.testStepForm.controls['testSuiteId'].setValue('');
          }
        } else {
          this.testSuiteId = 0;
          this.testStepForm.controls['testSuiteId'].setValue('');
        }
        if (this.testSuiteId > 0) {
          this.testStepForm.controls['testSuiteId'].setValue(this.testSuiteId);
          this.getAllTestCase(this.regionId, this.brandId, this.testSuiteId);
        }
      });
  }

  /* get all test case  */
  async getAllTestCase(regionId, brandId, testSuiteId) {
    let testcasename = Type.TestCaseName;
    let asc = Type.ascending;
    await this.dataservice
      .getAllSharedTestCasebyProjects(testcasename, asc, regionId, brandId, this.projectTypeid, '', this.isSharedParaFlag, testSuiteId)
      .subscribe((response: {}) => {
        this.testCaseList = response;
        if (response != null) {
          if (this.testCaseList.length == 1) {
            this.testStepForm.controls['testCaseId'].setValue(
              this.testCaseList[0].testCaseId
            );
          } else {
            this.testStepForm.controls['testCaseId'].setValue('');
          }
        } else {
          this.testCaseId = 0;
          this.testStepForm.controls['testCaseId'].setValue('');
        }
        if (this.testCaseId > 0) {
          this.testStepForm.controls['testCaseId'].setValue(this.testCaseId);
        }
      });
  }

  /* get all test step  */
  async getAllTestStep(regionId, brandId, testSuiteId, testCaseId) {
    await this.dataservice
      .getAllTestStep(regionId, brandId, testSuiteId, testCaseId)
      .subscribe((response: {}) => {
        this.testStepList = response;
      });
  }
  /* this method clear all messages and test case form also */
  clearData(val) {
    // this.txtSearch = " ";
    var r = false;
    if ((this.testStepForm.value.testStepDesc != null || this.testStepForm.value.keywordId != 0) && val == 0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.isElementVisible = true;
      this.submitted = false;
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);

      this.errorMessage = '';
      this.testStepForm.controls['testStepName'].setValue('');
      this.testStepForm.controls['testStepDesc'].setValue('');
      this.testStepForm.controls['keywordId'].setValue('');
      this.testStepForm.controls['elementId'].setValue('');
      this.testStepId = 0;
      this.isElementRequired = false;
      this.stepSequence = 0;
    }
  }

  /* delete test case by test case Id */
  deleteTestStep(testStepId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this test step?'
    );
    if (isDelete) {
      this.dataservice.deleteTestStep(testStepId).subscribe(
        response => {
          this.toastr.successToastr('Test step successfully deleted');
          this.clearData(1);
          this.getAllTestStep(
            this.regionId,
            this.brandId,
            this.testSuiteId,
            this.testCaseId
          );
        }
      );
    }
  }

  //Close select image pop-up 
  closePopUp(isselected) {
    if (!isselected) {
      this.imgURL = "assets/images/avatar.png";
      console.log('this.image', this.imgfile);
      this.imgfile.nativeElement.value = '';
      this.imagePath = null;
    }
    $('#selectImgModal').modal('hide');
  }

  changeTestStep() {
    this.isTextChanged = true;
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }
  //Re-order methods
  submitReorder() {
    let reorderDetails = [];
    console.log('listValues------', this.testStepList);
    console.log('lenth of the list', this.testStepList.length);
    for (let m = 0; m < this.sortedSteps.length; m++) {
      console.log('testStepId', this.testStepList[m].testStepId);
      console.log('After testStepOrderNumber', this.dataOrder[m]);

      reorderDetails.push({
        testStepId: this.sortedSteps[m],
        testStepOrderNumber: this.dataOrder[m]
      });
    }
    if (reorderDetails.length != 0) {
      this.dataservice.addReorderTestStep(reorderDetails).subscribe(res => {
        this.toastr.successToastr('Test Step  Reordered  successfully');
        this.getAllTestStep(
          this.regionId,
          this.brandId,
          this.testSuiteId,
          this.testCaseId
        );
      });
    } else {
      this.toastr.warningToastr(' Please Reorder the test step. ');
    }
  }
  cancelReorder() {
    var r = false;
    if (this.isSort != false) {
      r = confirm('Are you sure you want to cancel data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.getAllTestStep(
        this.regionId,
        this.brandId,
        this.testSuiteId,
        this.testCaseId
      );
    }
    this.isSort = false;
  }
}
