import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTestStepComponent } from './create-test-step.component';

describe('CreateTestStepComponent', () => {
  let component: CreateTestStepComponent;
  let fixture: ComponentFixture<CreateTestStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTestStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTestStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
