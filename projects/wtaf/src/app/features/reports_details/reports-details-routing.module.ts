import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ReportsDetailsComponent} from './reports-details/reports-details.component';
import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    component: ReportsDetailsComponent,
    data: { title: '' },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsDetailsRoutingModule {}
