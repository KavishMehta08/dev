import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import {ReportsDetailsComponent} from './reports-details/reports-details.component';

import { ReportsDetailsRoutingModule } from './reports-details-routing.module';
@NgModule({
  declarations: [ReportsDetailsComponent],
  imports: [CommonModule, SharedModule,ReportsDetailsRoutingModule]
})
export class ReportsDetailsModule {}
