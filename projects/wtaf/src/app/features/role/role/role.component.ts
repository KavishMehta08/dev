import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { Type } from '../../models';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from '../../../shared/services/loader.service';

@Component({
  selector: 'wtaf-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  submitted = false;
  successMessage: String = '';
  errorMessage: String = '';
  roleList: any = [];
  roleId: Number = 0;
  inValid: Boolean = false;
  minLengthMsg: Boolean = false;
  isTextChanged: Boolean = false;
  strOldValue: String = '';
  txtSearch = '';
  userId: number;
  userRoleForm = this.formBuilder.group({
    userRoleName: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50)])],
    roleDescription: ['']
  });
  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    public dataservice: DataService,
    public loaderService: LoaderService

  ) { }

  ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log("local usr id", this.userId);
    this.getAllRole();
  }
  get f() {
    return this.userRoleForm.controls;
  }

  //  Added by Akash - Sorting Table Asc Desc
  orderedArray = [{
    userRoleName: "ASC",
  }]
  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
    this.roleList = this.sort_by_key(this.roleList, key, order);
    console.log(this.roleList);
    this.loaderService.hide();
  }
  sort_by_key(array, key, order) {
    if (order === "ASC") {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];

        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
    else {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
  }
  /* for duplication check */
  async checkDuplication() {

    const roleDetails = this.userRoleForm.value;
    if (
      roleDetails['userRoleName'] != null &&
      roleDetails['userRoleName'] != 0 &&
      roleDetails['userRoleName'] != undefined
    ) {
      roleDetails.resource = this.userRoleForm.value.userRoleName
        .toString()
        .trim();
    }

    roleDetails.type = Type.UserRole;
    roleDetails.id = 0;
    await this.dataservice
      .checkDuplication(roleDetails)
      .toPromise()
      .then((response: Boolean) => {
        // Success
        this.inValid = response;
        if (this.inValid) {
          this.toastr.errorToastr('User role name "' + roleDetails.resource + '" is already exists'); this.isTextChanged = false;
        } else {
          this.errorMessage = '';
        }
      });
  }

  /* add and update method for role */
  async saveRole() {
    
    this.minLengthMsg = false;
    this.inValid = false;
    this.submitted = true;

    if (this.roleId == 0 || this.isTextChanged) {
      await this.checkDuplication();
    }

    if (this.userRoleForm.valid && !this.inValid) {
      if ( this.userRoleForm.value.userRoleName.length >= 2)
      {
        const roleDetails = this.userRoleForm.value;
        roleDetails.userId = this.userId;
        roleDetails.roleId = this.roleId;
        this.dataservice.saveRole(roleDetails).subscribe(response => {
          if (this.roleId != 0) {
            this.toastr.successToastr('Role updated successfully');
          } else {
            this.toastr.successToastr('Role created successfully');
          }
          this.clearData(1);
          this.getAllRole();
        });
      }
      else{
         this.minLengthMsg = true;
      }
    
    }
  }

  /* get role by role Id */
  getRoleById(roleId) {
    this.clearData(1);
    this.roleId = roleId;

    this.dataservice.getRoleById(roleId).subscribe(response => {
      this.strOldValue = response['userRoleName'];
      this.userRoleForm.patchValue(response);
      window.scroll(0, 0);
    });
  }

  /* get all roles  */
  getAllRole() {
    let roleId = Type.RoleId;
    let desc = Type.descending;
    this.dataservice.getAllRole(roleId, desc).subscribe((response: {}) => {
      this.roleList = response;
      console.log(this.roleList);

      this.dataservice.customFilter['userRoleName'] = '';
    });
  }

  /* this method clear all messages and user role form also */
  clearData(val) {
    // this.txtSearch = " ";
    var r = false;
    if ((this.userRoleForm.value.userRoleName != 0 || this.userRoleForm.value.roleDescription != 0) && val == 0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.submitted = false;
      this.userRoleForm.reset();
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      this.userRoleForm = this.formBuilder.group({
        userRoleName: ['', [Validators.required]],
        roleDescription: ['']
      });
      this.roleId = 0;
      this.errorMessage = '';
      this.inValid = false;
      this.minLengthMsg=false;
      this.strOldValue = '';
    }
  }

  /* delete role by role Id*/
  deleteRole(roleId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this Role?'
    );
    if (isDelete) {
      this.dataservice.deleteRole(roleId).subscribe(
        response => {
          this.toastr.successToastr('Role deleted successfully');
          this.getAllRole();
          this.clearData(1);
        },
        error => {
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
          setTimeout(() => {
            this.errorMessage = '';
            this.dataservice.errorWhenInUse = '';
          }, 2000);
        }
      );
    }
  }

  changeData(e) {
    if (
      this.strOldValue.toString().trim() != e.target.value.toString().trim() &&
      this.strOldValue != ''
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }

 


}
