import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { RoleComponent } from './role/role.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { RoleRoutingModule } from './role-routing.module';
@NgModule({
  declarations: [RoleComponent],
  imports: [CommonModule, SharedModule,RoleRoutingModule,Ng2SearchPipeModule]
})
export class RoleModule {}
