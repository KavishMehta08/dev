import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { LanguageComponent } from './language/language.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { LanguageRoutingModule } from './language-routing.module';
@NgModule({
  declarations: [LanguageComponent],
  imports: [CommonModule, SharedModule,LanguageRoutingModule,Ng2SearchPipeModule]
})
export class LanguageModule {}
