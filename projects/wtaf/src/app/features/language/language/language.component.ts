import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormGroupDirective,
  FormControl
} from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { Type } from '../../models';
import { ChangeDetectorRef } from '@angular/core';
import { async } from '@angular/core/testing';
import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'wtaf-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.css']
})
export class LanguageComponent implements OnInit {
  submitted = false;

  languageId: Number = 0;
  successMessage: String = '';
  errorMessage: String = '';
  languageNameerrorMessage: String = '';
  languageList: any = [];
  filterData: any = [];
  regionList: any = [];
  projectTypeList: any = [];
  inValid: Boolean = false;
  invalidLanguageCode: Boolean = false;
  invalidLanguageName: Boolean = false;

  txtSearch = '';
  strOldLanguageCode = '';
  strOldLanguageName = '';
  regionId = 0;

  languageForm = this.formBuilder.group({
    languageCode: ['', [Validators.required,noWhitespaceValidator]],
    languageName: ['', [Validators.required,noWhitespaceValidator]],
    languageDesc: ['']
  });
  @ViewChild(FormGroupDirective, { static: false })
  formGroupDirective: FormGroupDirective;
  constructor(
    private ref: ChangeDetectorRef,
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService
  ) {}
  async ngOnInit() {
    this.getAllLanguage();
    //await this.getAllProjectType();
    //  this.getAllRegion();
  }

  get f() {
    return this.languageForm.controls;
  }
  //  Added by Akash - Sorting Table Asc Desc
  sortTable(c) {
    var table,
      rows,
      switching,
      i,
      x,
      y,
      shouldSwitch,
      dir,
      switchcount = 0;
    table = document.getElementById('languageTable');
    switching = true;
    //Set the sorting direction to ascending:
    dir = 'asc';
    while (switching) {
      switching = false;
      rows = table.rows;
      for (i = 1; i < rows.length - 1; i++) {
        shouldSwitch = false;
        x = rows[i].getElementsByTagName('TD')[c];
        y = rows[i + 1].getElementsByTagName('TD')[c];
        if (dir == 'asc') {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        } else if (dir == 'desc') {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        switchcount++;
      } else {
        if (switchcount == 0 && dir == 'asc') {
          dir = 'desc';
          switching = true;
        }
      }
    }
  }
 


  /* for duplication check */
  async checkDuplication(data) {
    this.errorMessage = '';
    const languageDetails = this.languageForm.value;
    
    if (data == 'languageName') {
      if (
        this.languageId != 0 &&
        this.strOldLanguageName.toString().trim() ===
          this.languageForm.value.languageName.toString().trim() &&
        this.strOldLanguageName != ''
      ) {
        return;
      }

      languageDetails.resource = this.languageForm.value.languageName.toString().trim();
    }
    if (data == 'languageCode') {
      if (
        this.languageId != 0 &&
        this.strOldLanguageCode.toString().trim() ===
          this.languageForm.value.languageCode.toString().trim() &&
        this.strOldLanguageCode != ''
      ) {
        return;
      }

      languageDetails.resource = this.languageForm.value.languageCode.toString().trim();
    }

    languageDetails.type = Type.Language;
    languageDetails.id = 0;
      await this.dataservice
        .checkDuplication(languageDetails)
        .toPromise()
        .then((response: Boolean) => {
          this.inValid = response;
          if (data == 'languageName') {
            this.invalidLanguageName = this.inValid;
            this.errorMessage = '';
            if (this.invalidLanguageName) {
              this.toastr.errorToastr(
                'Language name is already in use for selected region\n '
              );
              // if (this.invalidBrandCode == true) {
              //   // this.errorMessage = "Brand code is already in use for selected region";
              //   this.toastr.errorToastr( "Brand name is already in use for selected region\n ");
              // }
              // else {
              //   this.errorMessage = "";
              //   // this.brandNameerrorMessage = "Brand name is already in use for selected region\n ";
              //  this.toastr.errorToastr( "Brand name is already in use for selected region\n ");
              // }
            } else if (this.invalidLanguageCode == true) {
              this.errorMessage = '';
              // this.brandNameerrorMessage = " Brand code is already in use for selected region\n";
              this.toastr.errorToastr(
                'Brand code is already in use for selected region\n'
              );
            } else if (
              this.invalidLanguageCode == false &&
              this.invalidLanguageName == false
            ) {
              this.errorMessage = '';
              this.languageNameerrorMessage = '';
            }
          } else if (data == 'languageCode') {
            this.invalidLanguageCode = this.inValid;
            this.languageNameerrorMessage = '';
            if (this.invalidLanguageCode) {
              this.toastr.errorToastr(
                '\nLanguage code is already in use for selected region'
              );
              // if (this.invalidBrandName == true) {
              //   this.toastr.errorToastr("\nBrand code is already in use for selected region");
              //   // this.brandNameerrorMessage = " Brand name is already in use for selected region";
              //   this.toastr.errorToastr(" Brand name is already in use for selected region");

              // }
              // else {
              //   this.brandNameerrorMessage = "";
              //   // this.errorMessage = "\nBrand code is already in use for selected region";
              //    this.toastr.errorToastr("\nBrand code is already in use for selected region");
              // }
            } else if (this.invalidLanguageName == true) {
              this.languageNameerrorMessage = '';
              // this.brandNameerrorMessage = " Brand name is already in use for selected region\n";
              this.toastr.errorToastr(
                'Brand name is already in use for selected region'
              );
            }
            // else if (this.invalidBrandCode == false && this.invalidBrandName == false) {
            //   this.errorMessage = "";
            //   this.brandNameerrorMessage = "";
            // }
          }
          // else {
          //   if (this.invalidBrandCode == false && this.invalidBrandName == false) {
          //     this.errorMessage = "";
          //     this.brandNameerrorMessage = "";
          //   }
          // }
        });
    
  }
  /* add and update method for brand */
  async saveLanguage() {
    
    this.submitted = true;
    this.invalidLanguageCode = false;
    this.invalidLanguageName = false;

    if (this.languageForm.value.languageCode != '') {
      await this.checkDuplication('languageCode');
    }
    if (this.languageForm.value.languageName != '') {
      await this.checkDuplication('languageName');
    }

    if (
      this.languageForm.valid &&
      !this.invalidLanguageCode &&
      !this.invalidLanguageName
    ) {
      const languageDetails = this.languageForm.value;
      languageDetails.userId = 1;
      languageDetails.languageId = this.languageId;
      this.dataservice.saveLanguage(languageDetails).subscribe(response => {
        if (this.languageId != 0) {
          // this.successMessage = "Language updated successfully";
          this.toastr.successToastr('Language updated successfully');
        } else {
          // this.successMessage = "Language added successfully";
          this.toastr.successToastr('Language created successfully');
        }
        this.clearData();
        this.getAllLanguage();
      });
    }
  }

  /* get brand by brand Id */
  getByLanguageId(languageId) {
    this.clearData();
    
    this.languageId = languageId;
    this.dataservice.getByLanguageId(languageId).subscribe(async response => {
      this.languageForm.patchValue(response);
      this.strOldLanguageName = response['languageName'];
      this.strOldLanguageCode = response['languageCode'];
      window.scroll(0, 0);
    });
  }

  /* get all brands */
  getAllLanguage() {
    this.dataservice.getAllLanguage().subscribe((response: {}) => {
      this.languageList = response;
      console.log(response);
      
    });
  }


  /*reset the region form*/
  clearData() {
    this.submitted = false;
    this.languageForm.reset();
    setTimeout(() => {
      this.successMessage = '';
    }, 2000);
    this.languageId = 0;
    this.errorMessage = '';
    this.inValid = false;
    this.languageNameerrorMessage = '';
    this.strOldLanguageName = '';
    this.strOldLanguageCode = '';
    this.regionList = [];
    this.languageForm = this.formBuilder.group({
      languageCode: ['', [Validators.required,noWhitespaceValidator]],
      languageName: ['', [Validators.required,noWhitespaceValidator]],
      languageDesc: ['']
    });
  }

  /* delete the Language by language Id */
  deleteLanguage(languageId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this brand?'
    );
    if (isDelete) {
      this.dataservice.deleteLanguageById(languageId).subscribe(
        response => {
          // this.successMessage = "language deleted successfully";
          this.toastr.successToastr('Language deleted successfully');
          this.getAllLanguage();
          this.clearData();
        },
        error => {
          // this.errorMessage = this.dataservice.errorWhenInUse;
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
          setTimeout(() => {
            this.errorMessage = '';
            this.dataservice.errorWhenInUse = '';
          }, 2000);
        }
      );
    }
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}
