import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { StartExecutionComponent } from './start-execution/start-execution.component';
import { StartExecutionRoutingModule } from './start_execution-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgPipesModule} from 'ngx-pipes';


@NgModule({
  declarations: [StartExecutionComponent],
  imports: [CommonModule, SharedModule,StartExecutionRoutingModule,Ng2SearchPipeModule,NgPipesModule]
})
export class StartExecutionModule {}
