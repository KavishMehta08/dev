import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartExecutionComponent } from './start-execution/start-execution.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
const routes: Routes = [
  {
    path: '',
    component: StartExecutionComponent,
    data: { title: '' }
  }
];

@NgModule({
  declarations: [
],
  imports: [RouterModule.forChild(routes), Ng2SearchPipeModule],
  exports: [RouterModule]
})
export class StartExecutionRoutingModule {}
