import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchTestSuiteComponent } from './search-test-suite/search-test-suite.component';

const routes: Routes = [
  {
    path: '',
    component: SearchTestSuiteComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchTestSuiteRoutingModule {}
