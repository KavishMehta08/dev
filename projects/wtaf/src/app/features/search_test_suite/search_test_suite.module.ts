import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { SearchTestSuiteComponent } from './search-test-suite/search-test-suite.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SearchTestSuiteRoutingModule } from './search_test_suite-routing.module';
@NgModule({
  declarations: [SearchTestSuiteComponent],
  imports: [CommonModule, SharedModule,SearchTestSuiteRoutingModule,Ng2SearchPipeModule]
})
export class SearchTestSuiteModule {}
