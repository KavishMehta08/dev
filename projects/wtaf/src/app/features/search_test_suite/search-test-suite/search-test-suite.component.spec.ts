import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTestSuiteComponent } from './search-test-suite.component';

describe('SearchTestSuiteComponent', () => {
  let component: SearchTestSuiteComponent;
  let fixture: ComponentFixture<SearchTestSuiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTestSuiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTestSuiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
