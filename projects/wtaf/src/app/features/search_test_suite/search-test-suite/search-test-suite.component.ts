import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../shared/services/data.service';

declare var $: any;
import { Role } from '../../../models/role';
import * as moment from 'moment';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Type } from '../../models/type';
import { async } from 'q';
import { LoaderService } from '../../../shared/services/loader.service';
import { HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import {SearchboxComponent} from '../../../layout/commonFeatures/searchbox/searchbox.component';
@Component({
  selector: 'wtaf-search-test-suite',
  templateUrl: './search-test-suite.component.html',
  styleUrls: ['./search-test-suite.component.css']
})
export class SearchTestSuiteComponent implements OnInit {
  successMessage: String = '';
  errorMessage: String = '';
  regionId: Number = 0;
  brandId: Number = 0;
  testSuiteId: Number = 0;
  projectTypeId: Number = 0;
  projectName: String = '';
  regionList: any = [];
  brandList: any = [];
  projectTypeList: any = [];
  projectNameList: any = [];
  testSuiteList: any = [];
  testSuiteIdRes: any = [];
  blob;
  txtSearch = '';
  role: string = "";
  Admin:string = "";
  Test_lead:string = "";
  Test_Mgr:string = "";
  throttle = 100;
  scrollDistance = 2;
  scrollUpDistance = 2;
  direction = "";
  pageNo:number = 0;
  pageSize:number = 25;
  scrollCheck: boolean = false;
  toolTip = 'Search By User or Suite Name';
  searchFilterText:string = '';
  isSuiteSearchFilter:boolean = false;
  isfromTestCase =false;
  @ViewChild(SearchboxComponent, {static: false}) SearchBoxComponent: SearchboxComponent;
  constructor(
    public toastr: ToastrManager,
    private router: Router,
    private dataservice: DataService,
    private activatedRoute: ActivatedRoute,
    public loaderService: LoaderService,
    public datePipe: DatePipe

  ) { }

  ngOnInit() {
    
    this.role=localStorage.getItem('userRole');
    this.Admin = Role.Admin;
    this.Test_lead = Role.Test_Lead;
    this.Test_Mgr = Role.Test_Manager;
    let flag=Boolean(
      this.activatedRoute.snapshot.paramMap.get('isFromCreateSuite')
    );

    if(!flag){
      this.cleareAllLocalStorage(flag);
     
     }
    
     this.isfromTestCase=flag;
    console.log("flag---------------",flag);
    
    this.getAllProjectType();
    // this.getAllRegion();
    this.getAllTestSuite(this.regionId,this.brandId,this.projectTypeId,this.projectName,this.pageNo,this.pageSize);
    $('body').click(function () {
      $('.customdrpcss').removeClass('show');
      $('.customdrpcss').css({ display: 'none' });
    });

    $('#projectTypeId').change(e => {
      this.changeProjectType(e);
    });
    $('#regionId').change(e => {
      this.changeRegion(e);
    });
    $('#brandId').change(e => {
      this.changeBrand(e);
    });
    $('#projectName').change(e => {
      this.changeProjectName(e);
    });
    setTimeout(() => {
      $('.js-example-basic-single').select2();
    }, 1500);
  }

  // Added by Akash - Sorting Table Asc Desc
  orderedArray = [{
    projectName: "ASC",
    brandName: "ASC",
    testSuiteName: "ASC",
    screen: "ASC",
    projectTypeName: "ASC",
    platformName: "ASC",
    applianceName: "ASC",
    modifiedOn: "ASC",
    modifiedBy: "ASC",
  }]
  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
    this.testSuiteList = this.sort_by_key(this.testSuiteList, key, order);
    console.log(this.testSuiteList);
    this.loaderService.hide();
  }
  sort_by_key(array, key, order) {
    if (order === "ASC") {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];

        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
    else {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
  }
  /* change region and get brands*/
  changeRegion(e) {
    // this.projectTypeList = [];
    // this.projectTypeId = 0;
    this.cleareAllLocalStorage(false);
    this.isfromTestCase = false;
    this.isSuiteSearchFilter = false;
    this.SearchBoxComponent.searchText = '';
    this.pageNo = 0;
    this.regionId = parseInt(e.target.value);
    this.projectNameList = [];
    this.projectName = '';
    this.getBrandsByRegionId(parseInt(e.target.value));
    this.getAllTestSuite(parseInt(e.target.value), 0, this.projectTypeId, '',this.pageNo,this.pageSize);
  }

  changeBrand(e) {
    this.cleareAllLocalStorage(false);
    this.isfromTestCase = false;
    this.isSuiteSearchFilter = false;
    this.SearchBoxComponent.searchText = '';
    this.brandId = parseInt(e.target.value);
    this.getAllProjectNameByTypeId(
      this.brandId,
      this.projectTypeId,
      this.regionId
    );
    this.pageNo = 0;
    this.getAllTestSuite(
      this.regionId,
      parseInt(e.target.value),
      this.projectTypeId,
      '',
      this.pageNo,
      this.pageSize
    );
  }

  changeProjectType(e) {
    
    this.cleareAllLocalStorage(false);
    this.isfromTestCase = false;
    this.isSuiteSearchFilter = false;
    this.SearchBoxComponent.searchText = '';
    this.pageNo = 0;
    this.regionList = [];
    this.regionId = 0;
    this.brandList = [];
    this.brandId = 0;
    this.projectNameList = [];
    this.projectName = '';
    this.testSuiteList = [];
    this.projectTypeId = e.target.value;
    if (this.projectTypeId.toString() != '') {
      this.getAllRegion_byProjectTypeId(this.projectTypeId);
      this.getAllTestSuite(
        this.regionId,
        this.brandId,
        this.projectTypeId,
        e.target.value,
        this.pageNo,
        this.pageSize
      );
    }
  }

  getAllRegion_byProjectTypeId(typeId) {
    this.dataservice
      .getAllRegionByProjectType(typeId)
      .subscribe((response: {}) => {
        this.regionList = response;
        //to place NAR region at first position in frop down
        this.regionList.unshift(this.regionList.splice(this.regionList.findIndex(item => item.regionName === 'NAR'), 1)[0])
        console.log(this.regionList);
        if (response != null) {
          if (this.regionList.length == 1) {
            this.regionId = this.regionList[0].regionId;
          } else {
            if (sessionStorage.getItem("SearchTestSuiteRegionId") != null && sessionStorage.getItem("SearchTestSuiteRegionId") != undefined && sessionStorage.getItem("SearchTestSuiteRegionId") != "") {
              this.regionId = parseInt(sessionStorage.getItem("SearchTestSuiteRegionId"));
            }
            else {
              this.regionId = 0;
            }
          }
        }
        if (this.regionId > 0) {
          this.projectNameList = [];
          this.projectName = '';
          this.getBrandsByRegionId(this.regionId);
          if(!this.isfromTestCase)
          {
            this.getAllTestSuite(this.regionId, 0, 0, '',this.pageNo,this.pageSize);
          }
         
        }
      });
  }

  changeProjectName(e) {
    this.cleareAllLocalStorage(false);
    this.isfromTestCase = false;
    this.isSuiteSearchFilter = false;
    this.SearchBoxComponent.searchText = '';
    this.pageNo = 0;
    this.projectName = e.target.value
    this.getAllTestSuite(
      this.regionId,
      this.brandId,
      this.projectTypeId,
      e.target.value,
      this.pageNo,
      this.pageSize
    );
  }

  /* get all regions */
  getAllRegion() {
    this.dataservice.getAllRegion().subscribe((response: {}) => {
      this.regionList = response;
      if (response != null) {
        if (this.regionList.length == 1) {
          this.regionId = this.regionList[0].regionId;
        } else {
          this.regionId = 0;
        }
      }
      if (this.regionId > 0) {
        // this.projectTypeList = [];
        this.projectTypeId = 0;
        this.projectNameList = [];
        this.projectName = '';
        this.getBrandsByRegionId(this.regionId);
        this.getAllTestSuite(this.regionId, 0, 0, '',this.pageNo,this.pageSize);
      }
    });
  }

  /* get all brands by region Id */
  getBrandsByRegionId(regionId) {
    this.dataservice.getBrandsByRegionId(regionId).subscribe((response: {}) => {
      console.log('brands', this.brandList);
      this.brandList = response;
      if (response != null) {
        if (this.brandList.length == 1) {
          this.brandId = this.brandList[0].brandId;
        } else {
          this.brandId = 0;
        }
      } else {
        this.brandId = 0;
      }

      if (sessionStorage.getItem("SearchTestSuiteBrandId") != null && sessionStorage.getItem("SearchTestSuiteBrandId") != undefined && sessionStorage.getItem("SearchTestSuiteBrandId") != "") {
        this.brandId = parseInt(sessionStorage.getItem("SearchTestSuiteBrandId"));
      }
      if (this.brandId > 0) {
        this.getAllProjectNameByTypeId(
          this.brandId,
          this.projectTypeId,
          this.regionId
        );
        if(!this.isfromTestCase)
        {
          this.getAllTestSuite(
            this.regionId,
            this.brandId,
            this.projectTypeId,
            '',
            this.pageNo,
            this.pageSize
          );
        }
      
        //this.getAllProjectTypeByBrandId(this.brandId);
      }
    });
  }

  /* get all project type */
  async getAllProjectType() {

    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(async response => {
        // Success
        this.projectTypeList = response;
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.projectTypeId = this.projectTypeList[0].projectTypeId;
          } else {
            this.projectTypeId = 0;
          }

          if (sessionStorage.getItem("SearchTestSuiteProjectTypeId") != null && sessionStorage.getItem("SearchTestSuiteProjectTypeId") != undefined && sessionStorage.getItem("SearchTestSuiteProjectTypeId") != "") {
            this.projectTypeId = parseInt(sessionStorage.getItem("SearchTestSuiteProjectTypeId"));
            await this.getAllRegion_byProjectTypeId(this.projectTypeId);
          }
        }
      });
  }

  /* get all project name by type Id */
  getAllProjectNameByTypeId(brandId, projectTypeId, regionId) {
    this.projectNameList = [];
    this.dataservice
      .getProjectNameByTypeId(brandId, projectTypeId, regionId)
      .subscribe((response: {}) => {
        this.projectNameList = response;
        console.log('getAllProjectNameByTypeId---response', response);
        if (response != null) {
          if (this.projectNameList.length == 1) {
            this.projectName = this.projectNameList[0].projectName;
          } else {
            this.projectName = '';
          }

          if (sessionStorage.getItem("SearchTestSuiteProjectName") != null && sessionStorage.getItem("SearchTestSuiteProjectName") != undefined && sessionStorage.getItem("SearchTestSuiteProjectName") != "") {
            this.projectName = sessionStorage.getItem("SearchTestSuiteProjectName");
            // if(!this.isfromTestCase)
            // {
              this.getAllTestSuite(this.regionId, this.brandId, this.projectTypeId, this.projectName,this.pageNo,this.pageSize);

            // }
          }

        }
      });
  }

  /* get all roles */
  getAllTestSuite(regionId, brandId, projectTypeId, projectName,pageNo,pageSize) {
    
    let TestSuiteId = 'Modified On'//Type.TestSuiteId;
    let desc = Type.descending;
    this.dataservice
      .getAllTestSuite(TestSuiteId, desc, regionId, brandId, projectTypeId, projectName,pageNo,pageSize)
      .toPromise().then((response:any) => {
        
        $('tblBody').show();
        
        if (response != null)
        {
        // this.testSuiteList = response;
        if(this.pageNo > 0){
          this.testSuiteList = [...this.testSuiteList, ...response];
        }else{
          $('.search-results').scrollTop({ top: 0, behavior: 'smooth' });
          this.testSuiteList = [];
           this.testSuiteList = response;
        }
        this.testSuiteList.forEach(element => {
          let modifiedOn_LocalTime = moment.utc(element.modifiedOn).toDate();
          element.convert_modifiedTime_format = moment(modifiedOn_LocalTime).format('YYYY-MM-DD HH:mm:ss');
        });
        }
        this.dataservice.customFilter['testSuiteName'] = '';
        console.log('getAllTestSuite----response', response);
      });
  }
//getAllSuiteBy search
getAllTestSuiteBySearch(searchvalue,pageNo,pageSize) {
 
  this.dataservice
    .getAllTestSuiteBySearch(searchvalue,pageNo,pageSize)
    .subscribe((response:any) => {
      
      $('tblBody').show();
      
      if (response != null)
      {
      // this.testSuiteList = response;
      if(this.pageNo > 0){
        this.testSuiteList = [...this.testSuiteList, ...response];
      }else{
        $('.search-results').scrollTop({ top: 0, behavior: 'smooth' });
        this.testSuiteList = [];
         this.testSuiteList = response;
      }
      console.log('getAllTestSuite----response---search 1', this.testSuiteList);
      
      this.testSuiteList.forEach(element => {
        let modifiedOn_LocalTime = moment.utc(element.modifiedOn).toDate();
        console.log('date time transform', modifiedOn_LocalTime);
        element.convert_modifiedTime_format = moment(modifiedOn_LocalTime).format('YYYY-MM-DD HH:mm:ss');
        console.log("modifiedOn_LocalTime..", element.convert_modifiedTime_format);
      });
      }
      else
      {
        this.testSuiteList = [];
      }
      this.dataservice.customFilter['testSuiteName'] = '';
      console.log('getAllTestSuite----response----2', response);
    });
}
  /* this method clear all messages and element form also */
  clearData(val) {
 
    // this.txtSearch = " ";
    var r = false;
    if ((this.projectTypeId != 0 || this.regionId != 0 || this.brandId != 0 || this.projectNameList != 0) && val == 0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      this.testSuiteId = 0;
      this.errorMessage = '';
      this.regionId = 0;
      this.brandId = 0;
      this.projectTypeId = 0;
      this.projectName = '';
      this.brandList = [];
      this.projectTypeList = [];
      this.projectNameList = [];
      this.testSuiteList = [];
      this.regionList = [];
      this.cleareAllLocalStorage(false);
      this.dataservice.customFilter['testSuiteName'] = '';
      this.getAllProjectType();
    }
  }

  /* delete test suite by test suite Id */
  async deleteTestSuite(testSuiteId, testSuitName) {

    let status;
    let message;
    var isDelete;
    await this.dataservice.checkTestSuiteUsedInExecution(testSuiteId).subscribe(
      {
        next: (response) => {
          console.log(response);
          status = response['status'];
          message = response['message'];

          if (status == Type.TEST_SUITE_CAN_DELETE) {
            isDelete = confirm(
              "Test suite '" +
              testSuitName +
              "' and all its test cases, steps will be deleted. Click OK to confirm or CANCEL to go back."
            );
          }
          else if (status == Type.TEST_SUITE_BEING_USED_CAN_NOT_DELETE) {
            alert(
              message
            );
          }
          else if (status == Type.TEST_SUITE_BEING_USED_CAN_DELETE) {
            isDelete = confirm(
              message
            );
          }
          if (isDelete) {
            this.dataservice.deleteTestSuite(testSuiteId).subscribe(
              response => {
                this.toastr.successToastr('Test suite deleted successfully');
                this.clearData(1);
                this.getAllProjectType();
              },
              error => {
                this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
                setTimeout(() => {
                  this.errorMessage = '';
                  this.dataservice.errorWhenInUse = '';
                }, 2000);
              }
            );
          }
        },
        error: (errorRes: HttpErrorResponse) => {
          console.log(errorRes);

        }
      }
    )


  }

  /* navigate to add test suite */
  async copyTestSuiteByIdAndFlag(testSuiteId, isCopying) {
    if (this.projectTypeId != null && this.projectTypeId != undefined && this.projectTypeId != 0) {
      sessionStorage.setItem("SearchTestSuiteProjectTypeId", this.projectTypeId.toString())
    }

    if (this.regionId != null && this.regionId != undefined && this.regionId != 0) {
      sessionStorage.setItem("SearchTestSuiteRegionId", this.regionId.toString())
    }

    if (this.brandId != null && this.brandId != undefined && this.brandId != 0) {
      sessionStorage.setItem("SearchTestSuiteBrandId", this.brandId.toString())
    }

    if (this.projectName != null && this.projectName != undefined && this.projectName != "") {
      sessionStorage.setItem("SearchTestSuiteProjectName", this.projectName.toString())
    }
    this.router.navigate([
      'test_suite/create_test_suite',
      { id: testSuiteId, flag: isCopying }
    ]);
  }

  async getTestSuiteById(testSuiteId) {
    
    if (this.projectTypeId != null && this.projectTypeId != undefined && this.projectTypeId != 0) {
      sessionStorage.setItem("SearchTestSuiteProjectTypeId", this.projectTypeId.toString())
    }

    if (this.regionId != null && this.regionId != undefined && this.regionId != 0) {
      sessionStorage.setItem("SearchTestSuiteRegionId", this.regionId.toString())
    }

    if (this.brandId != null && this.brandId != undefined && this.brandId != 0) {
      sessionStorage.setItem("SearchTestSuiteBrandId", this.brandId.toString())
    }

    if (this.projectName != null && this.projectName != undefined && this.projectName != "") {
      sessionStorage.setItem("SearchTestSuiteProjectName", this.projectName.toString())
    }

    this.router.navigate(['test_suite/create_test_suite', { id: testSuiteId }]);
  }

  searchTestCase(regionId, brandId, projectTypeId, projectName, testSuiteId) {
    sessionStorage.setItem("SearchTestCaseRegionId", regionId.toString());
    sessionStorage.setItem("SearchTestCaseBandId", brandId.toString());
    sessionStorage.setItem("SearchTestCaseProjectTypeId", projectTypeId.toString());
    sessionStorage.setItem("SearchTestCaseProjectName", projectName.toString());
    sessionStorage.setItem("SearchTestCaseTestSuiteId", testSuiteId.toString());

    sessionStorage.setItem("SearchTestSuiteProjectTypeId", projectTypeId.toString());
    sessionStorage.setItem("SearchTestSuiteRegionId", regionId.toString());
    sessionStorage.setItem("SearchTestSuiteBrandId", brandId.toString());
    sessionStorage.setItem("SearchTestSuiteProjectName", projectName.toString());

    this.router.navigate([
      'test_case/search_test_case',
      {
        isFromTestSuiteToTestCase: true
      }
    ]);
  }
  /* navigate to add test case */
  addTestCase(testSuiteId) {
    this.router.navigate([
      'test_case/create_test_case',
      { testSuiteId: testSuiteId }
    ]);
  }

  // Export Test suite Data and Element Data Object
  exportTestSuiteData(testsuiteName, testSuiteId) {
    this.dataservice.exportTestSuiteData(testSuiteId).subscribe(res => {
      const blob = new Blob([res], { type: 'application/octet-stream' });
      setTimeout(function () {
        var a = document.createElement('a');
        a.href = URL.createObjectURL(blob);
        a.download = testsuiteName + '.xls';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      }, 2000);
    });

    this.dataservice.exportElementsDataAsObject(testSuiteId).subscribe(res => {
      const blob = new Blob([res], { type: 'application/octet-stream' });
      setTimeout(function () {
        var a = document.createElement('a');
        a.href = URL.createObjectURL(blob);
        a.download = 'Objects_WP' + '.xls';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      }, 1000);
    });
  }

  showMenu(id, e) {
    console.log('e==>', e);
    e.stopPropagation();

    if ($('#drpMenu' + id).hasClass('show')) {
      $('.customdrpcss').removeClass('show');
      $('#drpMenu' + id).removeClass('show');
      $('#drpMenu' + id).css({ display: 'none' });
    } else {
      $('.customdrpcss').removeClass('show');
      $('.customdrpcss').css({ display: 'none' });
      $('#drpMenu' + id).addClass('show');
      $('#drpMenu' + id).css({ display: 'block' });
    }
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }

  CopyRoute() {
    // routerLink="/test_suite/create_test_suite"
  }

  cleareAllLocalStorage(flag) {
    flag = false;
    sessionStorage.setItem("SearchTestSuiteProjectTypeId", "");
    sessionStorage.setItem("SearchTestSuiteRegionId", "");
    sessionStorage.setItem("SearchTestSuiteBrandId", "");
    sessionStorage.setItem("SearchTestSuiteProjectName", "");
  }
  onScrollDown() {
    
   this.pageNo = this.pageNo+1;
   if(this.isSuiteSearchFilter){
    this.getAllTestSuiteBySearch(this.searchFilterText,this.pageNo,this.pageSize);
   }else{
    this.getAllTestSuite(this.regionId,this.brandId,this.projectTypeId,this.projectName,this.pageNo,this.pageSize);
   }
  }

  onUp() {
   this.pageNo== 0 ?this.pageNo = 0: this.pageNo = this.pageNo-1;
   this.getAllTestSuite(this.regionId,this.brandId,this.projectTypeId,this.projectName,this.pageNo,this.pageSize);
  
  }
  onTxtSearch(searchTxt: string) {
    this.isSuiteSearchFilter = true;
    this.searchFilterText = searchTxt;
    this.getAllTestSuiteBySearch(searchTxt,this.pageNo,this.pageSize);
  }
}