import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandwiseCapabilityComponent } from './brandwise-capability.component';

describe('BrandwiseCapabilityComponent', () => {
  let component: BrandwiseCapabilityComponent;
  let fixture: ComponentFixture<BrandwiseCapabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandwiseCapabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandwiseCapabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
