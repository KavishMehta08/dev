import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormGroupDirective
} from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DataService } from '../../../shared/services/data.service';
import { LoaderService } from '../../../shared/services/loader.service';
@Component({
  selector: 'wtaf-brandwise-capability',
  templateUrl: './brandwise-capability.component.html',
  styleUrls: ['./brandwise-capability.component.css']
})
export class BrandwiseCapabilityComponent implements OnInit {
  submitted = false;

  brandId: Number = 0;
  successMessage: String = '';
  errorMessage: String = '';
  brandNameerrorMessage: String = '';
  brandList: any = [];
  filterData: any = [];
  regionList: any = [];
  platformList: any = [];
  projectTypeList: any = [];
  brandwiseCapabilityList: any = [];
  allCapabilityList: any = [];
  inValid: Boolean = false;
  txtSearch = '';
  regionId = 0;
  userId: number;
  isDuplicate: Boolean = false;
  oldBrandId = 0;
  oldplatformId = 0;
  oldcapabilityId =0;
  oldenv = '';
  brandwiseCapabilityForm = this.formBuilder.group({
    env: ['', [Validators.required,]],
    brandId: ['', [Validators.required,]],
    platformId: ['', [Validators.required,]],
    regionId: ['', [Validators.required]],
    projectTypeId: ['', [Validators.required]],
    capabilityId: ['', [Validators.required]],
    capabilityValue: ['', [Validators.required]],
    capabilityName: [''],
    brandCapId: [0, [Validators.required]],
  });
  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    public loaderService: LoaderService) { }

  async ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    await this.getAllProjectType();
    this.getAllBrandWisecapability();
  }
  get f() {
    return this.brandwiseCapabilityForm.controls;
  }
  async getAllProjectType() {
    
    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        // Success
        this.projectTypeList = response;
        
        if (response != null) {
          this.getPlatformByProjectTypeId(
            this.projectTypeList[0].projectTypeId
          );
          if (this.projectTypeList.length == 1) {
          } else {
            this.brandwiseCapabilityForm.controls['projectTypeId'].setValue('');
          }
        }
      });
  }
  changeProjectType(event) {
    
    this.regionList = [];
    if (this.brandwiseCapabilityForm.controls['projectTypeId'].value != '') {
      this.getAllRegion_byProjectTypeId(
        this.brandwiseCapabilityForm.controls['projectTypeId'].value
      );
      this.getPlatformByProjectTypeId(
        this.brandwiseCapabilityForm.controls['projectTypeId'].value
      );
    }
  }
  changeRegion(event) {
    if (this.brandwiseCapabilityForm.controls['regionId'].value != '') {
      this.getBrandsByRegionId(
        this.brandwiseCapabilityForm.controls['regionId'].value
      );
    }
  }
  async getAllRegion_byProjectTypeId(typeId) {
    await this.dataservice
      .getAllRegionByProjectType(typeId)
      .toPromise()
      .then(async (response: {}) => {
        this.regionList = response;
        console.log("get all region ",this.regionList);
        let obj = this.regionList.find(o => o.defaultRegion === true);
        console.log("obj--------",obj);
        // to place NAR region at first position in drop down
        this.regionList.unshift(this.regionList.splice(this.regionList.findIndex(item => item.regionName === 'NAR'), 1)[0])
         
        if (response != null) {
          if (this.regionList.length == 1) {
            this.brandwiseCapabilityForm.controls['regionId'].setValue(
              this.regionList[0].regionId
            );
          } else {
            this.brandwiseCapabilityForm.controls['regionId'].setValue('');
          }
        }
      });
  }
  async getBrandsByRegionId(regionId) {
    await this.dataservice
      .getBrandsByRegionId(regionId)
      .toPromise()
      .then(response => {
        // Success
        console.log('brands', this.brandList);
        this.brandList = response;
        if (response != null) {
          if (this.brandList.length == 1) {
            this.brandId = this.brandList[0].brandId;
            this.brandwiseCapabilityForm.controls['brandId'].setValue(
              this.brandList[0].brandId
            );
          } else {
            this.brandwiseCapabilityForm.controls['brandId'].setValue('');
          }
        } else {
          this.brandwiseCapabilityForm.controls['brandId'].setValue('');
        }
      });
  }

  async getPlatformByProjectTypeId(projectTypeId) {
    await this.dataservice
      .getPlatformByProjectTypeId(projectTypeId)
      .toPromise()
      .then(response => {
        // Success
        this.platformList = response;
        console.log("platformList...",this.platformList);
        
        if (response != null) {
          if (this.platformList.length == 1) {
            this.brandwiseCapabilityForm.controls['platformId'].setValue(
              this.platformList[0].platformId
            );
          } else {
            this.brandwiseCapabilityForm.controls['platformId'].setValue('');
          }
        } else {
          this.brandwiseCapabilityForm.controls['platformId'].setValue('');
        }

      });
  }
  changePlatform(e) {
    let PlatformId = parseInt(e.target.value);
    if (PlatformId!= 0) {
      this.getAllCapability(PlatformId);
    }
  }
  async getAllCapability(platformId) {
    this.allCapabilityList = [];
    await this.dataservice
      .getCapabilitiesByProjectTypeId(platformId)
      .toPromise()
      .then(async (response: any) => {
        this.allCapabilityList = response;
        console.log('allcapabilities', this.allCapabilityList);
      })
      .catch(err => {
        this.loaderService.hide();
        console.log(err);
      });
  }
  changecapability(e){
    this.brandwiseCapabilityForm.controls['capabilityName'].setValue(e.target.options[e.target.options.selectedIndex].text);
  }
  async checkForDuplicateInbrandWiseCapability(brandwiseCapabilityDetails){
    await this.dataservice.checkForDuplicateInbrandWiseCapability(brandwiseCapabilityDetails).toPromise()
    .then((response: Boolean) => {
      if (response) {
        this.toastr.errorToastr('Brandwise capabilty already exist');
        this.isDuplicate = true;
      } else {
        this.isDuplicate = false;
      }
    });
    // await this.dataservice.checkForDuplicateInbrandWiseCapability(brandwiseCapabilityDetails).subscribe(response => {
    //   if (response) {
    //     this.toastr.errorToastr('Brandwise capabilty already exist');
    //     this.isDuplicate = true;
    //   } else {
    //     this.isDuplicate = false;
    //   }
    // });
    return this.isDuplicate;
  }
  async saveBrandwisecapability() {
    this.submitted = true;
    const brandwiseCapabilityDetails = this.brandwiseCapabilityForm.value;
    if(this.oldBrandId === brandwiseCapabilityDetails.brandId && 
      this.oldplatformId === brandwiseCapabilityDetails.platformId &&
      this.oldcapabilityId === brandwiseCapabilityDetails.capabilityId &&
      this.oldenv === brandwiseCapabilityDetails.env
      ){
        this.isDuplicate = false;
      }else{
        await this.checkForDuplicateInbrandWiseCapability(brandwiseCapabilityDetails);
      }
    if (!this.isDuplicate && this.brandwiseCapabilityForm.valid) {
      brandwiseCapabilityDetails.userId = this.userId;
      this.dataservice.saveBrandWiseCapability(brandwiseCapabilityDetails).subscribe(response => {
        if (brandwiseCapabilityDetails.brandCapId != 0) {
          this.toastr.successToastr('Brandwise capabilty updated successfully');
        } else {
          this.toastr.successToastr('Brandwise capabilty successfully saved');
        }
        this.clearData(1)
        this.getAllBrandWisecapability();
      });
      
    }
  }
  getAllBrandWisecapability(){
    this.dataservice.getAllBrandWiseCapability().subscribe((response: {}) => {
      this.brandwiseCapabilityList = response;
    });
  }
  deleteBrandwiseCapability(brandcapId){
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this brandwise capability?'
    );
    if (isDelete) {
      this.dataservice.deleteBrandwisecapabilityById(brandcapId).subscribe(
        response => {
          this.toastr.successToastr('Brandwise capabilty deleted successfully');
          this.getAllBrandWisecapability();
         this.clearData(1);
        },
        error => {
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
          setTimeout(() => {
            this.errorMessage = '';
            this.dataservice.errorWhenInUse = '';
          }, 2000);
        }
      );
    }
  }
  getBrandwisecapabilityById(brandCapId) {
    
    this.dataservice.getByBrandCapId(brandCapId).subscribe(async response => {
      this.oldBrandId = response['brandId'];
      this.oldcapabilityId = response['capabilityId'];
      this.oldplatformId = response['platformId'];
      this.oldenv = response['env'];
      await this.getAllRegion_byProjectTypeId(response['projectTypeId']);
      await this.getPlatformByProjectTypeId(response['projectTypeId']);
      await this.getBrandsByRegionId(response['regionId']);
      await this.getAllCapability(response['platformId']);
      this.brandwiseCapabilityForm.patchValue(response);

    });
  }
  clearData(val) {
    
    // this.txtSearch =" ";
    var r = false;
    if (val==0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
    this.submitted = false;
    this.brandwiseCapabilityForm.reset();
    this.brandwiseCapabilityForm = this.formBuilder.group({
      env: ['', [Validators.required,]],
      brandId: ['', [Validators.required,]],
      platformId: ['', [Validators.required,]],
      regionId: ['', [Validators.required]],
      projectTypeId: ['', [Validators.required]],
      capabilityId: ['', [Validators.required]],
      capabilityValue: ['', [Validators.required]],
      capabilityName: [''],
      brandCapId: [0, [Validators.required]],
    });
    this.oldenv = '';
    this.oldplatformId = 0;
    this.oldcapabilityId = 0;
    this.oldBrandId = 0;
    
    setTimeout(() => {
      this.successMessage = '';
    }, 2000);
    this.errorMessage = '';
    this.inValid = false;
    this.regionList = [];

  }
  }
  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}