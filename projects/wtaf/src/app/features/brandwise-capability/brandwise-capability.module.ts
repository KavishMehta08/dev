import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { BrandwiseCapabilityRoutingModule } from './brandwise-capability-routing.module';
import { BrandwiseCapabilityComponent } from './brandwise-capability/brandwise-capability.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
@NgModule({
  declarations: [BrandwiseCapabilityComponent],
  imports: [
    CommonModule,
    SharedModule,
    Ng2SearchPipeModule,
    BrandwiseCapabilityRoutingModule
  ]
})
export class BrandwiseCapabilityModule { }
