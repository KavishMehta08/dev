import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BrandwiseCapabilityComponent} from './brandwise-capability/brandwise-capability.component'
const routes: Routes = [
  {
    path: '',
    component: BrandwiseCapabilityComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandwiseCapabilityRoutingModule { }
