import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DataService } from '../../../shared/services/data.service';
import { Type } from '../../models';
import * as XLSX from 'xlsx';
declare var $: any;
@Component({
  selector: 'wtaf-create-test-case',
  templateUrl: './create-test-case.component.html',
  styleUrls: ['./create-test-case.component.css']
})
export class CreateTestCaseComponent implements OnInit {
  submitted = false;
  successMessage: String = '';
  errorMessage: String = '';
  brandList: any = [];
  regionList: any = [];
  testSuiteList: any = [];
  allProjectName: any = [];
  projectTypeList: any = [];
  regionId: Number = 0;
  brandId: Number = 0;
  testCaseId: Number = 0;
  testSuiteId: Number = 0;
  projectTypeId: Number = 0;
  isFromTestSuite: Boolean = false;
  IsCaseInvalid: Boolean = false;
  isCopying: Boolean = false;
  isTextChanged = false;
  sharedTestcaseFlag: boolean = true;
  strOldName = '';
  projectName = '';
  isClicked = false;
  isEmpty: Boolean = false;
  emptyKey: string;
  emptyColumnName: string;
  oldbrandId: Number = 0;
  oldtestSuiteName: any = [];
  oldjiraProjectName: any = [];
  oldtestSuiteDesc: any = [];
  oldCaseName: any = [];
  oldtestCaseDesc: any = [];

  oldBrandName = '';
  oldRegionName = '';
  oldAppliance = '';
  oldPlatformName = '';
  oldProjectType = '';
  oldProjectName = '';
  oldSuiteName = '';
  userId: number;
  // shared test case variables
  files: File;
  data: any = [];
  listIteraions: any = [];
  CaseJson = [];
  selectedFile: File;
  widthfooter = '0%';
  steps = '';
  isImportFlag: boolean = false;
  isSharedParaFlag: boolean;
  platformName: string;
  testCaseDesc:string;

  testCaseForm = this.formBuilder.group({
    brandId: ['', [Validators.required]],
    regionId: ['', [Validators.required]],
    testSuiteId: ['', [Validators.required]],
    testCaseName: ['', [Validators.required, noWhitespaceValidator]],
    projectName: ['', [Validators.required, noWhitespaceValidator]],
    projectTypeId: ['', [Validators.required]],
    testCaseDesc: [''],
    isShared: [false, [Validators.required]]
  });

  public onFileRemove(args): void {
    args.cancel = true;
  }
  @ViewChild('importFile', { static: false }) importFile: ElementRef;
  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  async ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log('local usr id', this.userId);
    this.testCaseId =
      this.activatedRoute.snapshot.paramMap.get('id') == null
        ? 0
        : parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    this.testSuiteId =
      this.activatedRoute.snapshot.paramMap.get('testSuiteId') == null
        ? 0
        : parseInt(this.activatedRoute.snapshot.paramMap.get('testSuiteId'));
    this.isCopying =
      this.activatedRoute.snapshot.paramMap.get('flag') == null
        ? false
        : Boolean(this.activatedRoute.snapshot.paramMap.get('flag'));
    this.isSharedParaFlag =
      this.activatedRoute.snapshot.paramMap.get('isShared') == null
        ? false
        : Boolean(
          this.activatedRoute.snapshot.paramMap.get('isShared') == 'true'
        );

    this.isClicked = false;
    this.getAllProjectType();

    if (this.testSuiteId > 0) {
      this.isFromTestSuite = true;
      this.dataservice
        .getTestSuiteById(this.testSuiteId)
        .subscribe(async (response: {}) => {
          await this.getAllRegion_byProjectTypeId(response['projectTypeId']);
          await this.getBrandsByRegionId(response['regionId']);
          await this.getAllTestSuite(
            response['regionId'],
            response['brandId'],
            response['projectName']
          );
          this.testCaseForm.controls['regionId'].setValue(response['regionId']);

          this.regionId = response['regionId'];
          this.brandId = response['brandId'];
          this.projectTypeId = response['projectTypeId'];
          this.projectName = response['projectName'];

          this.testCaseForm.get('regionId').disable();
          this.testCaseForm.get('brandId').disable();
          this.testCaseForm.get('testSuiteId').disable();
          this.testCaseForm.get('projectTypeId').disable();
          this.testCaseForm.get('projectName').disable();

          this.testCaseForm.controls['projectTypeId'].setValue(
            response['projectTypeId']
          );
          this.testCaseForm.controls['regionId'].setValue(response['regionId']);
          this.testCaseForm.controls['brandId'].setValue(response['brandId']);
          this.testCaseForm.controls['projectName'].setValue(
            response['projectName']
          );
          this.testCaseForm.controls['testSuiteId'].setValue(
            response['testSuiteId']
          );
        });
    } else {
      this.isFromTestSuite = false;
      this.testCaseForm.get('regionId').enable();
      this.testCaseForm.get('brandId').enable();
      this.testCaseForm.get('testSuiteId').enable();
      this.testCaseForm.get('projectTypeId').enable();
      this.testCaseForm.get('projectName').enable();
    }
    if (this.testCaseId > 0 && !this.isSharedParaFlag) {
      this.isFromTestSuite = false;
      // this.getTestCaseById(this.testCaseId);
      this.getSharedTestCaseById(this.testCaseId);
    } else if (this.testCaseId > 0 && this.isSharedParaFlag) {
      this.sharedTestcaseFlag = false;

      let formControl: FormControl = this.testCaseForm.get(
        'testSuiteId'
      ) as FormControl;
      formControl.clearValidators();
      formControl.setValidators(null);
      formControl.updateValueAndValidity();
      this.testCaseForm.controls['isShared'].setValue(true);
      this.getSharedTestCaseById(this.testCaseId);
      this.testCaseForm.get('testSuiteId').setValue('');
    }
  }

  get f() {
    return this.testCaseForm.controls;
  }

  /* get all regions  */
  getAllRegion() {
    this.dataservice.getAllRegion().subscribe((response: {}) => {
      this.regionList = response;
      if (response != null) {
        if (this.regionList.length == 1) {
          this.testCaseForm.controls['regionId'].setValue(
            this.regionList[0].regionId
          );
          this.getBrandsByRegionId(this.regionList[0].regionId);
        } else {
          this.testCaseForm.controls['regionId'].setValue('');
        }
      }
    });
  }

  async getAllProjectType() {
    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        // Success
        this.projectTypeList = response;
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.projectTypeId = this.projectTypeList[0].projectTypeId;
            this.testCaseForm.controls['projectTypeId'].setValue(
              this.projectTypeList[0].projectTypeId
            );
            this.getAllRegion_byProjectTypeId(this.projectTypeId);
          } else {
            this.testCaseForm.controls['projectTypeId'].setValue('');
          }
        }
      });
  }
  async getAllRegion_byProjectTypeId(typeId) {
    await this.dataservice
      .getAllRegionByProjectType(typeId)
      .toPromise()
      .then(async (response: {}) => {
        this.regionList = response;
        // to place NAR region at first position in frop down
        this.regionList.unshift(
          this.regionList.splice(
            this.regionList.findIndex(item => item.regionName === 'NAR'),
            1
          )[0]
        );
        console.log(this.regionList);
        if (response != null) {
          if (this.regionList.length == 1) {
            this.regionId = this.regionList[0].regionId;
            this.testCaseForm.controls['regionId'].setValue(this.regionId);
            this.getBrandsByRegionId(this.regionId);
          } else {
            this.testCaseForm.controls['regionId'].setValue('');
          }
        }
      });
  }

  /* change region and get brands*/
  changeRegion(e) {
    this.brandList = [];
    this.brandId = 0;
    this.testSuiteList = [];
    this.testSuiteId = 0;
    this.testCaseForm.controls['testSuiteId'].setValue('');
    this.allProjectName = [];
    this.projectName = '';
    this.testCaseForm.controls['projectName'].setValue('');
    this.regionId = parseInt(e.target.value);
    this.getBrandsByRegionId(this.regionId);
  }

  /* get all brands by region Id */
  async getBrandsByRegionId(regionId) {
    await this.dataservice
      .getBrandsByRegionId(regionId)
      .toPromise()
      .then(response => {
        console.log('brands', this.brandList);
        this.brandList = response;
        if (response != null) {
          if (this.brandList.length == 1) {
            this.brandId = this.brandList[0].brandId;
            this.testCaseForm.controls['brandId'].setValue(
              this.brandList[0].brandId
            );
          } else {
            this.testCaseForm.controls['brandId'].setValue('');
          }
        } else {
          this.brandId = 0;
          this.testCaseForm.controls['brandId'].setValue('');
        }
        if (this.brandId > 0) {
          this.testCaseForm.controls['brandId'].setValue(this.brandId);
        }
      });
  }

  changeBrand(e) {
    this.testSuiteList = [];
    this.testSuiteId = 0;
    this.projectName = '';
    this.brandId = parseInt(e.target.value);
    this.getAllProjectName(this.projectTypeId);
  }

  /* add and update method for test case */
  async saveTestCase() {
    this.submitted = true;

    if (this.isTextChanged || this.isCopying == true) {
      await this.checkDuplication();
    }
    if (this.sharedTestcaseFlag) {

      if (this.testCaseForm.valid && !this.IsCaseInvalid) {
        this.isClicked = true;
        const testCaseDetails = this.testCaseForm.getRawValue();
        if (this.testCaseForm.get('regionId').disabled) {
          testCaseDetails.regionId = this.regionId;
        }
        if (this.testCaseForm.get('brandId').disabled) {
          testCaseDetails.brandId = this.brandId;
        }
        if (this.testCaseForm.get('testSuiteId').disabled) {
          testCaseDetails.testSuiteId = this.testSuiteId;
        }
        testCaseDetails.testCaseId = this.testCaseId;
        testCaseDetails.userId = this.userId;
        let caseName = testCaseDetails['testCaseName'];
        if (this.files == null) {
          if (this.isCopying == true) {
            let savedTestSuiteId = testCaseDetails.testSuiteId;
            this.dataservice
              .cloneTestCase(testCaseDetails)
              .subscribe(async response => {
                await this.dataservice
                  .syncWithJira(savedTestSuiteId)
                  .subscribe(res => {
                    console.log('jira synch response', res);
                  });

                this.toastr.successToastr(
                  'Test case ' + caseName + '  successfully created'
                );
                this.clearData(1);
                this.router.navigate([
                  'test_case/search_test_case',
                  { isFromCreateCase: true }
                ]);
              });
          } else {

            this.dataservice
              .saveSharedTestCase(testCaseDetails)
              .subscribe(response => {
                if (
                  this.testCaseId != 0 &&
                  this.testCaseId != null &&
                  this.testCaseId != undefined
                ) {
                  this.toastr.successToastr(
                    'Test case ' + caseName + ' successfully updated.'
                  );
                } else {
                  this.toastr.successToastr(
                    'Test case ' + caseName + ' successfully created.'
                  );
                }
                this.clearData(1);
                this.router.navigate([
                  'test_case/search_test_case',
                  { isFromCreateCase: true }
                ]);
              });
          }
        } else {
          
          this.dataservice.saveTestCase(testCaseDetails).subscribe(response => {
            if (
              this.testCaseId != 0 &&
              this.testCaseId != null &&
              this.testCaseId != undefined
            ) {
              this.toastr.successToastr(
                'Test case ' + caseName + ' successfully updated.'
              );
            } else {
              this.toastr.successToastr(
                'Test case ' + caseName + ' successfully created.'
              );
            }
            this.clearData(1);
            this.router.navigate([
              'test_case/search_test_case',
              { isFromCreateCase: true }
            ]);
          });
        }
      }
    } else {
      let isShareTestCase = this.testCaseForm.value.isShared;
      if (isShareTestCase) {
        this.testCaseForm.removeControl('projectName');
      }
      if (this.testCaseForm.valid && !this.IsCaseInvalid) {
        const testCaseDetails = this.testCaseForm.getRawValue();
        if (this.testCaseForm.get('regionId').disabled) {
          testCaseDetails.regionId = this.regionId;
        }
        if (this.testCaseForm.get('brandId').disabled) {
          testCaseDetails.brandId = this.brandId;
        }
        if (this.testCaseForm.get('testSuiteId').disabled) {
          testCaseDetails.testSuiteId = this.testSuiteId;
        }
        this.testCaseDesc = this.testCaseForm.get('testCaseDesc').value;
        testCaseDetails.testCaseId = this.testCaseId;
        testCaseDetails.userId = this.userId;
        let caseName = testCaseDetails['testCaseName'];
        testCaseDetails.isShared = 1;
        testCaseDetails.testSuiteId = 0;
        if (this.files == null) {
          console.log(this.isCopying == true);
          if (this.isCopying == true) {
            let savedTestSuiteId = testCaseDetails.testSuiteId;
            this.dataservice
              .clonesharedTestCase(testCaseDetails)
              .subscribe(async response => {


                this.toastr.successToastr(
                  'Test case ' + caseName + '  successfully created'
                );
                this.clearData(1);
                this.router.navigate([
                  'test_case/search_test_case',
                  { isFromCreateCase: true }
                ]);
              });
          } else {
            this.dataservice
              .saveSharedTestCase(testCaseDetails)
              .subscribe(response => {
                if (
                  this.testCaseId != 0 &&
                  this.testCaseId != null &&
                  this.testCaseId != undefined
                ) {
                  this.toastr.successToastr(
                    'Shared test case ' + caseName + ' successfully updated.'
                  );
                } else {
                  this.toastr.successToastr(
                    'Shared test case ' + caseName + ' successfully created.'
                  );
                }
                this.clearData(1);
                this.router.navigate([
                  'test_case/search_test_case',
                  { isFromCreateCase: true }
                ]);
              });
          }
        } else {
          this.saveAfterImport();
        }
      }
    }
  }

  /* get test case by test case Id */
  async getTestCaseById(testCaseId) {
    this.testCaseId = testCaseId;
    await this.dataservice
      .getTestCaseById(testCaseId)
      .subscribe(async response => {
        await this.getAllRegion_byProjectTypeId(response['projectTypeId']);

        this.testCaseForm.controls['projectTypeId'].setValue(
          response['projectTypeId']
        );
        this.testCaseForm.controls['regionId'].setValue(response['regionId']);
        this.testCaseForm.controls['testCaseName'].setValue(
          response['testCaseName']
        );
        this.testCaseForm.controls['testCaseDesc'].setValue(
          response['testCaseDesc']
        );
        this.testCaseForm.controls['projectName'].setValue(
          response['projectName']
        );
        this.testCaseForm.controls['testSuiteId'].setValue(
          response['testSuiteId']
        );

        this.regionId = response['regionId'];
        this.brandId = response['brandId'];
        this.projectTypeId =
          response['projectTypeId'] == '0' ? 1 : response['projectTypeId'];
        this.testSuiteId = response['testSuiteId'];
        this.strOldName = response['testCaseName'];
        this.projectName = response['projectName'];
        await this.getAllTestSuite(
          parseInt(response['regionId']),
          this.brandId,
          response['projectName']
        );
        await this.getBrandsByRegionId(response['regionId']);

        // Added by Akash...
        this.oldbrandId = response['brandId'];
        this.oldCaseName = response['testCaseName'];
        this.oldjiraProjectName = response['jiraProjectName'];
        this.oldtestCaseDesc = response['testCaseDesc'];
        this.oldProjectName = response['projectName'];

        let arrBl = this.brandList.filter(pl => pl.brandId == this.oldbrandId);
        if (arrBl.length > 0) {
          this.oldBrandName = arrBl[0].brandName.toString().trim();
        }

        let arrRig = this.regionList.filter(
          pl => pl.regionId == response['regionId']
        );
        if (arrRig.length > 0) {
          this.oldRegionName = arrRig[0].regionName.toString().trim();
        }
        if (this.projectTypeId > 0) {
          this.testCaseForm.controls['projectTypeId'].setValue(
            this.projectTypeId
          );

          let arrPtl = this.projectTypeList.filter(
            pl => pl.projectTypeId == this.projectTypeId
          );
          if (arrPtl.length > 0) {
            this.oldProjectType = arrPtl[0].projectTypeName.toString().trim();
          }

          this.getAllProjectName(this.projectTypeId);
        }
      });
  }
  /* get shared test case by test case Id */
  async getSharedTestCaseById(testCaseId) {
    this.testCaseId = testCaseId;
    await this.dataservice
      .getSharedTestCaseById(testCaseId)
      .subscribe(async response => {
        await this.getAllRegion_byProjectTypeId(response['projectTypeId']);

        this.testCaseForm.controls['projectTypeId'].setValue(
          response['projectTypeId']
        );
        this.testCaseForm.controls['regionId'].setValue(response['regionId']);
        this.testCaseForm.controls['testCaseName'].setValue(
          response['testCaseName']
        );
        this.testCaseForm.controls['testCaseDesc'].setValue(
          response['testCaseDesc']
        );
        this.testCaseForm.controls['projectName'].setValue(
          response['projectName']
        );
        this.testCaseForm.controls['testSuiteId'].setValue(
          response['testSuiteId']
        );

        this.regionId = response['regionId'];
        this.brandId = response['brandId'];
        this.projectTypeId =
          response['projectTypeId'] == '0' ? 1 : response['projectTypeId'];
        this.testSuiteId = response['testSuiteId'];
        this.strOldName = response['testCaseName'];
        this.projectName = response['projectName'];
        await this.getAllTestSuite(
          parseInt(response['regionId']),
          this.brandId,
          response['projectName']
        );
        await this.getBrandsByRegionId(response['regionId']);

        // Added by Akash...
        this.oldbrandId = response['brandId'];
        this.oldCaseName = response['testCaseName'];
        this.oldjiraProjectName = response['jiraProjectName'];
        this.oldtestCaseDesc = response['testCaseDesc'];
        this.oldProjectName = response['projectName'];

        let arrBl = this.brandList.filter(pl => pl.brandId == this.oldbrandId);
        if (arrBl.length > 0) {
          this.oldBrandName = arrBl[0].brandName.toString().trim();
        }

        let arrRig = this.regionList.filter(
          pl => pl.regionId == response['regionId']
        );
        if (arrRig.length > 0) {
          this.oldRegionName = arrRig[0].regionName.toString().trim();
        }
        if (this.projectTypeId > 0) {
          this.testCaseForm.controls['projectTypeId'].setValue(
            this.projectTypeId
          );

          let arrPtl = this.projectTypeList.filter(
            pl => pl.projectTypeId == this.projectTypeId
          );
          if (arrPtl.length > 0) {
            this.oldProjectType = arrPtl[0].projectTypeName.toString().trim();
          }

          this.getAllProjectName(this.projectTypeId);
        }
      });
  }
  /* get all test suite  */
  async getAllTestSuite(regionId, brandId, projectName) {
    let TestSuiteName = Type.TestSuiteName;
    let asc = Type.ascending;
    await this.dataservice
      .getAllTestSuite(
        TestSuiteName,
        asc,
        regionId,
        brandId,
        this.projectTypeId,
        projectName,
        0,
        0
      )
      .toPromise()
      .then(async response => {
        // Success
        this.testSuiteList = response;
        if (response != null) {
          if (this.testSuiteList.length == 1) {
            this.testSuiteId = this.testSuiteList[0].testSuiteId;
            this.testCaseForm.controls['testSuiteId'].setValue(
              this.testSuiteList[0].testSuiteId
            );
          } else {
            this.testCaseForm.controls['testSuiteId'].setValue('');
          }
        } else {
          this.testSuiteId = 0;
          this.testCaseForm.controls['testSuiteId'].setValue('');
        }
        if (this.testSuiteId > 0) {
          let arrSl = this.testSuiteList.filter(
            pl => pl.testSuiteId == this.testSuiteId
          );
          if (arrSl.length > 0) {
            this.oldSuiteName = arrSl[0].testSuiteName.toString().trim();
          }
          this.testCaseForm.controls['testSuiteId'].setValue(this.testSuiteId);
        }
      });
  }

  /* this method clear all messages and test case form also */
  clearData(val) {
    var r = false;
    if (
      (this.testCaseForm.value.brandId != 0 ||
        this.testCaseForm.value.testSuiteId != 0 ||
        this.testCaseForm.value.projectName != 0 ||
        this.testCaseForm.value.regionId != 0 ||
        this.testCaseForm.value.testCaseName != 0 ||
        this.testCaseForm.value.projectTypeId != 0 ||
        this.testCaseForm.value.testCaseDesc != 0) &&
      val == 0
    ) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.submitted = false;
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      if (this.isFromTestSuite == false) {
        this.testCaseForm.reset();
        this.testCaseForm = this.formBuilder.group({
          brandId: ['', [Validators.required]],
          regionId: ['', [Validators.required]],
          testSuiteId: ['', [Validators.required]],
          testCaseName: ['', [Validators.required, noWhitespaceValidator]],
          projectName: ['', [Validators.required, noWhitespaceValidator]],
          projectTypeId: ['', [Validators.required]],
          testCaseDesc: [''],
          isShared: [false, [Validators.required]]
        });
        this.testSuiteId = 0;
        this.errorMessage = '';
        this.testCaseId = 0;
        this.regionId = 0;
        this.brandId = 0;
        this.testCaseForm.controls['regionId'].setValue('');
        this.testCaseForm.controls['brandId'].setValue('');
        this.testCaseForm.controls['testSuiteId'].setValue('');
        this.testCaseForm.controls['projectName'].setValue('');
        this.testCaseForm.controls['projectTypeId'].setValue('');
        this.testCaseForm.controls['isShared'].setValue(false);
        this.testCaseForm.get('regionId').enable();
        this.testCaseForm.get('brandId').enable();
        this.testCaseForm.get('testSuiteId').enable();
        this.brandList = [];
        this.testSuiteList = [];
        this.isImportFlag ? (this.importFile.nativeElement.value = '') : '';
        $('#importTable').hide();
        this.isImportFlag = false;
        this.sharedTestcaseFlag = true;

        this.data = [];
        this.selectedFile = null;
      } else {
        this.testCaseForm.controls['testCaseName'].setValue('');
        this.testCaseForm.controls['testCaseDesc'].setValue('');
      }
      this.strOldName = '';
    }
  }

  async checkDuplication() {
    if (
      this.testCaseForm.value.testCaseName != '' &&
      this.testCaseForm.value.testCaseName != null &&
      this.testCaseForm.value.testCaseName != undefined
    ) {
      const StepDetails = this.testCaseForm.value;
      StepDetails.resource = this.testCaseForm.value.testCaseName
        .toString()
        .trim();
      StepDetails.type = Type.TestCase;
      StepDetails.id = this.testSuiteId;
      await this.dataservice
        .checkDuplication(StepDetails)
        .toPromise()
        .then(async (response: Boolean) => {
          this.IsCaseInvalid = response;
          if (this.IsCaseInvalid) {
            this.toastr.errorToastr(
              'Test case "' + StepDetails.resource + '" is already exists'
            );
          } else {
            this.errorMessage = '';
          }
        });
    }
  }

  changeSuite(event) {
    this.testSuiteId = event.target.value;
  }

  changeTestCaseName(e) {
    if (
      this.strOldName.toString().trim() != e.target.value.toString().trim() &&
      this.strOldName != ''
    ) {
      this.isTextChanged = true;
    } else if (this.testCaseId == 0) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  getAllProjectName(projectTypeId) {
    this.dataservice
      .getProjectNameByTypeId(this.brandId, projectTypeId, this.regionId)
      .toPromise()
      .then(allProjNameRes => {
        // Success
        this.allProjectName = allProjNameRes;
        this.testCaseForm.controls['projectName'].setValue('');
        if (this.projectName != '') {
          this.testCaseForm.controls['projectName'].setValue(this.projectName);
          this.getAllTestSuite(this.regionId, this.brandId, this.projectName);
        }
        console.log('all product names', this.allProjectName);
      });
  }

  /* get all platforms by project type id */
  async changeProjectType(e) {
    this.brandList = [];
    this.brandId = 0;
    this.projectTypeId = e.target.value;
    this.testSuiteList = [];
    this.testSuiteId = 0;
    this.allProjectName = [];
    this.projectName = '';
    this.testCaseForm.controls['projectName'].setValue('');
    await this.getAllRegion_byProjectTypeId(this.projectTypeId);
  }

  /* get all project type by brand Id */
  getAllProjectTypeByBrandId(brandId) {
    this.dataservice
      .getProjectTypeByBrandId(brandId)
      .subscribe((response: {}) => {
        this.projectTypeList = response;
        console.log('response', response);
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.projectTypeId = this.projectTypeList[0].projectTypeId;
            this.testCaseForm.controls['projectTypeId'].setValue(
              this.projectTypeList[0].projectTypeId
            );
          } else {
            this.testCaseForm.controls['projectTypeId'].setValue('');
          }
        } else {
          this.testCaseForm.controls['projectTypeId'].setValue('');
        }
      });
  }

  changeProjectName(e) {
    this.projectName = e.target.value;
    this.getAllTestSuite(this.regionId, this.brandId, e.target.value);
  }
  // Universal/shared test case functionality

  checksharedtestcase(event) {
    
    if (event.target.checked) {
      this.sharedTestcaseFlag = false;
      this.resetForm(true)
      let formControl: FormControl = this.testCaseForm.get(
        'testSuiteId'
      ) as FormControl;
      formControl.clearValidators();
      formControl.setValidators(null);
      formControl.updateValueAndValidity();
      this.testCaseForm.get('testSuiteId').setValue('');
    } else {
      this.sharedTestcaseFlag = true;
      this.resetForm(false);
      let formControl: FormControl = this.testCaseForm.get(
        'testSuiteId'
      ) as FormControl;
      formControl.clearValidators();
      formControl.setValidators(Validators.required);
      formControl.updateValueAndValidity();
      let formControlcase: FormControl = this.testCaseForm.get(
        'testCaseName'
      ) as FormControl;
      formControlcase.clearValidators();
      formControlcase.setValidators(Validators.required);
      formControlcase.updateValueAndValidity();
      this.isImportFlag = false;
      this.importFile.nativeElement.value = '';
      $('#importTable').hide();
    }
  }
  resetForm(isSharedTesCase) {
    
    this.testCaseForm = this.formBuilder.group({
      brandId: ['', [Validators.required]],
      regionId: ['', [Validators.required]],
      testSuiteId: ['', [Validators.required]],
      testCaseName: ['', [Validators.required, noWhitespaceValidator]],
      projectName: ['', [Validators.required, noWhitespaceValidator]],
      projectTypeId: ['', [Validators.required]],
      testCaseDesc: [''],
      isShared: [isSharedTesCase, [Validators.required]]
    });
  }
  public onSuccess(args: any): void {
    this.data = [];
    this.errorMessage = '';
    this.files = args.target.files;
    if (args.target.files.length > 0) {
      // FileList object
      this.parseExcel(this.files[0]);
    }
  }
  parseExcel(file) {
    this.listIteraions = [];
    this.errorMessage = '';
    this.selectedFile = file;
    this.CaseJson = [];
    console.log('this.selectedFile', this.selectedFile);

    let reader = new FileReader();
    reader.onload = e => {
      let data = (<any>e.target).result;
      let workbook = XLSX.read(data, {
        type: 'binary'
      });
      let isExecuteCount = 0;
      workbook.SheetNames.forEach(
        function (sheetName) {
          // Here is your object

          isExecuteCount = isExecuteCount + 1;
          if (isExecuteCount <= 1) {
            let XL_row_object = XLSX.utils.sheet_to_json(
              workbook.Sheets[sheetName],
              { defval: '' }
            );
            if (XL_row_object.length != 0) {
              let IsInsertData = true;

              const TestData_cols = [];
              for (let k = 0; k < XL_row_object.length; k++) {
                TestData_cols.push({});
                for (const [key, value] of Object.entries(XL_row_object[k])) {
                  TestData_cols[k][key.toUpperCase()] = value;
                }
              }

              XL_row_object = TestData_cols;

              let rownum = 0;
              const header = [];
              if (
                !XL_row_object[0].hasOwnProperty('MOBILEAPP') ||
                !XL_row_object[0].hasOwnProperty('APPLIANCE') ||
                !XL_row_object[0].hasOwnProperty('MODULE') ||
                !XL_row_object[0].hasOwnProperty('PLATFORM') ||
                !XL_row_object[0].hasOwnProperty('TESTCASEID') ||
                !XL_row_object[0].hasOwnProperty('TESTSTEPID') ||
                !XL_row_object[0].hasOwnProperty('KEYWORD') ||
                !XL_row_object[0].hasOwnProperty('ELEMENT') ||
                !XL_row_object[0].hasOwnProperty('TESTCASENAME') ||
                !XL_row_object[0].hasOwnProperty('TESTSTEPDESCRIPTION')
              ) {
                this.importFile.nativeElement.value = '';
                this.toastr.errorToastr('Selected File has invalid columns');
                return;
              }
              let isIteration_exist = 0;
              for (let m = 0; m < XL_row_object.length; m++) {
                if (m < XL_row_object.length - 1) {
                  if (
                    XL_row_object[m]['MOBILEAPP'].toString() == '' ||
                    XL_row_object[m]['APPLIANCE'].toString() == '' ||
                    XL_row_object[m]['MODULE'].toString() == '' ||
                    XL_row_object[m]['PLATFORM'].toString() == '' ||
                    XL_row_object[m]['TESTCASEID'].toString() == '' ||
                    XL_row_object[m]['TESTSTEPID'].toString() == '' ||
                    XL_row_object[m]['KEYWORD'].toString() == '' ||
                    XL_row_object[m]['TESTCASENAME'].toString() == '' ||
                    XL_row_object[m]['TESTSTEPDESCRIPTION'].toString() == ''
                  ) {

                    IsInsertData = false;
                    rownum = rownum + 1;
                    console.log('rownum', rownum);

                    break;
                  }
                }
                if (IsInsertData == true) {
                  rownum = rownum + 1;
                }
              }
              if (IsInsertData == true) {
                console.log('XL_row_object-->', XL_row_object);
                let json_object = JSON.stringify(XL_row_object);
                this.data = XL_row_object;
                this.testSteps = this.data.reduce((a, b) => {
                  a[b.TESTCASENAME] = a[b.TESTCASENAME] || [];
                  return a;
                }, {});
                delete this.testSteps[''];
                for (let k = 0; k < this.data.length; k++) {
                  const newData = Object.keys(this.data[k]).reduce((object, key) => {
                    if (!key.startsWith('__EMPTY')) {
                      object[key] = this.data[k][key]
                    }
                    return object
                  }, {})
                  this.data[k] = newData;
                  // console.log(this.data[k]);

                  this.testcaseName = this.data[k].TESTCASENAME;
                  if (this.data[k].PLATFORM.trim() != '') {
                    this.platformName = this.data[k].PLATFORM;
                  }
                  if (this.testcaseName != '') {
                    let iterations: any = [];
                    this.listIteraions = [];
                    Object.keys(this.data[k]).map(key => {
                      if (
                        key != 'TestDefinition' &&
                        key != 'MOBILEAPP' &&
                        key != 'VERSION' &&
                        key != 'APPLIANCE' &&
                        key != 'MODULE' &&
                        key != 'PLATFORM' &&
                        key != 'TESTCASEID' &&
                        key != 'TESTCASENAME' &&
                        key != 'TESTSTEPID' &&
                        key != 'TESTSTEPDESCRIPTION' &&
                        key != 'ELEMENT' &&
                        key != 'KEYWORD' &&
                        key != '__EMPTY'
                      ) {
                        if (key.substr(0, 9).toLowerCase() == 'Iteration'.toLowerCase()) {
                          isIteration_exist = 1;
                          this.listIteraions.push({ IterationHeading: key });
                          iterations.push({ dataSetName: this.data[k][key] });


                        }
                      } else {
                        let checkEmpty = key.startsWith('__EMPTY');
                        if (checkEmpty) {
                          this.emptyKey = key;
                          this.isEmpty = true;
                        }

                      }
                    });


                    this.testSteps[this.testcaseName].push({
                      element: this.data[k].ELEMENT,
                      keyword: this.data[k].KEYWORD,
                      module: this.data[k].MODULE,
                      iterations: iterations,
                      testStepDesc: this.data[k].TESTSTEPDESCRIPTION
                    });
                  }
                }

                console.log('----------->', this.testSteps);
                if (isIteration_exist == 1 && this.listIteraions.length == 1) {
                  $('#importTable').show();
                  this.isImportFlag = true;
                  let formControl: FormControl = this.testCaseForm.get(
                    'testCaseName'
                  ) as FormControl;
                  formControl.clearValidators();
                  formControl.setValidators(null);
                  formControl.updateValueAndValidity();
                  let keys = Object.keys(this.testSteps);
                  for (let l = 0; l < keys.length; l++) {
                    this.TestCase = keys[l];
                    if (this.TestCase != '') {
                      let testSteps = [];
                      for (
                        let m = 0;
                        m < this.testSteps[this.TestCase].length;
                        m++
                      ) {
                        testSteps.push({
                          element: this.testSteps[this.TestCase][m].element,
                          keyword: this.testSteps[this.TestCase][m].keyword,
                          module: this.testSteps[this.TestCase][m].module,
                          testStepDesc: this.testSteps[this.TestCase][m]
                            .testStepDesc,
                          iterations: this.testSteps[this.TestCase][m]
                            .iterations
                        });
                      }
                      this.CaseJson.push({
                        testCaseName: this.TestCase,
                        testCaseNumber: 0,
                        testSuiteId: 0,
                        shared: true,
                        applianceId: 0,
                        brandId: 0,
                        platformId: 0,
                        platformName: this.platformName,
                        projectName: '',
                        projectTypeId: 0,
                        regionId: 0,
                        testSteps: testSteps
                      });
                    }
                  }
                } else {
                  $('#importTable').hide();
                  this.importFile.nativeElement.value = '';
                  this.data = [];
                  this.selectedFile = null;
                  if (this.listIteraions.length > 1) {
                    return this.toastr.errorToastr(
                      'Selected file requires only one Iteration column'
                    );
                  } else {
                    return this.toastr.errorToastr(
                      'Selected file requires Iteration column'
                    );
                  }

                }
              } else {
                this.importFile.nativeElement.value = '';
                this.toastr.errorToastr(
                  'Please fill mandatory field into excel,that you have selected...'
                );
              }
            }
          }

          // bind the parse excel file data to Grid
        }.bind(this),
        this
      );
    };

    reader.onerror = function (ex) {
      console.log(ex);
    };
    reader.readAsBinaryString(file);
  }

  //save after import method
  async saveAfterImport() {
    console.log('import file data');
    console.log(this.CaseJson);
    
    if (this.CaseJson.length > 0) {
      this.widthfooter = 0 + '%'.toString();
      $('#popProgress').modal('show');
      for (let k = 0; k < this.CaseJson.length; k++) {
        this.steps = 'Completed: ' + (k + 1) + '/ ' + this.CaseJson.length;
        const percentDone = Math.round((100 * (k + 1)) / this.CaseJson.length);
        this.widthfooter = percentDone + '%'.toString();
        console.log(`File is ${percentDone}% uploaded.`);

        let Json_TestCase = this.CaseJson[k];
        Json_TestCase.userId = this.userId;
        Json_TestCase.projectTypeId = this.projectTypeId;
        Json_TestCase.brandId = this.brandId;
        Json_TestCase.projectName = this.projectName;
        Json_TestCase.regionId = this.regionId;
        Json_TestCase.testCaseDesc = this.testCaseDesc;

        await this.dataservice
          .addImportSharedCaseFileData(Json_TestCase)
          .then(case_res => {
            console.log(case_res);
          });
      }

      $('#popProgress').modal('hide');
      this.toastr.successToastr('Shared test cases successfully created');
      if (
        this.testSuiteId != 0 &&
        this.testSuiteId != null &&
        this.testSuiteId != undefined
      ) {
        this.clearData(1);
      } else {
        this.clearData(1);
      }
    }
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? { whitespace: true } : null;
}
