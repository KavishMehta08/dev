import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { CreateTestCaseComponent } from './create-test-case/create-test-case.component';

import { CreateTestCaseRoutingModule } from './create_test_case-routing.module';
@NgModule({
  declarations: [CreateTestCaseComponent],
  imports: [CommonModule, SharedModule,CreateTestCaseRoutingModule]
})
export class CreateTestSuiteModule {}
