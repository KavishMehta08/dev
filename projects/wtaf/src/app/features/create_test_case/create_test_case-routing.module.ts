import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateTestCaseComponent } from './create-test-case/create-test-case.component';

const routes: Routes = [
  {
    path: '',
    component: CreateTestCaseComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateTestCaseRoutingModule {}
