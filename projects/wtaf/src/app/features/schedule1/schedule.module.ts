import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ScheduleComponent } from './schedule/schedule.component';
import { ScheduleRoutingModule } from './schedule-routing.module';

@NgModule({
  declarations: [ScheduleComponent],
  imports: [CommonModule, SharedModule,ScheduleRoutingModule]
})
export class ScheduleModule {}
