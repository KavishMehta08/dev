import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestSuiteComponent } from './test_suite/test-suite.component';

const routes: Routes = [
  {
    path: '',
    component: TestSuiteComponent,
    data: { title: '' },
    children:[
      {
        path: '',
        redirectTo: 'search_test_suite',
        pathMatch: 'full'
      },
      {
        path: 'search_test_suite',
        loadChildren: () =>
        import('../search_test_suite/search_test_suite.module').then(
          m => m.SearchTestSuiteModule
        ),
      }, 
       {
        path: 'create_test_suite',
        loadChildren: () =>
        import('../create_test_suite/create_test_suite.module').then(
          m => m.CreateTestSuiteModule
        ),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestSuiteRoutingModule {}
