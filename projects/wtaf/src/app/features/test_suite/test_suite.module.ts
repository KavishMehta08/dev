import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { TestSuiteComponent } from './test_suite/test-suite.component';

import { TestSuiteRoutingModule } from './test_suite-routing.module';
@NgModule({
  declarations: [TestSuiteComponent],
  imports: [CommonModule, SharedModule,TestSuiteRoutingModule]
})
export class TestSuiteModule {}
