import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { Type } from '../../models';
import { ToastrManager } from 'ng6-toastr-notifications';
import { parse } from 'path';
import { LoaderService } from '../../../shared/services/loader.service';

@Component({
  selector: 'wtaf-capability',
  templateUrl: './capability.component.html',
  styleUrls: ['./capability.component.css']
})
export class CapabilityComponent implements OnInit {
  searchText = '';
  submitted = false;
  successMessage: String = '';
  errorMessage: String = '';
  CapabilityList: any = [];
  capabilityId: Number = 0;
  inValid: Boolean = false;
  projectTypeList: any = [];
  platformList: any = [];
  platformId: Number = 0;
  isTextChanged: Boolean = false;
  isPlatformNotSelected: Boolean = false;
  strOldName: String = '';
  changeCapName: string = '';
  txtSearch = '';
  isCapnameAssign: number = 0;
  oldProjTypeId: number = 0;
  projectTypechange: number = 0;
  userId: number;
  CapabilityForm = this.formBuilder.group({
    capabilityName: ['', [Validators.required,noWhitespaceValidator]],
    capabilityDesc: [''],
    mandatory: [false],
    platformId: ['', [Validators.required]],
    projectTypeId: ['', [Validators.required]]
  });

  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    public loaderService: LoaderService

  ) { }

  async ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log("local usr id", this.userId);
    this.getAllCapability();
    await this.getAllProjectType();
  }

  get f() {
    return this.CapabilityForm.controls;
  }

  //  Added by Akash - Sorting Table Asc Desc
  orderedArray = [{
    projectTypeName: "ASC",
    platformName: "ASC",
    capabilityName: "ASC",
  }]
  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
    this.CapabilityList = this.sort_by_key(this.CapabilityList, key, order);
    console.log(this.CapabilityList);
    this.loaderService.hide();
  }
  sort_by_key(array, key, order) {
    if (order === "ASC") {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
    else {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
  }
  async checkDuplication() {
    

    const CapabilityDetails = this.CapabilityForm.value;
    if (
      this.CapabilityForm.value.capabilityName != 0 &&
      this.CapabilityForm.value.capabilityName != '' &&
      this.CapabilityForm.value.capabilityName != undefined
    ) {
      CapabilityDetails.resource = this.CapabilityForm.value.capabilityName
        .toString()
        .trim();
      CapabilityDetails.type = Type.Capability;
      let platformValue = parseInt(this.CapabilityForm.value.platformId);
      CapabilityDetails.platformId = platformValue;
      CapabilityDetails.id = parseInt(this.CapabilityForm.value.projectTypeId);
      CapabilityDetails.optionalId =
        this.platformList != null || this.platformList.length > 0
          ? platformValue
          : 0;

      await this.dataservice
        .checkDuplicationwithOptionalId(CapabilityDetails)
        .toPromise()
        .then((response: Boolean) => {
          // Success
          this.inValid = response;
          if (this.inValid) {
            this.toastr.errorToastr('Capability name "' + CapabilityDetails.resource + '" is already exists');
          } else {
            this.errorMessage = '';
          }
        });
    }
  }

  async saveCapability() {
    
    this.inValid = false;
    if (
      (this.changeCapName != null &&
        this.changeCapName != undefined &&
        this.changeCapName != '' &&
        this.strOldName
          .toString()
          .trim()
          .toUpperCase() != this.changeCapName &&
        this.strOldName.toString() != '') ||
      (this.projectTypechange != 0 &&
        this.oldProjTypeId != 0 &&
        this.oldProjTypeId.toString() != this.projectTypechange.toString())
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }

    if (this.capabilityId == 0 || this.isTextChanged) {
      await this.checkDuplication();
    }

    if (
      this.CapabilityForm.value.projectTypeId > 0 &&
      this.platformList != null
    ) {
      let platformId = this.CapabilityForm.value.platformId;
      if (platformId == 0) {
        this.isPlatformNotSelected = true;
      } else {
        this.isPlatformNotSelected = false;
      }
    } else {
      this.isPlatformNotSelected = false;
    }

    this.submitted = true;
    if (
      this.CapabilityForm.valid &&
      !this.inValid &&
      !this.isPlatformNotSelected
    ) {
      
      const CapabilityDetails = this.CapabilityForm.value;
      CapabilityDetails.userId = this.userId;
      CapabilityDetails.capabilityId = this.capabilityId;
      this.dataservice.saveCapability(CapabilityDetails).subscribe(response => {
        if (this.capabilityId != 0) {
          this.toastr.successToastr('Capability successfully updated. ');
        } else {
          this.toastr.successToastr('Capability created successfully');
        }
        this.clearData(1);
        this.getAllCapability();
      });
    }
  }

  /* get Capability by Capability Id */
  async getCapabilityById(capabilityId) {
    this.clearData(1);
    this.capabilityId = capabilityId;

    await this.getAllProjectType();
    await this.dataservice
      .getCapabilityById(capabilityId)
      .subscribe(response => {
        
        this.strOldName = response['capabilityName'];
        this.CapabilityForm.controls['capabilityName'].setValue(
          response['capabilityName']
        );
        this.CapabilityForm.controls['capabilityDesc'].setValue(
          response['capabilityDesc']
        );
        this.CapabilityForm.controls['mandatory'].setValue(
          response['mandatory']
        );
        this.CapabilityForm.controls['projectTypeId'].setValue(
          response['projectTypeId']
        );
        this.platformId = response['platformId'];
        console.log('this.platformId-------->', this.platformId);

        this.oldProjTypeId = response['projectTypeId'];
        console.log('this.oldProjTypeId---->', this.oldProjTypeId);

        this.getPlatformByProjectTypeId(response['projectTypeId']);
        window.scroll(0, 0);
      });
  }
  /* get all Capability  */
  getAllCapability() {
    
    this.dataservice.getAllCapability().subscribe((response: {}) => {
      this.CapabilityList = response;
      
      console.log(this.CapabilityList);
      this.dataservice.customFilter['capabilityName'] = '';
    });
  }

  /* this method clear all messages and user Capability form also */
  clearData(val) {
    // this.txtSearch = " ";
    var r = false;
    if ((this.CapabilityForm.value.capabilityName != 0 || this.CapabilityForm.value.capabilityDesc != 0 || this.CapabilityForm.value.platformId != 0
      || this.CapabilityForm.value.projectTypeId != 0 || this.CapabilityForm.value.mandatory != false) && val == 0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.submitted = false;
      this.isPlatformNotSelected = false;
      this.CapabilityForm.reset();
      this.inValid = false;
      this.CapabilityForm = this.formBuilder.group({
        capabilityName: ['', [Validators.required,noWhitespaceValidator]],
        capabilityDesc: [''],
        mandatory: [false],
        platformId: ['', [Validators.required]],
        projectTypeId: ['', [Validators.required]]
      });
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      this.capabilityId = 0;
      this.errorMessage = '';
      this.inValid = false;
      this.strOldName = '';
      this.changeCapName = '';
      this.projectTypechange = 0;
      this.oldProjTypeId = 0;
      this.isTextChanged = false;
    }
  }

  /* delete Capability by Capability Id*/
  deleteCapability(capabilityId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this Capability?'
    );
    if (isDelete) {
      this.dataservice.deleteCapability(capabilityId).subscribe(
        response => {
          this.toastr.successToastr('Capability deleted successfully');
          this.getAllCapability();
          this.clearData(1);
        },
        error => {
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
          setTimeout(() => {
            this.errorMessage = '';
            this.dataservice.errorWhenInUse = '';
          }, 2000);
        }
      );
    }
  }

  /* get all project types  */
  async getAllProjectType() {
    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        // Success
        this.projectTypeList = response;
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.CapabilityForm.controls['projectTypeId'].setValue(
              this.projectTypeList[0].projectTypeId
            );
            this.getPlatformByProjectTypeId(
              this.projectTypeList[0].projectTypeId
            );
          } else {
            this.CapabilityForm.controls['projectTypeId'].setValue('');
          }
        }
      });
  }

  /* get all platforms by project type Id */
  async getPlatformByProjectTypeId(projectTypeId) {
    
    await this.dataservice
      .getPlatformByProjectTypeId(projectTypeId)
      .subscribe((response: {}) => {
        this.platformList = response;

        if (response != null) {
          if (this.platformList.length == 1) {
            this.platformId = this.platformList[0].platformId;
            this.CapabilityForm.controls['platformId'].setValue(
              this.platformList[0].platformId
            );
          } else {
            this.CapabilityForm.controls['platformId'].setValue('');
          }
        } else {
          this.platformId = 0;
          this.CapabilityForm.controls['platformId'].setValue('');
        }
        if (this.platformId > 0) {
          this.CapabilityForm.controls['platformId'].setValue(this.platformId);
        }
      });
  }

  /* get all platforms by project type id */
  changeProjectType(e) {
    
    this.getPlatformByProjectTypeId(parseInt(e.target.value));
    this.projectTypechange = parseInt(e.target.value);

    if (this.projectTypechange != 0) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  changeData(e) {
    
    this.changeCapName = e.target.value
      .toString()
      .trim()
      .toUpperCase();
    if (
      this.strOldName
        .toString()
        .trim()
        .toUpperCase() != this.changeCapName &&
      this.strOldName.toUpperCase() != ''
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  onPlatformchange(e) {
    let platfromId = parseInt(e.target.value);
    console.log('platfromIdplatfromId-----', platfromId);
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}
