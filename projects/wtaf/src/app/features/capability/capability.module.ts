import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { CapabilityComponent } from './capability/capability.component';

import { CapabilityRoutingModule } from './capability-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
@NgModule({
  declarations: [CapabilityComponent],
  imports: [CommonModule, SharedModule, CapabilityRoutingModule,Ng2SearchPipeModule]
})
export class CapabilityModule { }
