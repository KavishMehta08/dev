import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LanguageLabelComponent } from './language_label/language_label.component';

const routes: Routes = [
  {
    path: '',
    component: LanguageLabelComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LanguageLabelRoutingModule {}
