import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { LanguageLabelComponent } from './language_label/language_label.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { LanguageLabelRoutingModule } from './language_label-routing.module';
@NgModule({
  declarations: [LanguageLabelComponent],
  imports: [CommonModule, SharedModule,LanguageLabelRoutingModule,Ng2SearchPipeModule]
})
export class LanguageLabelModule {}
