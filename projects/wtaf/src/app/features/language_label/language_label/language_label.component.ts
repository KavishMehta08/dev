import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import * as XLSX from 'xlsx';
import { async } from '@angular/core/testing';
import { Type } from '../../models';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from '../../../shared/services/loader.service';
declare var $: any;
@Component({
  selector: 'wtaf-languageLabel',
  templateUrl: './language_label.component.html',
  styleUrls: ['./language_label.component.css']
})
export class LanguageLabelComponent implements OnInit {
  submitted = false;
  successMessage: String = '';
  errorMessage: String = '';
  languageLabelList: any = [];
  languageList: any = [];
  brandList: any = [];
  regionList: any = [];
  projectTypeList: any = [];
  platformList: any = [];
  platformId: Number = 0;
  brandId: Number = 0;
  languageId: Number = 0;
  labelId: Number = 0;
  data: any = [];
  selectedFile: File;
  isRadioChecked = 1;
  isLanguageNotSelected: Boolean = false;
  inValid: Boolean = false;
  isTextChanged: Boolean = false;

  form: any;
  txtSearch = '';
  strOldName = '';
  labelCode: String = '';
  ENGLISH: String = '';
  ITALIAN: String = '';
  GERMAN: String = '';
  DUTCH: String = '';
  FRENCH: String = '';
  POLISH: String = '';
  PORTUGUESE: String = '';
  SPANISH: String = '';
  labelName: String = '';
  labelDesc: String = '';
  oldPlatformId: number = 0;
  Platformchange: number = 0;
  brandChange: number = 0;
  changeElementName: string = '';
  oldBrand: number = 0;
  arrElements: any = [];
  distElement: any = [];
  userId: number;
  isSave: boolean = false;
  elementForm = this.formBuilder.group({
    brandId: ['', [Validators.required]],
    regionId: ['', [Validators.required]],
    labelName: ['', [Validators.required,noWhitespaceValidator]],
    languageId: ['', [Validators.required]],
    labelCode: ['', [Validators.required,noWhitespaceValidator]],
    labelDesc: ['', [Validators.required]],
    projectTypeId: ['', [Validators.required]]
  });

  elementFormImport = this.formBuilder.group({
    brandId: ['', [Validators.required]],
    regionId: ['', [Validators.required]],
    projectTypeId: ['', [Validators.required]]
  });

  elementFormExport = this.formBuilder.group({
    brandId: ['', [Validators.required]],
    regionId: ['', [Validators.required]],
    projectTypeId: ['', [Validators.required]]
  });

  XL_row_object;
  @ViewChild('importFile', { static: false }) importFile: ElementRef;
  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    public loaderService: LoaderService
  ) {}

  async ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log('local usr id', this.userId);
    await this.getAllLanguageLabel();
    await this.getAllLanguage();
    //     await this.getAllRegion();
    await this.getAllProjectType();
    $('#importTable').hide();
  }

  get f() {
    return this.elementForm.controls;
  }
  get fExp() {
    return this.elementFormExport.controls;
  }
  get fImp() {
    return this.elementFormImport.controls;
  }

  //  Added by Akash - Sorting Table Asc Desc
  orderedArray = [
    {
      projectTypeName: 'ASC',
      brandName: 'ASC',
      languageName: 'ASC',
      labelName: 'ASC',
      labelCode: 'ASC'
    }
  ];

  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    this.orderedArray[0][key] = order == 'ASC' ? 'DESC' : 'ASC';
    this.languageLabelList = this.sort_by_key(
      this.languageLabelList,
      key,
      order
    );
    console.log(this.languageLabelList);
    this.loaderService.hide();
  }
  sort_by_key(array, key, order) {
    if (order === 'ASC') {
      return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];
        
        if (
          (x != null && x != undefined && x != '') ||
          (y != null && y != undefined && y != '')
        ) {
          return String(x).toLowerCase() < String(y).toLowerCase()
            ? -1
            : String(x).toLowerCase() > String(y).toLowerCase()
            ? 1
            : 0;
        }
      });
    } else {
      return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];
        if (
          (x != null && x != undefined && x != '') ||
          (y != null && y != undefined && y != '')
        ) {
          return String(x).toLowerCase() > String(y).toLowerCase()
            ? -1
            : String(x).toLowerCase() < String(y).toLowerCase()
            ? 1
            : 0;
        }
      });
    }
  }
  /* add and update method for element */
  async saveElement() {
    
    this.inValid = false;
    this.submitted = true;
    this.isSave = false;
    if (
      (this.changeElementName != null &&
        this.changeElementName != undefined &&
        this.changeElementName != '' &&
        this.strOldName
          .toString()
          .trim()
          .toUpperCase() != this.changeElementName &&
        this.strOldName.toString() != '') ||
      (this.brandChange != 0 &&
        this.oldBrand != 0 &&
        this.oldBrand.toString() != this.brandChange.toString())
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }

    let platformId = 0;
    let plById = [];
    let result = [];

    

    if (this.isRadioChecked == 1) {
      this.form = this.elementForm;
    } else if (this.isRadioChecked == 2) {
      
      this.form = this.elementFormImport;

      //(this.selectedFile != null || this.selectedFile == undefined)

      if (this.selectedFile == null) {
        // this.errorMessage = "Please select file to import."
        this.toastr.errorToastr('Please select file to import.');
        return;
      }
    }

    if (this.labelId == 0 && this.isRadioChecked != 2) {
      await this.checkDuplication();
    }

    if (this.form.value.languageId > 0 && this.languageList != null) {
      let languageId = this.form.value.languageId;
      if (languageId == 0 || languageId == null || languageId == undefined) {
        this.isLanguageNotSelected = true;
      } else {
        this.isLanguageNotSelected = false;
      }
    } else {
      this.isLanguageNotSelected = false;
    }
    //(this.selectedFile != null || this.selectedFile == undefined)
    if (
      this.form.valid &&
      !this.isLanguageNotSelected &&
      this.errorMessage == '' &&
      !this.inValid &&
      (this.selectedFile != null || this.selectedFile == undefined)
    ) {
      let arrElement = [];
      if (this.isRadioChecked == 2) {
        $('#importTable').show();
        let doc = this;
        this.data.forEach(function(element) {
          element.brandId = doc.form.value.brandId;
          element.regionId = doc.form.value.regionId;
          element.labelCode = element.labelCode;
          element.english = element.english;
          element.italian = element.italian;
          element.projectTypeId = doc.form.value.projectTypeId;
          element.german = element.german;
          element.dutch = element.dutch;
          element.french = element.french;
          element.polish = element.polish;
          element.portuguese = element.portuguese;
          element.spanish = element.spanish;
          element.userId = doc.userId;
          element.elementId = 0;
          element.importFlag = true;
        });
        arrElement = this.data;
      } else if (this.isRadioChecked == 1) {
        const elementDetails = this.elementForm.value;
        elementDetails.brandId = this.elementForm.value.brandId;
        elementDetails.projectTypeId = parseInt(
          this.elementForm.value.projectTypeId
        );
        elementDetails.userId = this.userId;
        elementDetails.labelId = this.labelId;
        arrElement.push(elementDetails);
      }
      let isValidtoSave = false;
      if (this.distElement != null || this.distElement != undefined) {
        if (this.distElement.length > 0) {
          this.isSave = confirm(
            this.distElement +
              ' ' +
              '  duplicate element(s),do you want to override?'
          );
          if (this.isSave) {
            isValidtoSave = true;
          } else {
            isValidtoSave = false;
          }
        }
      }

      if (this.arrElements.length == 0) {
        isValidtoSave = true;
      }
      //  else {
      //    isValidtoSave=false;
      //  }
      if (this.isRadioChecked == 2 || this.isRadioChecked == 1) {
        this.dataservice.saveLabel(arrElement).subscribe(response => {
          if (this.labelId != 0) {
            // this.successMessage = "Element updated successfully";
            this.toastr.successToastr('Label updated successfully');
          } else {
            // this.successMessage = "Element added successfully";
            this.toastr.successToastr('Label created successfully');
          }
          this.clearData(1);
          this.getAllLanguageLabel();
          isValidtoSave = false;
          this.distElement = [];
          this.arrElements = [];
        });
      } else if (!this.isSave) {
        this.distElement = [];
        this.arrElements = [];
      }
    }
  }

  /* get element by element Id */
  async getLanguageLabelById(labelId) {
    
    this.isLanguageNotSelected = false;
    this.labelId = labelId;
    await this.dataservice
      .getByLabelId(labelId)
      .toPromise()
      .then(async response => {
        // Success
        console.log('response -----response ', response);

        
        this.elementForm.controls['labelCode'].setValue(response['labelCode']);
        this.elementForm.controls['languageId'].setValue(
          response['languageId']
        );
        this.elementForm.controls['labelName'].setValue(response['labelName']);
        this.elementForm.controls['labelDesc'].setValue(response['labelDesc']);

        await this.getAllRegion_byProjectTypeId(response['projectTypeId']);
        await this.getBrandsByRegionId(response['regionId']);
        this.elementForm.controls['brandId'].setValue(response['brandId']);
        this.elementForm.controls['regionId'].setValue(response['regionId']);
        this.elementForm.controls['projectTypeId'].setValue(
          response['projectTypeId']
        );

        window.scroll(0, 0);
      });
  }

  /* change region and get brands*/
  changeRegion(e) {
    this.getBrandsByRegionId(parseInt(e.target.value));
  }

  /* get all regions  */
  async getAllRegion() {
    await this.dataservice
      .getAllRegion()
      .toPromise()
      .then(response => {
        // Success
        this.regionList = response;
        if (response != null) {
          if (this.regionList.length == 1) {
            this.elementForm.controls['regionId'].setValue(
              this.regionList[0].regionId
            );
            this.getBrandsByRegionId(this.regionList[0].regionId);
          } else {
            this.elementForm.controls['regionId'].setValue('');
          }
        }
      });
  }

  /* get all project types  */
  async getAllProjectType() {
    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        // Success
        this.projectTypeList = response;
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.elementForm.controls['projectTypeId'].setValue(
              this.projectTypeList[0].projectTypeId
            );
            this.getPlatformByProjectTypeId(
              this.projectTypeList[0].projectTypeId
            );
          } else {
            this.elementForm.controls['projectTypeId'].setValue('');
          }
        }
      });
  }

  /* change project type and get platforms*/
  async changeProjectType(e) {
    
    this.platformList = [];
    this.regionList = [];
    this.brandList = [];
    let projectTypeid_ = e.target.value;
    if (projectTypeid_.toString() != '') {
      this.getAllRegion_byProjectTypeId(projectTypeid_);
    }
  }

  changeBrand(e) {
    
    this.brandChange = parseInt(e.target.value);
    console.log('brand brandbrandbrandbrandbrandbrand', this.brandChange);

    if (this.brandChange != 0) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  async getAllRegion_byProjectTypeId(typeId) {
    
    await this.dataservice
      .getAllRegionByProjectType(typeId)
      .toPromise()
      .then((response: {}) => {
        this.regionList = response;
        //to place NAR region at first position in frop down
        this.regionList.unshift(
          this.regionList.splice(
            this.regionList.findIndex(item => item.regionName === 'NAR'),
            1
          )[0]
        );
        console.log(this.regionList);
        if (response != null) {
          
          if (this.regionList.length == 1) {
            this.elementForm.controls['regionId'].setValue(
              this.regionList[0].regionId
            );
            this.getBrandsByRegionId(this.regionList[0].regionId);
          } else {
            this.elementForm.controls['regionId'].setValue('');
          }
        }
      });
  }

  changePlatform(e) {
    
    this.Platformchange = parseInt(e.target.value);
    if (this.Platformchange != 0) {
      this.isTextChanged = true;
      this.isLanguageNotSelected = false;
    } else {
      this.isTextChanged = false;
    }
  }

  /* get all platforms by project type id */
  async getPlatformByProjectTypeId(projectTypeId) {
    await this.dataservice
      .getPlatformByProjectTypeId(projectTypeId)
      .toPromise()
      .then(response => {
        // Success
        this.platformList = response;
        if (response != null) {
          if (this.platformList.length == 1) {
            this.elementForm.controls['platformId'].setValue(
              this.platformList[0].platformId
            );
          } else {
            this.elementForm.controls['platformId'].setValue('');
          }
        } else {
          this.elementForm.controls['platformId'].setValue('');
        }
        if (this.platformId > 0) {
          this.elementForm.controls['platformId'].setValue(this.platformId);
        }
      });
  }

  /* get all brands by region Id */
  async getBrandsByRegionId(regionId) {
    await this.dataservice
      .getBrandsByRegionId(regionId)
      .toPromise()
      .then(response => {
        // Success
        console.log('brands', this.brandList);
        this.brandList = response;
        if (response != null) {
          if (this.brandList.length == 1) {
            this.brandId = this.brandList[0].brandId;
            this.elementForm.controls['brandId'].setValue(
              this.brandList[0].brandId
            );
          } else {
            this.elementForm.controls['brandId'].setValue('');
          }
        } else {
          this.elementForm.controls['brandId'].setValue('');
        }
        // if (this.brandId > 0) {
        //   this.elementForm.controls['brandId'].setValue(this.brandId);
        // }
      });
  }

  /* this method clear all messages and element form also */
  /* this method clear all messages and element form also */
  clearData(val) {
    
    var r = false;
    if (this.isRadioChecked == 1) {
      if (
        (this.elementForm.value.brandId != 0 ||
          this.elementForm.value.regionId != 0 ||
          this.elementForm.value.labelName != 0 ||
          this.elementForm.value.labelDesc != 0 ||
          this.elementForm.value.labelCode != 0 ||
          this.elementForm.value.platformId != 0 ||
          this.elementForm.value.projectTypeId != 0) &&
        val == 0
      ) {
        r = confirm('Are you sure you want to reset data?');
      } else {
        r = true;
      }
      if (r == true) {
        this.elementForm = this.formBuilder.group({
          brandId: ['', [Validators.required]],
          regionId: ['', [Validators.required]],
          languageId: ['', [Validators.required]],
          labelCode: ['', [Validators.required,noWhitespaceValidator]],
          labelName: [''],
          labelDesc: ['', [Validators.required]],
          projectTypeId: ['', [Validators.required]]
        });
      }
    } else if (this.isRadioChecked == 2) {
      if (
        (this.elementFormImport.value.brandId != 0 ||
          this.elementFormImport.value.regionId != 0 ||
          this.elementFormImport.value.languageId != 0 ||
          this.elementFormImport.value.projectTypeId != 0) &&
        val == 0
      ) {
        r = confirm('Are you sure you want to reset data?');
      } else {
        r = true;
      }
      if (r == true) {
        this.elementFormImport = this.formBuilder.group({
          brandId: ['', [Validators.required]],
          regionId: ['', [Validators.required]],
          languageId: ['', [Validators.required]],
          projectTypeId: ['', [Validators.required]]
        });
      }
    } else if (this.isRadioChecked == 3) {
      if (
        (this.elementFormExport.value.brandId != 0 ||
          this.elementFormExport.value.regionId != 0 ||
          this.elementFormExport.value.platformId != 0 ||
          this.elementFormExport.value.projectTypeId != 0) &&
        val == 0
      ) {
        r = confirm('Are you sure you want to reset data?');
      } else {
        r = true;
      }
      if (r == true) {
        this.elementFormExport = this.formBuilder.group({
          brandId: ['', [Validators.required]],
          regionId: ['', [Validators.required]],
          languageId: ['', [Validators.required]],
          projectTypeId: ['', [Validators.required]]
        });
      }
    }
    if (this.importFile != undefined) {
      this.importFile.nativeElement.value = '';
    }
    this.data = [];
    this.selectedFile = null;
    this.inValid = false;
    this.submitted = false;
    this.strOldName = '';
    this.changeElementName = '';
    this.brandChange = 0;
    this.oldBrand = 0;
    this.Platformchange = 0;
    this.platformId = 0;
    this.XL_row_object = [];
    this.isTextChanged = false;
    this.importFile = null;
    this.distElement = [];
    this.isSave = false;
    // this.elementForm.reset();
    // this.elementFormImport.reset();
    // this.elementFormExport.reset();
    // this.elementFormImport.reset();
    this.txtSearch = '';
    if (this.isRadioChecked == 1) {
      this.dataservice.customFilter['elementName'] = '';
    } else if (this.isRadioChecked == 2) {
      this.dataservice.customFilter['Element'] = '';
    }
    setTimeout(() => {
      this.successMessage = '';
    }, 2000);
    this.errorMessage = '';
    this.labelId = 0;
    this.errorMessage = '';
    this.isLanguageNotSelected = false;
    this.elementForm.controls['brandId'].setValue('');
    this.elementForm.controls['regionId'].setValue('');
    this.elementForm.controls['projectTypeId'].setValue('');
    this.elementForm.controls['languageId'].setValue('');
    this.regionList = [];
  }

  /* delete element by element Id */
  async deleteLanguageLabel(labelId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this label?'
    );
    if (isDelete) {
      await this.dataservice
        .deleteLabelById(labelId)
        .toPromise()
        .then(
          response => {
            // Success
            // this.successMessage = "Element deleted successfully";
            this.toastr.successToastr('Label deleted successfully');
            this.getAllLanguageLabel();
            this.clearData(1);
          },
          error => {
            // this.errorMessage = this.dataservice.errorWhenInUse;
            this.toastr.errorToastr(
              ' ' + this.dataservice.errorWhenInUse + ' '
            );
            setTimeout(() => {
              this.errorMessage = '';
              this.dataservice.errorWhenInUse = '';
            }, 2000);
          }
        );
    }
  }

  // -----------------------------------------------------XLSX-------------------------------------------------------

  onFileChange(ev) {
    
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = event => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});

      const dataString = JSON.stringify(jsonData);
      console.log('dataString-->', dataString);
      document.getElementById('output').innerHTML = dataString;
      // document.getElementById('output').innerHTML = dataString.slice(0, 300).concat("...");
      // this.setDownload(dataString);
    };
    reader.readAsBinaryString(file);
  }

  parseExcel(file) {
    
    this.errorMessage = '';
    this.selectedFile = file;
    this.data = [];
    console.log('this.selectedFile', this.selectedFile);
    let reader = new FileReader();
    reader.onload = e => {
      let data = (<any>e.target).result;
      let workbook = XLSX.read(data, {
        type: 'binary'
      });
      let isExecuteCount = 0;
      workbook.SheetNames.forEach(
        function(sheetName) {
          // Here is your object
          isExecuteCount = isExecuteCount + 1;
          if (isExecuteCount <= 1) {
            this.XL_row_object = XLSX.utils.sheet_to_json(
              workbook.Sheets[sheetName],
              { defval: '' }
            );
            if (this.XL_row_object.length != 0) {
              let IsInsertData = true;
              let rownum = 0;
              

              console.log('data', this.XL_row_object);

              if (IsInsertData == true) {
                console.log('XL_row_object-->', this.XL_row_object);
                let json_object = JSON.stringify(this.XL_row_object);
                console.log('json_object-->', json_object);
                
                this.data = this.XL_row_object;
                $('#importTable').show();
                console.log(this.CaseJson);
                console.log('this.data-->', this.data);
                $('#importTable').show();
                this.dataservice.customFilter['Element'] = '';
                this.errorMessage = '';
              } else {
                this.importFile.nativeElement.value = '';
                // this.errorMessage = "Please fill mandatory fields into excel, that you are selected... "
                this.toastr.errorToastr(
                  'Please fill mandatory fields into excel, that you are selected... '
                );
              }
            }
          }

          // bind the parse excel file data to Grid
          // this.gridObj.dataSource = JSON.parse(json_object);
        }.bind(this),
        this
      );
    };

    reader.onerror = function(ex) {
      console.log(ex);
    };

    if (file != undefined && file != null) {
      reader.readAsBinaryString(file);
    }
  }
  public onSuccess(args: any): void {
    this.data = [];
    let files = args.target.files; // FileList object
    this.parseExcel(files[0]);
  }

  radioCheck(id) {
    this.isRadioChecked = id;
    this.submitted = false;
    this.txtSearch = '';
    this.isTextChanged = false;
    this.languageId;
    this.Platformchange;
    this.XL_row_object = [];
    this.oldBrand = 0;
    this.strOldName = '';
    this.Platformchange = 0;
    this.elementForm.reset();
    this.elementFormExport.reset();
    this.elementFormImport.reset();
    this.data = [];
    this.selectedFile = null;

    this.successMessage = '';
    this.errorMessage = '';

    if (id == 1) {
      this.dataservice.customFilter['elementName'] = '';
    } else if (id == 2) {
      this.dataservice.customFilter['Element'] = '';
    }

    this.elementForm = this.formBuilder.group({
      brandId: ['', [Validators.required]],
      regionId: ['', [Validators.required]],
      languageId: ['', [Validators.required]],
      labelCode: ['', [Validators.required,noWhitespaceValidator]],
      labelName: [''],
      labelDesc: ['', [Validators.required,noWhitespaceValidator]],
      projectTypeId: ['', [Validators.required]]
    });

    this.elementFormExport = this.formBuilder.group({
      brandId: ['', [Validators.required]],
      regionId: ['', [Validators.required]],
      projectTypeId: ['', [Validators.required]]
    });

    this.elementFormImport = this.formBuilder.group({
      brandId: ['', [Validators.required]],
      regionId: ['', [Validators.required]],
      projectTypeId: ['', [Validators.required]]
    });
  }

  filter() {
    
    let key = '';
    if (this.isRadioChecked == 1) {
      key = 'elementName';
    } else if (this.isRadioChecked == 2) {
      key = 'Element';
    }

    this.dataservice.filter(key, this.txtSearch);
  }

  exportElementFile() {
    
    this.submitted = true;
    this.form = this.elementFormExport;

    if (this.form.valid) {
      // let exportElementFileUrl = this.dataservice.exportElementFile(
      //   this.form.value.brandId,
      //   platformId,
      //   this.form.value.projectTypeId);

      // setTimeout(function () {
      //   window.location.assign(exportElementFileUrl);
      // }, 2000);

      this.dataservice.exportLabelFile().subscribe(res => {
        // const blob = new Blob([res], { type: 'application/octet-stream' });
        // const url= window.URL.createObjectURL(blob);
        // window.open(url);
        const blob = new Blob([res], { type: 'application/octet-stream' });
        //   const file = new File([blob], 'WPLanguageLebels' + '.xlsx', { type: 'application/vnd.ms.excel' });
        var a = document.createElement('a');
        a.href = URL.createObjectURL(blob);
        a.download = 'WPLanguageLebels' + '.xls';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      });
    }
  }

  async getAllLanguage() {
    await this.dataservice.getAllLanguage().subscribe((response: {}) => {
      this.languageList = response;
      console.log(response);
      if (response != null) {
        if (this.languageList.length == 1) {
          // this.brandForm.controls["projectTypeId"].setValue(this.projectTypeList[0].projectTypeId);
        } else {
          //  this.languageLabelForm.controls['languageId'].setValue('');
        }
      }
      
    });
  }

  // async getAllLanguageLabel() {
  //   
  //   await this.dataservice.getAllLabel().subscribe((response: {}) => {
  //     this.languageLabelList = response;
  //     console.log(response);
  //     
  //   });
  // }

  async getAllLanguageLabel() {
    await this.dataservice
      .getAllLabel()
      .toPromise()
      .then((response: {}) => {
        this.languageLabelList = response;
      });
  }

  changeLabelName(e) {
    
    this.labelName = e.target.value;
    if (this.labelName != '') {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  async checkDuplication() {
    

    if (
      this.elementForm.value.labelCode != 0 &&
      this.elementForm.value.labelCode != '' &&
      this.elementForm.value.labelCode != undefined
    ) {
      const ElementDetails = this.elementForm.value;
      ElementDetails.resource = this.elementForm.value.labelCode
        .toString()
        .trim();
      ElementDetails.type = Type.LanguageLabel;
      let languageValue = parseInt(this.elementForm.value.languageId);
      ElementDetails.id = this.elementForm.value.brandId;
      ElementDetails.languageId = languageValue;
      ElementDetails.optionalId =
        this.languageList != null || this.languageList.length > 0
          ? languageValue
          : 0;

      await this.dataservice
        .checkDuplicationwithOptionalId(ElementDetails)
        .toPromise()
        .then((response: Boolean) => {
          // Success
          this.inValid = response;
          if (this.inValid) {
            // this.errorMessage = ElementDetails.resource + " already exist";
            this.toastr.errorToastr(
              'LabelLabel name "' +
                ElementDetails.resource +
                '" is already exists'
            );
          } else {
            this.errorMessage = '';
          }
        });
    }
  }

  changeLanguage(e) {
    
  }
  changeData(e) {
    
    this.changeElementName = e.target.value
      .toString()
      .trim()
      .toUpperCase();
    if (
      this.strOldName
        .toString()
        .trim()
        .toUpperCase() != this.changeElementName &&
      this.strOldName.toUpperCase() != ''
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}