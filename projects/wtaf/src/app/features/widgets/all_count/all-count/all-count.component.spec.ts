import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllCountComponent } from './all-count.component';

describe('AllCountComponent', () => {
  let component: AllCountComponent;
  let fixture: ComponentFixture<AllCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
