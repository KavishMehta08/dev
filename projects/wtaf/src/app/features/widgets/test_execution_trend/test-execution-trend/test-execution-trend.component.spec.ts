import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestExecutionTrendComponent } from './test-execution-trend.component';

describe('TestExecutionTrendComponent', () => {
  let component: TestExecutionTrendComponent;
  let fixture: ComponentFixture<TestExecutionTrendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestExecutionTrendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestExecutionTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
