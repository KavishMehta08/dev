import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Chart } from 'chart.js';
import { DataService } from '../../../../shared/services/data.service';
import * as $ from 'jquery';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import * as Highcharts from 'highcharts';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'wtaf-test-suite-status',
  templateUrl: './test-suite-status.component.html',
  styleUrls: ['./test-suite-status.component.css']
})
export class TestSuiteStatusComponent implements OnInit {
  chart:any;
  projectTypeList : any = [];
  testSuiteStatusform: FormGroup;
  toDate: any;
  fromDate: any;
  projecttypeId: any;
  platformList : any = [];
  regionList : any = [];
  regionName:any;
  brandList : any = [];
  regionId : number;
  environmentList = ['STG', 'Dev', 'Prod', 'QA'];
  isSubmitted = false;
  isValidDate:any;
  error:any={isError:false,errorMessage:''};

  brandId : number;
  projectNameList : any = [];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  updateFlag = true;
  highcharts : any = Highcharts;
   chartOptions = {   
      chart : {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
      },
      mapNavigation: {
        enableMouseWheelZoom: true
    },
      title : {
         text: ''   
      },
  
      tooltip : {
         pointFormat: '{series.name}: <b>{point.y}</b>'
      },
      plotOptions : {
         pie: {
          colors: [
             "#007bff",
             "#dc3545",
             "#ffc107",
          ],
            allowPointSelect: true,
            cursor: 'pointer',
      
            dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %'
          },
      
           showInLegend: true
         }
      },
      series: [],
   };
  
  constructor(private dataservice: DataService,
    public fb: FormBuilder,
    public DatePipe: DatePipe,
    public toastr: ToastrManager) {
      let nowDate = new Date();
      this.toDate = this.DatePipe.transform(nowDate, 'yyyy-MM-dd');
      this.fromDate = moment().subtract('days', 7).format('YYYY-MM-DD');
  
      this.testSuiteStatusform = this.fb.group({
        projectTypeId: ['', [Validators.required]],
        regionId: ['', [Validators.required]],
        brandId: ['', [Validators.required]],
        productNames: [[], [Validators.required]],
        platformId: ['', [Validators.required]],
        environment: ['STG', [Validators.required]],
        startDate: [this.fromDate, [Validators.required]],
        endDate: [this.toDate, [Validators.required]],
         projectNames:[],
      });
     }

  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'projectName',
      textField: 'projectName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    $('#testSuiteStatusFromDate').attr("max", this.toDate);
    $('#testSuiteStatusToDate').attr('max', this.toDate);
    this.chartOptions.series=[{
      name: 'Count',
      colorByPoint: true,
      data: [{
          name: 'Passed',
          y: 59.00,
          sliced: false,
          selected: false
      },{
          name: 'Failed',
          y: 4.00
      }, {
          name: 'Unknown',
          y: 1.00
      }]
  }];
  this.updateFlag = true;
    $('#testSuiteStatusFromDate').attr("max", this.toDate);
    $('#testSuiteStatusToDate').attr('max', this.toDate);
    this.getAllProjectType();
    let Pie = document.getElementById('Pie');
  }
  
  validateDates(sDate: string, eDate: string){

    this.isValidDate = true;
    if((sDate == null || eDate ==null)){
      this.error={isError:true,errorMessage:'Start date and end date are required.'};
      this.isValidDate = false;
    }

    if((sDate != null && eDate !=null) && (eDate) < (sDate)){
      this.error={isError:true,errorMessage:'To date should be greater than from date.'};
      this.isValidDate = false;
    }
    return this.isValidDate;
  }
  onItemSelect(item:any){
    console.log(item);
    console.log(this.selectedItems);
}
OnItemDeSelect(item:any){
    console.log(item);
    console.log(this.selectedItems);
}
onSelectAll(items: any){
    console.log(items);
}
onDeSelectAll(items: any){
    console.log(items);
}

onFormSubmit() {
// this.testSuiteStatusform.controls['testSuiteId'].setValue(this.testSuiteid);
  this.isSubmitted = true;
  this.isValidDate = this.validateDates(this.DatePipe.transform(this.testSuiteStatusform.controls['startDate'].value, 'yyyy-MM-dd'), this.DatePipe.transform(this.testSuiteStatusform.controls['endDate'].value, 'yyyy-MM-dd'));
  if (!this.isValidDate || this.testSuiteStatusform.invalid ) {
    return;
  }
  else{
    if (this.projectNameList != null) {
      let temp_projectName_list = this.testSuiteStatusform.value['productNames'].map((element) => {
        return element.projectName
      })
      this.testSuiteStatusform.controls['projectNames'].setValue(temp_projectName_list);
      console.log('temp_projectName_list....', this.testSuiteStatusform.controls['projectNames'].setValue(temp_projectName_list));
      console.log(this.testSuiteStatusform.value);
    }
    this.dataservice.testSuiteExeStatusTrend(this.testSuiteStatusform.value).subscribe((res:any) =>{
   console.log('test suite exec status res' , res );
   let mindate = moment(this.toDate).subtract(24, 'months').format('YYYY-MM-DD');
   $('#testSuiteStatusFromDate').attr("max", this.toDate);
   $('#testSuiteStatusToDate').attr('max', this.toDate); 
   $('#testSuiteStatusFromDate').attr("min", mindate);
   $('#testSuiteStatusToDate').attr('min', mindate); 
   this.error={isError:false,errorMessage:''};
   if(res.length === 0){
    this.toastr.errorToastr('No records found');
   }
   if(res != null ){
   this.chartOptions.series=[{
    name: 'Count',
    colorByPoint: true,
    data: [{
        name: 'Pass',
        y: res.passPercentage,
        sliced: false,
        selected: false
    },{
        name: 'Fail',
        y: res.failPercentage
    }, {
        name: 'Not Executed',
        y: res.notExecPercentage
    }]
}];
this.updateFlag = true;
   }
    },
    error => {
      console.log(error);
    })
  }
}
  async getAllProjectType() {
    await this.dataservice.getAllProjectType().toPromise().then(
      async response => { // Success
        this.projectTypeList = response;
        console.log('this.projectTypeList.length_widget_3');
        console.log('this.projectTypeList.length_widget_3' ,this.projectTypeList);
        if (response != null) {
          this.testSuiteStatusform.controls["projectTypeId"].setValue(this.projectTypeList[0].projectTypeId);
          this.getPlatformByProjectTypeId(this.projectTypeList[0].projectTypeId);
          await this.getAllRegion(this.projectTypeList[0].projectTypeId);
        }
      });
  }

  async getPlatformByProjectTypeId(projectTypeId) {
    await this.dataservice.getPlatformByProjectTypeId(projectTypeId).subscribe(
      (response: {}) => {
        this.platformList = response;
        console.log(this.platformList);
        if (this.platformList != null) {
          if (this.platformList.length == 1) {
            this.testSuiteStatusform.controls['platformId'].setValue(
              this.platformList[0].platformId
            );
          } else {
            this.testSuiteStatusform.controls['platformId'].setValue(
              this.platformList[1].platformId
            );
          }
        }
      },
      error => {
        console.log(error.message);
      }
    );
  }


  getAllRegion(Projecttypeid) {
    this.dataservice
      .getAllRegionByProjectType(Projecttypeid)
      .subscribe(async (response: {}) => {
        this.regionList = response;
        this.regionList.unshift(
          this.regionList.splice(
            this.regionList.findIndex(item => item.regionName === 'NAR'),
            1
          )[0]
        );
        if (response != null) {
          
          this.testSuiteStatusform.controls['regionId'].setValue(
            this.regionList[0].regionId
          );
          this.regionName = this.regionList[0].regionName;
          await this.getBrandsByRegionId(this.testSuiteStatusform.get('regionId').value);
        }
      });
  }

  async getBrandsByRegionId(regionId) {
    await this.dataservice
      .getBrandsByRegionId(regionId)
      .toPromise()
      .then((response: {}) => {
        this.brandList = response;
        console.log('this.brandList.....>' , this.brandList);
        
        if (response != null) {
          if (this.brandList) {
            this.regionId = this.testSuiteStatusform.get('regionId').value;
            this.projecttypeId = this.testSuiteStatusform.get(
              'projectTypeId'
            ).value;

            this.brandList.unshift(
              this.brandList.splice(
                this.brandList.findIndex(
                  item => item.brandName.toLowerCase() === 'whirlpool'
                ),
                1
              )[0]
            );
            this.testSuiteStatusform.controls['brandId'].setValue(
              this.brandList[0].brandId
            );
            this.brandId = this.brandList[0].brandId;
          }
          this.getAllProjectName(
            this.regionId,
            this.brandId,
            this.projecttypeId
          );
        } else {
        }
      })
      .catch(err => {
        console.log(err);
        // this.toastr.errorToastr('Something went wrong');
      });
  }

  async getAllProjectName(regionId, brandId, projecttypeId) {
    
    await this.dataservice
      .getProjectNameByTypeId(brandId, projecttypeId, regionId)
      .subscribe((allProjNameRes) => {
        this.projectNameList = allProjNameRes;
        console.log('all product names', this.projectNameList);
      });
  }

  changeProjectType(e) {
    this.dataservice.isfromWidget = false;
    console.log(e);
    this.projecttypeId = e.target.value;
    console.log(this.projecttypeId);
    

    this.projecttypeId = e.target.value;
    this.regionList = [];
    this.brandList = [];
    this.brandId = 0;
    this.projectNameList = [];
    // this.testSuiteList = [];
    this.testSuiteStatusform.controls['regionId'].setValue('');
    // this.testSuiteStatusform.controls['testSuiteId'].setValue('');
    this.testSuiteStatusform.controls['productNames'].setValue('');
    this.getPlatformByProjectTypeId(parseInt(e.target.value));
    this.getAllRegion(parseInt(e.target.value));
  }

  changeRegion(e) {
    this.dataservice.isfromWidget = false;
    this.projectNameList = [];
    // this.testSuiteList = [];
    // this.testSuiteStatusform.controls['testSuiteId'].setValue('');
    this.testSuiteStatusform.controls['productNames'].setValue('');
    this.getBrandsByRegionId(parseInt(e.target.value));
  }

  changeBrand(e) {
    this.dataservice.isfromWidget = false;
    // this.testSuiteStatusform.controls['testSuiteId'].setValue('');
    // this.testSuiteStatusform.controls['testSuiteName'].setValue('');
    this.testSuiteStatusform.controls['productNames'].setValue('');
    this.brandId = parseInt(e.target.value);
    this.regionId = this.testSuiteStatusform.get('regionId').value;
    this.projecttypeId = this.testSuiteStatusform.get('projectTypeId').value;

    this.getAllProjectName(this.regionId,this.brandId, this.projecttypeId);
  }


  onFromDateChange(e) {
    let fromDate = this.DatePipe.transform(e.target.value, 'yyyy-MM-dd');
    let currentDate = new Date();
    let currentmonth =  currentDate.getMonth();   
    let currentyear =  currentDate.getFullYear();   
    let currentmonth1 =  new Date(fromDate).getMonth();   
    let currentyear1 =  new Date(fromDate).getFullYear(); 
    if (
      new Date(this.DatePipe.transform(currentDate, 'yyyy-MM-dd')).getTime() ===
      new Date(this.DatePipe.transform(fromDate, 'yyyy-MM-dd')).getTime()
    ) {
      let maxDate = fromDate;
      fromDate = moment(fromDate)
        .subtract(1, 'months')
        .format('YYYY-MM-DD');
      $('#testSuiteStatusToDate').attr(
        'min',
        this.DatePipe.transform(fromDate, 'yyyy-MM-dd')
      );
      $('#testSuiteStatusToDate').attr(
        'max',
        this.DatePipe.transform(maxDate, 'yyyy-MM-dd')
      );
    } else {
      let date_1 = new Date(currentyear, currentmonth);
      let date_2 = new Date(currentyear1, currentmonth1);
     if(date_1.getTime() === date_2.getTime()){
      let maxDate = fromDate;
      fromDate = moment(fromDate)
      .subtract(1, 'months')
      .format('YYYY-MM-DD');
    let toDate = moment(this.DatePipe.transform(e.target.value, 'yyyy-MM-dd'))
      .add(1, 'months')
      .format('YYYY-MM-DD');
    $('#testSuiteStatusToDate').attr(
      'min',
      this.DatePipe.transform(fromDate, 'yyyy-MM-dd')
    );
    $('#testSuiteStatusToDate').attr(
      'max',
      this.DatePipe.transform(currentDate, 'yyyy-MM-dd')
    );
    }else{
      // fromDate = moment(fromDate)
      // .subtract(1, 'months')
      // .format('YYYY-MM-DD');
      let toDate = moment(this.DatePipe.transform(e.target.value, 'yyyy-MM-dd'))
      .add(1, 'months')
      .format('YYYY-MM-DD');
  
      let date_1 = new Date(this.DatePipe.transform(currentDate, 'yyyy-MM-dd')).getTime();
      let date_2 = new Date(this.DatePipe.transform(toDate, 'yyyy-MM-dd')).getTime();
      
      if(date_2 > date_1){
        toDate = this.DatePipe.transform(currentDate, 'yyyy-MM-dd') ;
      }
    $('#testSuiteStatusToDate').attr(
      'min',
      this.DatePipe.transform(fromDate, 'yyyy-MM-dd')
    );
    $('#testSuiteStatusToDate').attr(
      'max',
      this.DatePipe.transform(toDate, 'yyyy-MM-dd')   
    );
    this.testSuiteStatusform.controls['endDate'].setValue(toDate);
    }

    }
  }
  onToDateChange(e) {
    let toDate = this.DatePipe.transform(e.target.value, 'yyyy-MM-dd');
    let currentDate = new Date();
    let currentmonth =  currentDate.getMonth();   
    let currentyear =  currentDate.getFullYear();   
    let currentmonth1 =  new Date(toDate).getMonth();   
    let currentyear1 =  new Date(toDate).getFullYear(); 
    if (
      new Date(this.DatePipe.transform(currentDate, 'yyyy-MM-dd')).getTime() ===
      new Date(this.DatePipe.transform(toDate, 'yyyy-MM-dd')).getTime()
    ) {
      toDate = moment(toDate)
        .subtract(1, 'months')
        .format('YYYY-MM-DD');
      $('#testSuiteStatusFromDate').attr(
        'min',
        this.DatePipe.transform(toDate, 'yyyy-MM-dd')
      );
    } else {
      let date_1 = new Date(currentyear, currentmonth);
      let date_2 = new Date(currentyear1, currentmonth1);
     if(date_1.getTime() === date_2.getTime()){
      toDate = moment(toDate)
        .subtract(1, 'months')
        .format('YYYY-MM-DD');
      let fromDate = moment(
        this.DatePipe.transform(e.target.value, 'yyyy-MM-dd')
      )
        .add(1, 'months')
        .format('YYYY-MM-DD');
      $('#testSuiteStatusFromDate').attr(
        'min',
        this.DatePipe.transform(toDate, 'yyyy-MM-dd')
      );
      $('#testSuiteStatusFromDate').attr(
        'max',
        this.DatePipe.transform(currentDate, 'yyyy-MM-dd')
      );
    }else{
      toDate = moment(toDate)
      .subtract(1, 'months')
      .format('YYYY-MM-DD');
    let fromDate = moment(
      this.DatePipe.transform(e.target.value, 'yyyy-MM-dd')
    )
      .add(1, 'months')
      .format('YYYY-MM-DD');
    $('#testSuiteStatusFromDate').attr(
      'min',
      this.DatePipe.transform(toDate, 'yyyy-MM-dd')
    );
    $('#testSuiteStatusFromDate').attr(
      'max',
      this.DatePipe.transform(fromDate, 'yyyy-MM-dd')
    );
    }
  }
  }

}
