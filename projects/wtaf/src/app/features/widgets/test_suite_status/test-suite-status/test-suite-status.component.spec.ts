import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestSuiteStatusComponent } from './test-suite-status.component';

describe('TestSuiteStatusComponent', () => {
  let component: TestSuiteStatusComponent;
  let fixture: ComponentFixture<TestSuiteStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestSuiteStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestSuiteStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
