import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestSuiteExecutionTimeTrendComponent } from './test-suite-execution-time-trend.component';

describe('TestSuiteExecutionTimeTrendComponent', () => {
  let component: TestSuiteExecutionTimeTrendComponent;
  let fixture: ComponentFixture<TestSuiteExecutionTimeTrendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestSuiteExecutionTimeTrendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestSuiteExecutionTimeTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
