import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyExecutionStatusComponent } from './monthly-execution-status.component';

describe('MonthlyExecutionStatusComponent', () => {
  let component: MonthlyExecutionStatusComponent;
  let fixture: ComponentFixture<MonthlyExecutionStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyExecutionStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyExecutionStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
