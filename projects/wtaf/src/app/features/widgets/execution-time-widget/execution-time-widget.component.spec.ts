import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutionTimeWidgetComponent } from './execution-time-widget.component';

describe('ExecutionTimeWidgetComponent', () => {
  let component: ExecutionTimeWidgetComponent;
  let fixture: ComponentFixture<ExecutionTimeWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutionTimeWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionTimeWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
