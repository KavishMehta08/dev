import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import {ReportComponent} from './report/report.component';

import { ReportRoutingModule } from './report-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgPipesModule} from 'ngx-pipes';
@NgModule({
  declarations: [ReportComponent],
  imports: [CommonModule, SharedModule,ReportRoutingModule,Ng2SearchPipeModule,NgPipesModule]
})
export class ReportModule {}
