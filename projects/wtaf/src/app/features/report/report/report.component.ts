import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { DataService } from '../../../shared/services/data.service';
import { Router, RouterLink } from '@angular/router';
// import { LocationStrategy, DatePipe } from '@angular/common';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
declare var $: any;
@Component({
  selector: 'wtaf-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  chart = [];
  History_Details: any = [];
  PiaChartStatus_Details: any = [];
  jobPassed: Number = 0;
  jobFailed: Number = 0;
  jobRunning: Number = 0;
  suitePassed: Number = 0;
  suiteFailed: Number = 0;
  suiteRunning: Number = 0;
  casePassed: Number = 0;
  caseFailed: Number = 0;
  caseRunning: Number = 0;
  resLength: any = [];
  startTime1: any;
  endTime1: any;
  isSeemore: boolean = false;
  historyDetailsRes: any = [];
  jobDetailsHistoryBy_historyId: any;
  txtSearch = '';
  jobName;
  constructor(private dataservice: DataService, public datePipe: DatePipe) {}
  ngOnInit() {
    this.historyDetails();
    //============ PiaChartStatus details================---- AKASH
    this.dataservice.getStatusCountsForPieCharts().subscribe((response: {}) => {
      this.PiaChartStatus_Details = response;
      //---Doughnut--Chart for Execution --Akash
      this.chart = new Chart('DoughnutExecution', {
        type: 'doughnut',
        data: {
          labels: ['Passed', 'Failed', 'Running'],
          datasets: [
            {
              data: [
                this.PiaChartStatus_Details.testExecutionReport.jobPassed,
                this.PiaChartStatus_Details.testExecutionReport.jobFailed,
                this.PiaChartStatus_Details.testExecutionReport.jobRunning
              ],
              backgroundColor: ['#43B2B3', '#FF007D', '#012F60'],
              render: 'percentage',
              precision: 2,
              fill: true
            }
          ]
        },
        options: {
          legend: {
            display: true,
            position: 'left',
            fontweight: 'bold'
          },
          scales: {
            xAxes: [
              {
                display: false
              }
            ],
            yAxes: [
              {
                display: false
              }
            ]
          }
        }
      });
      //---Doughnut--Chart for Suite --Akash
      this.chart = new Chart('DoughnutSuite', {
        type: 'doughnut',
        data: {
          labels: ['Passed', 'Failed', 'Running'],
          datasets: [
            {
              data: [
                this.PiaChartStatus_Details.testSuiteReport.suitePassed,
                this.PiaChartStatus_Details.testSuiteReport.suiteFailed,
                this.PiaChartStatus_Details.testSuiteReport.suiteRunning
              ],
              backgroundColor: ['#43B2B3', '#FF007D', '#012F60'],
              fill: true
            }
          ]
        },
        options: {
          legend: {
            display: true,
            position: 'left',
            fontweight: 'bold'
          },
          scales: {
            xAxes: [
              {
                display: false
              }
            ],
            yAxes: [
              {
                display: false
              }
            ]
          }
        }
      });
      //---Doughnut--Chart for Case --Akash
      this.chart = new Chart('DoughnutCase', {
        type: 'doughnut',
        data: {
          labels: ['Passed', 'Failed', 'Running'],
          datasets: [
            {
              data: [
                this.PiaChartStatus_Details.testCaseReport.casePassed,
                this.PiaChartStatus_Details.testCaseReport.caseFailed,
                this.PiaChartStatus_Details.testCaseReport.caseRunning
              ],
              backgroundColor: ['#43B2B3', '#FF007D', '#012F60'],
              fill: true
            }
          ]
        },
        options: {
          legend: {
            display: true,
            position: 'left',
            fontweight: 'bold'
          },
          scales: {
            xAxes: [
              {
                display: false
              }
            ],
            yAxes: [
              {
                display: false
              }
            ]
          }
        }
      });
      console.log('PiaChartStatus Details....', this.History_Details);
    });
  }

  //============ History details===================
  historyDetails() {
    this.dataservice.getJobHistory('', 0, 1).subscribe((response: {}) => {
      this.History_Details = response;
      console.log('History Details....', this.History_Details);
      let timeDifference;
      let startTime;
      let endTime;
      var eventStartTime;
      var eventEndTime;
      var startDate;
      var endDate;
      var offset;
      this.History_Details.forEach(element => {
        // Added by Akash  StartTime EndTime Local Conversion
        eventEndTime = element.endTime;
        eventStartTime = element.startTime;
        if (
          eventStartTime != null &&
          eventStartTime != '' &&
          eventStartTime != undefined &&
          eventStartTime != 'Invalid date'
        ) {
          if (
            eventEndTime != null &&
            eventEndTime != '' &&
            eventEndTime != undefined &&
            eventEndTime != 'Invalid date'
          ) {
            startDate = element.startDate;
            startDate = this.datePipe.transform(startDate, 'yyyy-MM-dd');
            endDate = element.endDate;
            endDate = this.datePipe.transform(endDate, 'yyyy-MM-dd');
            endTime = endDate + ' ' + eventEndTime;
            endTime = moment.utc(endTime).toDate();
            var new_startDate = new Date(endTime);
            var date2 = moment(new_startDate).format('hh:mm:ss');
            console.log('endTime===' + endTime + 'time===' + date2);
            endTime = Date.parse(endTime);
            startTime = startDate + ' ' + eventStartTime;
            startTime = moment.utc(startTime).toDate();
            var new_startDate = new Date(startTime);
            var date1 = moment(new_startDate).format('hh:mm:ss');
            console.log('endTime===' + startTime + 'time===' + date1);
            startTime = Date.parse(startTime);
            timeDifference = endTime - startTime;
            let seconds = Math.floor((timeDifference / 1000) % 60);
            let minutes = Math.floor((timeDifference / (1000 * 60)) % 60);
            let hours = Math.floor(timeDifference / (1000 * 60 * 60));
            console.log(seconds < 10 ? '0' + seconds : seconds);
            element.differentTime =
              hours + 'h' + ' ' + minutes + 'm' + ' ' + seconds + 's';
            element.startTime1 = date1;
            element.endTime1 = date2;
          }
        } else {
          element.differentTime = ' -- : -- : --';
          startTime = ' -- : -- : --';
          date1 = startTime;
          element.startTime1 = date1;
          endTime = ' -- : -- : --';
          date2 = endTime;
          element.endTime1 = date2;
        }
        return (
          element.differentTime,
          (startTime = element.startTime1),
          (endTime = element.endTime1)
        );
      });
    });
  }
  getJobDetails(exeHeaderId, exeHistoryId, seeMore) {
    let doc = this;
    let timeDifference;
    let startTime;
    let endTime;
    var eventStartTime;
    var eventEndTime;
    var startDate;
    var endDate;
    this.dataservice
      .getJobHistoryDetails(exeHeaderId, exeHistoryId, seeMore)
      .subscribe(res => {
        this.historyDetailsRes[exeHistoryId] = res;
        this.jobDetailsHistoryBy_historyId = this.historyDetailsRes[
          exeHistoryId
        ];
        this.historyDetailsRes[exeHistoryId].map(element => {
          // Added by Akash  StartTime EndTime Local Conversion
          eventEndTime = element.endTime;
          eventStartTime = element.startTime;
          if (
            eventStartTime != null &&
            eventStartTime != '' &&
            eventStartTime != undefined &&
            eventStartTime != 'Invalid date'
          ) {
            if (
              eventEndTime != null &&
              eventEndTime != '' &&
              eventEndTime != undefined &&
              eventEndTime != 'Invalid date'
            ) {
              startDate = element.startDate;
              startDate = this.datePipe.transform(startDate, 'yyyy-MM-dd');
              endDate = element.endDate;
              endDate = this.datePipe.transform(endDate, 'yyyy-MM-dd');
              endTime = endDate + ' ' + eventEndTime;
              endTime = moment.utc(endTime).toDate();
              var new_startDate = new Date(endTime);
              var date2 = moment(new_startDate).format('hh:mm:ss');
              console.log('endTime===' + endTime + 'time===' + date2);
              endTime = Date.parse(endTime);
              startTime = startDate + ' ' + eventStartTime;
              startTime = moment.utc(startTime).toDate();
              var new_startDate = new Date(startTime);
              var date1 = moment(new_startDate).format('hh:mm:ss');
              console.log('endTime===' + startTime + 'time===' + date1);
              startTime = Date.parse(startTime);
              timeDifference = endTime - startTime;
              let seconds = Math.floor((timeDifference / 1000) % 60);
              let minutes = Math.floor((timeDifference / (1000 * 60)) % 60);
              let hours = Math.floor((timeDifference / (1000 * 60 * 60)) % 24);
              let days = Math.floor(timeDifference / (24 * 60 * 60 * 1000));
              console.log(seconds < 10 ? '0' + seconds : seconds);
              element.differentTime =
                (days < 10 ? '0' : '') +
                days +
                ':' +
                (hours < 10 ? '0' : '') +
                hours +
                ':' +
                (minutes < 10 ? '0' : '') +
                minutes +
                ':' +
                (seconds < 10 ? '0' : '') +
                seconds;
              element.startTime1 = date1;
              element.endTime1 = date2;
            }
          } else {
            element.differentTime = ' -- : -- : --';
            startTime = ' -- : -- : --';
            date1 = startTime;
            element.startTime1 = date1;
            endTime = ' -- : -- : --';
            date2 = endTime;
            element.endTime1 = date2;
          }
          return (
            element.differentTime,
            (startTime = element.startTime1),
            (endTime = element.endTime1)
          );
        });
        this.resLength = res;
        console.log(
          'history details  response------->',
          this.historyDetailsRes
        );
        if (this.resLength.length > 3) {
          if (seeMore == true) {
            this.isSeemore = true;
          } else {
            this.isSeemore = false;
          }
        }
      });
    if (seeMore == false) {
      if ($('#' + exeHistoryId).hasClass('out')) {
        $('#' + exeHistoryId).addClass('in');
        $('#' + exeHistoryId).removeClass('out');
        $('#r' + exeHistoryId).removeClass('caret');
        $('#r' + exeHistoryId).addClass('caret caret-down');
      } else {
        $('#' + exeHistoryId).addClass('out');
        $('#' + exeHistoryId).removeClass('in');
        $('#r' + exeHistoryId).removeClass('out');
        $('#r' + exeHistoryId).removeClass('caret caret-down');
        $('#r' + exeHistoryId).addClass('caret');
      }
    }
  }
  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }
}
