import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ReportComponent} from './report/report.component';
import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    component: ReportComponent,
    data: { title: '' },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule {}
