import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { HighchartsChartComponent } from 'highcharts-angular';

import { SharedModule } from '../../shared/shared.module';
import { DashboardComponent } from './dashboard/dashboard.component';
// import { TestExecutionTrendComponent } from './features/widgets/test_execution_trend/test-execution-trend/test-execution-trend.component';

import { DashboardRoutingModule } from './dashboard-routing.module';
@NgModule({
  declarations: [DashboardComponent],
  imports: [CommonModule, SharedModule,DashboardRoutingModule]
})
export class DashboardModule {}
