import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material";
import { Chart } from 'chart.js';
import { DataService } from '../../../shared/services/data.service';
import { Type } from '../../models/type';
// import * as Highcharts from 'highcharts';

@Component({
  selector: 'wtaf-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  chart = [];

  projectTypeList: any = [];
  regionList: any = [];
  testCaseList: any = [];
  testSuiteList: any = [];
  projectTypeId: number = 0;
  projectTypeId1: number = 0;
  regionId1: any;
  testCaseId1: Number = 0;
  testSuiteId1: Number = 0;
  regionId: number = 0;
  projectTypeId2: number = 0;
  regionId2: any;
  testCaseId2: Number = 0;
  testSuiteId2: Number = 0;

  projectTypeId3: number = 0;
  regionId3: any;
  testCaseId3: Number = 0;
  testSuiteId3: Number = 0;

  projectTypeId4: number = 0;
  regionId4: any;
  testCaseId4: Number = 0;
  testSuiteId4: Number = 0;


  regionList1: any = [];
  regionList2: any = [];
  regionList3: any = [];
  regionList4: any = [];

  allProjectName: Object;
  projectName: string = '';
  platformId: Number = 0;
  trendExecutionFromDate;
  trendExecutionToDate;
  trendSuiteFromDate;
  trendSuiteToDate;
  testSuitePassFromDate;
  testSuitePassToDate;
  testSuiteExecutionTimeToDate;
  testSuiteExecutionTimeFromDate;
  dashboardCountData: any = [];

  title = 'myHighchart';

  // highcharts = Highcharts;
  chartOptions = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Browser market shares at a specific website, 2014'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',

        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }

        // showInLegend: true
      }
    },
    series: [{
      name: 'Brands',
      colorByPoint: true,
      data: [{
        name: 'Chrome',
        y: 61.41,
        sliced: true,
        selected: true
      }, {
        name: 'Internet Explorer',
        y: 11.84
      }, {
        name: 'Firefox',
        y: 10.85
      }, {
        name: 'Edge',
        y: 4.67
      }, {
        name: 'Safari',
        y: 4.18
      }, {
        name: 'Sogou Explorer',
        y: 1.64
      }, {
        name: 'Opera',
        y: 1.6
      }, {
        name: 'QQ',
        y: 1.2
      }, {
        name: 'Other',
        y: 2.61
      }]
    }]
  };
  constructor(private dataservice: DataService, private dialog: MatDialog) { }

  ngOnInit() {
    //---Line Chart--Akash
    this.dataservice.isfromWidget = true;
    this.chart = new Chart('Line', {
      type: 'line',
      data: {
        labels: ['Suite-1', 'Suite-2', 'Suite-3', 'Suite-4', 'Suite-5'],
        datasets: [
          {
            data: [0, 35, 33, 50, 25],
            label: ['Pass %'],
            borderColor: '#328efe',
            backgroundColor: "#96c1f2",
            fill: true
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [{
            display: true,
            ticks: { min: 0, max: 100 }
          }],
          yAxes: [{
            display: true,
            ticks: { min: 0, max: 100 }
          }],
        }
      }
    });
  }

  ngOnDestroy() {
    this.dataservice.isfromWidget = false;
  }
}