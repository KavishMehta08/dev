import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ConfigurationComponent } from './configuration/configuration.component';
import { ConfigurationRoutingModule } from './configuration-routing.module';

@NgModule({
  declarations: [ConfigurationComponent],
  imports: [CommonModule, SharedModule,ConfigurationRoutingModule]
})
export class ConfigurationModule {}
