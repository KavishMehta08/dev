import { Component, OnInit } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Role } from '../../../models/role';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'wtaf-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {
  routeLinksAdmin: any = [];
  routeLinksTestMgr: any = [];
  routeLinksTestLead: any = [];
  rolename: string;
  Test_Engg: string = '';
  Admin: string = '';
  Test_Mgr: string = '';
  Test_Lead: string = '';
  constructor(private router: Router) {
    this.routeLinksAdmin = [
      {
        label: 'Brand',
        link: 'brand'
      },
      {
        label: 'Appliance Category',
        link: 'appliance_category'
      },
      {
        label: 'Appliance',
        link: 'appliance'
      },
      {
        label: 'Element',
        link: 'element'
      },
      {
        label: 'Keyword',
        link: 'keyword'
      },
      {
        label: 'User Role',
        link: 'user_role'
      },
      {
        label: 'User',
        link: 'user'
      },
      {
        label: 'Capability',
        link: 'capability'
      },
      {
        label: 'Language',
        link: 'language'
      },
      {
        label: 'Language Label',
        link: 'language_label'
      },
      {
        label: 'Capability Template',
        link: 'capability-template'
      },
      {
        label: 'BrandWise Capability',
        link: 'brandwise-capability'
      }
    ];

    this.routeLinksTestMgr = [
      {
        label: 'Brand',
        link: 'brand'
      },
      {
        label: 'Appliance Category',
        link: 'appliance_category'
      },
      {
        label: 'Appliance',
        link: 'appliance'
      },
      {
        label: 'Element',
        link: 'element'
      },
      {
        label: 'Keyword',
        link: 'keyword'
      },
      {
        label: 'User',
        link: 'user'
      },
      {
        label: 'Capability',
        link: 'capability'
      },
      {
        label: 'Language',
        link: 'language'
      },
      {
        label: 'Language Label',
        link: 'language_label'
      },
      {
        label: 'Capability Template',
        link: 'capability-template'
      },
      {
        label: 'BrandWise Capability',
        link: 'brandwise-capability'
      }
    ];

    this.routeLinksTestLead = [
      {
        label: 'Element',
        link: 'element'
      },
      {
        label: 'BrandWise Capability',
        link: 'brandwise-capability'
      }
    ];
  }

  ngOnInit() {
    this.rolename = localStorage.getItem('userRole');
    this.Test_Engg = Role.Test_Engineer;
    this.Admin = Role.Admin;
    this.Test_Lead = Role.Test_Lead;
    this.Test_Mgr = Role.Test_Manager;
    //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'dashboard';
    if (this.rolename == this.Test_Engg) {
      this.router.navigate(['/dashboard']);
    }
    if (this.rolename == this.Admin || this.rolename == this.Test_Mgr) {
      this.router.navigate(['/configuration/brand']);
    }
    if (this.rolename == this.Test_Lead) {
      this.router.navigate(['/configuration/element']);
    }
  }
}
