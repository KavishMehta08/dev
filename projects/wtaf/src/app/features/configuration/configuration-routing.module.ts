import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigurationComponent } from './configuration/configuration.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigurationComponent,
    data: { title: '' },
    children: [
      {
        path: '',
        redirectTo: 'brand',
        pathMatch: 'full'
      },
      {
        path: 'region',
        loadChildren: () =>
          import('../region/region.module').then(m => m.RegionModule)
      },
      {
        path: 'brand',
        loadChildren: () =>
          import('../brand/brand.module').then(m => m.BrandModule)
      },
      {
        path: 'appliance_category',
        loadChildren: () =>
          import('../appliance_category/appliance_category.module').then(
            m => m.ApplianceCategoryModule
          )
      },
      {
        path: 'appliance',
        loadChildren: () =>
          import('../appliance/appliance.module').then(m => m.ApplianceModule)
      },
      {
        path: 'keyword',
        loadChildren: () =>
          import('../keyword/keyword.module').then(m => m.KeywordModule)
      },
      {
        path: 'user_role',
        loadChildren: () =>
          import('../role/role.module').then(m => m.RoleModule)
      },
      {
        path: 'user',
        loadChildren: () =>
          import('../user/user.module').then(m => m.UserModule)
      },
      {
        path: 'capability',
        loadChildren: () =>
          import('../capability/capability.module').then(
            m => m.CapabilityModule
          )
      },
      {
        path: 'element',
        loadChildren: () =>
          import('../element/element.module').then(m => m.ElementModule)
      },
      {
        path: 'language',
        loadChildren: () =>
          import('../language/language.module').then(m => m.LanguageModule)
      },
      {
        path: 'capability-template',
        loadChildren: () =>
          import('../capability-template/capability-template.module').then(
            m => m.CapabilityTemplateModule
          )
      },
      {
        path: 'language_label',
        loadChildren: () =>
          import('../language_label/language_label.module').then(
            m => m.LanguageLabelModule
          )
      },
      {
        path: 'brandwise-capability',
        loadChildren: () =>
          import('../brandwise-capability/brandwise-capability.module').then(
            m => m.BrandwiseCapabilityModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule {}
