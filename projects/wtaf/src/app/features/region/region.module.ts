import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { RegionComponent } from './region/region.component';

import { RegionRoutingModule } from './region-routing.module';
@NgModule({
  declarations: [RegionComponent],
  imports: [CommonModule, SharedModule,RegionRoutingModule]
})
export class RegionModule {}
