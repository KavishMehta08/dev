import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'wtaf-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.css']
})
export class RegionComponent implements OnInit {
  submitted = false;
  successMessage: String = '';
  errorMessage: String = '';
  regionList: any = [];
  regionId: Number = 0;
  userId: number;

  regionForm = this.formBuilder.group({
    regionName: ['', [Validators.required]],
    regionCode: ['', [Validators.required]],
    regionDesc: ['']
  });
  constructor(public toastr: ToastrManager, private formBuilder: FormBuilder, private dataservice: DataService) {

  }

  ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log("local usr id", this.userId);
    this.getAllRegion();
  }
  get f() { return this.regionForm.controls; }

  /* add and update method for region */
  saveRegion() {
    this.submitted = true;
    if (this.regionForm.valid) {
      const regionDetails = this.regionForm.value;
      regionDetails.userId = this.userId;
      regionDetails.regionId = this.regionId;
      this.dataservice.saveRegion(regionDetails).subscribe((response) => {
        if (this.regionId != 0) {
          this.toastr.successToastr("Region updated successfully");
        }
        else {
          this.toastr.successToastr("Region added successfully");
        }
        this.clearData();
        this.getAllRegion();
      });
    }
  }

  /* get region by region Id */
  getByRegionId(regionId) {
    this.regionId = regionId;
    this.dataservice.getRegionById(regionId).subscribe((response) => {
      this.regionForm.patchValue(response);
    });
  }

  /* get all regions  */
  getAllRegion() {
    this.dataservice.getAllRegion().subscribe((response: {}) => {
      this.regionList = response;
    });
  }

  /* this method clear all messages and region form also */
  clearData() {
    this.submitted = false;
    this.regionForm.reset();
    setTimeout(() => {
      this.successMessage = '';
    }, 2000);
    this.regionId = 0;
  }

  /* delete region */
  deleteRegion(regionId) {
    var isDelete = confirm('Are you sure that you want to permanently delete this Region?');
    if (isDelete) {
      this.dataservice.deleteRegion(regionId).subscribe((response) => {
        this.toastr.successToastr("Region deleted successfully");
        this.getAllRegion();
        this.clearData();
      });
    }
  }

}
