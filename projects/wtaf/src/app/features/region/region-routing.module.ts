import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegionComponent } from './region/region.component';

const routes: Routes = [
  {
    path: '',
    component: RegionComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegionRoutingModule {}
