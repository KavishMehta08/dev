import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { BrandComponent } from './brand/brand.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { BrandRoutingModule } from './brand-routing.module';
@NgModule({
  declarations: [BrandComponent],
  imports: [CommonModule, SharedModule,BrandRoutingModule,Ng2SearchPipeModule]
})
export class BrandModule {}
