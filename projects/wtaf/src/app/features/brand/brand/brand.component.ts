import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormGroupDirective
} from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { Type } from '../../models';
import { ChangeDetectorRef } from '@angular/core';
import { async } from '@angular/core/testing';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from '../../../shared/services/loader.service';

@Component({
  selector: 'wtaf-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {
  submitted = false;

  brandId: Number = 0;
  successMessage: String = '';
  errorMessage: String = '';
  brandNameerrorMessage: String = '';
  brandList: any = [];
  filterData: any = [];
  regionList: any = [];
  projectTypeList: any = [];
  inValid: Boolean = false;
  invalidBrandCode: Boolean = false;
  invalidBrandName: Boolean = false;

  txtSearch = '';
  strOldBrandCode = '';
  strOldBrandName = '';
  regionId = 0;
  userId:number;
  brandForm = this.formBuilder.group({
    brandCode: ['', [Validators.required,noWhitespaceValidator]],
    brandName: ['', [Validators.required,noWhitespaceValidator]],
    regionId: ['', [Validators.required]],
    projectTypeId: ['', [Validators.required]],
    brandDesc: ['']
  });
  @ViewChild(FormGroupDirective, { static: false })
  formGroupDirective: FormGroupDirective;
  constructor(
    private ref: ChangeDetectorRef,
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    public loaderService: LoaderService

  ) {}
  async ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log("local usr id", this.userId);
    this.getAllBrand();
    await this.getAllProjectType();
  }

  get f() {
    return this.brandForm.controls;
  }
 //  Added by Akash - Sorting Table Asc Desc
orderedArray = [{
  projectTypeName : "ASC",
  regionCode: "ASC",
  brandCode: "ASC",
  brandName: "ASC",
}]
sortTable(key) {
  this.loaderService.show();
  let order = this.orderedArray[0][key];
  this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
  this.brandList = this.sort_by_key(this.brandList, key, order);
  console.log(this.brandList);
  this.loaderService.hide();
}
sort_by_key(array, key, order) {
  if (order === "ASC") {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      
      if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '' ) {
      return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
      }
    });
  }
  else {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '' ) {
      return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
      }
    });
  }
}
  changeRegion() {
    if (this.brandId == 0) {
      this.brandForm = this.formBuilder.group({
        projectTypeId: [this.brandForm.value.projectTypeId],
        brandCode: ['', [Validators.required, noWhitespaceValidator]],
        brandName: ['', [Validators.required,noWhitespaceValidator]],
        regionId: [this.brandForm.value.regionId, [Validators.required]],
        brandDesc: ['']
      });
      this.errorMessage = '';
      this.brandNameerrorMessage = '';
      this.strOldBrandCode = '';
      this.strOldBrandName = '';
    }
  }

  // --------Get All Project Types
  async getAllProjectType() {
    
    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        // Success
        this.projectTypeList = response;
        
        if (response != null) {
          if (this.projectTypeList.length == 1) {
          } else {
            this.brandForm.controls['projectTypeId'].setValue('');
          }
        }
      });
  }

  changeProjectType(event) {
    
    this.regionList = [];
    if (this.brandForm.controls['projectTypeId'].value != '') {
      this.getAllRegion_byProjectTypeId(
        this.brandForm.controls['projectTypeId'].value
      );
    }
  }

  /* for duplication check */
  async checkDuplication(data) {
    this.errorMessage = '';
    const brandDetails = this.brandForm.value;

    if (data == 'brandName') {
      if (
        this.brandId != 0 &&
        this.regionId == this.brandForm.value.regionId &&
        this.strOldBrandName.toString().trim() ===
          this.brandForm.value.brandName.toString().trim() &&
        this.strOldBrandName != ''
      ) {
        return;
      }

      brandDetails.resource = this.brandForm.value.brandName.toString().trim();
    }
    if (data == 'brandCode') {
      if (
        this.brandId != 0 &&
        this.regionId == this.brandForm.value.regionId &&
        this.strOldBrandCode.toString().trim() ===
          this.brandForm.value.brandCode.toString().trim() &&
        this.strOldBrandCode != ''
      ) {
        return;
      }

      brandDetails.resource = this.brandForm.value.brandCode.toString().trim();
    }

    brandDetails.type = Type.Brand;
    brandDetails.id = this.brandForm.value.regionId;

    if (brandDetails.id != 0 && brandDetails.id != '') {
      await this.dataservice
        .checkDuplication(brandDetails)
        .toPromise()
        .then((response: Boolean) => {
          this.inValid = response;
          if (data == 'brandName') {
            this.invalidBrandName = this.inValid;
            this.errorMessage = '';
            if (this.invalidBrandName) {
              this.toastr.errorToastr(
                'Brand name "'+brandDetails.resource+'" is already in use for selected region\n '
              );
            } else if (
              this.invalidBrandCode == false &&
              this.invalidBrandName == false
            ) {
              this.errorMessage = '';
              this.brandNameerrorMessage = '';
            }
          } else if (data == 'brandCode') {
            this.invalidBrandCode = this.inValid;
            this.brandNameerrorMessage = '';
            if (this.invalidBrandCode) {
              this.toastr.errorToastr(
                '\nBrand code "'+brandDetails.resource+'" is already in use for selected region'
              );
              }
          }
        });
    }
  }
  /* add and update method for brand */
  async saveBrand() {
    this.submitted = true;
    this.invalidBrandCode = false;
    this.invalidBrandName = false;

    if (this.brandForm.value.brandCode.trim() != '') {
      await this.checkDuplication('brandCode');
    }
    if (this.brandForm.value.brandName.trim() != '') {
      await this.checkDuplication('brandName');
    }

    if (
      this.brandForm.valid &&
      !this.invalidBrandCode &&
      !this.invalidBrandName
    ) {
      const brandDetails = this.brandForm.value;
      brandDetails.regionId = parseInt(this.brandForm.value.regionId);
      brandDetails.userId = this.userId;
      brandDetails.brandId = this.brandId;
      this.dataservice.saveBrand(brandDetails).subscribe(response => {
        if (this.brandId != 0) {
          this.toastr.successToastr('Brand updated successfully');
        } else {
          this.toastr.successToastr('Brand created successfully');
        }
        this.clearData(1);
        this.getAllBrand();
      });
    }
  }

  /* get brand by brand Id */
  getByBrandId(brandId) {
    this.clearData(1);
    
    this.brandId = brandId;
    this.dataservice.getByBrandId(brandId).subscribe(async response => {
      await this.getAllRegion_byProjectTypeId(response['projectTypeId']);
      this.brandForm.patchValue(response);

      this.regionId = response['regionId'];
      this.strOldBrandName = response['brandName'];
      this.strOldBrandCode = response['brandCode'];
      window.scroll(0, 0);
    });
  }

  /* get all brands */
  getAllBrand() {
    this.dataservice.getAllBrand().subscribe((response: {}) => {
      this.brandList = response;
      console.log(response);
      
      
      this.dataservice.customFilter['regionCode'] = '';
    });
  }

  /* get all regions */
  getAllRegion() {
    this.dataservice.getAllRegion().subscribe((response: {}) => {
      this.regionList = response;
      if (response != null) {
        if (this.regionList.length == 1) {
          this.brandForm.controls['regionId'].setValue(
            this.regionList[0].regionId
          );
        } else {
          this.brandForm.controls['regionId'].setValue('');
        }
      }
    });
  }

  async getAllRegion_byProjectTypeId(typeId) {
    await this.dataservice
      .getAllRegionByProjectType(typeId)
      .toPromise()
      .then(async (response: {}) => {
        this.regionList = response;
        console.log("get all region ",this.regionList);
        let obj = this.regionList.find(o => o.defaultRegion === true);
        console.log("obj--------",obj);
        // to place NAR region at first position in drop down
        this.regionList.unshift(this.regionList.splice(this.regionList.findIndex(item => item.regionName === 'NAR'), 1)[0])
        console.log(this.regionList);
         
        if (response != null) {
          if (this.regionList.length == 1) {
            this.brandForm.controls['regionId'].setValue(
              this.regionList[0].regionId
            );
          } else {
            this.brandForm.controls['regionId'].setValue('');
          }
        }
      });
  }
  /*reset the region form*/
  clearData(val) {
    
    // this.txtSearch =" ";
    var r = false;
    if ((this.brandForm.value.projectTypeId != 0 || this.brandForm.value.brandName != 0 || this.brandForm.value.brandCode != 0
      || this.brandForm.value.brandDesc != 0)&& val==0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
    this.submitted = false;
    this.brandForm.reset();
    setTimeout(() => {
      this.successMessage = '';
    }, 2000);
    this.brandId = 0;
    this.brandForm.controls['regionId'].setValue('');
    this.errorMessage = '';
    this.inValid = false;
    this.brandNameerrorMessage = '';
    this.strOldBrandName = '';
    this.strOldBrandCode = '';
    this.regionList = [];
    this.brandForm = this.formBuilder.group({
      brandCode: ['', [Validators.required,noWhitespaceValidator]],
      brandName: ['', [Validators.required, noWhitespaceValidator]],
      regionId: [this.brandForm.value.regionId, [Validators.required]],
      projectTypeId: ['', [Validators.required]],
      brandDesc: ['']
    });
  }
  }

  /* delete the brands by brand Id */
  deleteBrand(brandId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this brand?'
    );
    if (isDelete) {
      this.dataservice.deleteBrandById(brandId).subscribe(
        response => {
          this.toastr.successToastr('brand deleted successfully');
          this.getAllBrand();
          this.clearData(1);
        },
        error => {
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
          setTimeout(() => {
            this.errorMessage = '';
            this.dataservice.errorWhenInUse = '';
          }, 2000);
        }
      );
    }
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}
