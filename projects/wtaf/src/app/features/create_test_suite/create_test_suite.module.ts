import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { CreateTestSuiteComponent } from './create-test-suite/create-test-suite.component';

import { CreateTestSuiteRoutingModule } from './create_test_suite-routing.module';
@NgModule({
  declarations: [CreateTestSuiteComponent],
  imports: [CommonModule, SharedModule,CreateTestSuiteRoutingModule]
})
export class CreateTestSuiteModule {}
