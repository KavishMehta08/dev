import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import * as XLSX from 'xlsx';
import { DataService } from '../../../shared/services/data.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { Type } from '../../models';

//import * as $ from 'jquery';
declare var $: any;
@Component({
  selector: 'wtaf-create-test-suite',
  templateUrl: './create-test-suite.component.html',
  styleUrls: ['./create-test-suite.component.css']
})
export class CreateTestSuiteComponent implements OnInit {
  listIteraions: any = [];
  widthfooter = '0%';
  submitted = false;
  isTextChanged = false;
  successMessage: String = '';
  errorMessage: String = '';
  brandList: any = [];
  regionList: any = [];
  brandList1: any = [];
  projectTypeList: any = [];
  platformList: any = [];
  applianceList: any = [];
  platformId: Number = 0;
  brandId: Number = 0;
  applianceId: Number = 0;
  selectedFile: File;
  regionId: Number = 0;
  testSteps: any = [];
  steps = '';
  oldbrandId: Number = 0;
  oldtestSuiteName: any = [];
  oldjiraProjectName: any = [];
  oldtestSuiteDesc: any = [];
  allProjectName: any = [];
  inValid: Boolean = false;
  AllJiraProjectNameRes: any = [];
  files: File;
  args: any;
  CaseJson = [];
  TestCase: any = [];
  testcaseName: string;
  testSuiteId: number = 0;
  isPlatformNotSelected: Boolean = false;
  isCopying: Boolean = false;
  projectTypeId: Number = 0;
  spacepattern = '/^\S*$/';
  testSuiteForm = this.formBuilder.group({
    brandId: ['', [Validators.required]],
    regionId: ['', [Validators.required]],
    testSuiteName: ['', [Validators.required,noWhitespaceValidator]],
    testSuiteDesc: [''],
    jiraProjectName: ['', Validators.required],
    projectName: ['', [Validators.required,noWhitespaceValidator]],
    applianceId: ['', [Validators.required]],
    platformId: ['', [Validators.required]],
    OldRegionId: [''],
    projectTypeId: ['', [Validators.required]]
  });

  name = 'This is XLSX TO JSON CONVERTER';
  willDownload = false;
  data: any = [];
  strOldName = '';
  isClicked = false;

  oldBrandName = '';
  oldRegionName = '';
  oldAppliance = '';
  oldPlatformName = '';
  oldProjectType = '';
  oldProjectName = '';
  LoaderService: any;
  userId: number;

  public onFileRemove(args): void {
    args.cancel = true;
  }
  @ViewChild('importFile', { static: false }) importFile: ElementRef;

  constructor(
    public loaderService: LoaderService,
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  async ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log("local usr id", this.userId);
    this.files = null;
    this.isClicked = false;
    this.testSuiteId = parseInt(
      this.activatedRoute.snapshot.paramMap.get('id')
    );
    this.isCopying =
      this.activatedRoute.snapshot.paramMap.get('flag') == null
        ? false
        : Boolean(this.activatedRoute.snapshot.paramMap.get('flag'));
    if (isNaN(this.testSuiteId)) {
      this.testSuiteId = 0;
    }

    // StartSpinner---Akash--
    // this.loaderService.show();
    // this.loaderService.show();
    // this.getAllRegion();
    // await this.getAllAppliance();
    this.getAllProjectType();
    // this.getAllProjectName();
    await this.getAllJiraProjectsName();
    if (this.testSuiteId > 0) {
      this.testSuiteForm.controls['brandId'].setValue('');
      this.testSuiteForm.controls['regionId'].setValue('');
      this.testSuiteForm.controls['projectTypeId'].setValue('');
      this.testSuiteForm.controls['platformId'].setValue('');
      this.testSuiteForm.controls['applianceId'].setValue('');
      await this.getTestSuiteById(this.testSuiteId);
    }

    let data1 = [
      {
        Appliance: 'Minerva',
        Element: '',
        Keyword: 'ForceWait',
        MobileApp: 'WHP',
        Module: 'Login',
        Platform: 'Both',
        TestDefinition: '',
        TestStepDescription: 'wait for app to load',
        TestStepID: 0,
        TestcaseID: 1,
        TestcaseName: 'Login',
        Version: '4.0.17',
        aosData: 5000,
        iosData: 5000
      },
      {
        Appliance: 'Minerva',
        Element: 'WhirlpoolLogo',
        Keyword: 'WaitForElement',
        MobileApp: 'WHP',
        Module: 'Login',
        Platform: 'Both',
        TestDefinition: '',
        TestStepDescription: 'Wait for App home page',
        TestStepID: 1,
        TestcaseID: 1,
        TestcaseName: 'Login',
        Version: '4.0.17',
        aosData: '',
        iosData: ''
      }
    ];

    var dt = data1.reduce((a, b) => {
      a[b.Module] = a[b.Module] || [];
      return a;
    }, {});

    for (let k = 0; k < data1.length; k++) {
      let mod = data1[k].Module;

      dt[mod].push({
        element: data1[k].Element,
        keyword: data1[k].Keyword,
        module: '',
        testStepDesc: data1[k].TestStepDescription
      });
    }
    console.log('dt', dt);
    setTimeout(() => {
      $('.js-example-basic-single').select2();
    }, 1300);
  }
  get f() {
    return this.testSuiteForm.controls;
  }

  //  Added by Akash - Sorting Table Asc Desc
  sortTable(c) {
    var table,
      rows,
      switching,
      i,
      x,
      y,
      shouldSwitch,
      dir,
      switchcount = 0;
    table = document.getElementById('createTestSuiteTable');
    switching = true;
    //Set the sorting direction to ascending:
    dir = 'asc';
    while (switching) {
      switching = false;
      rows = table.rows;
      for (i = 1; i < rows.length - 1; i++) {
        shouldSwitch = false;
        x = rows[i].getElementsByTagName('TD')[c];
        y = rows[i + 1].getElementsByTagName('TD')[c];
        if (dir == 'asc') {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        } else if (dir == 'desc') {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        switchcount++;
      } else {
        if (switchcount == 0 && dir == 'asc') {
          dir = 'desc';
          switching = true;
        }
      }
    }
  }
  changeTestSuiteName(e) {
    if (
      this.strOldName.toString().trim() != e.target.value.toString().trim() &&
      this.strOldName != ''
    ) {
      this.isTextChanged = true;
    } else if (this.testSuiteId == 0) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  /* add and update method for test suite */
  saveTestSuite() {
    this.submitted = true;

    if (this.files == null && (this.isTextChanged || this.isCopying)) {
      const testSuiteFormDetails = this.testSuiteForm.value;
      if (
        testSuiteFormDetails['testSuiteName'] != 0 &&
        testSuiteFormDetails['testSuiteName'] != null &&
        testSuiteFormDetails['testSuiteName'] != undefined &&
        testSuiteFormDetails['testSuiteName'].trim() != ' '
      ) {
        let suiteName = testSuiteFormDetails['testSuiteName'];
        testSuiteFormDetails.resource = this.testSuiteForm.value.testSuiteName
          .toString()
          .trim();
        testSuiteFormDetails.type = Type.TestSuite;
        console.log("testSuiteFormDetails.brandId------>", testSuiteFormDetails.brandId);
        testSuiteFormDetails.id = parseInt(this.testSuiteForm.value.brandId);
        this.dataservice
          .checkDuplication(testSuiteFormDetails)
          .subscribe((response: Boolean) => {
            this.inValid = response;
            if (this.inValid) {
              this.isClicked = false;
              this.toastr.errorToastr(
                'Test suite "' + suiteName + '" already exists'
              );
            } else {
              this.errorMessage = '';
              this.saveData();
            }
          });
      }
    } else {
      this.saveData();
    }
  }

  saveData() {

    if (
      this.testSuiteForm.value.projectTypeId > 0 &&
      this.platformList != null
    ) {
      let platformId = this.testSuiteForm.value.platformId;
      if (platformId == 0) {
        this.isPlatformNotSelected = true;
      } else {
        this.isPlatformNotSelected = false;
      }
    } else {
      this.isPlatformNotSelected = false;
    }
    if (this.testSuiteForm.valid && !this.isPlatformNotSelected) {
      this.isClicked = true;

      const testSuiteDetails = this.testSuiteForm.value;
      if (this.AllJiraProjectNameRes != null) {
        const jiraProjectKeyArray = this.AllJiraProjectNameRes.find(
          s => s.projectName == this.testSuiteForm.value.jiraProjectName
        );
        testSuiteDetails.jiraProjectKey = jiraProjectKeyArray.projectKey;
      }
      testSuiteDetails.testSuiteId = this.testSuiteId;
      testSuiteDetails.userId = this.userId;
      let suiteName = testSuiteDetails['testSuiteName'];
      if (this.files == null) {
        if (this.isCopying == true) {
          this.dataservice
            .cloneTestSuite(testSuiteDetails)
            .subscribe(response => {
              let savedTestSuiteId = response['savedTestSuiteId'];
              this.dataservice.syncWithJira(savedTestSuiteId).subscribe(res => {
                console.log('jira synch response', res);
              });
              this.toastr.successToastr(
                'Test suite ' + suiteName + 'successfully created. '
              );
              this.clearData(1);
              this.router.navigate(['test_suite/search_test_suite', { isFromCreateSuite: true }]);
            });
        } else {
          this.dataservice
            .saveTestSuite(testSuiteDetails)
            .subscribe(response => {
              if (
                this.testSuiteId != 0 &&
                this.testSuiteId != null &&
                this.testSuiteId != undefined
              ) {
                this.toastr.successToastr(
                  'Test suite  ' + suiteName + ' successfully updated. '
                );
              } else {
                this.toastr.successToastr(
                  'Test suite  ' + suiteName + ' successfully created. '
                );
              }
              this.clearData(1);
              this.loaderService.hide();

              this.router.navigate(['test_suite/search_test_suite', { isFromCreateSuite: true }]);
            });
        }
      } else {
        const testSuiteFormDetails = this.testSuiteForm.value;
        let suiteName = testSuiteFormDetails['testSuiteName'];
        let brandId = testSuiteFormDetails['brandId'];
        console.log("testSuiteFormDetails['brandId']", brandId);

        testSuiteFormDetails.resource = suiteName;
        testSuiteFormDetails.type = Type.TestSuite;
        testSuiteFormDetails.id = parseInt(brandId);
console.log("check duplicate data", testSuiteFormDetails);

        this.dataservice
          .checkDuplication(testSuiteFormDetails)
          .subscribe((response: Boolean) => {

            this.inValid = response;
            if (this.inValid) {
              var isAlradyExists = confirm(
                'This ' +
                '"' +
                suiteName +
                '"' +
                ' suite already exists. Would you like to overwrite? Click OK to confirm or CANCEL to go back.'
              );
              if (isAlradyExists) {
                this.saveAfterImport(testSuiteDetails);
              } else {
                this.clearData(1);
              }
            } else {
              this.saveAfterImport(testSuiteDetails);
            }
          });
      }
    }
  }

  saveAfterImport(testSuiteDetails) {
    console.log(testSuiteDetails);
    this.dataservice
      .saveTestSuiteAfterImport(testSuiteDetails)
      .subscribe(async response => {

        console.log(' this.CaseJson', this.CaseJson);
        if (response != null) {
          if (this.CaseJson.length > 0) {
            this.widthfooter = 0 + '%'.toString();
            $('#popProgress').modal('show');
            for (let k = 0; k < this.CaseJson.length; k++) {
              this.steps =
                'Completed: ' + (k + 1) + '/ ' + this.CaseJson.length;
              const percentDone = Math.round(
                (100 * (k + 1)) / this.CaseJson.length
              );
              this.widthfooter = percentDone + '%'.toString();
              console.log(`File is ${percentDone}% uploaded.`);

              let Json_TestCase = this.CaseJson[k];
              Json_TestCase.userId = this.userId;
              Json_TestCase.testSuiteId = response['savedTestSuiteId'];
              await this.dataservice
                .addImportFileData(Json_TestCase)
                .then(case_res => {
                  console.log(case_res);
                });
            }

            $('#popProgress').modal('hide');
            if (
              this.testSuiteId != 0 &&
              this.testSuiteId != null &&
              this.testSuiteId != undefined
            ) {
              this.clearData(1);
              this.toastr.successToastr(
                'Test suite  ' +
                testSuiteDetails['testSuiteName'] +
                ' successfully updated. '
              );
            } else {
              this.clearData(1);
              this.toastr.successToastr(
                'Test suite  ' +
                testSuiteDetails['testSuiteName'] +
                ' successfully created. '
              );
            }
          }
        }
        this.clearData(1);
        this.router.navigate(['test_suite/search_test_suite']);
      });
  }

  /* get test suite by test suite Id */
  async getTestSuiteById(testSuiteId) {
    this.testSuiteId = testSuiteId;

    await this.dataservice
      .getTestSuiteById(testSuiteId)
      .toPromise()
      .then(async response => {
        // Success Akash
        this.testSuiteForm.controls['projectTypeId'].setValue(
          response['projectTypeId']
        );
        await this.getAllRegion_byProjectTypeId(
          this.testSuiteForm.controls['projectTypeId'].value
        );

        await this.getBrandsByRegionId(response['regionId']);
        await this.getPlatformByProjectTypeId(response['projectTypeId']);

        this.brandId = response['brandId'];
        this.applianceId = response['applianceId'];
        this.platformId = response['platformId'];
        this.strOldName = response['testSuiteName'];
        await this.getApplianceByBrand(this.brandId);

        this.testSuiteForm.controls['regionId'].setValue(response['regionId']);
        this.testSuiteForm.controls['brandId'].setValue(response['brandId']);
        this.testSuiteForm.controls['jiraProjectName'].setValue(
          response['jiraProjectName']
        );
        this.testSuiteForm.controls['testSuiteName'].setValue(
          response['testSuiteName']
        );
        this.testSuiteForm.controls['projectName'].setValue(
          response['projectName']
        );
        this.testSuiteForm.controls['testSuiteDesc'].setValue(
          response['testSuiteDesc']
        );
        this.testSuiteForm.controls['applianceId'].setValue(
          response['applianceId']
        );
        this.testSuiteForm.controls['projectTypeId'].setValue(
          response['projectTypeId']
        );

        //added by mustaid platfrom 7/2/2020 
        this.testSuiteForm.controls['platformId'].setValue(
          response['platformId']
        );

        this.oldjiraProjectName = response['jiraProjectName'];
        this.oldProjectName = response['projectName'];
        this.oldtestSuiteDesc = response['testSuiteDesc'];
        this.oldbrandId = response['brandId'];
        this.oldtestSuiteName = response['testSuiteName'];

        // Added by Akash--------
        if (this.isCopying == true) {

          if (this.regionList != null) {
            let arrRig = this.regionList.filter(
              pl => pl.regionId == response['regionId']
            );
            if (arrRig.length > 0) {
              this.oldRegionName = arrRig[0].regionName.toString().trim();
            }
          }

          if (this.brandList != null) {
            let arrBl = this.brandList.filter(
              pl => pl.brandId == this.oldbrandId
            );
            if (arrBl.length > 0) {
              this.oldBrandName = arrBl[0].brandName.toString().trim();
              console.log("this.oldBrandName............", this.oldBrandName);

            }
          }
          console.log("this.platformList---------", this.platformList);


          if (this.platformList != null) {
            let arrPl = this.platformList.filter(
              pl => pl.platformId == response['platformId']
            );
            if (arrPl.length > 0) {
              this.oldPlatformName = arrPl[0].platformName.toString().trim();
              console.log("this.oldPlatformNam............", this.oldPlatformName);

            }
          }

          if (this.projectTypeList != null) {
            let arrPtl = this.projectTypeList.filter(
              pl => pl.projectTypeId == response['projectTypeId']
            );
            if (arrPtl.length > 0) {
              this.oldProjectType = arrPtl[0].projectTypeName.toString().trim();
            }
          }

          if (this.applianceList != null && this.applianceList != undefined) {
            let arrAl = this.applianceList.filter(
              pl => pl.applianceId == response['applianceId']
            );
            if (arrAl.length > 0) {
              this.oldAppliance = arrAl[0].applianceName.toString().trim();
            }
          }
        }
      });
  }

  /* change region and get brands*/
  changeRegion(e) {
    this.brandId = 0;
    this.regionId = e.target.value;
    this.testSuiteForm.controls['applianceId'].setValue('');
    this.getBrandsByRegionId(parseInt(e.target.value));
  }

  /* get all regions  */
  async getAllRegion() {
    await this.dataservice
      .getAllRegion()
      .toPromise()
      .then(response => {
        this.regionList = response;
        console.log(this.regionList);

        if (response != null) {
          if (this.regionList.length == 1) {
            this.testSuiteForm.controls['regionId'].setValue(
              this.regionList[0].regionId
            );
            this.getBrandsByRegionId(this.regionList[0].regionId);
          } else {
            this.testSuiteForm.controls['regionId'].setValue('');

            let getNarId = this.regionList.filter(function (item) {
              return item.regionName === 'NAR';
            });
            //get brand by region Id of default selected NAR region
            let narId = getNarId[0].regionId;
            this.getBrandsByRegionId(narId);
            if (getNarId != null) {
              this.testSuiteForm.controls['regionId'].setValue(
                getNarId[0].regionId
              );
            }
          }
        }
      });
  }

  /* get all project types  */
  async getAllProjectType() {
    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        this.projectTypeList = response;
        console.log("project type", this.projectTypeList);

        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.testSuiteForm.controls['projectTypeId'].setValue(
              this.projectTypeList[0].projectTypeId
            );
            this.getPlatformByProjectTypeId(
              this.projectTypeList[0].projectTypeId
            );
          } else {
            this.testSuiteForm.controls['projectTypeId'].setValue('');
          }
        }
      });
  }

  /* get all platforms by project type id */
  async changeProjectType(e) {
    this.projectTypeId = e.target.value;
    this.allProjectName = [];
    this.isPlatformNotSelected = false;
    this.regionList = [];
    this.brandList = [];
    this.brandId = 0;
    this.testSuiteForm.controls['jiraProjectName'].setValue('');
    this.testSuiteForm.controls['testSuiteName'].setValue('');
    this.testSuiteForm.controls['projectName'].setValue('');
    this.testSuiteForm.controls['applianceId'].setValue('');
    this.testSuiteForm.controls['brandId'].setValue('');
    this.testSuiteForm.controls['brandId'].setValue('');

    if (this.testSuiteForm.controls['projectTypeId'].value != '') {
      await this.getAllRegion_byProjectTypeId(
        this.testSuiteForm.controls['projectTypeId'].value
      );
    }

    this.getPlatformByProjectTypeId(parseInt(e.target.value));
  }

  async getAllRegion_byProjectTypeId(typeId) {
    await this.dataservice
      .getAllRegionByProjectType(typeId)
      .toPromise()
      .then((response: {}) => {
        this.regionList = response;
        // to place NAR region at first position in frop down
        this.regionList.unshift(this.regionList.splice(this.regionList.findIndex(item => item.regionName === 'NAR'), 1)[0])
        console.log(this.regionList);
        if (response != null) {
          if (this.regionList.length == 1) {
            this.regionId = this.regionList[0].regionId;
            this.testSuiteForm.controls['regionId'].setValue(
              this.regionList[0].regionId
            );
            this.getBrandsByRegionId(this.regionId);
          } else {
            this.testSuiteForm.controls['regionId'].setValue('');
            this.regionId = 0;
          }
        }
      });
  }

  /* get all platforms by project type Id */
  async getPlatformByProjectTypeId(projectTypeId) {
    await this.dataservice
      .getPlatformByProjectTypeId(projectTypeId)
      .toPromise()
      .then((response: {}) => {
        this.platformList = response;
        console.log("platform------------------from api", this.platformList);

        if (response != null) {
          if (this.platformList.length == 1) {
            this.testSuiteForm.controls['platformId'].setValue(
              this.platformList[0].platformId
            );
          } else {
            this.testSuiteForm.controls['platformId'].setValue('');
          }
        } else {
          this.platformId = 0;
          this.testSuiteForm.controls['platformId'].setValue('');
        }
        if (this.platformId > 0) {
          this.testSuiteForm.controls['platformId'].setValue(this.platformId);
        }
      });
  }

  /* get all brands by region Id */
  async getBrandsByRegionId(regionId) {
    await this.dataservice
      .getBrandsByRegionId(regionId)
      .toPromise()
      .then(response => {
        // Success
        console.log('brands', this.brandList);
        this.brandList = response;
        if (response != null) {
          if (this.brandList.length == 1) {
            this.brandId = this.brandList[0].brandId;
            this.testSuiteForm.controls['brandId'].setValue(
              this.brandList[0].brandId
            );
          } else {
            this.testSuiteForm.controls['brandId'].setValue('');
          }
        } else {
          this.brandId = 0;
          this.testSuiteForm.controls['brandId'].setValue('');
        }

        if (this.brandId > 0) {
          this.testSuiteForm.controls['brandId'].setValue(this.brandId);
        }
        this.getApplianceByBrand(this.brandId);
      });
  }

  /* this method clear all messages and test case form also */
  clearData(val) {
    var r = false;
    if ((this.testSuiteForm.value.brandId != 0 || this.testSuiteForm.value.regionId != 0 || this.testSuiteForm.value.testSuiteName != 0 || this.testSuiteForm.value.testSuiteDesc != 0 || this.testSuiteForm.value.jiraProjectName != 0
      || this.testSuiteForm.value.projectName != 0 || this.testSuiteForm.value.applianceId != 0 || this.testSuiteForm.value.platformId != 0 || this.testSuiteForm.value.projectTypeId != 0) && val == 0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.importFile.nativeElement.value = '';
      let args: any;
      this.isClicked = false;
      this.submitted = false;
      this.testSuiteForm.reset();
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      this.testSuiteForm = this.formBuilder.group({
        brandId: ['', [Validators.required]],
        regionId: ['', [Validators.required]],
        testSuiteName: ['', [Validators.required,noWhitespaceValidator]],
        testSuiteDesc: [''],
        jiraProjectName: ['', Validators.required],
        projectName: ['', [Validators.required,noWhitespaceValidator]],
        applianceId: ['', [Validators.required]],
        platformId: ['', [Validators.required]],
        OldRegionId: [''],
        projectTypeId: ['', [Validators.required]]
      });
      this.listIteraions = [];
      this.testSuiteId = 0;
      this.errorMessage = '';
      this.brandId = 0;
      this.platformId = 0;
      this.testSuiteForm.controls['brandId'].setValue('');
      this.testSuiteForm.controls['regionId'].setValue('');
      let getNarId = this.regionList.filter(function (item) {
        return item.regionName === 'NAR';
      });
      //get brand by region Id of default selected NAR region
      if (getNarId.length > 0) {
        let narId = getNarId[0].regionId;
        this.getBrandsByRegionId(narId);
        if (getNarId != null) {
          this.testSuiteForm.controls['regionId'].setValue(getNarId[0].regionId);
        }
      }
      this.testSuiteForm.controls['projectTypeId'].setValue('');
      this.testSuiteForm.controls['platformId'].setValue('');
      this.testSuiteForm.controls['applianceId'].setValue('');
      this.testSuiteForm.controls['jiraProjectName'].setValue('');
      this.testSuiteForm.controls['regionId'].setValue('');
      this.testSuiteForm.controls['brandId'].setValue('');
      this.regionList = [];
      this.brandList = [];
      this.platformList = [];
      this.data = [];
      this.selectedFile = null;
      this.CaseJson = [];
      this.strOldName = '';
      this.isPlatformNotSelected = false;
    }
  }

  async getAllJiraProjectsName() {
    await this.dataservice
      .getAllJiraProjectsName()
      .toPromise()
      .then(allJiraProjNameRes => {
        // Success

        this.AllJiraProjectNameRes = allJiraProjNameRes;
        console.log(
          'AllJiraProjectNameRes------->',
          this.AllJiraProjectNameRes
        );
      });
  }

  getAllProjectName() {
    this.dataservice
      .getProjectNameByTypeId(this.brandId, this.projectTypeId, this.regionId)
      .toPromise()
      .then(allProjNameRes => {
        // Success
        this.allProjectName = allProjNameRes;
        console.log('all product names', this.allProjectName);
      });
  }

  // -----------------------------------------------XLSX-------------------------------------------------------

  onFileChange(ev) {

    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = event => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});

      const dataString = JSON.stringify(jsonData);
      console.log('dataString-->', dataString);
      document.getElementById('output').innerHTML = dataString;
    };
    reader.readAsBinaryString(file);
  }

  parseExcel(file) {

    this.listIteraions = [];
    this.errorMessage = '';
    this.selectedFile = file;
    this.CaseJson = [];
    console.log('this.selectedFile', this.selectedFile);

    let reader = new FileReader();
    reader.onload = e => {
      let data = (<any>e.target).result;
      let workbook = XLSX.read(data, {
        type: 'binary'
      });
      let isExecuteCount = 0;
      workbook.SheetNames.forEach(
        function (sheetName) {
          // Here is your object
          isExecuteCount = isExecuteCount + 1;
          if (isExecuteCount <= 1) {
            let XL_row_object = XLSX.utils.sheet_to_json(
              workbook.Sheets[sheetName],
              { defval: '' }
            );
            if (XL_row_object.length != 0) {
              let IsInsertData = true;

              const TestData_cols = [];
              for (let k = 0; k < XL_row_object.length; k++) {
                TestData_cols.push({});
                for (const [key, value] of Object.entries(XL_row_object[k])) {
                  TestData_cols[k][key.toUpperCase()] = value;
                }
              }

              XL_row_object = TestData_cols;


              let rownum = 0;
              const header = []
              if (
                !XL_row_object[0].hasOwnProperty(('MOBILEAPP')) ||
                !XL_row_object[0].hasOwnProperty('APPLIANCE') ||
                !XL_row_object[0].hasOwnProperty('MODULE') ||
                !XL_row_object[0].hasOwnProperty('PLATFORM') ||
                !XL_row_object[0].hasOwnProperty('TESTCASEID') ||
                !XL_row_object[0].hasOwnProperty('TESTSTEPID') ||
                !XL_row_object[0].hasOwnProperty('KEYWORD') ||
                !XL_row_object[0].hasOwnProperty('ELEMENT') ||
                !XL_row_object[0].hasOwnProperty('TESTCASENAME') ||
                !XL_row_object[0].hasOwnProperty('TESTSTEPDESCRIPTION')
              ) {
                this.importFile.nativeElement.value = "";
                this.toastr.errorToastr("Selected File has invalid columns");
                return;
              }
              let isIteration_exist = 0;
              for (let m = 0; m < XL_row_object.length; m++) {

                if (m < XL_row_object.length - 1) {
                  if (
                    XL_row_object[m]['MOBILEAPP'].toString() == '' ||
                    XL_row_object[m]['APPLIANCE'].toString() == '' ||
                    XL_row_object[m]['MODULE'].toString() == '' ||
                    XL_row_object[m]['PLATFORM'].toString() == '' ||
                    XL_row_object[m]['TESTCASEID'].toString() == '' ||
                    XL_row_object[m]['TESTSTEPID'].toString() == '' ||
                    XL_row_object[m]['KEYWORD'].toString() == '' ||
                    XL_row_object[m]['TESTCASENAME'].toString() == '' ||
                    XL_row_object[m]['TESTSTEPDESCRIPTION'].toString() == ''
                  ) {
                    IsInsertData = false;
                    rownum = rownum + 1;
                    console.log('rownum', rownum);

                    break;
                  }
                }
                if (IsInsertData == true) {
                  rownum = rownum + 1;
                }
              }
              if (IsInsertData == true) {
                console.log('XL_row_object-->', XL_row_object);
                let json_object = JSON.stringify(XL_row_object);
                console.log('json_object-->', json_object);
                this.data = XL_row_object;
                this.testSteps = this.data.reduce((a, b) => {
                  a[b.TESTCASENAME] = a[b.TESTCASENAME] || [];
                  return a;
                }, {});
                for (let k = 0; k < this.data.length; k++) {
                  this.testcaseName = this.data[k].TESTCASENAME;
                  if (this.testcaseName != '') {
                    let iterations: any = [];
                    this.listIteraions = [];
                    Object.keys(this.data[k]).map(key => {
                      if (
                        key != 'TestDefinition' &&
                        key != 'MOBILEAPP' &&
                        key != 'VERSION' &&
                        key != 'APPLIANCE' &&
                        key != 'MODULE' &&
                        key != 'PLATFORM' &&
                        key != 'TESTCASEID' &&
                        key != 'TESTCASENAME' &&
                        key != 'TESTSTEPID' &&
                        key != 'TESTSTEPDESCRIPTION' &&
                        key != 'ELEMENT' &&
                        key != 'KEYWORD' &&
                        key != '__EMPTY'
                      )
                        if (key.substr(0, 9).toLowerCase() == 'Iteration'.toLowerCase()) {
                          isIteration_exist = 1;
                          this.listIteraions.push({ IterationHeading: key });
                          iterations.push({ dataSetName: this.data[k][key] });
                        }
                    });

                    console.log('iterations------------>', iterations);

                    this.testSteps[this.testcaseName].push(
                      {
                        element: (this.data[k].ELEMENT).trim(),
                        keyword: this.data[k].KEYWORD,
                        module: this.data[k].MODULE,
                        iterations: iterations,
                        testStepDesc: this.data[k].TESTSTEPDESCRIPTION
                      }
                    );
                  }
                }

                console.log('----------->', this.testSteps);
                if (isIteration_exist == 1) {
                  $('#importTable').show();

                  let keys = Object.keys(this.testSteps);
                  for (let l = 0; l < keys.length; l++) {
                    this.TestCase = keys[l];
                    if (this.TestCase != '') {
                      let testSteps = [];
                      for (
                        let m = 0;
                        m < this.testSteps[this.TestCase].length;
                        m++
                      ) {
                        testSteps.push({
                          element: this.testSteps[this.TestCase][m].element,
                          keyword: this.testSteps[this.TestCase][m].keyword,
                          module: this.testSteps[this.TestCase][m].module,
                          testStepDesc: this.testSteps[this.TestCase][m]
                            .testStepDesc,
                          iterations: this.testSteps[this.TestCase][m]
                            .iterations
                        });
                      }
                      this.CaseJson.push({
                        testCaseName: this.TestCase,
                        testSteps: testSteps
                      });
                    }
                  }
                } else {

                  $('#importTable').hide();
                  this.importFile.nativeElement.value = '';
                  this.data = [];
                  this.selectedFile = null;
                  return this.toastr.errorToastr(
                    'Selected file requires Iteration column'
                  );
                }
                console.log(this.CaseJson);
                console.log('this.data-->', this.data);
              } else {

                this.importFile.nativeElement.value = '';
                this.toastr.errorToastr(
                  'Please fill mandatory field into excel,that you have selected...'
                );
              }
            }
          }

          // bind the parse excel file data to Grid
        }.bind(this),
        this
      );
    };

    reader.onerror = function (ex) {
      console.log(ex);
    };
    reader.readAsBinaryString(file);
  }

  public onSuccess(args: any): void {
    this.data = [];
    this.errorMessage = '';
    this.files = args.target.files;
    if (args.target.files.length > 0) {
      // FileList object
      this.parseExcel(this.files[0]);
    }
  }

  /* get all applience by brandId Id */
  async getApplianceByBrand(brandId) {
    await this.dataservice.getApplianceByBrand(brandId).then(res => {
      // Success
      this.applianceList = res;
      if (res != null) {
        if (this.applianceList.length == 1) {
          this.applianceId = this.applianceList[0].applianceId;
          this.testSuiteForm.controls['applianceId'].setValue(
            this.applianceList[0].applianceId
          );
        } else {
          this.testSuiteForm.controls['applianceId'].setValue('');
        }
        if (this.applianceId > 0) {
          this.testSuiteForm.controls['applianceId'].setValue(this.applianceId);
        }
      }
    });
  }

  async changeBrand(e) {
    this.brandId = e.target.value;
    this.testSuiteForm.controls['applianceId'].setValue('');
    await this.getApplianceByBrand(this.brandId);
    this.getAllProjectName();
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}