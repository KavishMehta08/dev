import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateTestSuiteComponent } from './create-test-suite/create-test-suite.component';

const routes: Routes = [
  {
    path: '',
    component: CreateTestSuiteComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateTestSuiteRoutingModule {}
