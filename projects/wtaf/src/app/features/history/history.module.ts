import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { HistoryComponent } from './history/history.component';
import { HistoryRoutingModule } from './history-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgPipesModule } from 'ngx-pipes';

@NgModule({
  declarations: [HistoryComponent],
  imports: [
    CommonModule,
    SharedModule,
    HistoryRoutingModule,
    Ng2SearchPipeModule,
    NgPipesModule
  ]
})
export class HistoryModule {}
