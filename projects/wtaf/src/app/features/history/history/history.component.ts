import { Component, OnInit } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DataService } from '../../../shared/services/data.service';
import { Router } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { LocationStrategy, DatePipe } from '@angular/common';
import * as moment from 'moment';
import { environment } from '../../../../environments/environment';
import { encode } from 'punycode';
import { async } from '@angular/core/testing';
import {
  HttpHeaders,
  HttpRequest,
  HttpEventType,
  HttpResponse,
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import { LoaderService } from '../../../shared/services/loader.service';
import { Type } from '../../models/type';
import { Role } from '../../../models/role';
import { headSpinToken, InterceptorSkipHeader } from '../../../core/auth/jwt.interceptor';
import { range } from 'rxjs';
import { Constvar } from '../../../models/constVar';
import { DomSanitizer } from '@angular/platform-browser';

const AWS = require('aws-sdk');

declare var $: any;
interface Window {
  webkitURL?: any;
}
declare var window: any;
const jenkinsapi = require('jenkins-api');
const jenkins = jenkinsapi.init(environment.urlConfig.jenkinsUrl);
AWS.config.update({
  credentials: new AWS.CognitoIdentityCredentials({
    IdentityPoolId: environment.AwsConfig.IdentityPoolId
  }),
  region: environment.AwsConfig.DF_region
});
AWS.config.update({
  accessKeyId: environment.AwsConfig.accessKeyId,
  secretAccessKey: environment.AwsConfig.secretAccessKey
});
@Component({
  selector: 'wtaf-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  jobName: any;
  toolTip = 'Search By User or Job Name';
  History_Details: any = [];
  Extended_Reports: any = [];
  SelectedJobData: any;
  exeHeaderId: any;
  projectTypeName: string = '';
  executionMode = '';
  slaveOs: string;
  IsjobName_error = '';
  arrJenkinsSlave: any = [];
  arraySlave: any = [];
  jenkinsJobIdNum: number = 0;
  buildJobInfo: any;
  localJobIdNum: any;
  errorMessage: String = '';
  historyDetailsRes: any = [];
  isSeemore: boolean = false;
  seeMoreLess: string;
  resLength: any = [];
  today: string;
  widthfooter = '0%';
  steps = '';
  ByUpload: any = '';
  selectedAppFileName = '';
  mobileAppFileName = '';
  appArn = '';
  file_arn = '';
  isFileUpload = false;
  platform = '';
  selectedDevicePool = '';
  Apptype = 'ANDROID_APP';
  githubCheckout = environment.githubCheckout; //'GitHub-Checkout-AWS';
  envValue = environment.urlConfig.envValue;
  jenkinsConsole_log: any;
  timeDiff: string;
  testDate = new Date();
  differentTime: any;
  strJobName: string = '';
  IsZipSuccess = '';
  txtjobName = '';
  executionname = '';
  currentJobName = '';
  jobExe_duration: number;
  Jenkins_nodeName: any;
  jenkins_nodeOs: any;
  saveJenkinsData: any = {};
  serverDevice_array: boolean = false;
  saveJenkinsConsole_Path: any = [];
  jobDetailsHistoryBy_historyId: any;
  strLabelMsg: string = '';
  userId: number;
  startTime1: any;
  userName: string;
  timeStart_execution: any;
  endTime1: any;
  historyId: number;
  Job_arn: '';
  txtSearch: string;
  serverDevices: any = [];
  SelectedDevicePool_arn = '';
  reExecute_historyDetails: any = [];
  isReExecute: boolean = false;
  radioId: Number = 1;
  newJobname_HeaderId: number;
  inValid: Boolean = false;
  EnterJobname_error = '';
  role: string = '';
  Admin: string = '';
  Test_lead: string = '';
  Test_Mgr: string = '';
  hisHeaderId: number;
  hisHistoryId: number;
  baseUrl: string = '';
  suiteid: Number;
  totalPages: number = 0;
  isCommon_devices: Boolean = false;
  isHSExecution: Boolean = false;
  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];
  activePage: number = 1;
  totalRecords: number = 0;
  pagesCount: number;
  recordsPerPage: number = 25;
  languageCode: string;
  localeCapabilityValue = 'US';
  searchTxt = '';
  executedDeviceList: any = [];
  allUploadedHeadspinFiles: any = [];
  bundleId = '';
  appVersion = '';
  isAnyDevice = false;
  constructor(
    public toastr: ToastrManager,
    private dataservice: DataService,
    private router: Router,
    private ref: ChangeDetectorRef,
    public datePipe: DatePipe,
    private http: HttpClient,
    public loaderService: LoaderService,
    private sanitizer: DomSanitizer
  ) {}

  async ngOnInit() {
    this.role = localStorage.getItem('userRole');
    this.userName = localStorage.getItem('userDisplayName');
    this.Admin = Role.Admin;
    this.Test_lead = Role.Test_Lead;
    this.Test_Mgr = Role.Test_Manager;
    $('.modal-backdrop').remove();
    await this.historyDetails('', this.activePage, this.recordsPerPage, false);
    this.getJenkinsSlave();
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log('local usr id', this.userId);
  }

  displayActivePage(paginationObj) {
    let pageSize;
    let currntPage = paginationObj.currentPage;
    let currntRecords = parseInt(paginationObj.recordsPerPage);
    this.pagesCount = paginationObj.pages.length;
    pageSize = this.pagesCount;
    this.totalPages = paginationObj.totalPages;

    if (this.activePage != currntPage || this.recordsPerPage != currntRecords) {
      this.recordsPerPage = currntRecords;
      this.activePage = currntPage;
      this.historyDetails(
        this.searchTxt,
        this.activePage,
        this.recordsPerPage,
        false
      );
    } else {
      this.recordsPerPage = currntRecords;
      this.activePage = currntPage;
    }
  }

  //Get Search text from search component using @Output
  onTxtSearch(searchTxt: string) {
    this.searchTxt = searchTxt;
    this.activePage = 1;
    this.historyDetails(searchTxt, this.activePage, this.recordsPerPage, false);
  }

  async historyDetails(searchTxt, pageNo, recordsPerPage, isFromDelete) {
    // In DB Index is treat as current page and it start from 0
    let currentPage = pageNo - 1;
    await this.dataservice
      .getJobHistory(searchTxt, currentPage, recordsPerPage)
      .toPromise()
      .then((historyDetailsRes: any) => {
        // If is from delete job and isFromDelete var is true then active - 1
        if (isFromDelete && historyDetailsRes.listOfRecords == 0) {
          this.activePage = this.activePage - 1;
          this.historyDetails(
            searchTxt,
            this.activePage,
            recordsPerPage,
            isFromDelete
          );
        }
        this.History_Details = historyDetailsRes.listOfRecords;
        this.totalRecords = historyDetailsRes.totalCount;
        this.ref.detectChanges();

        // console.log('History Details....', this.History_Details);
        let timeDifference;
        let startTime;
        let endTime;
        var eventStartTime;
        var eventEndTime;
        var startDate;
        var endDate;
        var offset;
        this.History_Details.forEach(element => {
          // Added by Akash  StartTime EndTime Local Conversion
          eventEndTime = element.endTime;
          eventStartTime = element.startTime;
          if (
            eventStartTime != null &&
            eventStartTime != '' &&
            eventStartTime != undefined &&
            eventStartTime != 'Invalid date'
          ) {
            startDate = element.startDate;
            startDate = this.datePipe.transform(startDate, 'yyyy-MM-dd');
            startTime = startDate + ' ' + eventStartTime;
            startTime = moment.utc(startTime).toDate();
            var new_startDate = new Date(startTime);
            var date1 = moment(new_startDate).format('hh:mm:ss');
            // console.log('endTime===' + startTime + 'time===' + date1);
            startTime = Date.parse(startTime);
            element.startTime1 = date1;

            if (
              eventEndTime != null &&
              eventEndTime != '' &&
              eventEndTime != undefined &&
              eventEndTime != 'Invalid date'
            ) {
              endDate = element.endDate;
              endDate = this.datePipe.transform(endDate, 'yyyy-MM-dd');
              endTime = endDate + ' ' + eventEndTime;
              endTime = moment.utc(endTime).toDate();
              var new_startDate = new Date(endTime);
              var date2 = moment(new_startDate).format('hh:mm:ss');
              // console.log('endTime===' + endTime + 'time===' + date2);
              endTime = Date.parse(endTime);
              element.endTime1 = date2;
              timeDifference = endTime - startTime;
              let seconds = Math.floor((timeDifference / 1000) % 60);
              let minutes = Math.floor((timeDifference / (1000 * 60)) % 60);
              let hours = Math.floor(timeDifference / (1000 * 60 * 60));
              // console.log(seconds < 10 ? '0' + seconds : seconds);
              element.differentTime =
                hours + 'h' + ' ' + minutes + 'm' + ' ' + seconds + 's';
            } else {
              element.differentTime = ' -- : -- : --';

              endTime = ' -- : -- : --';
              date2 = endTime;
              element.endTime1 = date2;
            }
          } else {
            element.differentTime = ' -- : -- : --';
            startTime = ' -- : -- : --';
            date1 = startTime;
            element.startTime1 = date1;
            endTime = ' -- : -- : --';
            date2 = endTime;
            element.endTime1 = date2;
          }
          return (
            element.differentTime,
            (startTime = element.startTime1),
            (endTime = element.endTime1)
          );
        });
      });
  }
  /* get getExtendedReportList by exeHeaderId*/
  async extendedReportList(exeHeaderId, exeHistoryId) {
    await this.dataservice
      .getExtendedReportListById(exeHeaderId, exeHistoryId)
      .then(async (response: {}) => {
        this.Extended_Reports = response;
        console.log('Extended Reports....', this.Extended_Reports);
      });
  }
  hsPerformanceReport(sessionId): void {
    let reportPathUrl =
      'https://ui-dev.headspin.io/sessions/' + sessionId + '/waterfall';
    if (sessionId != null) {
      window.open(reportPathUrl);
    } else {
      this.toastr.errorToastr('There is no report for the job');
    }
  }

  // Added by Akash----
  async extendReportModal(historydata) {
    this.Job_arn = '';
    this.strLabelMsg = 'Download';
    if (historydata.executionMode == Constvar.Headspin) {
      this.isHSExecution = true;
    } else {
      this.isHSExecution = false;
    }
    this.saveJenkinsConsole_Path = [];
    this.SelectedJobData = historydata;
    this.Job_arn = this.SelectedJobData.jobArn;

    console.log('this.SelectedJobData-----------?', this.SelectedJobData);
    if (
      this.SelectedJobData.jenkinsJobId != null &&
      this.SelectedJobData.jenkinsJobId != undefined &&
      this.SelectedJobData.jenkinsJobId != ''
    ) {
      this.SelectedJobData.jenkinsJobId = parseInt(
        this.SelectedJobData.jenkinsJobId
      );
      let jenkinsData;
      if (
        this.SelectedJobData.jenkinsLogFilePath == null ||
        this.SelectedJobData.jenkinsLogFilePath == undefined ||
        this.SelectedJobData.jenkinsLogFilePath == '' ||
        this.saveJenkinsConsole_Path == null ||
        this.saveJenkinsConsole_Path == undefined
      ) {
        let doc = this;
        this.loaderService.show();
        await new Promise((resolve, reject) => {
          jenkins.console_output(
            doc.dataservice.commonJobName,
            doc.SelectedJobData.jenkinsJobId,
            async function(err, data) {
              if (err) {
                doc.loaderService.hide();
                // reject(err);
                console.log('error check for jenkins job', err);
                doc.toastr.errorToastr(
                  'Unable to find result , Please check whether selected node is active / Jenkins URL configured correctly / Jenkins Job Id Exists'
                );
                return;
              } else {
                doc.loaderService.hide();
                resolve(data);
                if (data != undefined) {
                  console.log('data cherck for jenkins job', data);
                  jenkinsData = data.body;
                  console.log('jenkinsData----->', jenkinsData);
                } else {
                  return;
                }
              }
            }
          );
        });
        this.saveJenkinsData = {
          exeHistoryId: this.SelectedJobData.exeHistoryId,
          log: jenkinsData
        };

        this.dataservice.saveJenkinsJob(this.saveJenkinsData).subscribe(res => {
          console.log('res-------odijsdoif>', res);

          this.saveJenkinsConsole_Path = res;
        });
      }
    }
    await this.getExtendReport(
      this.SelectedJobData.exeHeaderId,
      this.SelectedJobData.exeHistoryId
    );

    if (
      this.SelectedJobData.canJenkinsLogFileDownload == false &&
      this.SelectedJobData.jenkinsLogFilePath != null
    ) {
      this.strLabelMsg = 'Log file too Large';
      return;
    } else if (
      this.SelectedJobData.canJenkinsLogFileDownload == true &&
      this.SelectedJobData.jenkinsLogFilePath == null
    ) {
      this.strLabelMsg = 'Log not generated';
      return;
    }
  }

  getExtendReport(exeHeaderId, exeHistoryId) {
    this.dataservice
      .getExtendedReportListById(exeHeaderId, exeHistoryId)
      .then(async (response: {}) => {
        this.Extended_Reports = response;

        this.Extended_Reports = this.Extended_Reports.map(report => {
          var temp = Object.assign({}, report);
          let modifiedOn_LocalTime = moment
            .utc(temp.executionDateTime)
            .toDate();
          let convert_modifiedTime_format = moment(modifiedOn_LocalTime).format(
            'HH:mm:ss'
          );
          let date = this.datePipe.transform(
            temp.executionDateTime,
            'yyyy-MM-dd'
          );
          let executionDateTime = date + ' ' + convert_modifiedTime_format;
          temp.executionDateTime = executionDateTime;
          return temp;
        });

        $('#ExtendreportandAppiumModal').modal('show');

        console.log('Extended Reports....', this.Extended_Reports);
        let suiteid = this.Extended_Reports[0].testSuiteId;
        console.log('test suite id', suiteid);
        this.baseUrl = environment.urlConfig.baseUrl;
        console.log('base url', this.baseUrl);

        // if (!(this.baseUrl.indexOf(this.envValue) > -1)) {
          if (
            this.Job_arn != '' &&
            this.Job_arn != undefined &&
            this.Job_arn != null
          ) {
            this.suiteid = suiteid;
            await this.getDevicefarmJobStatus(
              this.Job_arn,
              this.SelectedJobData,
              this.suiteid,
              false
            );
          }
        // }
      });
  }
  async getDevicefarmJobStatus(jobarn, exec_data, suiteid, isfromRefresh) {
    if (this.Job_arn != '') {
      await new Promise(async (resolve, reject) => {
        var devicefarm = new AWS.DeviceFarm();
        var params = {
          arn: jobarn
        };
        await devicefarm
          .getRun(params)
          .promise()
          .then(
            async data => {
              console.log('1..........');
              resolve(resolve);
              let status = data.run['status'];
              if (status == 'COMPLETED') {
                var params_ = {
                  type: 'FILE',
                  arn: jobarn
                };

                await devicefarm
                  .listArtifacts(params_)
                  .promise()
                  .then(async data => {
                    console.log('2..........');
                    let Files = data.artifacts.filter(
                      o => o.name === 'Customer Artifacts'
                    );

                    //multiple files to upload
                    let objUrl = [];
                    for (let i = 0; i < Files.length; i++) {
                      objUrl.push(Files[i].url);
                    }
                    console.log('aws zip url', objUrl);

                    await this.dataservice
                      .addDeviceFarmExtentedReport(
                        exec_data.exeHeaderId,
                        exec_data.exeHistoryId,
                        objUrl,
                        suiteid
                      )
                      .then(async res => {
                        console.log(res);
                        if (res != null && res != undefined) {
                          await this.getExtendReport_after(
                            exec_data.exeHeaderId,
                            exec_data.exeHistoryId,
                            isfromRefresh
                          );
                        }
                      })
                      .catch(err => {
                        this.dataservice.loaderService.hide();
                        console.log(
                          'add device farm extent report error block',
                          err
                        );
                        return this.toastr.errorToastr(
                          this.dataservice.error_500_status
                        );
                      });
                  });
              }
            },
            error => {
              this.dataservice.loaderService.hide();
              reject(error);
              console.error('getting upload failed with error: ', error);
            }
          );
      });
    }
  }

  getExtendReport_after(exeHeaderId, exeHistoryId,isfromRefresh) {
    this.dataservice
      .getExtendedReportListById(exeHeaderId, exeHistoryId)
      .then(async (response: {}) => {
        this.Extended_Reports = response;
        this.Extended_Reports = this.Extended_Reports.map(report => {
          var temp = Object.assign({}, report);
          let modifiedOn_LocalTime = moment
            .utc(temp.executionDateTime)
            .toDate();
          let convert_modifiedTime_format = moment(modifiedOn_LocalTime).format(
            'HH:mm:ss'
          );
          let date = this.datePipe.transform(
            temp.executionDateTime,
            'yyyy-MM-dd'
          );
          let executionDateTime = date + ' ' + convert_modifiedTime_format;
          temp.executionDateTime = executionDateTime;
          return temp;
        });
        setTimeout(() => {
          $('#ExtendreportandAppiumModal').modal('show');
        }, 5000);
        console.log('Extended Reports....2', this.Extended_Reports);
        let result_of_missingExtentedRprt = this.Extended_Reports.filter(
          paths => {
            return paths.paths.length < 1;
          }
        );
        if(isfromRefresh)
        {
          if (result_of_missingExtentedRprt.length > 0) {
            this.toastr.infoToastr(
              result_of_missingExtentedRprt.length +
                ' ' +
                'Extent report(s) are missing'
            );
          } else {
            this.toastr.successToastr('All extent report(s) are updated');
          }
        }
        
        console.log(
          'Missing extented report list....',
          result_of_missingExtentedRprt
        );
        console.log(
          'Missing extented report list LENGTH....',
          result_of_missingExtentedRprt.length
        );
      });
  }

  async downloadJenkinsLog(
    jenkinsLogFilePath,
    jenkinsJobId,
    exeHeaderId,
    exeHistoryId
  ) {
    if (
      (this.SelectedJobData.canJenkinsLogFileDownload == false &&
        this.SelectedJobData.jenkinsLogFilePath != null &&
          this.SelectedJobData.jenkinsLogFilePath != '') ||
      (this.SelectedJobData.canJenkinsLogFileDownload == true &&
        this.SelectedJobData.jenkinsLogFilePath == null &&
          this.SelectedJobData.jenkinsLogFilePath == '')
    ) {
      return;
    } else {
      if (
        jenkinsJobId != null &&
        jenkinsJobId != undefined &&
        jenkinsJobId != ''
      ) {
        console.log(
          'this.saveJenkinsConsole_Path',
          this.saveJenkinsConsole_Path
        );
        if (this.saveJenkinsConsole_Path.length != 0) {
          if (
            this.saveJenkinsConsole_Path != null ||
            this.saveJenkinsConsole_Path != undefined
          ) {
            this.dataservice
              .downloadJenkinsLog(this.saveJenkinsConsole_Path.filePath)
              .subscribe(res => {
                const blob = new Blob([res], {
                  type: 'application/octet-stream'
                });
                var a = document.createElement('a');
                a.href = URL.createObjectURL(blob);
                a.download =
                  this.SelectedJobData.nodeName +
                  '_' +
                  exeHistoryId +
                  '_' +
                  exeHeaderId +
                  '.log';
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
              });
          }
        } else {
          this.dataservice
            .downloadJenkinsLog(jenkinsLogFilePath)
            .subscribe(res => {
              const blob = new Blob([res], {
                type: 'application/octet-stream'
              });
              var a = document.createElement('a');
              a.href = URL.createObjectURL(blob);
              a.download =
                this.SelectedJobData.nodeName +
                '_' +
                exeHistoryId +
                '_' +
                exeHeaderId +
                '.log';
              document.body.appendChild(a);
              a.click();
              document.body.removeChild(a);
            });
        }
      } else if (this.SelectedJobData.executionMode == Constvar.AWS) {
        var devicefarm = new AWS.DeviceFarm();
        this.Job_arn;
        var params_ = {
          type: 'FILE',
          arn: this.Job_arn
        };
        this.loaderService.show();
        await devicefarm
          .listArtifacts(params_)
          .promise()
          .then(async data => {
            this.loaderService.hide();
            let Files = data.artifacts.filter(
              o => o.name === Constvar.testSpecOutput
            );
            var element = document.createElement('a');
            element.setAttribute('href', Files[0].url);

            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
          })
          .catch(err => {
            console.log(err);
            this.loaderService.hide();
          });
      } else {
        this.toastr.errorToastr(
          'There is no execution log file for' +
            ' "' +
            this.SelectedJobData.jobName +
            '" ' +
            'job'
        );
      }
    }
  }

  CopyTxtArea(): void {
    var copyText = document.getElementById(
      'txtAreaAppiumId'
    ) as HTMLInputElement;
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand('copy');
    this.toastr.successToastr('Copied');
  }
  //Appiun Log download  ----Added by Akash
  saveTextAsFile() {
    var copyText = document.getElementById(
      'txtAreaAppiumId'
    ) as HTMLInputElement;
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    var textFileAsBlob = new Blob([copyText.value], { type: 'text/plain' });
    var fileNameToSaveAs = 'AppiumLog.txt';
    var downloadLink = document.createElement('a');
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = 'Download File';

    if (window.webkitURL != null) {
      downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
    } else {
      downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
      document.body.appendChild(downloadLink);
    }
    downloadLink.click();
  }

  showReport(path) {
    this.dataservice
      .showExtendReport(path)
      .toPromise()
      .then(res => {
        let str = res['key'];
        var blob = new Blob([str], { type: 'text/html' });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        window.open(link.href);
      })
      .catch(err => {
        console.log(err);
        this.loaderService.hide();
        this.dataservice.error_500_status =
          'There is no extent report file exists';
        this.toastr.errorToastr(this.dataservice.error_500_status);
        setTimeout(() => {
          this.dataservice.error_500_status = '';
        }, 2000);
        return;
      });
  }

  async getlocalDeviceFromJenkinsJob(nodeOs, platform, nodeName) {
    let jobName_from_getlocalDevices;
    let get_Data_frm_jobName;
    this.platform = platform;
    this.jenkins_nodeOs = nodeOs;
    this.serverDevices = [];
    this.dataservice.serverDevices = [];

    if (
      this.jenkins_nodeOs == '' ||
      this.jenkins_nodeOs == null ||
      this.jenkins_nodeOs == undefined
    ) {
      this.toastr.errorToastr(
        'Unable to find node , Please check selected node is active'
      );
      this.loaderService.hide();
      this.IsjobName_error = '';
      return;
    }

    jobName_from_getlocalDevices = await this.dataservice.getLocalDevices(
      this.jenkins_nodeOs,
      nodeName,
      this.platform,
      false
    );
    console.log(
      '.jobName_from_getlocalDevices.......',
      jobName_from_getlocalDevices
    );

    if (
      jobName_from_getlocalDevices != '' &&
      jobName_from_getlocalDevices != undefined
    ) {
      get_Data_frm_jobName = await this.dataservice.getJobInfo_by_jobName(
        jobName_from_getlocalDevices
      );
      console.log('getinfo', get_Data_frm_jobName);
    }

    if (get_Data_frm_jobName != null && get_Data_frm_jobName != undefined) {
      await new Promise<void>(resolve =>
        setTimeout(() => resolve(), 1500)
      ).then(async () => {
        this.dataservice.interval = setInterval(async () => {
          let consoleOut_datares;
          consoleOut_datares = await this.dataservice.readConsoleOutPut(
            jobName_from_getlocalDevices,
            get_Data_frm_jobName,
            'getDevices'
          );
          if (
            consoleOut_datares.consoleOutdata_res.body.indexOf(
              'Finished: SUCCESS'
            ) > -1
          ) {
            clearInterval(this.dataservice.interval);
            await this.consoleOutData(
              consoleOut_datares.consoleOutdata_res,
              consoleOut_datares.doc,
              consoleOut_datares.jobType
            );
          }
        }, 1000);
      });
    }
  }

  async consoleOutData(data, doc, jobType) {
    doc = this;
    console.log('data.body----------------------', data.body);
    if (data.body.indexOf('Finished: SUCCESS') > -1) {
      //clear selected device list
      doc.serverDevices = [];
      clearInterval(doc.interval);
      if (jobType == 'getDevices') {
        if (
          doc.platform == 'Android' &&
          data.body.indexOf('[ro.boot.serialno]') == -1
        ) {
          clearInterval(doc.interval);
          doc.toastr.errorToastr(
            'Failed to load devices. Please connect devices.'
          );

          setTimeout(() => {
            doc.IsjobName_error = '';
            // doc.ref.detectChanges();
          }, 2500);
          return;
        } else {
          //  Akash
          // doc.Invalid = false;
          if (
            doc.jenkins_nodeOs != null &&
            doc.jenkins_nodeOs != undefined &&
            doc.jenkins_nodeOs != ''
          ) {
            if (doc.jenkins_nodeOs.toString().indexOf('Windows') > -1) {
              //  For Android
              doc.serverDevices = await doc.dataservice.parseAndroidFile(
                data,
                doc,
                doc.active_step
              );

              $('#popProgress').modal('show');
              await new Promise<void>(resolve =>
                setTimeout(() => resolve(), 1500)
              ).then(async () => {
                this.isCommon_devices = await this.check_commonDevices(
                  this.hisHeaderId,
                  this.hisHistoryId
                );
                console.log(this.isCommon_devices);
              });

              if (this.isCommon_devices == true || this.platform == 'All') {
                this.reExecutionJob_afterValidation(
                  this.hisHeaderId,
                  this.hisHistoryId
                );
              } else {
                $('#popProgress').modal('hide');
                this.toastr.errorToastr(
                  'Unable to find The existing  device(s),hence unable to re-execute job'
                );
                return;
              }
              console.log('this.serverDevices', doc.serverDevices);
            } else {
              if (doc.platform == 'Android') {
                doc.serverDevices = await doc.dataservice.parseAndroidFile(
                  data,
                  doc,
                  doc.active_step
                );
                $('#popProgress').modal('show');
                // await new Promise(resolve => setTimeout(() => resolve(), 5000)).then(
                //   async () => {
                this.isCommon_devices = await this.check_commonDevices(
                  this.hisHeaderId,
                  this.hisHistoryId
                );
                console.log(this.isCommon_devices);
                // }
                // );

                if (this.isCommon_devices == true || this.platform == 'All') {
                  this.reExecutionJob_afterValidation(
                    this.hisHeaderId,
                    this.hisHistoryId
                  );
                } else {
                  $('#popProgress').modal('hide');
                  this.toastr.errorToastr(
                    'Unable to find The existing  device(s),hence unable to re-execute job'
                  );
                  return;
                }
              } else {
                // For IOS
                doc.serverDevices = await doc.dataservice.parseIosFile(
                  data,
                  doc,
                  doc.active_step
                );
                $('#popProgress').modal('show');
                await new Promise<void>(resolve =>
                  setTimeout(() => resolve(), 5000)
                ).then(async () => {
                  this.isCommon_devices = await this.check_commonDevices(
                    this.hisHeaderId,
                    this.hisHistoryId
                  );
                  console.log(this.isCommon_devices);
                });
              }

              if (this.isCommon_devices == true || this.platform == 'All') {
                this.reExecutionJob_afterValidation(
                  this.hisHeaderId,
                  this.hisHistoryId
                );
              } else {
                $('#popProgress').modal('hide');
                this.toastr.errorToastr(
                  'Unable to find The existing  device(s),hence unable to re-execute job'
                );
                return;
              }
            }
          } else {
            doc.toastr.errorToastr('Something went wrong.Please try again.');
          }
        }
      }
    } else if (data.body.indexOf('Finished: FAILURE') > -1) {
      $('#popProgress').modal('hide');
      doc.isDevicesLoading = false;
      clearInterval(doc.interval);
      doc.toastr.infoToastr('Something went wrong, please try again.', 'Error');
      $('#btnSelectDevices').prop('disabled', false);
      return;
    }
  }

  radioCheck(id) {
    this.radioId = id;
    console.log('this.radioId', this.radioId);
  }
  openReexecutionModal(historydata) {
    this.reExecute_historyDetails = [];
    this.reExecute_historyDetails = historydata;
    console.log('reExecute_historyDetails', this.reExecute_historyDetails);

    $('#ReexecutionModal').modal('show');
    $('#customRadio').prop('checked', true);
    this.radioId = 1;
  }

  reexecutionRadioCheck() {
    $('#ReexecutionModal').modal('show');
    let executionId = this.reExecute_historyDetails.exeHeaderId;
    let exeHistoryId = this.reExecute_historyDetails.exeHistoryId;
    console.log(
      'executionId',
      executionId,
      'exeHistoryId',
      exeHistoryId,
      'reExecute_historyDetails',
      this.reExecute_historyDetails
    );

    if (this.radioId == 1) {
      this.reExecution(this.reExecute_historyDetails);
    } else if (this.radioId == 2) {
      this.reExecutionwithchanges(executionId, exeHistoryId);
    }
  }
  reExecutionwithchanges(executionId, exeHistoryId) {
    document.getElementById('ReexecutionModal').style.opacity = '0';
    this.router.navigate([
      '/execution/start_execution',
      executionId,
      exeHistoryId
    ]);
  }
  async reExecution(historydata) {
    $('#ReexecutionModal').modal('hide');
    this.hisHeaderId = historydata.exeHeaderId;
    this.hisHistoryId = historydata.exeHistoryId;
    let executionId = historydata.exeHeaderId;
    let exeHistoryId = historydata.exeHistoryId;
    this.Jenkins_nodeName = historydata.nodeName;
    this.platform = historydata.platform;
    this.txtjobName = historydata.jobName;
    this.jenkins_nodeOs = historydata.nodeOs;
    let executionMode = historydata.executionMode;
    this.executionMode = executionMode;
    let jobArn = historydata.jobArn;
    this.SelectedDevicePool_arn = historydata.devicepoolArn;
    let isCommon_devices;
    this.loaderService.show();

    if (executionMode == 'Local') {
      if (this.platform != 'All') {
        $('#popProgress').modal('show');
        await this.getlocalDeviceFromJenkinsJob(
          this.jenkins_nodeOs,
          this.platform,
          this.Jenkins_nodeName
        );
      } else if (this.platform == 'All') {
        this.reExecutionJob_afterValidation(executionId, exeHistoryId);
      } else {
        $('#popProgress').modal('hide');
        this.toastr.errorToastr(
          'Unable to find The existing  device(s),hence unable to re-execute job'
        );
        return;
      }
    } else if (executionMode == Constvar.Headspin) {
      // await this.dataservice
      // .getTemplateNameByHistoryId(exeHistoryId)
      // .toPromise()
      // .then(async (executedDeviceList: any) => {
      // console.log('executedDeviceList', executedDeviceList);
      // this.executedDeviceList = executedDeviceList;
      await this.reExecutionJob_afterValidation(executionId, exeHistoryId);
      // })
    } else if (executionMode == 'AWS') {
      await this.getAWSDevForCapability_Creation(
        executionId,
        exeHistoryId,
        this.SelectedDevicePool_arn
      );
    }
  }

  async reExecutionJob_afterValidation(executionId, exeHistoryId) {
    await this.dataservice
      .jobReExecution(executionId, exeHistoryId, this.userId)
      .subscribe(
        async res => {
          let executionRes = res;

          console.log('executionRes--->', executionRes);
          let newExecutionId = executionRes['savedExeHeaderId'];
          let HistoryID = executionRes['savedExeHistoryId'];
          this.historyId = HistoryID;
          console.log('this.historyId............', this.historyId);
          let languageId = executionRes['savedLanguageId'];
          this.projectTypeName = executionRes['projectTypeName'];
          this.SelectedDevicePool_arn = executionRes['devicepoolArn'];
          this.mobileAppFileName = executionRes['mobileAppFileName'];
          this.languageCode = executionRes['language'];
          this.localeCapabilityValue = executionRes['locale'];
          console.log('this.Jenkins_nodeName---->', this.Jenkins_nodeName);
          let arn = executionRes['jobArn'];
          //just toexecute device farm job becuase arn is null from api side
          console.log('this.jenkins_nodeOs---->', this.jenkins_nodeOs);

          if (
            executionRes['message'] == null &&
            executionRes['message'] == undefined
          ) {
            if (newExecutionId != null && newExecutionId != undefined) {
              $('#popProgress').modal('show');
              this.isFileUpload = false;
              // let strJobName='';
              this.localJobIdNum = HistoryID;
              this.dataservice
                .exportData(newExecutionId, HistoryID, languageId)
                .subscribe(
                  async exportRes => {
                    if (exportRes != null) {
                      if (
                        exportRes['response'] == undefined &&
                        exportRes['response'] == null
                      ) {
                        if (
                          this.SelectedDevicePool_arn == '' ||
                          this.SelectedDevicePool_arn == undefined ||
                          this.SelectedDevicePool_arn == null
                        ) {
                          this.timeStart_execution = await new Promise<void>(
                            resolve => setTimeout(() => resolve(), 1500)
                          ).then(async () => {
                            clearInterval(this.timeStart_execution);
                            await this.executeJobAfterDevicesValidation(
                              this.Jenkins_nodeName
                            );
                          });
                        } else if (
                          this.SelectedDevicePool_arn != null &&
                          this.SelectedDevicePool_arn != undefined &&
                          this.SelectedDevicePool_arn != ''
                        ) {
                          this.isFileUpload = true;
                          await this.UploadFiles('App', HistoryID);
                        }
                      } else {
                        $('#popProgress').modal('hide');
                        this.toastr.errorToastr(
                          'Incorrect region and brand name, valid region and brand values are <region>-<brand> e.g. emea-bauknecht,emea-hotpoint,emea-whirlpool,nar-jennair,nar-kitchenaid,nar-maytag,nar-whirlpool'
                        );
                        return;
                      }
                    }
                  },
                  error => {
                    console.log(error);
                  }
                );
            }
          } else {
            this.toastr.errorToastr(executionRes['message']);
            return;
          }
        },
        error => {
          console.log(error);
          return this.toastr.errorToastr(this.dataservice.error_500_status);
        }
      );
  }

  async check_commonDevices(executionId, exeHistoryId): Promise<Boolean> {
    let executed_DeviceName;
    let current_DeviceName;
    let ISExist = false;
    let ListOfCommon_Devices: any = [];
    await this.dataservice
      .getTemplateNameByHistoryId(exeHistoryId)
      .toPromise()
      .then((executedDeviceList: [{}]) => {
        console.log('executedDeviceList.....', executedDeviceList);

        if (executedDeviceList != null && executedDeviceList != undefined) {
          for (let i = 0; i < executedDeviceList.length; i++) {
            executed_DeviceName = executedDeviceList[i]['deviceName']
              .toString()
              .toUpperCase()
              .trim();
            for (let j = 0; j < this.serverDevices.length; j++) {
              current_DeviceName = this.serverDevices[j]['name']
                .toString()
                .toUpperCase()
                .trim();
              let serveDeviceList_deviceName = current_DeviceName;
              if (executed_DeviceName == serveDeviceList_deviceName) {
                ListOfCommon_Devices.push(executed_DeviceName);
              }
            }
          }
          console.log(
            'executedDeviceList.length....',
            executedDeviceList.length
          );
          console.log(
            'ListOfCommon_Devices.length',
            ListOfCommon_Devices.length
          );

          if (executedDeviceList.length == ListOfCommon_Devices.length) {
            ISExist = true;
          } else {
            ISExist = false;
          }
        }
      });
    return ISExist;
  }
  async executeJobAfterDevicesValidation(nodeName) {
    // let exeHeaderId=executionId;
    if (this.projectTypeName == 'Mobile') {
      this.strJobName = this.dataservice.commonJobName;
    } else {
      this.strJobName = this.dataservice.commonJobName;
    }
    let doc = this;
    jenkins.build_with_params(
      doc.strJobName,
      {
        BaseURL: environment.urlConfig.baseUrl,
        RunJobNode: nodeName,
        OS: this.jenkins_nodeOs,
        historyId: this.historyId,
        projectType: this.projectTypeName,
        delay: 0
      }, //OS: this.slaveOs,
      function(err, data) {
        if (err) {
          return console.log(err);
        }
        jenkins.job_info(doc.strJobName, async function(err, buildJobInfo) {
          if (err) {
            return console.log(err);
          }
          console.log(buildJobInfo);
          doc.jenkinsJobIdNum = buildJobInfo.builds[0].number;
          console.log('-----Jenkin Job Id------', doc.jenkinsJobIdNum);
          doc.updateJobId();
        });
      }
    );
    $('#popProgress').modal('hide');
    this.clearExecutionFields();
  }

  updateJobId() {
    let jenkinsJobId = this.jenkinsJobIdNum;
    let localJobId = this.localJobIdNum;
    console.log('localJobIdNum----->', this.localJobIdNum);
    if (
      (jenkinsJobId != null ||
        jenkinsJobId != 0 ||
        jenkinsJobId != undefined) &&
      (localJobId != null || localJobId != '' || localJobId != undefined)
    ) {
      this.dataservice
        .addJenkinsJobId(localJobId, jenkinsJobId)
        .subscribe(res => {
          console.log('update job Id------------------', res);
          // alert('Updated successfully');
          //changes by mustaid for updating start time after re-execution start
          if (res != null && res != '' && res != undefined) {
            this.dataservice
              .updateHistorystatus(this.historyId, true, 1, 1)
              .subscribe(update_status_res => {
                console.log(' update status res--------', update_status_res);
              });
          }
        });
    } else {
      this.errorMessage = 'undefined';
    }
  }

  async getJenkinsSlave() {
    let doc = this;
    let arraySlave;
    await jenkins.computers(function(err, data) {
      if (err) {
        doc.IsjobName_error = err;
        doc.ref.detectChanges();
        setTimeout(() => {
          doc.IsjobName_error = '';
          doc.ref.detectChanges();
        }, 2500);
        return console.log('computers error ==>' + err);
      }

      doc.arrJenkinsSlave = data.computer;
      arraySlave = doc.arrJenkinsSlave;
      console.log('this.arraySlave----->', arraySlave);

      doc.ref.detectChanges();
      console.log('computers ==>' + data.computer);
    });

    this.arraySlave = arraySlave;
    console.log('this.arraySlave----->', this.arraySlave);
  }

  //download appium log file
  downloadAppiumLog(jobname, exeHeaderId, exeHistoryId) {
    this.dataservice
      .downloadAppiumLog(exeHeaderId, exeHistoryId)
      .subscribe(res => {
        if (res.search('Message') == -1) {
          const blob = new Blob([res], { type: 'application/octet-stream' });
          var a = document.createElement('a');
          a.href = URL.createObjectURL(blob);
          a.download =
            jobname + '_' + exeHistoryId + '_' + exeHeaderId + '.log';
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        } else {
          this.toastr.errorToastr(
            'There is no appium log file for' + ' "' + jobname + '" ' + 'job'
          );
        }
      });
  }

  //get job history details

  getJobDetails(exeHeaderId, exeHistoryId, seeMore) {
    let doc = this;
    let timeDifference;
    let startTime;
    let endTime;
    var eventStartTime;
    var eventEndTime;
    var startDate;
    var endDate;
    this.dataservice
      .getJobHistoryDetails(exeHeaderId, exeHistoryId, seeMore)
      .subscribe(res => {
        this.historyDetailsRes[exeHistoryId] = res;

        this.jobDetailsHistoryBy_historyId = this.historyDetailsRes[
          exeHistoryId
        ];
        this.historyDetailsRes[exeHistoryId].map(element => {
          // Added by Akash  StartTime EndTime Local Conversion
          eventEndTime = element.endTime;
          eventStartTime = element.startTime;
          if (
            eventStartTime != null &&
            eventStartTime != '' &&
            eventStartTime != undefined &&
            eventStartTime != 'Invalid date'
          ) {
            if (
              eventEndTime != null &&
              eventEndTime != '' &&
              eventEndTime != undefined &&
              eventEndTime != 'Invalid date'
            ) {
              startDate = element.startDate;
              startDate = this.datePipe.transform(startDate, 'yyyy-MM-dd');
              endDate = element.endDate;
              endDate = this.datePipe.transform(endDate, 'yyyy-MM-dd');
              endTime = endDate + ' ' + eventEndTime;
              endTime = moment.utc(endTime).toDate();
              var new_startDate = new Date(endTime);
              var date2 = moment(new_startDate).format('hh:mm:ss');
              console.log('endTime===' + endTime + 'time===' + date2);
              endTime = Date.parse(endTime);
              startTime = startDate + ' ' + eventStartTime;
              startTime = moment.utc(startTime).toDate();
              var new_startDate = new Date(startTime);
              var date1 = moment(new_startDate).format('hh:mm:ss');
              console.log('endTime===' + startTime + 'time===' + date1);
              startTime = Date.parse(startTime);
              timeDifference = endTime - startTime;
              let seconds = Math.floor((timeDifference / 1000) % 60);
              let minutes = Math.floor((timeDifference / (1000 * 60)) % 60);
              let hours = Math.floor((timeDifference / (1000 * 60 * 60)) % 24);
              let days = Math.floor(timeDifference / (24 * 60 * 60 * 1000));
              console.log(seconds < 10 ? '0' + seconds : seconds);
              element.differentTime =
                (days < 10 ? '0' : '') +
                days +
                ':' +
                (hours < 10 ? '0' : '') +
                hours +
                ':' +
                (minutes < 10 ? '0' : '') +
                minutes +
                ':' +
                (seconds < 10 ? '0' : '') +
                seconds;
              element.startTime1 = date1;
              element.endTime1 = date2;
            }
          } else {
            element.differentTime = ' -- : -- : --';
            startTime = ' -- : -- : --';
            date1 = startTime;
            element.startTime1 = date1;
            endTime = ' -- : -- : --';
            date2 = endTime;
            element.endTime1 = date2;
          }
          return (
            element.differentTime,
            (startTime = element.startTime1),
            (endTime = element.endTime1)
          );
        });
        this.resLength = res;
        console.log(
          'history details  response------->',
          this.historyDetailsRes
        );
        //length condition changed as first history record will be hidden
        if (this.resLength.length > 4) {
          if (seeMore == true) {
            this.isSeemore = true;
          } else {
            this.isSeemore = true;
          }
        }
      });

    if (seeMore == false) {
      if ($('#' + exeHistoryId).hasClass('out')) {
        $('#' + exeHistoryId).addClass('in');
        $('#' + exeHistoryId).removeClass('out');
        $('#r' + exeHistoryId).removeClass('caret');
        $('#r' + exeHistoryId).addClass('caret caret-down');
      } else {
        $('#' + exeHistoryId).addClass('out');
        $('#' + exeHistoryId).removeClass('in');
        $('#r' + exeHistoryId).removeClass('out');
        $('#r' + exeHistoryId).removeClass('caret caret-down');
        $('#r' + exeHistoryId).addClass('caret');
      }
    }
  }

  getjobDetailsForseeLess(exeHeaderId, exeHistoryId, seeMore) {
    let doc = this;
    let timeDifference;
    let startTime;
    let endTime;
    var eventStartTime;
    var eventEndTime;
    var startDate;
    var endDate;
    this.dataservice
      .getJobHistoryDetails(exeHeaderId, exeHistoryId, seeMore)
      .subscribe(res => {
        this.historyDetailsRes[exeHistoryId] = res;
        this.historyDetailsRes[exeHistoryId].map(element => {
          // Added by Akash  StartTime EndTime Local Conversion
          eventEndTime = element.endTime;
          eventStartTime = element.startTime;
          if (
            eventStartTime != null &&
            eventStartTime != '' &&
            eventStartTime != undefined &&
            eventStartTime != 'Invalid date'
          ) {
            if (
              eventEndTime != null &&
              eventEndTime != '' &&
              eventEndTime != undefined &&
              eventEndTime != 'Invalid date'
            ) {
              startDate = element.startDate;
              startDate = this.datePipe.transform(startDate, 'yyyy-MM-dd');
              endDate = element.endDate;
              endDate = this.datePipe.transform(endDate, 'yyyy-MM-dd');
              endTime = endDate + ' ' + eventEndTime;
              endTime = moment.utc(endTime).toDate();
              var new_startDate = new Date(endTime);
              var date2 = moment(new_startDate).format('hh:mm:ss');
              console.log('endTime===' + endTime + 'time===' + date2);
              endTime = Date.parse(endTime);
              startTime = startDate + ' ' + eventStartTime;
              startTime = moment.utc(startTime).toDate();
              var new_startDate = new Date(startTime);
              var date1 = moment(new_startDate).format('hh:mm:ss');
              console.log('endTime===' + startTime + 'time===' + date1);
              startTime = Date.parse(startTime);
              timeDifference = endTime - startTime;
              let seconds = Math.floor((timeDifference / 1000) % 60);
              let minutes = Math.floor((timeDifference / (1000 * 60)) % 60);
              let hours = Math.floor((timeDifference / (1000 * 60 * 60)) % 24);
              let days = Math.floor(timeDifference / (24 * 60 * 60 * 1000));
              console.log(seconds < 10 ? '0' + seconds : seconds);
              element.differentTime =
                (days < 10 ? '0' : '') +
                days +
                ':' +
                (hours < 10 ? '0' : '') +
                hours +
                ':' +
                (minutes < 10 ? '0' : '') +
                minutes +
                ':' +
                (seconds < 10 ? '0' : '') +
                seconds;
              element.startTime1 = date1;
              element.endTime1 = date2;
            }
          } else {
            element.differentTime = ' -- : -- : --';
            startTime = ' -- : -- : --';
            date1 = startTime;
            element.startTime1 = date1;
            endTime = ' -- : -- : --';
            date2 = endTime;
            element.endTime1 = date2;
          }
          return (
            element.differentTime,
            (startTime = element.startTime1),
            (endTime = element.endTime1)
          );
        });
        this.isSeemore = false;
        console.log(
          'history details  response------->',
          this.historyDetailsRes
        );
        //length condition changed as first history record will be hidden
        if (this.resLength.length > 4) {
          if (seeMore == true) {
            this.isSeemore = true;
          } else {
            this.isSeemore = false;
          }
        }
      });
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }

  async upload(
    url: string,
    body: any,
    uploadFileType,
    HistoryID
  ): Promise<any> {
    this.widthfooter = 0 + '%'.toString();
    return new Promise((resolve, reject) => {
      const httpOptions = {
        headers: new HttpHeaders({
          'content-type': 'application/octet-stream'
        }).set(InterceptorSkipHeader, ''),
        reportProgress: true
      };

      const req = new HttpRequest('put', url, body, httpOptions);
      this.http.request(req).subscribe(async event => {
        if (event.type === HttpEventType.UploadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.widthfooter = percentDone + '%'.toString();
          console.log(`File is ${percentDone}% uploaded.`);
        } else if (event instanceof HttpResponse) {
          console.log('File is completely uploaded!');
          if (uploadFileType == 'App') {
            this.steps = 'Uploading Files [Step: 2 / 3]';
            this.UploadFiles('SourceCode', HistoryID);
          } else if (uploadFileType == 'SourceCode') {
            this.steps = 'Scheduling Test [Step: 3 / 3]';
            setTimeout(async () => {
              await this.RunTest(HistoryID);
            }, 3000);
          }
        }
      });
    });
  }

  async UploadFiles(UploadType, HistoryID) {
    this.ByUpload = 'Using_Arn';
    if (this.ByUpload == 'Using_Arn' && this.mobileAppFileName != '') {
      $('#popProgress').modal('show');
      AWS.config.credentials.get(function(err) {
        if (err) {
          console.log(err);
        } else {
          console.log(AWS.config.credentials);
        }
      });

      var devicefarm = new AWS.DeviceFarm();
      let projectArn = environment.AwsConfig.ProjectArn;
      let name_ = this.selectedAppFileName;

      //For Android---- > ANDROID_APP
      //For Java TestNG---- > APPIUM_JAVA_TESTNG_TEST_PACKAGE
      //For IOS------> 'IOS_APP'
      let params_upload = {
        projectArn: projectArn,
        name: 'zip -with-dependencies.zip',
        type: 'APPIUM_JAVA_TESTNG_TEST_PACKAGE',
        contentType: 'application/octet-stream'
      };

      this.steps = 'Scheduling Test [Step: 2 / 3]';
      return devicefarm.createUpload(params_upload, async (err, data) => {
        if (err) {
          console.log(err, err.stack); // an error occurred
        } else {
          let uploadArn = data['upload']['arn'];
          let preSignedUrl = data['upload']['url'];
          this.file_arn = uploadArn;
          let zipFile = await this.GetZipFromWorkSpace();
          this.upload(preSignedUrl, zipFile, 'SourceCode', HistoryID);
        }
      });
    }
  }

  async RunTest(HistoryID) {
    let doc = this;
    let historyID = HistoryID;
    var devicefarm = new AWS.DeviceFarm();
    var params = {
      arn: this.file_arn // You can get the test ARN by using the list-uploads CLI command.
    };
    devicefarm.getUpload(params, function(err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
      } else {
        console.log(data);
        // successful response
        let uploadStatus = data['upload']['status'];
        console.log('upload status......', uploadStatus);
        if (uploadStatus == Constvar.SucceedStatus) {
          doc.scheduleRun(HistoryID);
        } else {
          doc.RunTest(HistoryID);
        }
      }
    });
  }

  scheduleRun(HistoryID) {
    if (
      this.localeCapabilityValue == undefined ||
      this.localeCapabilityValue == null
    ) {
      this.localeCapabilityValue = Constvar.localeUS;
    }
    var devicefarm = new AWS.DeviceFarm();

    let param = {
      configuration: {
        locale: this.languageCode + '_' + this.localeCapabilityValue
      },
      // for testing commenting this.appArn and Putting hard coded value
      executionConfiguration: {
        videoCapture: true
      },
      appArn: this.mobileAppFileName,
      name: this.txtjobName,
      devicePoolArn: this.SelectedDevicePool_arn,
      projectArn: environment.AwsConfig.ProjectArn,
      test: {
        type: 'APPIUM_JAVA_TESTNG',
        testPackageArn: this.file_arn,
        testSpecArn:
          this.platform == 'Android'
            ? environment.AwsConfig.testSpecArnAndroid
            : this.platform == 'IOS'
            ? environment.AwsConfig.testSpecArnIOS
            : ''
      }
    };
    this.steps = 'Scheduling Test [Step: 3 / 3]';

    console.log('this.mobileAppFileName', this.mobileAppFileName);
    console.log('testPackageArn', this.file_arn);
    console.log('Run Test params', param);

    console.log('schedule run params', param);

    devicefarm.scheduleRun(param, (err, data) => {
      if (err) {
        console.log('error  schedule run', err, err.stack); // an error occurred
        this.steps = err;
        setTimeout(() => {
          this.clearExecutionFieldsWithError();
          $('#popProgress').modal('hide');
          this.getshedulelink();
        }, 5000);
      } else {
        console.log('schedule data......', data);

        let uploadArn = data.run['arn'];
        this.steps = 'Updating execution status';
        this.dataservice
          .updateHistoryWithArn(HistoryID, uploadArn, this.mobileAppFileName)
          .subscribe(async UpdateRes => {
            //changes by mustaid for updating start time after execution start
            if (
              UpdateRes != null &&
              UpdateRes != '' &&
              UpdateRes != undefined
            ) {
              this.dataservice
                .updateHistorystatus(HistoryID, true, 1, 1)
                .subscribe(update_status_res => {
                  console.log(' update status res--------', update_status_res);
                });
            }
            this.steps = 'Test execution scheduled successfully...';
            console.log('UpdateRes[Updated rows]', UpdateRes['Updated rows']);
            console.log('UpdateRes------>', UpdateRes);
            console.log('data', data);
            this.clearExecutionFields();
            setTimeout(() => {
              $('#popProgress').modal('hide');
              this.getshedulelink();
            }, 3000);
          });
      }
    });
  }
  clearExecutionFields() {
    this.txtjobName = '';
    this.file_arn = '';
    this.IsjobName_error = '';
    this.selectedAppFileName = '';
    this.jenkinsJobIdNum = 0;
    this.getshedulelink();
  }

  clearExecutionFieldsWithError() {
    this.txtjobName = '';
    this.file_arn = '';
    this.IsjobName_error = '';
    this.getshedulelinkWithError();
  }

  //=========navigate schedule page.===================
  getshedulelink() {
    this.toastr.infoToastr('Execution has started.', 'Success');
    $('#popProgress').modal('hide');
    $('.modal-backdrop').remove();
    this.router.navigate(['/execution/inprogress']);
  }

  getshedulelinkWithError() {
    this.toastr.infoToastr('Unable to schedule test.', 'warning');
    $('#popProgress').modal('hide');
    $('.modal-backdrop').remove();
    this.router.navigate(['/execution/inprogress']);
  }

  async GetZipFromWorkSpace(): Promise<File> {
    let zipFile = null;

    let doc = this;
    await this.cloneFromGithub();

    let JenkinsCheckoutJobUrl = await this.CreateaZipForDevicefarm();
    await doc.sleep(2000);
    //Wtaf Env-URL===>  'execution/node/5/ws/WTAF-mobileFramework/target/zip-with-dependencies.zip';
    // let url =
    //   JenkinsCheckoutJobUrl +
    //   'execution/node/5/ws/framework/target/zip-with-dependencies.zip';
    let url = JenkinsCheckoutJobUrl + environment.JenkinsCheckoutJobUrl;
    let apiHeaders = new Headers();
    if (environment.wtafEnv == 'false') {
      apiHeaders.append('Access-Control-Allow-Origin', '*');
      apiHeaders.append('Access-Control-Allow-Methods', '*');
      apiHeaders.append('type', 'application/zip');
    } else {
      apiHeaders.append('Access-Control-Allow-Origin', '*');
      apiHeaders.append('Access-Control-Allow-Methods', '*');
      apiHeaders.append('Access-Control-Allow-Credentials', 'true');
      apiHeaders.append('Authorization', 'Basic ' + btoa('USERW:Pkm12345'));
      apiHeaders.append('type', 'application/zip');
    }
    let response = await fetch(url, {
      headers: apiHeaders
    }).then(async res => {
      console.log('zip ......', res);
      let data = await res.blob();
      let metadata = {
        type: 'application/zip'
      };

      zipFile = new File([data], 'zip-with-dependencies.zip', metadata);
      console.log('zipFile.....', zipFile);
    });
    return zipFile;
  }

  async CreateaZipForDevicefarm(): Promise<string> {
    let doc = this;
    let JenkinsUrl = '';
    let JenkinsData;
    await new Promise(async (resolve, reject) => {
      await jenkins.job_info(doc.githubCheckout, async (err, data) => {
        if (err) {
          doc.IsjobName_error = err;
          doc.ref.detectChanges();
          setTimeout(() => {
            doc.IsjobName_error = '';
            doc.ref.detectChanges();
          }, 2500);
          this.toastr.errorToastr(
            'Please check whether your node has internet connection, selected node is online'
          );

          return console.log('jenkins build error -->' + err);
        }
        resolve(data);
        if (data != null) {
          JenkinsUrl = data.lastBuild.url;
          let IsZipReady = '';

          JenkinsData = data;
          console.log('Reading');
        }
      });
    });
    await doc.sleep(5000);
    let a = await doc.checkIfZipCreatedOrNot(
      doc,
      doc.githubCheckout,
      JenkinsData,
      'checkOut'
    );

    console.log('a......', a);
    return JenkinsUrl;
  }

  async checkIfZipCreatedOrNot(doc, jobName, data, jobType): Promise<string> {
    await new Promise(async (resolve, reject) => {
      await jenkins.console_output(
        jobName,
        data.builds[0].number,
        async (err, data) => {
          if (err) {
            this.IsZipSuccess = 'Error';
          } else {
            resolve(data);
            if (data.body != undefined) {
              if (data.body.indexOf('Finished: SUCCESS') > -1) {
                this.IsZipSuccess = 'Success';
              } else if (data.body.indexOf('Finished: FAILURE') > -1) {
                this.IsZipSuccess = 'Failure';
              } else {
                this.IsZipSuccess = 'Pending';
              }
            }
          }
        }
      );
    });
    console.log('IsSuccess', this.IsZipSuccess);

    if (this.IsZipSuccess == 'Pending') {
      await doc.checkIfZipCreatedOrNot(doc, jobName, data, 'checkOut');
    } else if (this.IsZipSuccess == 'Failure') {
      return this.IsZipSuccess;
    } else {
      return this.IsZipSuccess;
    }
  }

  sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }

  async cloneFromGithub() {
    let doc = this;
    await new Promise(async (resolve, reject) => {
      await jenkins.build_with_params(
        doc.githubCheckout,
        //changed hard coded master name to this.selectedSlave by mustaid 12/6/2020
        {
          BaseURL: environment.urlConfig.baseUrl,
          RunJobOnNode: 'master',
          historyId: this.historyId,
          projectType: this.projectTypeName,
          delay: 0
        },
        async (err, data) => {
          if (err) {
            doc.IsjobName_error = err;
            doc.ref.detectChanges();
            setTimeout(() => {
              doc.IsjobName_error = '';
              doc.ref.detectChanges();
            }, 2500);
            this.toastr.errorToastr(
              'Please check whether your node has internet connection, selected node is online'
            );

            return console.log('jenkins build error -->' + err);
          }
          resolve(data);
        }
      );
    });
  }

  async validateCapabilityTemplate(templateList) {
    console.log('in validateCapabilityTemplate');
    for (let i = 0; i < templateList.length; i++) {
      console.log(
        'in templateList[i].usrCapabilityTemplateName',
        templateList[i].usrCapabilityTemplateName
      );
      await this.dataservice
        .getcapabilityTemplateByName(templateList[i].usrCapabilityTemplateName)
        .subscribe(res => {
          console.log(res);
          if (res == null) {
            console.log('in show error');
            this.toastr.errorToastr(
              'Capability Template removed, hence cannot re-execute. Please re-execute with changes with a new capability template.'
            );
            return;
          }
          let dup_capTemplate = this.dataservice.checkIfCapabilityTemplateHasDuplicates(
            res['capabilityIdValuesViews']
          );
          console.log('this.dup_capTemplate', dup_capTemplate);
          if (dup_capTemplate != null) {
            this.toastr.errorToastr(dup_capTemplate);
            return;
          }

          let cor_capTemplate = this.dataservice.checkIfCapabilityTemplateIsCorrputed(
            res['capabilityIdValuesViews']
          );
          console.log('this.cor_capTemplate', cor_capTemplate);
          if (cor_capTemplate != null) {
            this.toastr.errorToastr(cor_capTemplate);
            return;
          }
        });
    }
  }

  async getAWSDevForCapability_Creation(
    executionId,
    HistoryID,
    SelectedDevicePool_arn
  ) {
    this.SelectedDevicePool_arn = SelectedDevicePool_arn;
    this.serverDevices = [];
    this.dataservice.serverDevices = [];
    let templateList: any = [];
    let devicepoolName = '';
    let var_arn = this.SelectedDevicePool_arn;
    if (var_arn != '') {
      await this.dataservice
        .getTemplateNameByHistoryId(HistoryID)
        .subscribe(res => {
          console.log(res);
          templateList = res;
          console.log('template list res', templateList);
          this.validateCapabilityTemplate(templateList);
        });
      var devicefarm = new AWS.DeviceFarm();
      var params = {
        arn: var_arn
      };

      devicefarm.getDevicePool(params, async (err, devicepool_data) => {
        if (err) {
          console.log('error', err, err.stack);
        } else {
          let devicePoolRules = devicepool_data.devicePool.rules;
          devicepoolName = devicepool_data.devicePool.name;
          console.log('device pool name', devicepoolName);
          let ResourceType = '';
          let FilterKeys = [];
          devicePoolRules.forEach(function(element) {
            if (element.attribute === 'ARN') {
              ResourceType = 'ARN';
              element['values'] = element.value;
            } else {
              let ele_value = element.value.substring(
                1,
                element.value.length - 1
              );
              element['values'] = [ele_value];
              FilterKeys.push({
                key: element.attribute,
                op: element.operator,
                val: ele_value
              });
            }
            delete element.value;
          });
          if (ResourceType != 'ARN') {
            var params_devicelist = {
              filters: devicePoolRules
            };

            devicefarm.listDevices(params_devicelist, (err, data) => {
              if (err) {
                console.log('list devices error----', err);
              } else {
                let filterCondition = false;
                for (let k = 0; k < FilterKeys.length; k++) {
                  let Avl_val = FilterKeys[k].val;
                  if (Avl_val == 'AVAILABLE') filterCondition = true;
                }
                console.log('filterCondition----', filterCondition);
                let filtered_data = [];
                if (filterCondition == true) {
                  filtered_data = data.devices.filter(
                    o => o.availability === 'AVAILABLE'
                  );
                } else {
                  filtered_data = data.devices;
                }
                this.serverDevices = filtered_data;
                let index = 1;
                this.serverDevices.forEach(function(element) {
                  element.selected = false;
                  element.id = index;
                  index = index + 1;
                });
              }
            });
          } else {
            let arnStr = devicePoolRules[0].values.replace('[', '');
            arnStr = arnStr.replace(']', '');
            var arnlist = arnStr.split(',');

            for (let l = 0; l < arnlist.length; l++) {
              let var_arn = arnlist[l].substring(1, arnlist[l].length - 1);
              var arn_params = {
                arn: var_arn
              };

              await devicefarm.getDevice(arn_params, async (err, data) => {
                if (err) {
                  console.log('devicefarm.getDevice....', err);
                } else {
                  if (data.device != undefined) {
                    this.serverDevices.push({
                      capabilityTemplateName:
                        data.device.name + '_' + devicepoolName
                    });
                    console.log('this.server diveces', this.serverDevices);
                  }
                }
              });

              if (l == arnlist.length - 1) {
                setTimeout(async () => {
                  await this.getAwsDevices(
                    templateList,
                    executionId,
                    HistoryID,
                    devicepoolName
                  );
                }, 2000);
              }
            }
          }
        }
      });
    } else {
      this.loaderService.hide();
      this.toastr.infoToastr(
        'Unable to get device list of selected device pool'
      );
    }
  }

  async getAwsDevices(tempList, executionId, HistoryID, devicepoolName) {
    let templateList = tempList;
    let isTemplateExist;
    let capabilityTemplateName;
    for (let i = 0; i < this.serverDevices.length; i++) {
      capabilityTemplateName =
        this.serverDevices[i].capabilityTemplateName +
        '_' +
        templateList[i].usrCapabilityTemplateName;
      isTemplateExist = templateList.filter(
        o => o.capabilityTemplateName === capabilityTemplateName
      );
    }
    if (isTemplateExist != undefined && isTemplateExist != null) {
      if (isTemplateExist.length > 0) {
        $('#popProgress').modal('show');
        this.isFileUpload = true;
        await this.reExecutionJob_afterValidation(executionId, HistoryID);
      } else {
        $('#popProgress').modal('hide');
        this.toastr.errorToastr(
          'Devices either added or removed from' +
            ' ' +
            devicepoolName +
            ' ' +
            'devices pool,hence cannot re-execute'
        );
        return;
      }
    }
  }

  openRenameExecutionModal(exeHeaderId, jobName) {
    this.executionname = '';
    this.EnterJobname_error = '';
    this.newJobname_HeaderId = exeHeaderId;
    console.log('newJobname_HeaderId', this.newJobname_HeaderId);
    this.currentJobName = jobName;
    $('#ReexecutionNameModal').modal('show');
  }

  async updateExecutionName() {
    this.EnterJobname_error = '';
    if (this.executionname != '' && this.executionname != undefined) {
      await this.isExeNameDuplicate();
      if (this.inValid == false) {
        $('#ReexecutionNameModal').modal('hide');
        await this.dataservice
          .updateExecutionName(
            this.newJobname_HeaderId,
            this.executionname.trim()
          )
          .subscribe(async res => {
            console.log('updated job res', res);
            if (res != null && res != '' && res != undefined) {
              this.toastr.successToastr(res['status']);
              $('#ReexecutionNameModal').modal('hide');
              await this.historyDetails(
                '',
                this.activePage,
                this.recordsPerPage,
                false
              );
            }
          });
      } else {
        return;
      }
    } else {
      this.EnterJobname_error = 'Please Enter Execution Name';
      return;
    }
  }

  async isExeNameDuplicate() {
    if (this.executionname != '') {
      let Data = {
        resource: this.executionname.trim(),
        type: Type.Execution,
        id: 0
      };
      await this.dataservice
        .checkDuplication(Data)
        .toPromise()
        .then((response: Boolean) => {
          // Success
          this.inValid = response;
          if (this.inValid) {
            this.toastr.errorToastr(
              'Test execution name "' + this.executionname + '" already exists'
            );
          } else {
          }
        });
    }
  }

  // delete the job from history
  deleteJob(i, headerId, historyId) {
    let isconfirm_delete;
    if (i == 0) {
      isconfirm_delete = confirm(
        'Please confirm if all the records to be deleted below?'
      );
      if (isconfirm_delete) {
        console.log('delete job');
        this.dataservice
          .DeleteAllJob_by_HeaderID(headerId)
          .subscribe(async res => {
            console.log('delete job by header id', res);
            if (res != null && res != undefined) {
              this.toastr.successToastr('Job deleted sucessfully');

              await this.historyDetails(
                '',
                this.activePage,
                this.recordsPerPage,
                true
              );
            }
          });
      } else {
        console.log('dont delete job');
      }
    } else if (i == 1) {
      isconfirm_delete = confirm(
        'Please confirm if the  record to be deleted below?'
      );
      if (isconfirm_delete) {
        console.log('delete job');
        this.dataservice
          .DeleteAllJob_by_historyID(historyId)
          .subscribe(async res => {
            console.log('delete job by history id', res);
            if (res != null && res != undefined) {
              this.toastr.successToastr('Job deleted sucessfully');

              await this.historyDetails(
                '',
                this.activePage,
                this.recordsPerPage,
                true
              );
            }
          });
      } else {
        console.log('dont delete job');
      }
    }
  }

  async refreshExtentReport(exeHeaderId, exeHistoryId) {
    this.dataservice.loaderService.show();
    if (
      this.Job_arn != '' &&
      this.Job_arn != undefined &&
      this.Job_arn != null
    ) {
      await this.getDevicefarmJobStatus(
        this.Job_arn,
        this.SelectedJobData,
        this.suiteid,
        true
      );
    }
  }
  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  downloadAppiumFile(sessionId) {
    let link =
      environment.urlConfig.headspinBaseUrl +
      'sessions/' +
      sessionId +
      '.appium.log';

    if (sessionId != null) {
      var element = document.createElement('a');
      element.setAttribute('href', link);

      element.style.display = 'none';
      document.body.appendChild(element);

      element.click();

      document.body.removeChild(element);
    } else {
      this.toastr.errorToastr('There is no session Id for job');
    }
  }
}
