import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchTestStepComponent } from './search-test-step/search-test-step.component';

const routes: Routes = [
  {
    path: '',
    component: SearchTestStepComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchTestStepRoutingModule {}
