import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTestStepComponent } from './search-test-step.component';

describe('SearchTestStepComponent', () => {
  let component: SearchTestStepComponent;
  let fixture: ComponentFixture<SearchTestStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTestStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTestStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
