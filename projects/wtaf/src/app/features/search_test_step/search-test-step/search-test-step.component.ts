import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../shared/services/data.service';
import { async } from '@angular/core/testing';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Type } from '../../models';
import { Role } from '../../../models/role';
import * as moment from 'moment';
declare var $: any;
@Component({
  selector: 'wtaf-search-test-step',
  templateUrl: './search-test-step.component.html',
  styleUrls: ['./search-test-step.component.css']
})
export class SearchTestStepComponent implements OnInit {
  projectTypeId = 0;
  error: string;
  projectTypeList: any = [];
  successMessage: String = '';
  errorMessage: String = '';
  regionId: Number = 0;
  brandId: Number = 0;
  testSuiteId: Number = 0;
  testCaseId: Number = 0;
  testStepId: any;
  testStepList: any = [];
  testCaseList: any = [];
  dataOrder: any = [];
  stepOrderJson: [];
  regionList: any = [];
  brandList: any = [];
  testSuiteList: any = [];
  issyncFlag = true;
  testSuiteRes: string = '';
  isFromCase = true;
  txtSearch = '';
  isTestCaseSelected = false;
  updatedStepOrderNum: any;
  parameters: Array<any> = [];
  sequenceNum: any;
  sortedStepId: any;
  sortedSteps: any = [];
  isDraggable = false;
  isSort = false;
  role: string = '';
  Admin: string = '';
  Test_lead: string = '';
  Test_Mgr: string = '';
  isSharedParaFlag: boolean = false;
  addSharedcaseFlag: boolean = false;
  constructor(
    public toastr: ToastrManager,
    private router: Router,
    private dataservice: DataService,
    private activatedRoute: ActivatedRoute
  ) {}

  async ngOnInit() {
    // await this.getAllRegion();
    this.role = localStorage.getItem('userRole');
    this.Admin = Role.Admin;
    this.Test_lead = Role.Test_Lead;
    this.Test_Mgr = Role.Test_Manager;
    let flag = Boolean(
      this.activatedRoute.snapshot.paramMap.get('isFromCreateStep')
    );

    // let isfromTestCase=Boolean(
    //   this.activatedRoute.snapshot.paramMap.get('fromTestCase')
    // );
    if (!flag) {
      this.cleareAllLocalStorage(flag);
    }
    await this.getAllProjectType();
    let QueryRegionID = this.activatedRoute.snapshot.paramMap.get('regionId');
    //drag and drop
    function fixWidthHelper(e, ui) {
      ui.children().each(function() {
        $(this).width($(this).width());
      });
      return ui;
    }
    let jQueryInstance = this;
    $('#tblTestStep').sortable({
      //   connectWith: 'table',
      items: 'tbody tr',
      //  items: 'tr',
      cursor: 'pointer',
      axis: 'y',
      opacity: 0.35,
      dropOnEmpty: true,
      helper: fixWidthHelper,
      containment: '#tblTestStep',

      start: function(e, ui) {
        jQueryInstance.isSort = true;
        console.log('UI----', ui);
        jQueryInstance.sortedSteps = [];
        jQueryInstance.dataOrder = [];

        ui.item.addClass('selected');
      },
      stop: function(e, ui) {
        jQueryInstance.isSort = true;
        ui.item.removeClass('selected');
        var listValues = [];
        $(this)
          .find('tr')
          .each(function(index) {
            if (index > 0) {
              var rowDetails = $(this)
                .find('td')
                .eq(0)
                .html(index);
              console.log('rowDetails----', rowDetails);
              jQueryInstance.updatedStepOrderNum = index;
              var sortstepId = $(this)
                .find('td')
                .eq(1)
                .html();
              jQueryInstance.sortedStepId = sortstepId;
              console.log('New sequence==', jQueryInstance.updatedStepOrderNum);
              console.log('New sequence==', jQueryInstance.sortedStepId);
              listValues.push(jQueryInstance.updatedStepOrderNum);
              jQueryInstance.sortedSteps.push(jQueryInstance.sortedStepId);

              jQueryInstance.dataOrder = listValues;
              console.log('steps', jQueryInstance.dataOrder);
              console.log('ID', jQueryInstance.sortedSteps);

              var sortedIDs = $(this).sortable('toArray');
              console.log('sortedIds----', sortedIDs);
            }
            var widget = $('.selector').sortable('widget');
          });
      }
    });

    if (QueryRegionID != null) {
      this.regionId = parseInt(QueryRegionID);
    }
    
    if (this.regionId > 0) {
      
      this.isFromCase = false;
      this.projectTypeId = parseInt(
        this.activatedRoute.snapshot.paramMap.get('projectTypeId')
      );
      await this.getAllRegion_byProjectTypeId(this.projectTypeId);

      this.regionId = parseInt(
        this.activatedRoute.snapshot.paramMap.get('regionId')
      );
      await this.getBrandsByRegionId(this.regionId);

      this.brandId = parseInt(
        this.activatedRoute.snapshot.paramMap.get('brandId')
      );
      await this.getAllTestSuite(this.regionId, this.brandId);

      this.testSuiteId = parseInt(
        this.activatedRoute.snapshot.paramMap.get('testSuiteId')
      );
      this.isSharedParaFlag =
        this.activatedRoute.snapshot.paramMap.get('isShared') == null
          ? false
          : Boolean(
              this.activatedRoute.snapshot.paramMap.get('isShared') == 'true'
            );
      this.testCaseId = parseInt(
        this.activatedRoute.snapshot.paramMap.get('testCaseId')
      );
      if (this.isSharedParaFlag) {
        await this.getSharedTestCaseById(this.testCaseId);
      } else {
        await this.getTestCaseByTestSuiteId(this.testSuiteId);
      }
      this.testStepList = [];
      await this.getAllTestStep(
        this.regionId,
        this.brandId,
        this.testSuiteId,
        this.testCaseId
      );

      this.isTestCaseSelected = true;
    } else {
      this.isFromCase = true;
    }
  }

  /* change region and get brands*/
  changeRegion(e) {
    this.testStepList = [];
    this.testCaseList = [];
    this.testStepId = 0;
    this.testSuiteList = [];
    this.testSuiteId = 0;
    this.testCaseId = 0;
    this.regionId = parseInt(e.target.value);
    this.getBrandsByRegionId(this.regionId);
  }

  changeBrand(e) {
    this.testStepList = [];
    this.testCaseList = [];
    this.testStepId = 0;
    this.testSuiteList = [];
    this.testSuiteId = 0;
    this.testCaseId = 0;
    this.brandId = parseInt(e.target.value);
    if(this.isSharedParaFlag){
      this.getAllSharedCase();
    }else{
      this.getAllTestSuite(this.regionId, this.brandId);
    }
  }
  changeTestSuite(e) {
    this.testCaseList = [];
    this.testCaseId = 0;
    this.isSort = false;
    this.getTestCaseByTestSuiteId(parseInt(e.target.value));
    console.log('test suite id', parseInt(e.target.value));
  }

  changeProjectType(event) {
    this.regionList = [];
    this.brandList = [];
    this.testSuiteList = [];
    this.testStepList = [];
    this.testCaseList = [];
    this.projectTypeId = event.target.value;
    this.getAllRegion_byProjectTypeId(this.projectTypeId);
  }

  changeTestCase(e) {
    this.isSort = false;
    this.isTestCaseSelected = true;
    this.getAllTestStep(
      this.regionId,
      this.brandId,
      this.testSuiteId,
      parseInt(e.target.value)
    );
  }

  async getAllRegion_byProjectTypeId(typeId) {
    await this.dataservice
      .getAllRegionByProjectType(typeId)
      .toPromise()
      .then(
        (response: {}) => {
          this.regionList = response;
          //to place NAR region at first position in drop down
          this.regionList.unshift(
            this.regionList.splice(
              this.regionList.findIndex(item => item.regionName === 'NAR'),
              1
            )[0]
          );
          console.log(this.regionList);
          if (this.regionList.length == 1) {
            this.regionId = this.regionList[0].regionId;
          } else {
            this.regionId = 0;
          }

          if (localStorage.getItem('regionId') != null) {
            this.regionId = parseInt(localStorage.getItem('regionId'));
          }

          if (this.regionId > 0) {
            this.getBrandsByRegionId(this.regionId);
          }
        },
        error => {
          this.error = error;
        }
      );
  }

  async getAllProjectType() {
    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(
        response => {
          // Success
          this.projectTypeList = response;
          if (response != null) {
            if (this.projectTypeList.length == 1) {
              this.projectTypeId = this.projectTypeList[0].projectTypeId;
            } else {
              this.projectTypeId = 0;
            }
          }
          if (localStorage.getItem('projectTypeId') != null) {
            this.projectTypeId = parseInt(
              localStorage.getItem('projectTypeId')
            );
          }

          if (this.projectTypeId > 0) {
            this.getAllRegion_byProjectTypeId(this.projectTypeId);
          }
        },
        error => {
          this.error = error;
        }
      );
  }

  /* get all regions  */
  async getAllRegion() {
    await this.dataservice
      .getAllRegion()
      .toPromise()
      .then(
        async response => {
          this.regionList = response;
          if (response != null) {
            if (this.regionList.length == 1) {
              this.regionId = this.regionList[0].regionId;
            } else {
              this.regionId = 0;
            }
          }
          if (this.regionId > 0) {
            this.testCaseList = [];
            this.testStepId = 0;
            this.testSuiteList = [];
            this.testSuiteId = 0;
            this.testCaseList = [];
            this.testCaseId = 0;
            this.getBrandsByRegionId(this.regionId);
          }
        },
        error => {
          this.error = error;
        }
      );
  }

  /* get all brands by region Id */
  async getBrandsByRegionId(regionId) {
    await this.dataservice
      .getBrandsByRegionId(regionId)
      .toPromise()
      .then(
        response => {
          this.brandList = response;
          if (this.brandList.length == 1) {
            this.brandId = this.brandList[0].brandId;
          } else {
            this.brandId = 0;
            this.testSuiteId = 0;
            this.testCaseId = 0;
          }

          if (localStorage.getItem('brandId') != null) {
            this.brandId = parseInt(localStorage.getItem('brandId'));
          }

          if (this.brandId > 0) {
            this.getAllTestSuite(this.regionId, this.brandId);
          }

          if (this.isFromCase == true) {
          }
        },
        error => {
          this.error = error;
        }
      );
  }

  /* get all test suite  */
  async getAllTestSuite(regionId, brandId) {
    
    let TestSuiteName = Type.TestSuiteName;
    let asc = Type.ascending;
    await this.dataservice
      .getAllTestSuite(TestSuiteName, asc, regionId, brandId, this.projectTypeId, '',0,0)
      .toPromise()
      .then(
        async response => {
          this.testSuiteList = response;
          console.log('test suite list------', this.testSuiteList);

          if (response != null) {
            if (this.testSuiteList.length == 1) {
              this.testSuiteId = this.testSuiteList[0].testSuiteId;
            } else {
              this.testSuiteId = 0;
            }
          } else {
            this.testSuiteId = 0;
          }

          if (localStorage.getItem('testSuiteId') != null) {
            this.testSuiteId = parseInt(localStorage.getItem('testSuiteId'));
          }

          if (this.testSuiteId > 0) {
            await this.getTestCaseByTestSuiteId(this.testSuiteId);
          }
          if (this.isFromCase == true) {
          }
        },
        error => {
          this.error = error;
          // this.loading = false;
        }
      );
  }
async getAllSharedCase() {
  let testCaseId = Type.TestCaseId;
  let desc = Type.descending;
  await this.dataservice
  .getAllSharedTestCasebyProjects(testCaseId, desc, this.regionId, this.brandId, this.projectTypeId,'',this.isSharedParaFlag,0)
  .toPromise()
  .then(
    async response => {
      console.log('shared case list------', response);
        this.testCaseList = response;
      if (response != null) {
        if (this.testCaseList.length == 1) {
          this.testCaseId = this.testCaseList[0].testCaseId;
        } else {
          this.testCaseId = 0;
        }
      } else {
        this.testCaseId = 0;
      }
    },
    error => {
      this.error = error;
      // this.loading = false;
    }
  );
}
  /* get all test case  */
  async getAllTestCase(regionId, brandId, testSuiteId) {
    let TestCaseName = Type.TestCaseName;
    let asc = Type.ascending;
    await this.dataservice
      .getAllTestCase(TestCaseName, asc, regionId, brandId, testSuiteId)
      .toPromise()
      .then(
        async response => {
          this.testCaseList = response;
          console.log('this.testCaseList------', this.testCaseList);

          console.log(response);
          if (response != null) {
            if (this.testCaseList.length == 1) {
              this.testCaseId = parseInt(this.testCaseList[0].testCaseId);
            } else {
              this.testCaseId = 0;
            }
            this.getAllTestStep(
              this.regionId,
              this.brandId,
              this.testSuiteId,
              this.testCaseId
            );
          }
        },
        error => {
          this.error = error;
        }
      );
  }

  /* get all test case  */
  async getTestCaseByTestSuiteId(testSuiteId) {
    console.log('test suite id', testSuiteId);

    let TestCaseName = Type.TestCaseName;
    let asc = Type.ascending;
    await this.dataservice
      .getTestCaseByTestSuiteId(TestCaseName, asc, testSuiteId)
      .toPromise()
      .then(
        async response => {
          this.testCaseList = response;
          console.log('response-----', response);
          if (response != null) {
            if (this.testCaseList.length == 1) {
              this.testCaseId = parseInt(this.testCaseList[0].testCaseId);
            }
          } else {
            this.testCaseId = 0;
          }

          if (localStorage.getItem('testCaseId') != null) {
            this.testCaseId = parseInt(localStorage.getItem('testCaseId'));
          }

          if (this.isFromCase == true) {
            this.isTestCaseSelected = false;
            this.testStepList = [];
            if (this.testCaseId > 0) {
              this.getAllTestStep(
                this.regionId,
                this.brandId,
                this.testSuiteId,
                this.testCaseId
              );
            }
          }
        },
        error => {
          this.error = error;
        }
      );
  }
  /* get shared test case by test case Id */
  async getSharedTestCaseById(testCaseId) {
    this.testCaseId = testCaseId;
    await this.dataservice
      .getSharedTestCaseById(testCaseId)
      .subscribe(async response => {
        this.testCaseList = [response];
        this.testCaseId = parseInt(this.testCaseList[0].testCaseId);
      });
  }
  /* get all test step  */
  async getAllTestStep(regionId, brandId, testSuiteId, testCaseId) {
    this.dataservice
      .getAllTestStep(regionId, brandId, testSuiteId, testCaseId)
      .toPromise()
      .then(
        async response => {
          console.log('testStepList', response);
          this.isTestCaseSelected == true;
          this.testStepList = response;
          console.log('sorted--', this.testStepList);
          if (this.testStepList != null) {
            this.testStepList.forEach(element => {
              element.IsSelected = false;
              let modifiedOn_LocalTime = moment
                .utc(element.modifiedOn)
                .toDate();
              console.log('date time transform', modifiedOn_LocalTime);
              element.convert_modifiedTime_format = moment(
                modifiedOn_LocalTime
              ).format('YYYY-MM-DD HH:mm:ss');
              console.log(
                'modifiedOn_LocalTime..',
                element.convert_modifiedTime_format
              );
            });
          }
          this.dataservice.customFilter['testStepName'] = '';
          if (
            this.testSuiteList != null &&
            this.testSuiteList != [] &&
            this.testSuiteList.length > 0 &&
            this.testSuiteList != undefined
          ) {
            console.log('this.testSuiteList -- > ' + this.testSuiteList[0]);
            var lenChecked = this.testSuiteList.filter(
              o => o.syncWithJira != true && o.testSuiteId == testSuiteId
            );
            if (lenChecked.length > 0) {
              console.log('Done');
              this.issyncFlag = false;
            } else {
              this.issyncFlag = true;
            }

            //  }
          }

          this.cleareAllLocalStorage(false);
        },
        error => {
          this.error = error;
        }
      );
  }

  /* this method clear all messages and variables */
  clearData(val) {
    // this.txtSearch = " ";
    var r = false;
    if (
      (this.projectTypeId != 0 ||
        this.regionId != 0 ||
        this.brandId != 0 ||
        this.testSuiteId != 0 ||
        this.testCaseId != 0) &&
      val == 0
    ) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      this.testSuiteId = 0;
      this.errorMessage = '';
      this.regionId = 0;
      this.regionList = [];
      this.projectTypeId = 0;
      this.projectTypeList = [];
      this.brandId = 0;
      this.testStepId = 0;
      this.testCaseId = 0;
      this.brandList = [];
      this.testSuiteList = [];
      this.isTestCaseSelected = false;
      this.testCaseList = [];
      this.issyncFlag = true;
      this.isFromCase = true;
      this.testStepList = [];
      this.projectTypeId = 0;
      this.dataservice.customFilter['testStepName'] = '';
      this.cleareAllLocalStorage(false);
      this.getAllProjectType();
    }
  }
  clearData_delete() {
    setTimeout(() => {
      this.successMessage = '';
    }, 2000);

    this.errorMessage = '';
    this.testStepId = 0;
    this.issyncFlag = true;
    this.isFromCase = true;
    this.testStepList = [];
    this.getAllTestStep(
      this.regionId,
      this.brandId,
      this.testSuiteId,
      this.testCaseId
    );
  }

  getTestStepById(testStepId, projectTypeId) {
    localStorage.setItem('projectTypeId', this.projectTypeId.toString());
    localStorage.setItem('regionId', this.regionId.toString());
    localStorage.setItem('brandId', this.brandId.toString());
    localStorage.setItem('testSuiteId', this.testSuiteId.toString());
    localStorage.setItem('testCaseId', this.testCaseId.toString());
    this.router.navigate([
      'test_step/create_test_step',
      { id: testStepId, projectTypeid: projectTypeId }
    ]);
  }

  /* delete test case by test case Id */
  deleteTestStep(testStepId, testStepName) {
    var isDelete = confirm(
      "Test step '" +
        testStepName +
        "' will be deleted. Click OK to confirm or CANCEL to go back."
    );
    if (isDelete) {
      this.dataservice.deleteTestStep(testStepId).subscribe(
        response => {
          this.toastr.successToastr('Test step deleted successfully');
          this.clearData_delete();
        },

        error => {
          this.error = error;
        }
      );
    }
  }

  //Synch with jira by testSuiteId
  syncWithJira() {
    if (this.testSuiteId != null) {
      console.log('test suite Id', this.testSuiteId);

      this.dataservice.syncWithJira(this.testSuiteId).subscribe(res => {
        console.log('jira synch response', res);
        this.testSuiteRes = res;

        if (this.testSuiteRes != '') {
          this.issyncFlag = true;
          this.testCaseList = [];
          this.testStepId = 0;
          this.testSuiteList = [];
          this.testSuiteId = 0;
          this.testCaseList = [];
          this.testCaseId = 0;
          this.getAllTestSuite(this.regionId, this.brandId);
        } else {
          this.issyncFlag = false;
        }
      });
    }
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }

  submitReorder() {
    let reorderDetails = [];
    console.log('listValues------', this.testStepList);
    console.log('lenth of the list', this.testStepList.length);
    for (let m = 0; m < this.sortedSteps.length; m++) {
      console.log('testStepId', this.testStepList[m].testStepId);
      console.log('After testStepOrderNumber', this.dataOrder[m]);

      reorderDetails.push({
        testStepId: this.sortedSteps[m],
        testStepOrderNumber: this.dataOrder[m]
      });
    }
    console.log('Updated Details---', reorderDetails);
    if (reorderDetails.length != 0) {
      this.dataservice.addReorderTestStep(reorderDetails).subscribe(res => {
        this.toastr.successToastr('Test Step  Reordered  successfully');
        this.getAllTestStep(
          this.regionId,
          this.brandId,
          this.testSuiteId,
          this.testCaseId
        );
      });
    } else {
      this.toastr.warningToastr(' Please Reorder the test step. ');
    }
  }
  cancelReorder() {
    var r = false;
    if (this.isSort != false) {
      r = confirm('Are you sure you want to cancel data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.getAllTestStep(
        this.regionId,
        this.brandId,
        this.testSuiteId,
        this.testCaseId
      );
    }
    this.isSort = false;
  }

  cleareAllLocalStorage(flag) {
    flag = false;
    localStorage.removeItem('projectTypeId');
    localStorage.removeItem('regionId');
    localStorage.removeItem('brandId');
    localStorage.removeItem('testSuiteId');
    localStorage.removeItem('testCaseId');
  }
  checksharedtestcase(event) {
    if (event.target.checked) {
      this.addSharedcaseFlag = true;
      this.isSharedParaFlag = this.addSharedcaseFlag;
      this.testSuiteId = 0;
      this.testSuiteList = [];
      this.testStepList = [];
      this.getAllSharedCase();
    } else {
      this.addSharedcaseFlag = false;
      this.isSharedParaFlag = this.addSharedcaseFlag;
      this.testSuiteList = [];
      this.testCaseList = [];
      this.testStepList = [];
      this.getAllTestSuite(this.regionId, this.brandId);
    }
  }
}
