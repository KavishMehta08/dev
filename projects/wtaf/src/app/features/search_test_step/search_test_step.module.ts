import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { SearchTestStepComponent } from './search-test-step/search-test-step.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { SearchTestStepRoutingModule } from './search_test_step-routing.module';
@NgModule({
  declarations: [SearchTestStepComponent],
  imports: [CommonModule, SharedModule,SearchTestStepRoutingModule,Ng2SearchPipeModule]
})
export class SearchTestStepModule {}
