import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { UserComponent } from './user/user.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


import { UserRoutingModule } from './user-routing.module';
@NgModule({
  declarations: [UserComponent],
  imports: [CommonModule, SharedModule,UserRoutingModule,Ng2SearchPipeModule]
})
export class UserModule {}
