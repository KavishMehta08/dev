import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { Type } from '../../models';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from '../../../shared/services/loader.service';

@Component({
  selector: 'wtaf-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  submitted = false;
  successMessage: String = '';
  roleList: any = [];
  userList: any = [];
  profileId: Number = 0;
  errorMessage: String = '';
  inValid: Boolean = false;
  isTextChanged: Boolean = false;
  oldUser: string = '';
  txtSearch = '';
  userId: number;
  userForm = this.formBuilder.group({
    userId: ['', [Validators.required,Validators.maxLength(10),noWhitespaceValidator]],
    userName: ['', [Validators.required,noWhitespaceValidator]],
    userRoleId: ['', [Validators.required]]
  });
  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    public dataservice: DataService,
    public loaderService: LoaderService

  ) { }

  ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log("local usr id", this.userId);
    this.getAllRole();
    this.getAllUser();
  }

  get f() {
    return this.userForm.controls;
  }

  //  Added by Akash - Sorting Table Asc Desc
  orderedArray = [{
    userId: "ASC",
    userName: "ASC",
    userRoleName: "ASC",
  }]
  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
    this.userList = this.sort_by_key(this.userList, key, order);
    console.log(this.userList);
    this.loaderService.hide();
  }
  sort_by_key(array, key, order) {
    if (order === "ASC") {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];

        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
    else {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
  }
  /* for duplication check */
  async checkDuplication() {

    const userDetails = this.userForm.value;
    if (
      userDetails['userName'] != null &&
      userDetails['userName'] != 0 &&
      userDetails['userName'] != undefined
    ) {
      userDetails.resource = this.userForm.value.userId.toString().trim();
    }
    userDetails.type = Type.User;
    userDetails.id = 0;
    await this.dataservice
      .checkDuplication(userDetails)
      .toPromise()
      .then((response: Boolean) => {
        // Success
        this.inValid = response;
        if (this.inValid) {
          this.toastr.errorToastr('UserId "' + userDetails.resource + '" is already exists');
        } else {
          this.errorMessage = '';
        }
      });
  }

  /* add and update method for user */
  async saveUser() {
    
    this.submitted = true;

    // if (this.profileId == 0 || this.isTextChanged) {
    //   if(this.userForm.value.userId != null)
    //   {
    //     await this.checkDuplication();
    //   }
    // }
    if (this.profileId == 0 ) { //|| this.isTextChanged
      await this.checkDuplication();
    }

    if (this.userForm.valid && !this.inValid) {
      const userDetails = this.userForm.value;
      userDetails.userRoleId = parseInt(this.userForm.value.userRoleId);
      userDetails.loggedInUserId = this.userId;
      userDetails.profileId = this.profileId;
      this.dataservice.saveUser(userDetails).subscribe(response => {
        if (this.profileId != 0) {
          this.toastr.successToastr('User updated successfully');
        } else {
          this.toastr.successToastr('User created successfully');
        }
        this.clearData(1);
        this.getAllUser();
      });
    }
  }

  /* get user by user profile Id */
  async getUserById(profileId) {
    this.clearData(1);
    this.profileId = profileId;
    await this.dataservice.getUserById(profileId).subscribe(response => {
      console.log('data', response);
      this.oldUser = response['userId'];
      this.userForm.controls['userId'].setValue(response['userId']);
      this.userForm.controls['userName'].setValue(response['userName']);
      this.userForm.controls['userRoleId'].setValue(response['userRoleId']);
      window.scroll(0, 0);
    });
  }

  /* get all roles  */
  getAllRole() {
    let roleName = Type.RoleName;
    let asc = Type.ascending;
    this.dataservice.getAllRole(roleName, asc).subscribe((response: {}) => {
      this.roleList = response;
      if (response != null) {
        if (this.roleList.length == 1) {
          this.userForm.controls['userRoleId'].setValue(
            this.roleList[0].roleId
          );
        } else {
          this.userForm.controls['userRoleId'].setValue('');
        }
      }
    });
  }

  /* get all users  */
  getAllUser() {
    this.dataservice.getAllUser().subscribe((response: {}) => {
      this.userList = response;
      console.log('response',response);
      
      this.dataservice.customFilter['userName'] = '';
    });
  }

  /* this method clear all messages and user form also */
  clearData(val) {
    // this.txtSearch = " ";
    var r = false;
    if ((this.userForm.value.userId != 0 || this.userForm.value.userName != 0 || this.userForm.value.userRoleId != 0) && val == 0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.submitted = false;
      this.userForm.reset();
      setTimeout(() => {
        this.successMessage = '';
      }, 1000);
      this.userForm = this.formBuilder.group({
        userId: ['', [Validators.required,noWhitespaceValidator]],
        userName: ['', [Validators.required,noWhitespaceValidator]],
        userRoleId: ['', [Validators.required]]
      });
      this.userForm.controls['userRoleId'].setValue('');
      this.profileId = 0;
      this.errorMessage = '';
      this.inValid = false;
      this.oldUser = '';
    }
  }

  /* delete user  by profile Id*/
  deleteUser(profileId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this user?'
    );
    if (isDelete) {
      this.dataservice.deleteUser(profileId).subscribe(
        response => {
          this.toastr.successToastr('User deleted successfully');
          this.getAllUser();
          this.clearData(1);
        },
        error => {
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
          setTimeout(() => {
            this.errorMessage = '';
            this.dataservice.errorWhenInUse = '';
          }, 1000);
        }
      );
    }
  }

  changeData(e) {
    if (
      this.oldUser.toString().trim() != e.target.value.toString().trim() &&
      this.oldUser != ''
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }

  changeRole() {
    this.isTextChanged = true;
  }

  onlyNumberValidation(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 12 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}
