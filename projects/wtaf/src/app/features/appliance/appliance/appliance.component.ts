import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  Validators,
  FormBuilder,
  FormControl
} from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { Type } from '../../models';
import { async } from '@angular/core/testing';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from '../../../shared/services/loader.service';

@Component({
  selector: 'wtaf-appliance',
  templateUrl: './appliance.component.html',
  styleUrls: ['./appliance.component.css']
})
export class ApplianceComponent implements OnInit {
  submitted = false;
  applianceId: Number = 0;
  successMessage: String = '';
  errorMessage: String = '';
  applianceList: any = [];
  applianceCategoryList: any = [];
  inValid: Boolean = false;
  isTextChanged: Boolean = false;
  strOldName: String = '';

  txtSearch = '';
  brandList: any = [];
  regionList: any = [];
  brandId = 0;
  changeAppliance: string = '';
  projectTypeList: any = [];
  userId: number;
  applianceForm = this.formBuilder.group({
    applianceCategoryId: ['', [Validators.required]],
    applianceCode: ['', [Validators.required,noWhitespaceValidator]],
    brandId: ['', [Validators.required]],
    regionId: ['', [Validators.required]],
    applianceName: ['', [Validators.required,noWhitespaceValidator]],
    applianceDesc: [''],
    projectTypeId: ['', [Validators.required]]
  });

  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    public loaderService: LoaderService

  ) { }

  ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log("local usr id", this.userId);
    this.getAllApplianceCategory();
    this.getAllAppliance();
    this.getAllProjectType();
  }

  get f() {
    return this.applianceForm.controls;
  }

  //  Added by Akash - Sorting Table Asc Desc
  orderedArray = [{
    projectTypeName: "ASC",
    regionName: "ASC",
    brandName: "ASC",
    applianceCategoryName: "ASC",
    applianceCode: "ASC",
    applianceName: "ASC"
  }]
  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
    this.applianceList = this.sort_by_key(this.applianceList, key, order);
    console.log(this.applianceList);
    this.loaderService.hide();
  }
  sort_by_key(array, key, order) {
    if (order === "ASC") {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];

        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
    else {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
  }
  /* for duplication check */
  async checkDuplication() {
    const applianceDetails = this.applianceForm.value;
    if (
      this.applianceForm.value.applianceCategoryId != 0 &&
      this.applianceForm.value.applianceCategoryId != ''
    ) {
      applianceDetails.resource = this.applianceForm.value.applianceCode
        .toString()
        .trim();
      applianceDetails.type = Type.Appliance;
      applianceDetails.id = parseInt(
        this.applianceForm.value.applianceCategoryId
      );
      await this.dataservice
        .checkDuplication(applianceDetails)
        .toPromise()
        .then((response: Boolean) => {
          // Success
          this.inValid = response;
          if (this.inValid) {
            this.toastr.errorToastr(
              'Appliance code "' + applianceDetails.resource + '" is already exists for selected appliance category');
            this.isTextChanged = false;
          } else {
            this.errorMessage = '';
          }
        });
    }
  }

  /* add and update method for appliance */
  async saveAppliance() {
    this.inValid = false;
    this.submitted = true;
    if (this.applianceId == 0 || this.isTextChanged) {
      await this.checkDuplication();
    }


    if (this.applianceForm.valid && !this.inValid) {
      const applianceDetails = this.applianceForm.value;
      applianceDetails.userId = this.userId;
      applianceDetails.applianceId = this.applianceId;
      this.dataservice.saveAppliance(applianceDetails).subscribe(async response => {
        if (this.applianceId != 0) {
          this.toastr.successToastr('Appliance updated successfully');
        } else {
          this.toastr.successToastr('Appliance created successfully');
        }
       this.clearData(1);
      await this.getAllAppliance();
      },
      error =>
      {
        return this.toastr.errorToastr(this.dataservice.error_500_status);
      });
    }
  }

  // --------Get All Project Types
  async getAllProjectType() {

    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        // Success
        this.projectTypeList = response;
        if (response != null) {
          if (this.projectTypeList.length == 1) {
          } else {
            this.applianceForm.controls['projectTypeId'].setValue('');
          }
        }
      });
  }

  changeProjectType(event) {
    this.regionList = [];
    this.brandList = [];
    this.applianceForm.controls['applianceCategoryId'].setValue('');
    this.applianceForm.controls['applianceName'].setValue('');
    this.applianceForm.controls['applianceCode'].setValue('');
    this.applianceForm.controls['applianceDesc'].setValue('');
    this.applianceForm.controls['brandId'].setValue('');
    if (this.applianceForm.controls['projectTypeId'].value != '') {
      this.getAllRegion_byProjectTypeId(
        this.applianceForm.controls['projectTypeId'].value
      );
    }
  }

  async getAllRegion_byProjectTypeId(typeId) {
    await this.dataservice
      .getAllRegionByProjectType(typeId)
      .toPromise()
      .then(async (response: {}) => {
        this.regionList = response;
        // to place NAR region at first position in frop down
        this.regionList.unshift(this.regionList.splice(this.regionList.findIndex(item => item.regionName === 'NAR'), 1)[0])
        console.log(this.regionList);
        if (response != null) {
          if (this.regionList.length == 1) {
            this.applianceForm.controls['regionId'].setValue(
              this.regionList[0].regionId
            );
            this.getBrandsByRegionId(this.regionList[0].regionId);
          } else {
            this.applianceForm.controls['regionId'].setValue('');
          }
        }
      });
  }

  /* get all applianceCategories  */
  getAllApplianceCategory() {
    let ApplianceCategoryName = Type.ApplianceCategoryName;
    let asc = Type.ascending;
    this.dataservice.getAllApplianceCategory(ApplianceCategoryName, asc).subscribe((response: {}) => {
      this.applianceCategoryList = response;
      if (response != null) {
        if (this.applianceCategoryList.length == 1) {
          this.applianceForm.controls['applianceCategoryId'].setValue(
            this.applianceCategoryList[0].applianceCategoryId
          );
        } else {
          this.applianceForm.controls['applianceCategoryId'].setValue('');
        }
      }
    });
  }

  /* get all appliance */
 async getAllAppliance() {
   
    await this.dataservice.getAllAppliance().then(res => {
      // Success
      console.log('Appliances', res);
      this.applianceList = res;
      // this.dataservice.customFilter['applianceCategoryName'] = '';
    });

    // .subscribe((response: {}) => {
    //   this.applianceList = response;
    // });
  }

  /* get appliance category by applianceId */
  async getApplianceById(applianceId) {
    this.clearData(1);
    this.applianceId = applianceId;
    await this.dataservice
      .getByApplianceId(applianceId)
      .toPromise()
      .then(async response => {

        await this.getAllRegion_byProjectTypeId(response['projectTypeId']);
        await this.getBrandsByRegionId(response['regionId']);
        this.strOldName = response['applianceCode'];
        this.brandId = response['brandId'];
        this.applianceForm.patchValue(response);
        window.scroll(0, 0);
      });
  }

  /* this method clear all messages and applianceForm also */

  clearData(val) {
    var r = false;
    if ((this.applianceForm.value.projectTypeId != 0 || this.applianceForm.value.applianceName != 0 || this.applianceForm.value.applianceCategoryId != 0
      || this.applianceForm.value.applianceCode != 0 || this.applianceForm.value.applianceDesc != 0 || this.applianceForm.value.regionId != 0 || this.applianceForm.value.brandId != 0) && val == 0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.submitted = false;
      this.applianceForm.reset();
      this.applianceForm = this.formBuilder.group({
        applianceCategoryId: ['', [Validators.required]],
        applianceCode: ['', [Validators.required,noWhitespaceValidator]],
        brandId: ['', [Validators.required]],
        regionId: ['', [Validators.required]],
        applianceName: ['', [Validators.required,noWhitespaceValidator]],
        applianceDesc: [''],
        projectTypeId: ['', [Validators.required]]
      });
      this.applianceId = 0;
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      this.errorMessage = '';
      this.inValid = false;
      this.strOldName = '';
    }
  }

  /* delete appliance */
  deleteApplianceById(applianceId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this Appliance?'
    );
    if (isDelete) {
      this.dataservice.deleteApplianceById(applianceId).subscribe(
        async response => {
          this.toastr.successToastr('Appliance deleted successfully');
        await  this.getAllAppliance();
          this.clearData(1);
        },
        error => {
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
          setTimeout(() => {
            this.errorMessage = '';
            this.dataservice.errorWhenInUse = '';
          }, 2000);
        }
      );
    }
  }

  changeData(e) {
    this.changeAppliance = e.target.value
      .toString()
      .trim()
      .toUpperCase();
    if (
      this.strOldName
        .toString()
        .trim()
        .toUpperCase() != this.changeAppliance &&
      this.strOldName.toUpperCase() != ''
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }

  /* get all brands by region Id */
  async getBrandsByRegionId(regionId) {
    await this.dataservice
      .getBrandsByRegionId(regionId)
      .toPromise()
      .then(response => {


        console.log('brands', this.brandList);
        this.brandList = response;
        if (response != null) {
          if (this.brandList.length == 1) {
            this.brandId = this.brandList[0].brandId;
            this.applianceForm.controls['brandId'].setValue(
              this.brandList[0].brandId
            );
          } else {
            this.applianceForm.controls['brandId'].setValue('');
          }
        } else {
          this.brandId = 0;
          this.applianceForm.controls['brandId'].setValue('');
        }
      });
  }

  /* get all regions */
  getAllRegion() {
    this.dataservice.getAllRegion().subscribe((response: {}) => {
      this.regionList = response;
      if (response != null) {
        if (this.regionList.length == 1) {
          this.applianceForm.controls['regionId'].setValue(
            this.regionList[0].regionId
          );
        } else {
          this.applianceForm.controls['regionId'].setValue('');
        }
      }
    });
  }

  /* change region and get brands*/
  changeRegion(e) {
    this.applianceForm.controls['applianceName'].setValue('');
    this.applianceForm.controls['applianceCode'].setValue('');
    this.applianceForm.controls['applianceDesc'].setValue('');
    this.applianceForm.controls['applianceCategoryId'].setValue('');
    this.getBrandsByRegionId(parseInt(e.target.value));
  }

  changeApplianceCatagory(e) {
    this.isTextChanged;
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}