import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { ApplianceComponent } from './appliance/appliance.component';
import { ApplianceRoutingModule } from './appliance-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [ApplianceComponent],
  imports: [CommonModule, SharedModule,ApplianceRoutingModule,Ng2SearchPipeModule]
})
export class ApplianceModule {}
