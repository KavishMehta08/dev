import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CapabilityTemplateComponent } from './capability-template/capability-template.component';
import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    component: CapabilityTemplateComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CapabilityTemplateRoutingModule {}
