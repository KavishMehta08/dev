import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
  FormArray
} from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from '../../../shared/services/loader.service';
import { Chart } from 'chart.js';
import { DataService } from '../../../shared/services/data.service';
import { Router, RouterLink } from '@angular/router';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { Type } from '../../models';
import { Role } from '../../../models/role';
import { HttpResponse } from '@angular/common/http';

declare var $: any;
@Component({
  selector: 'wtaf-capability-template',
  templateUrl: './capability-template.component.html',
  styleUrls: ['./capability-template.component.css']
})
export class CapabilityTemplateComponent implements OnInit {
  submitted = false;
  successMessage: String = '';
  capabilityTemplateList: any = [];
  capabilityTemplateId: Number = 0;
  errorMessage: String = '';
  inValid: Boolean = false;
  isTextChanged: Boolean = false;
  strOldName: String = '';
  changeDocumentName: String = '';
  txtSearch = '';
  userId: number;
  role: string = '';
  Admin: string = '';
  Test_lead: string = '';
  Test_Mgr: string = '';
  capabilitytemplateForm: FormGroup;
  isEditable: boolean = false;
  allCapabilityList: any = [];
  IsEdit: boolean;
  msg_capTemplate: String = '';
  addCapSuccess: string = '';
  capabilityError: string = '';
  filtered: any = [];
  deletedCapability: any = [];
  deletedCapList: any = [];
  capTemplateList: any = [];
  capabilityTemplatedetails: any = [];
  updatedCapTempList;
  deviceType;
  oldTemplateName;

  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    public dataservice: DataService,
    public loaderService: LoaderService
  ) {}
  @ViewChild('createTemplateModal', { static: false })
  createTemplateModal: TemplateRef<any>;

  ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log('local usr id', this.userId);
    this.role = localStorage.getItem('userRole');
    this.Admin = Role.Admin;
    this.Test_lead = Role.Test_Lead;
    this.Test_Mgr = Role.Test_Manager;
    this.getAllCapabilityTemplates();
    console.log('after get all ', this.capabilityTemplateList);
    this.capabilitytemplateForm = this.formBuilder.group({
      templateName: ['', Validators.required],
      capabilityIdValuesViews: this.formBuilder.array([
        this.formBuilder.group({
          capabilityId: ['', Validators.required],
          capabilityValue: ['', Validators.required]
        })
      ])
    });
  }

  get f() {
    return this.capabilitytemplateForm.controls;
  }

  orderedArray = [
    {
      capabilityTemplateId: 'ASC',
      capabilityTemplateName: 'ASC'
    }
  ];

  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    console.log('in sort table ', this.orderedArray[0][key]);
    this.orderedArray[0][key] = order == 'ASC' ? 'ASC' : 'DESC';
    this.capabilityTemplateList = this.sort_by_key(
      this.capabilityTemplateList,
      key,
      order
    );
    console.log(this.capabilityTemplateList);
    this.loaderService.hide();
  }

  sort_by_key(array, key, order) {
    if (order === 'ASC') {
      return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];

        if (
          (x != null && x != undefined && x != '') ||
          (y != null && y != undefined && y != '')
        ) {
          return String(x).toLowerCase() < String(y).toLowerCase()
            ? -1
            : String(x).toLowerCase() > String(y).toLowerCase()
            ? 1
            : 0;
        }
      });
    } else {
      return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];
        if (
          (x != null && x != undefined && x != '') ||
          (y != null && y != undefined && y != '')
        ) {
          return String(x).toLowerCase() > String(y).toLowerCase()
            ? -1
            : String(x).toLowerCase() < String(y).toLowerCase()
            ? 1
            : 0;
        }
      });
    }
  }
  /* for duplication check */
  async checkDuplication() {
    const capabilityTemplateDetails = this.capabilitytemplateForm.value;
    if (
      capabilityTemplateDetails['capabilityTemplateName'] != null &&
      capabilityTemplateDetails['capabilityTemplateName'] != 0 &&
      capabilityTemplateDetails['capabilityTemplateName'] != undefined
    ) {
      capabilityTemplateDetails.resource = this.capabilitytemplateForm.value.capabilityTemplateName
        .toString()
        .trim();
    }
    capabilityTemplateDetails.type = Type.User;
    capabilityTemplateDetails.id = 0;
    await this.dataservice
      .checkDuplication(capabilityTemplateDetails)
      .toPromise()
      .then((response: Boolean) => {
        // Success
        this.inValid = response;
        if (this.inValid) {
          this.toastr.errorToastr(
            'Capability Template "' +
              capabilityTemplateDetails.resource +
              '" already exists'
          );
        } else {
          this.errorMessage = '';
        }
      });
  }

  getAllCapabilityTemplates() {
    this.dataservice
      .getcapabilityTemplateName(0)
      .toPromise()
      .then(response => {
        this.capabilityTemplateList = response;
        this.dataservice.customFilter['capabilityTemplateName'] = '';
      });
    console.log('end ', this.capabilityTemplateList);
  }

  /* delete capabilityTemplate*/
  deleteCapabilityTemplate(capabilityTemplateName) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this Capability Template?'
    );
    if (isDelete) {
      this.dataservice
        .deleteEntireCapabilityTemplate(capabilityTemplateName, this.userId)
        .subscribe(
          response => {
            this.toastr.successToastr(
              'Capability Template deleted successfully'
            );
            this.getAllCapabilityTemplates();
            this.clear();
          },
          error => {
            this.toastr.errorToastr(
              ' ' + this.dataservice.errorWhenInUse + ' '
            );
            setTimeout(() => {
              this.errorMessage = '';
              this.dataservice.errorWhenInUse = '';
            }, 1000);
          }
        );
    }
  }

  //close modal
  closeModal() {
    this.errorMessage = '';
    $('#createTemplateModal').modal('hide');
  }

  clearFormGroup() {
    //
    this.capabilitytemplateForm = this.formBuilder.group({
      templateName: [''],
      capabilityIdValuesViews: this.formBuilder.array([])
    });
    this.capabilitytemplateForm = this.formBuilder.group({
      templateName: ['', Validators.required],
      capabilityIdValuesViews: this.formBuilder.array([
        this.formBuilder.group({
          capabilityId: ['', Validators.required],
          capabilityValue: ['', Validators.required]
        })
      ])
    });

    this.submitted = false;
  }

  //reset capability template form
  clear() {
    //
    this.isEditable = false;
    this.clearFormGroup();
    this.capabilitytemplateForm.reset();

    this.inValid = false;
    this.errorMessage = '';
    this.capabilityError = '';
    this.capTemplateList = [];
    this.deletedCapability = [];
    this.deletedCapList = [];
    this.oldTemplateName = '';
  }

  get capabilityIdValuesViews() {
    return this.capabilitytemplateForm.get(
      'capabilityIdValuesViews'
    ) as FormArray;
  }

  async getAllCapability(platformId) {
    this.allCapabilityList = [];
    await this.dataservice
      .getCapabilitiesByProjectTypeId(platformId)
      .toPromise()
      .then(async (response: any) => {
        this.allCapabilityList = response;
        console.log('allcapabilities', this.allCapabilityList);
      })
      .catch(err => {
        this.loaderService.hide();
        console.log(err);
      });
  }

  addcapabilityRow() {
    //
    if (this.isEditable != true) {
      this.capabilityIdValuesViews.push(
        this.formBuilder.group({
          capabilityId: ['', Validators.required],
          capabilityValue: ['', Validators.required]
        })
      );
    } else {
      this.capabilityIdValuesViews.push(
        this.formBuilder.group({
          capabilityId: ['', Validators.required],
          capabilityValue: ['', Validators.required],
          capTemplateId: 0
        })
      );
    }
    this.submitted = true;
  }

  async onSelectionChange(e, row_index) {
    this.capabilityError = '';
    const capabilityId = e.target.value;
    let ds = capabilityId;

    let capabilityTemplate = this.allCapabilityList.filter(function(item) {
      return item.capabilityId == ds;
    });

    let capabilityName = capabilityTemplate[0].capabilityName;

    this.capabilityIdValuesViews.value[
      row_index
    ].capabilityName = capabilityName;

    let dup_capTemplate = this.dataservice.checkIfCapabilityTemplateHasDuplicates(
      this.capabilityIdValuesViews.value
    );

    if (dup_capTemplate != null) {
      this.capabilityError = 'Selected Template Capability already added.';
      this.toastr.errorToastr('Selected Template Capability already added.');
    }
  }

  deleteCapabilityRow(index) {
    this.deletedCapability = this.capabilityIdValuesViews.value[index];

    let ds = this.deletedCapability.capabilityId;
    this.filtered = this.capabilityIdValuesViews.value.filter(item => {
      return item.capabilityId == ds;
    });

    if (this.filtered.length > 0) {
      this.deletedCapList.push(this.deletedCapability);
    }
    this.capabilityIdValuesViews.removeAt(index);
    delete this.capTemplateList[index];
  }

  //Save capability Template
  async savecapabilityTemplate() {
    this.submitted = true;
    if (!this.IsEdit) {
      return;
    }
    if (this.capabilitytemplateForm.invalid == true) {
      return;
    }

    if (
      this.capabilitytemplateForm.valid &&
      !this.inValid &&
      this.capabilityError == ''
    ) {
      this.capabilityTemplatedetails = this.capabilitytemplateForm.value;

      if (this.capabilityTemplatedetails.capabilityIdValuesViews.length == 0) {
        this.toastr.errorToastr('Please add capabilities to template');
        return;
      }

      let templateName = this.capabilityTemplatedetails.templateName;
      this.capabilityTemplatedetails.userId = this.userId;
      this.capabilityTemplatedetails.usrTemplateName = templateName;
      this.capabilityTemplatedetails.templateName = this.oldTemplateName;
      console.log('capabilityTemplatedetails', this.capabilityTemplatedetails);

      await this.dataservice
        .AddCapabilityTemplate(this.capabilityTemplatedetails)
        .toPromise()
        .then(async res => {
          const response = res;
          console.log('add/update capability', response);
          if (response != null) {
            this.toastr.successToastr('Capability updated successfully');
          }
        })
        .catch(err => {
          console.log(err);
          this.toastr.errorToastr(
            'Something went wrong while updating capability'
          );
        });
    }
    console.log(
      'this.deletedCapability_________this.deletedCapability',
      this.deletedCapability
    );

    if (this.deletedCapability != null) {
      for (let k = 0; k < this.deletedCapability.length; k++) {
        let capid = this.deletedCapability[k].capabilityId;
        let IsDelete = this.capabilityIdValuesViews.value.filter(function(
          item
        ) {
          //
          return item.capabilityId == capid;
        });
        if (IsDelete > 0) {
          delete this.deletedCapability[k];
        }
      }
    }

    if (this.deletedCapability != null && this.isEditable == true) {
      for (let k = 0; k < this.deletedCapList.length; k++) {
        this.updatedCapTempList = this.deletedCapList[k].capTemplateId;
        let df_capabilityId = this.deletedCapList[k].capabilityId;
        let df_usertemplate_name = this.capabilitytemplateForm.value
          .templateName;

        await this.dataservice
          .deleteCapabilityTemplateById(this.updatedCapTempList, this.userId)
          .toPromise()
          .then(async res => {
            const response = res;
            console.log('delete template capability', response);
            if (response != null) {
            }
          })
          .catch(err => {
            console.log(err);
            this.toastr.errorToastr(
              'Something went wrong while deleting capability'
            );
          });
      }
    }
    this.closeModal();
    this.getAllCapabilityTemplates();
  }

  //get capabiity template by name
  async getTemplateByName(capabilityTemplate, isEdit) {
    this.clear();
    this.capabilityError = '';
    this.loaderService.show();
    if (isEdit) {
      this.IsEdit = true;

      console.log('isEdit', isEdit);
    } else {
      this.IsEdit = false;
    }
    console.log('this.IsEdit', this.IsEdit);
    this.errorMessage = '';
    this.inValid = false;

    this.capabilitytemplateForm = this.formBuilder.group({
      templateName: ['', Validators.required],
      capabilityIdValuesViews: this.formBuilder.array([])
    });

    await this.dataservice
      .getcapabilityTemplateByName(capabilityTemplate)
      .toPromise()
      .then(async (res: any) => {
        console.log('capability Template Name', res);
        let capabilityIdValuesViews_res = res['capabilityIdValuesViews'];

        const capability_result = [];
        const map = new Map();
        for (const item of capabilityIdValuesViews_res) {
          if (!map.has(item.capabilityId)) {
            map.set(item.capabilityId, true); // set any value to Map
            capability_result.push({
              capTemplateId: item.capTemplateId,
              capabilityId: item.capabilityId,
              capabilityName: item.capabilityName,
              capabilityValue: item.capabilityValue,
              platformId: item.platformId
            });
          }
        }

        res['capabilityIdValuesViews'] = capability_result;
        console.log('unique_result......get tempalte name', capability_result);

        if (res != null) {
          this.isEditable = true;

          this.oldTemplateName = res.templateName;

          let platformId = capabilityIdValuesViews_res[0].platformId;

          this.allCapabilityList = this.allCapabilityList.filter(o => {
            return o.platformId == platformId;
          });

          await this.getAllCapability(platformId);

          let udid = res['capabilityIdValuesViews'].filter(capability => {
            return capability.capabilityName == 'udid';
          });

          if (typeof udid[0] === 'undefined') {
            this.deviceType = 'Local';
          } else {
            let udidValue = udid[0].capabilityValue;
            console.log('udidValue ', udidValue);

            console.log(
              'device farm ',
              udidValue.indexOf('arn:aws:devicefarm')
            );

            if (udidValue.indexOf('arn:aws:devicefarm') !== -1) {
              this.deviceType = 'DeviceFarm';
            } else {
              this.deviceType = 'Local';
            }
          }

          //
          for (let i = 0; i < res['capabilityIdValuesViews'].length; i++) {
            this.capabilityIdValuesViews.push(
              this.formBuilder.group({
                capabilityId: [
                  {
                    value: res['capabilityIdValuesViews'][i].capabilityId,
                    disabled: !this.IsEdit
                  },
                  Validators.required
                ],
                capabilityValue: [
                  {
                    value: res['capabilityIdValuesViews'][i].capabilityName,
                    disabled: !this.IsEdit
                  },
                  Validators.required
                ],
                capabilityName: [
                  {
                    value: res['capabilityIdValuesViews'][i].capabilityName,
                    disabled: !this.IsEdit
                  },
                  Validators.required
                ],
                capTemplateId: res['capabilityIdValuesViews'][i].capTemplateId
              })
            );
          }

          this.capabilitytemplateForm.patchValue(res);
          this.capabilitytemplateForm.controls['templateName'].setValue(
            res.usrTemplateName
          );

          if (this.IsEdit == false) {
            this.capabilitytemplateForm.controls['templateName'].disable();
          } else {
            this.capabilitytemplateForm.controls['templateName'].enable();
          }

          console.log(
            'sfssssffff',
            this.capabilitytemplateForm.controls['templateName'].setValue(
              res.usrTemplateName
            )
          );

          $('#createTemplateModal').modal('show');
        } else {
          return;
        }
      })
      .catch(err => {
        console.log(err);
        this.toastr.errorToastr('Something went wrong');
      });
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }

  changeRole() {
    this.isTextChanged = true;
  }
}

export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? { whitespace: true } : null;
}
