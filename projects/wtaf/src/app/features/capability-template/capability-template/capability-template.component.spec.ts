import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapabilityTemplateComponent } from './capability-template.component';

describe('CapabilityTemplateComponent', () => {
  let component: CapabilityTemplateComponent;
  let fixture: ComponentFixture<CapabilityTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CapabilityTemplateComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapabilityTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
