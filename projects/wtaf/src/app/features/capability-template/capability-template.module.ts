import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { CapabilityTemplateComponent } from './capability-template/capability-template.component';

import { CapabilityTemplateRoutingModule } from './capability-template-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgPipesModule } from 'ngx-pipes';
@NgModule({
  declarations: [CapabilityTemplateComponent],
  imports: [
    CommonModule,
    SharedModule,
    CapabilityTemplateRoutingModule,
    Ng2SearchPipeModule,
    NgPipesModule
  ]
})
export class CapabilityTemplateModule {}
