import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { TestStepComponent } from './test-step/test-step.component';

import { TestStepRoutingModule } from './test_step-routing.module';
@NgModule({
  declarations: [TestStepComponent],
  imports: [CommonModule, SharedModule,TestStepRoutingModule]
})
export class TestStepModule {}
