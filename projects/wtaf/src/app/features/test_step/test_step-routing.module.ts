import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestStepComponent } from './test-step/test-step.component';

const routes: Routes = [
  {
    path: '',
    component: TestStepComponent,
    data: { title: '' },
    children:[
      {
        path: '',
        redirectTo: 'search_test_step',
        pathMatch: 'full'
      },
      {
        path: 'search_test_step',
        loadChildren: () =>
        import('../search_test_step/search_test_step.module').then(
          m => m.SearchTestStepModule
        ),
      }, 
       {
        path: 'create_test_step',
        loadChildren: () =>
        import('../create_test_step/create_test_step.module').then(
          m => m.CreateTestStepModule
        ),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestStepRoutingModule {}
