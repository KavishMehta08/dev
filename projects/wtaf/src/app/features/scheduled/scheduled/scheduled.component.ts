import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';
import {
  HttpClient,
  HttpRequest,
  HttpHeaders,
  HttpEventType,
  HttpResponse
} from '@angular/common/http';
import { InterceptorSkipHeader } from './../../../core/auth/jwt.interceptor';
import { LoaderService } from '../../../shared/services/loader.service';
import { Subject } from 'rxjs';
import { TimeFormat } from '../../../shared/pipes/timeformat.pipe';
import { DatePipe } from '@angular/common';
import { Role } from '../../../models/role';
import { ToastrManager } from 'ng6-toastr-notifications';
import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'wtaf-scheduled',
  templateUrl: './scheduled.component.html',
  styleUrls: ['./scheduled.component.css']
})
export class ScheduledComponent implements OnInit, OnDestroy {
  schedules_Details: any = [];
  custom_schedules_Details: any;
  txtSearch: string;
  modalFlag: boolean = false;
  modalweekFlag: boolean = false;
  Frequancy: any;
  repeatFrequancy: any;
  scheduleTime: any;
  StartDate: any;
  endDate: any;
  EndDate: any;
  active: boolean;
  hide_custom: boolean = false;
  hide_weekdays: boolean = false;
  exeHeaderId: any;
  updateresponse: any;
  deleteresponse: any;
  destroy$: Subject<boolean> = new Subject<boolean>();
  frequancy_option = [
    { id: 0, name: 'Does not repeat' },
    { id: 1, name: 'Daily' },
    { id: 2, name: 'Weekly' },
    { id: 3, name: 'Custom' }
  ];
  ocuurance_option = [
    { id: 3, name: 'Weeks' },
    { id: 4, name: 'Months' }
  ];
  repeatCount: number;
  userName: string = '';
  role: string = '';
  Admin: string = '';
  Test_lead: string = '';
  Test_Mgr: string = '';
  weekdays = [];
  dayNames = [
    'sunday',
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday'
  ];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];
  activePage: number = 1;
  totalRecords: number = 0;
  pagesCount: number;
  recordsPerPage: number = 25;
  searchTxt = '';
  totalPages: number = 0;
  toolTip = 'Search By User or Job Name';

  constructor(
    private dataservice: DataService,
    private http: HttpClient,
    public loaderService: LoaderService,
    public datepipe: DatePipe,
    public toastr: ToastrManager
  ) {}

  ngOnInit() {
    this.role = localStorage.getItem('userRole');
    this.userName = localStorage.getItem('userDisplayName');
    this.Admin = Role.Admin;
    this.Test_lead = Role.Test_Lead;
    this.Test_Mgr = Role.Test_Manager;
    this.getScheduleDetails('', this.activePage, this.recordsPerPage, false);
    let dtToday = new Date();
    let month: any = dtToday.getMonth() + 1;
    let day: any = dtToday.getDate();
    let year = dtToday.getFullYear();
    if (month < 10) month = '0' + month.toString();
    if (day < 10) day = '0' + day.toString();

    let maxDate = year + '-' + month + '-' + day;
    $('#StartDate').attr('min', maxDate);
    this.repeatFrequancy = 4;
  }

  onChangeActivePage(paginationObj) {

    let pageSize;
    let currntPage =paginationObj.currentPage;
    let currntRecords = parseInt(paginationObj.recordsPerPage);
    console.log('paginationObj', paginationObj);
    this.pagesCount = paginationObj.pages.length;
    pageSize = this.pagesCount;
    this.totalPages = paginationObj.totalPages;
    if (this.activePage != currntPage || this.recordsPerPage != currntRecords) {
      this.recordsPerPage = currntRecords;
      this.activePage = currntPage;
      this.getScheduleDetails(this.searchTxt, this.activePage, this.recordsPerPage, false)
    }
    else {
      this.recordsPerPage = currntRecords;
      this.activePage = currntPage;
    }
   
  }

   //Get Search text from search component using @Output 
   onScheduleTxtSearch(searchTxt: string) {
    this.searchTxt = searchTxt;
    this.activePage = 1;
    this.getScheduleDetails(searchTxt, this.activePage, this.recordsPerPage, false);
  }
  getScheduleDetails(searchTxt, pageNo, recordsPerPage, isFromDelete) {
    
      // In DB Index is treat as current page and it start from 0
      let currentPage = pageNo - 1;
    this.dataservice.getScheduleDetails(searchTxt, currentPage, recordsPerPage).subscribe((response: any) => {
     
      // If is from delete job and isFromDelete var is true then active - 1
      if (isFromDelete && (response.listOfRecords == 0 || response.listOfRecords ==null)) {
        this.activePage = this.activePage - 1;
        this.getScheduleDetails(searchTxt, this.activePage, recordsPerPage, isFromDelete);
      }
      this.schedules_Details = response.listOfRecords;
      this.totalRecords = response.totalCount;
      let updateddata;
      if(this.schedules_Details != null && this.schedules_Details != undefined)
      { 
         updateddata = this.schedules_Details.map((schedule)=>{
          var temp = Object.assign({}, schedule);
          let tdate = this.datepipe.transform(temp.modifiedOn, 'yyyy-MM-dd');
          let commondate = tdate.toString() +" " +  temp.time;
          let modifiedOn_LocalTime = moment.utc(commondate).toDate();
          let convert_modifiedTime_format = moment(modifiedOn_LocalTime).format('HH:mm:ss');
          if(temp.dbTimeZone == 'UTC'){
            temp.time = convert_modifiedTime_format;
          }
          return temp;
        });
      }
     
       this.schedules_Details = updateddata;
      if(this.schedules_Details == null || this.schedules_Details == undefined){
        this.schedules_Details = [];
      }
    },
    error => {
      this.toastr.errorToastr('Something went wrong');
      console.log(error);
      
    });
  }
  opencustomscheduleModal(scheduledata) {
    this.modalFlag = true;
    this.hide_weekdays = false;
    this.Frequancy = scheduledata.frequency;
    this.scheduleTime = scheduledata.time;
    this.repeatCount = scheduledata.repeatSchedule;
    this.active = scheduledata.active;
    this.StartDate = this.datepipe.transform(
      scheduledata.startDate,
      'yyyy-MM-dd'
    );
    this.endDate = this.datepipe.transform(scheduledata.endDate, 'yyyy-MM-dd');
    this.hide_custom = true;
    $('.weekday').prop('checked', false);
    $('#StartDate').prop('disabled', true);
    $('#StartDate').prop('disabled', true);
    $('#scheduleTime').prop('disabled', true);
    $('#Frequancy').prop('disabled', true);
    $('#repeatFrequancy').prop('disabled', true);
    $('#repeatCount').prop('disabled', true);
    $('#EndDate').prop('disabled', true);
    $('#switch-sm').prop('disabled', true);
    if (scheduledata.daysOfWeek != '' && scheduledata.daysOfWeek != null) {
      let data = scheduledata.daysOfWeek.split(',');
      data.map(weekday => {
        this.weekdaycheck(weekday);
      });
      $('.weekday').prop('disabled', true);

      this.hide_weekdays = true;
      this.repeatFrequancy = 3;

      this.modalweekFlag = true;
    } else if (this.Frequancy == 4) {
      this.Frequancy = 3;
      this.hide_weekdays = false;
      this.repeatFrequancy = 4;
    }
    this.custom_schedules_Details = scheduledata;
    console.log(this.custom_schedules_Details);
    $('#testSuiteTemplateModal').modal('show');
  }
  weekdaycheck(weekday) {
    switch (weekday) {
      case 'monday':
        $('#weekday-mon').prop('checked', true);
        break;
      case 'tuesday':
        $('#weekday-tue').prop('checked', true);
        break;
      case 'wednesday':
        $('#weekday-wed').prop('checked', true);
        break;
      case 'thursday':
        $('#weekday-thu').prop('checked', true);
        break;
      case 'friday':
        $('#weekday-fri').prop('checked', true);
        break;
      case 'saturday':
        $('#weekday-sat').prop('checked', true);
        break;
      case 'sunday':
        $('#weekday-sun').prop('checked', true);
        break;
      default:
    }
  }
  onFrequency_Change(evalue) {
    evalue == 3 ? (this.hide_custom = true) : (this.hide_custom = false);
    if(evalue == 3){this.repeatFrequancy =4; this.repeatCount =0;}
  }
  onOccurance_Change(ocuval) {
    ocuval == 3 ? (this.hide_weekdays = true) : (this.hide_weekdays = false);
  }
  onChange(e) {
    this.endDate = this.datepipe.transform(e.target.value, 'yyyy-MM-dd');
    $('#EndDate').attr('min', e.target.value);
  }
  getscheduleJobDetails(schedule) {
    if (schedule) {
      if ($('#' + schedule.exeHeaderId).hasClass('out')) {
        $('#' + schedule.exeHeaderId).addClass('in');
        $('#' + schedule.exeHeaderId).removeClass('out');
        $('#s' + schedule.exeHeaderId).removeClass('caret');
        $('#s' + schedule.exeHeaderId).addClass('caret caret-down');
      } else {
        $('#' + schedule.exeHeaderId).addClass('out');
        $('#' + schedule.exeHeaderId).removeClass('in');
        $('#s' + schedule.exeHeaderId).removeClass('out');
        $('#s' + schedule.exeHeaderId).removeClass('caret caret-down');
        $('#s' + schedule.exeHeaderId).addClass('caret');
      }
    }
  }

  selectDays(e) {
    if (e.target.checked) {
      const resultArr = this.weekdays.filter(
        arrObj => arrObj !== e.target.value
      );
      if (resultArr.length === this.weekdays.length) {
        resultArr.push(e.target.value);
        this.weekdays = resultArr;
      }
    } else {
      const resultArr = this.weekdays.filter(
        arrObj => arrObj !== e.target.value
      );
      this.weekdays = resultArr;
    }
    console.log(this.weekdays);
  }
  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }
  deleteSchedule(headerid) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this schedule?'
    );
    if (isDelete) {
      this.dataservice.deleteSchedule(headerid).subscribe(
        response => {
          this.deleteresponse = response;
          if (this.deleteresponse.Status == 1) {
            this.toastr.successToastr('Schedule deleted sucessfully');
            this.getScheduleDetails('', this.activePage, this.recordsPerPage, true);
          } else if (this.deleteresponse.Status == 0) {
            this.toastr.errorToastr(
              'Schedule is in Progress you can not delete it'
            );
          }
        },
        error => {
          this.toastr.errorToastr('Something went wrong');
        }
      );
    }
  }
  updateSchedule(scheduledata) {
    $('#StartDate').prop('disabled', false);
    $('#StartDate').prop('disabled', false);
    $('#scheduleTime').prop('disabled', false);
    $('#Frequancy').prop('disabled', false);
    $('#repeatFrequancy').prop('disabled', false);
    $('#repeatCount').prop('disabled', false);
    $('#EndDate').prop('disabled', false);
    $('#switch-sm').prop('disabled', false);
    $('.weekday').prop('disabled', false);
    this.modalFlag = false;

    this.exeHeaderId = scheduledata.exeHeaderId;
    this.hide_weekdays = false;
    this.hide_custom = false;
    this.Frequancy = scheduledata.frequency;
    this.scheduleTime = scheduledata.time;
    this.repeatCount = scheduledata.repeatSchedule;
    this.active = scheduledata.active;
    this.StartDate = this.datepipe.transform(
      scheduledata.startDate,
      'yyyy-MM-dd'
    );
    this.endDate = this.datepipe.transform(scheduledata.endDate, 'yyyy-MM-dd');
    $('#EndDate').attr('min', this.datepipe.transform(scheduledata.startDate, 'yyyy-MM-dd'));
    if (
      scheduledata.daysOfWeek != '' &&
      scheduledata.daysOfWeek != null &&
      this.Frequancy == 3
    ) {
      this.hide_custom = true;
      let data = scheduledata.daysOfWeek.split(',');
      this.weekdays = data;
      data.map(weekday => {
        this.weekdaycheck(weekday);
      });
      // $('.weekday').prop('disabled', true);

      this.hide_weekdays = true;
      this.repeatFrequancy = 3;

      this.modalweekFlag = true;
    } else if (this.Frequancy == 4) {
      this.hide_custom = true;
      this.Frequancy = 3;
      this.hide_weekdays = false;
      this.repeatFrequancy = 4;
    }
    this.custom_schedules_Details = scheduledata;
    $('#testSuiteTemplateModal').modal('show');
  }
  hideModel() {
    $('#testSuiteTemplateModal .close').click();
  }
  updateScheduleDetails() {
    let today = this.dayNames[new Date(this.StartDate.toString()).getDay()];
    if (this.repeatFrequancy == 3) {
      const sorter = {
        monday: 1,
        tuesday: 2,
        wednesday: 3,
        thursday: 4,
        friday: 5,
        saturday: 6,
        sunday: 7
      };
      this.weekdays.length !=0?this.weekdays:this.weekdays.push('monday');
      this.weekdays.sort(function sortByDay(a, b) {
        let day1 = a.toLowerCase();
        let day2 = b.toLowerCase();
        return sorter[day1] - sorter[day2];
      });
    }
    let tdate = this.datepipe.transform(this.StartDate, 'yyyy-MM-dd');
    let commondate = tdate.toString() +" " +  this.scheduleTime;
    let convert_modifiedTime_format = moment(commondate).format('HH:mm');
    let var_ScheduleDetails = {
      active: this.active,
      startDate: this.StartDate,
      endDate: this.endDate,
      executeTime: convert_modifiedTime_format  +':'+Intl.DateTimeFormat().resolvedOptions().timeZone,
      frequency: this.Frequancy == 3 ? this.repeatFrequancy : this.Frequancy,
      repeatSchedule: this.Frequancy == 3 ? this.repeatCount : 0,
      exeHeaderId: this.exeHeaderId,
      userId: localStorage.getItem('userId'),
      daysOfWeek:
        this.repeatFrequancy == 3
          ? this.weekdays.join()
          : this.Frequancy == 2
          ? today.toString()
          : ''
    };
    let currenttime = new Date();
    let scheduletime = this.scheduleTime;
    let nowtime = this.datepipe.transform(currenttime, 'HH:mm');
    let regExp = /(\d{1,2})\:(\d{1,2})/;

    if (
      new Date(this.datepipe.transform(currenttime, 'yyyy-MM-dd')).getTime() ===
      new Date(this.datepipe.transform(this.StartDate, 'yyyy-MM-dd')).getTime() && this.Frequancy < 1 
    ) {
      
      if (
        parseInt(nowtime.replace(regExp, '$1$2')) >
        parseInt(scheduletime.replace(regExp, '$1$2'))
      ) {
        this.toastr.errorToastr('Please select current or future time');
      } else {
        
        console.log(var_ScheduleDetails);

        this.dataservice.updateScheduleDetails(var_ScheduleDetails).subscribe(
          response => {
            this.updateresponse = response;
            if (this.updateresponse.Status == 1) {
              this.toastr.successToastr('Schedule updated sucessfully');
              this.hideModel();
              this.getScheduleDetails('', this.activePage, this.recordsPerPage, false);
            } else if (this.updateresponse.Status == 0) {
              this.toastr.errorToastr(
                'Schedule is in progress you can not Update it'
              );
              this.hideModel();
            }
          },
          error => {
            this.toastr.errorToastr('Something went wrong');
            this.hideModel();
          }
        );
      }
    } else {
      this.dataservice.updateScheduleDetails(var_ScheduleDetails).subscribe(
        response => {
          this.updateresponse = response;
          if (this.updateresponse.Status == 1) {
            this.toastr.successToastr('Schedule updated sucessfully');
            this.hideModel();
            this.getScheduleDetails('', this.activePage, this.recordsPerPage, false);
          } else if (this.updateresponse.Status == 0) {
            this.toastr.errorToastr(
              'Schedule is in Progress you can not Update it'
            );
            this.hideModel();
          }
        },
        error => {
          this.toastr.errorToastr('Something went wrong');
          this.hideModel();
        }
      );
    }
  }
  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
