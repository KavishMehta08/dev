import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ScheduledComponent } from './scheduled/scheduled.component';
import { ScheduledRoutingModule } from './scheduled-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgPipesModule} from 'ngx-pipes';

@NgModule({
  declarations: [ScheduledComponent],
  imports: [CommonModule, SharedModule,ScheduledRoutingModule,Ng2SearchPipeModule,NgPipesModule]
})
export class ScheduledModule {}
