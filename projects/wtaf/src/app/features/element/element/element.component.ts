import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import * as XLSX from 'xlsx';
import { async } from '@angular/core/testing';
import { Type } from '../../models';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from '../../../shared/services/loader.service';
declare var $: any;
@Component({
  selector: 'wtaf-element',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.css']
})
export class ElementComponent implements OnInit {
  submitted = false;
  successMessage: String = '';
  errorMessage: String = '';
  elementList: any = [];
  brandList: any = [];
  regionList: any = [];
  projectTypeList: any = [];
  platformList: any = [];
  platformId: Number = 0;
  brandId: Number = 0;
  elementId: Number = 0;
  data: any = [];
  selectedFile: File;
  isRadioChecked = 1;
  isPlatformNotSelected: Boolean = false;
  inValid: Boolean = false;
  isTextChanged: Boolean = false;

  form: any;
  txtSearch = '';
  strOldName = '';
  oldPlatformId: number = 0;
  Platformchange: number = 0;
  brandChange: number = 0;
  changeElementName: string = '';
  oldBrand: number = 0;
  arrElements: any = [];
  distElement: any = [];
  userId:number;
  isSave: boolean = false;
  elementForm = this.formBuilder.group({
    brandId: ['', [Validators.required]],
    regionId: ['', [Validators.required]],
    elementName: ['', [Validators.required,noWhitespaceValidator]],
    elementDesc: [''],
    elementPath: ['', [Validators.required,noWhitespaceValidator]],
    module: ['', [Validators.required,noWhitespaceValidator]],
    screen: ['', [Validators.required,noWhitespaceValidator]],
    platformId: ['', [Validators.required]],
    projectTypeId: ['', [Validators.required]]
  });

  elementFormImport = this.formBuilder.group({
    brandId: ['', [Validators.required]],
    regionId: ['', [Validators.required]],
    platformId: ['', [Validators.required]],
    projectTypeId: ['', [Validators.required]]
  });

  elementFormExport = this.formBuilder.group({
    brandId: ['', [Validators.required]],
    regionId: ['', [Validators.required]],
    platformId: ['', [Validators.required]],
    projectTypeId: ['', [Validators.required]]
  });

  XL_row_object;
  @ViewChild('importFile', { static: false }) importFile: ElementRef;
  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    public loaderService: LoaderService

  ) {}

  async ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log("local usr id", this.userId);
    await this.getAllElement();
    await this.getAllProjectType();
    $('#importTable').hide();
  }

  get f() {
    return this.elementForm.controls;
  }
  get fExp() {
    return this.elementFormExport.controls;
  }
  get fImp() {
    return this.elementFormImport.controls;
  }

  //  Added by Akash - Sorting Table Asc Desc
  orderedArray = [{
    projectTypeName : "ASC",
    brandName: "ASC",
    module: "ASC",
    screen: "ASC",
    elementName: "ASC",
    platformName: "ASC"
  }]
  
  sortTable(key) {
    this.loaderService.show();
      let order = this.orderedArray[0][key];
      this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
      this.elementList = this.sort_by_key(this.elementList, key, order);
      console.log(this.elementList);
      this.loaderService.hide();
  }
  sort_by_key(array, key, order) {
    if (order === "ASC") {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '' ) {
        return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
    else {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '' ) {
        return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
  }
  /* add and update method for element */
  async saveElement() {
    
    this.inValid = false;
    this.submitted = true;
    this.isSave = false;
    if (
      (this.changeElementName != null &&
        this.changeElementName != undefined &&
        this.changeElementName != '' &&
        this.strOldName
          .toString()
          .trim()
          .toUpperCase() != this.changeElementName &&
        this.strOldName.toString() != '') ||
      (this.brandChange != 0 &&
        this.oldBrand != 0 &&
        this.oldBrand.toString() != this.brandChange.toString()) ||
      (this.Platformchange != 0 &&
        this.platformId != 0 &&
        this.platformId.toString() != this.Platformchange.toString())
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }

    let platformId = 0;
    let plById = [];
    let result = [];
    
    if (this.XL_row_object != null && this.XL_row_object != undefined) {
      if (this.XL_row_object.length != 0) {
        this.XL_row_object.map(obj => {
          obj.projectTypeId = parseInt(
            this.elementFormImport.value.projectTypeId
          );
          obj.brandId = parseInt(this.elementFormImport.value.brandId);
          // or via brackets
          console.log('XL_row_object---------->', obj);
          return obj;
        });

        let result = this.XL_row_object.filter(o =>
          this.elementList.some(
            ({ brandId, projectTypeId, elementName }) =>
              o.brandId === brandId &&
              o.projectTypeId === projectTypeId &&
              o.Element === elementName
          )
        );

        console.log('Duplicate-------->', result);

        result.forEach(element => {
          this.arrElements.push(element.Element);
        });
        console.log('result======>', this.arrElements);

        var flags = [],
          output = [],
          l = this.arrElements.length,
          i;
        for (i = 0; i < l; i++) {
          if (flags[this.arrElements[i]]) continue;
          flags[this.arrElements[i]] = true;
          this.distElement.push(this.arrElements[i]);
        }
        console.log('output======>', this.distElement);

        if (
          this.elementFormImport.value.projectTypeId > 0 &&
          this.platformList != null
        ) {
          platformId = this.elementFormImport.value.platformId;

          if (platformId != null && platformId != undefined) {
            plById = this.platformList.filter(
              pl => pl.platformId === parseInt(platformId.toString())
            );
          }
        }
        
        if (
          plById.length > 0 &&
          plById[0]['platformName'].toString() === 'Android'
        ) {
          if (!this.XL_row_object[0].hasOwnProperty('Android')) {
            this.importFile.nativeElement.value = '';
            this.toastr.errorToastr('Selected File has invalid data.');
            return;
          }
        } else if (
          plById.length > 0 &&
          plById[0]['platformName'].toString() === 'IOS'
        ) {
          if (!this.XL_row_object[0].hasOwnProperty('IOS')) {
            this.importFile.nativeElement.value = '';
            
            this.toastr.errorToastr('Selected File has invalid data.');
            return;
          }
        } else {
          if (
            !this.XL_row_object[0].hasOwnProperty('Web') ||
            !this.XL_row_object[0].hasOwnProperty('Web')
          ) {
            this.importFile.nativeElement.value = '';
            this.toastr.errorToastr('Selected File has invalid data.');
            return;
          }
        }
      }
    }

    if (this.elementId == 0 || this.isTextChanged) {
      await this.checkDuplication();
    }

    if (this.isRadioChecked == 1) {
      this.form = this.elementForm;
    } else if (this.isRadioChecked == 2) {
      
      this.form = this.elementFormImport;
      if (this.selectedFile == null) {
        this.toastr.errorToastr('Please select file to import.');
        return;
      }
    }

    if (this.form.value.projectTypeId > 0 && this.platformList != null) {
      let platformId = this.form.value.platformId;
      if (platformId == 0 || platformId == null || platformId == undefined) {
        this.isPlatformNotSelected = true;
      } else {
        this.isPlatformNotSelected = false;
      }
    } else {
      this.isPlatformNotSelected = false;
    }
    if (
      this.form.valid &&
      !this.isPlatformNotSelected &&
      this.errorMessage == '' &&
      !this.inValid &&
      (this.selectedFile != null || this.selectedFile == undefined)
    ) {
      let arrElement = [];
      if (this.isRadioChecked == 2) {
        
        $('#importTable').show();
        let doc = this;
        this.data.forEach(function(element) {
          element.brandId = doc.form.value.brandId;
          element.regionId = doc.form.value.regionId;
          element.elementName = element.Element;
          element.elementDesc =
            'Mobile App ' + element.MobileApp + ' Version' + element.Version;
          element.elementPath = element.Android || element.IOS || element.Web;
          element.module = element.Module;
          element.screen = element.Screen;
          element.platformId = doc.form.value.platformId;
          element.projectTypeId = doc.form.value.projectTypeId;
          element.userId = doc.userId;
          element.elementId = 0;
        });
        arrElement = this.data;
      } else if (this.isRadioChecked == 1) {
        const elementDetails = this.elementForm.value;
        elementDetails.brandId = this.elementForm.value.brandId;
        elementDetails.projectTypeId = parseInt(
          this.elementForm.value.projectTypeId
        );
        elementDetails.userId = this.userId;
        elementDetails.elementId = this.elementId;
        arrElement.push(elementDetails);
      }
      let isValidtoSave = false;
      if (this.distElement != null || this.distElement != undefined) {
        if (this.distElement.length > 0) {
          this.isSave = confirm(
            this.distElement +
              ' ' +
              '  duplicate element(s),do you want to override?'
          );
          if (this.isSave) {
            isValidtoSave = true;
          } else {
            isValidtoSave = false;
          }
        }
      }

      if (this.arrElements.length == 0) {
        isValidtoSave = true;
      }
      if (isValidtoSave || this.isRadioChecked == 1) {
        this.dataservice.saveElement(arrElement).subscribe(response => {
          if (this.elementId != 0) {
            this.toastr.successToastr('Element updated successfully');
          } else {
            this.toastr.successToastr('Element created successfully');
          }
          this.clearData(1);
          this.getAllElement();
          isValidtoSave = false;
          this.distElement = [];
          this.arrElements = [];
        });
      } else if (!this.isSave) {
        this.distElement = [];
        this.arrElements = [];
      }
    }
  }

  /* get element by element Id */
  async getElementById(elementId) {
    
    this.isPlatformNotSelected = false;
    this.elementId = elementId;
    await this.dataservice
      .getElementById(elementId)
      .toPromise()
      .then(async response => {
        // Success
        console.log('response -----response ', response);
        this.oldBrand = response['brandId'];
        console.log('brand Id---------------------', this.oldBrand);
        this.brandId = response['brandId'];
        this.platformId = response['platformId'];
        this.strOldName = response['elementName'];
        await this.getAllRegion_byProjectTypeId(response['projectTypeId']);
        await this.getBrandsByRegionId(response['regionId']);
        await this.getPlatformByProjectTypeId(response['projectTypeId']);
        

        this.elementForm.controls['regionId'].setValue(response['regionId']);
        this.elementForm.controls['brandId'].setValue(response['brandId']);
        this.elementForm.controls['elementName'].setValue(
          response['elementName']
        );
        this.elementForm.controls['elementDesc'].setValue(
          response['elementDesc']
        );
        this.elementForm.controls['elementPath'].setValue(
          response['elementPath']
        );
        this.elementForm.controls['module'].setValue(response['module']);
        this.elementForm.controls['screen'].setValue(response['screen']);
        this.elementForm.controls['projectTypeId'].setValue(
          response['projectTypeId']
        );

        window.scroll(0, 0);
      });
  }

  /* get all elements  */
  /* get all elements  */
  async getAllElement() {
    let ElementId=Type.ElementId;
    let desc=Type.descending;
    await this.dataservice.getAllElement(ElementId,desc).then(
      response => { // Success
         $("#importTable").hide();
        this.elementList = response;
        console.log("elemenet response----->",response);
        this.dataservice.customFilter['elementName'] = '';
      });
  }

  /* change region and get brands*/
  changeRegion(e) {
    this.getBrandsByRegionId(parseInt(e.target.value));
  }

  /* get all regions  */
  async getAllRegion() {
    await this.dataservice
      .getAllRegion()
      .toPromise()
      .then(response => {
        // Success
        this.regionList = response;
        if (response != null) {
          if (this.regionList.length == 1) {
            this.elementForm.controls['regionId'].setValue(
              this.regionList[0].regionId
            );
            this.getBrandsByRegionId(this.regionList[0].regionId);
          } else {
            this.elementForm.controls['regionId'].setValue('');
          }
        }
      });
  }

  /* get all project types  */
  async getAllProjectType() {
    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        // Success
        this.projectTypeList = response;
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.elementForm.controls['projectTypeId'].setValue(
              this.projectTypeList[0].projectTypeId
            );
            this.getPlatformByProjectTypeId(
              this.projectTypeList[0].projectTypeId
            );
          } else {
            this.elementForm.controls['projectTypeId'].setValue('');
          }
        }
      });
  }

  /* change project type and get platforms*/
  async changeProjectType(e) {
    
    this.platformList = [];
    this.regionList = [];
    this.brandList = [];
    let projectTypeid_ = e.target.value;
    if (projectTypeid_.toString() != '') {
      this.getAllRegion_byProjectTypeId(projectTypeid_);
    }
    await this.getPlatformByProjectTypeId(parseInt(e.target.value));
  }

  changeBrand(e) {
    
    this.brandChange = parseInt(e.target.value);
    console.log('brand brandbrandbrandbrandbrandbrand', this.brandChange);

    if (this.brandChange != 0) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  async getAllRegion_byProjectTypeId(typeId) {
    
    await this.dataservice
      .getAllRegionByProjectType(typeId)
      .toPromise()
      .then((response: {}) => {
        this.regionList = response;
           //to place NAR region at first position in frop down
           this.regionList.unshift(this.regionList.splice(this.regionList.findIndex(item => item.regionName === 'NAR'), 1)[0])
           console.log(this.regionList);
        if (response != null) {
          
          if (this.regionList.length == 1) {
            this.elementForm.controls['regionId'].setValue(
              this.regionList[0].regionId
            );
            this.getBrandsByRegionId(this.regionList[0].regionId);
          } else {
            this.elementForm.controls['regionId'].setValue('');
          }
        }
      });
  }

  changePlatform(e) {
    this.Platformchange = parseInt(e.target.value);
    if (this.Platformchange != 0) {
      this.isTextChanged = true;
      this.isPlatformNotSelected = false;
    } else {
      this.isTextChanged = false;
    }
  }

  /* get all platforms by project type id */
  async getPlatformByProjectTypeId(projectTypeId) {
    await this.dataservice
      .getPlatformByProjectTypeId(projectTypeId)
      .toPromise()
      .then(response => {
        // Success
        this.platformList = response;
        console.log("platformList...",this.platformList);
        
        if (response != null) {
          if (this.platformList.length == 1) {
            this.elementForm.controls['platformId'].setValue(
              this.platformList[0].platformId
            );
          } else {
            this.elementForm.controls['platformId'].setValue('');
          }
        } else {
          this.elementForm.controls['platformId'].setValue('');
        }
        if (this.platformId > 0) {
          this.elementForm.controls['platformId'].setValue(this.platformId);
        }
      });
  }

  /* get all brands by region Id */
  async getBrandsByRegionId(regionId) {
    await this.dataservice
      .getBrandsByRegionId(regionId)
      .toPromise()
      .then(response => {
        // Success
        console.log('brands', this.brandList);
        this.brandList = response;
        if (response != null) {
          if (this.brandList.length == 1) {
            this.brandId = this.brandList[0].brandId;
            this.elementForm.controls['brandId'].setValue(
              this.brandList[0].brandId
            );
          } else {
            this.elementForm.controls['brandId'].setValue('');
          }
        } else {
          this.elementForm.controls['brandId'].setValue('');
        }
      });
  }

  /* this method clear all messages and element form also */
clearData(val) {
  
  // this.txtSearch =" ";
  var r = false;
  if (this.isRadioChecked == 1) {
    if ((this.elementForm.value.brandId != 0 || this.elementForm.value.regionId != 0 || this.elementForm.value.elementName != 0 || this.elementForm.value.elementDesc != 0 || this.elementForm.value.elementPath != 0
      || this.elementForm.value.module != 0 || this.elementForm.value.screen != 0 || this.elementForm.value.platformId != 0 || this.elementForm.value.projectTypeId != 0)&& val==0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.elementForm = this.formBuilder.group({
        brandId: ['', [Validators.required]],
        regionId: ['', [Validators.required]],
        elementName: ['', [Validators.required,noWhitespaceValidator]],
        elementDesc: [''],
        elementPath: ['', [Validators.required,noWhitespaceValidator]],
        module: ['', [Validators.required,noWhitespaceValidator]],
        screen: ['', [Validators.required,noWhitespaceValidator]],
        platformId: ['', [Validators.required]],
        projectTypeId: ['', [Validators.required]]
      });
    }
  }
  else if (this.isRadioChecked == 2) {
    if ((this.elementFormImport.value.brandId != 0 || this.elementFormImport.value.regionId != 0 || this.elementFormImport.value.platformId != 0 || this.elementFormImport.value.projectTypeId != 0)&& val==0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.elementFormImport = this.formBuilder.group({
        brandId: ['', [Validators.required]],
        regionId: ['', [Validators.required]],
        platformId: ['', [Validators.required]],
        projectTypeId: ['', [Validators.required]]
      });
    }
  } else if (this.isRadioChecked == 3) {
    if ((this.elementFormExport.value.brandId != 0 || this.elementFormExport.value.regionId != 0 || this.elementFormExport.value.platformId != 0 || this.elementFormExport.value.projectTypeId != 0)&& val==0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.elementFormExport = this.formBuilder.group({
        brandId: ['', [Validators.required]],
        regionId: ['', [Validators.required]],
        platformId: ['', [Validators.required]],
        projectTypeId: ['', [Validators.required]]
      });
    }
  }
  if (this.importFile != undefined) {
    this.importFile.nativeElement.value = '';
  }
  this.data = [];
  this.selectedFile = null;
  this.inValid = false;
  this.submitted = false;
  this.strOldName = '';
  this.changeElementName = '';
  this.brandChange = 0;
  this.oldBrand = 0;
  this.Platformchange = 0;
  this.platformId = 0;
  this.XL_row_object = [];
  this.isTextChanged = false;
  this.importFile = null;
  this.distElement = [];
  this.isSave = false;
  // this.txtSearch = '';
  if (this.isRadioChecked == 1) {
    this.dataservice.customFilter['elementName'] = '';
  } else if (this.isRadioChecked == 2) {
    this.dataservice.customFilter['Element'] = '';
  }
  setTimeout(() => {
    this.successMessage = '';
  }, 2000);
  this.errorMessage = '';
  this.elementId = 0;
  this.errorMessage = '';
  this.elementForm.controls['brandId'].setValue('');
  this.elementForm.controls['regionId'].setValue('');
  this.elementForm.controls['projectTypeId'].setValue('');
  this.elementForm.controls['platformId'].setValue('');
  this.isPlatformNotSelected = false;
  this.regionList = [];
}

  /* delete element by element Id */
  async deleteElement(elementId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this element?'
    );
    if (isDelete) {
      await this.dataservice
        .deleteElement(elementId)
        .toPromise()
        .then(
          response => {
            // Success
            this.toastr.successToastr('Element deleted successfully');
            this.getAllElement();
            this.clearData(1);
          },
          error => {
            this.toastr.errorToastr(
              ' ' + this.dataservice.errorWhenInUse + ' '
            );
            setTimeout(() => {
              this.errorMessage = '';
              this.dataservice.errorWhenInUse = '';
            }, 2000);
          }
        );
    }
  }

  // -----------------------------------------------------XLSX-------------------------------------------------------

  onFileChange(ev) {
    
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = event => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});

      const dataString = JSON.stringify(jsonData);
      console.log('dataString-->', dataString);
      document.getElementById('output').innerHTML = dataString;
    };
    reader.readAsBinaryString(file);
  }

  parseExcel(file) {
    
    this.errorMessage = '';
    this.selectedFile = file;
    this.data = [];
    console.log('this.selectedFile', this.selectedFile);
    let reader = new FileReader();
    reader.onload = e => {
      let data = (<any>e.target).result;
      let workbook = XLSX.read(data, {
        type: 'binary'
      });
      let isExecuteCount = 0;
      workbook.SheetNames.forEach(
        function(sheetName) {
          // Here is your object
          isExecuteCount = isExecuteCount + 1;
          if (isExecuteCount <= 1) {
            this.XL_row_object = XLSX.utils.sheet_to_json(
              workbook.Sheets[sheetName],
              { defval: '' }
            );
            if (this.XL_row_object.length != 0) {
              let IsInsertData = true;
              let rownum = 0;
              
              if (
                !this.XL_row_object[0].hasOwnProperty('MobileApp') ||
                !this.XL_row_object[0].hasOwnProperty('Version') ||
                !this.XL_row_object[0].hasOwnProperty('Module') ||
                !this.XL_row_object[0].hasOwnProperty('Screen') ||
                !this.XL_row_object[0].hasOwnProperty('Element')
              ) {
                this.importFile.nativeElement.value = '';
                
                this.toastr.errorToastr('Selected File has invalid data');
                return;
              }
              console.log('data', this.XL_row_object);

              for (let m = 0; m < this.XL_row_object.length; m++) {
                let duplicateElement;
                if (m < this.XL_row_object.length - 1) {
                  if (
                    this.XL_row_object[m]['MobileApp'].toString() == '' ||
                    this.XL_row_object[m]['Version'].toString() == '' ||
                    this.XL_row_object[m]['Module'].toString() == '' ||
                    this.XL_row_object[m]['Screen'].toString() == '' ||
                    this.XL_row_object[m]['Element'].toString() == ''
                  ) {
                    
                    console.log(
                      'test------',
                      this.XL_row_object[m]['MobileApp'] == '',
                      this.XL_row_object[m]['Appliance'] == '',
                      this.XL_row_object[m]['Module'] == '' ||
                        this.XL_row_object[m]['Screen'] == '',
                      this.XL_row_object[m]['Element'] == ''
                    );

                    IsInsertData = false;
                    rownum = rownum + 1;
                    break;
                  }
                }

                if (IsInsertData == true) {
                  rownum = rownum + 1;
                }
              }

              if (IsInsertData == true) {
                console.log('XL_row_object-->', this.XL_row_object);
                let json_object = JSON.stringify(this.XL_row_object);
                console.log('json_object-->', json_object);
                
                this.data = this.XL_row_object;
                $('#importTable').show();

                this.testSteps = this.data.reduce((a, b) => {
                  a[b.TestcaseName] = a[b.TestcaseName] || [];
                  return a;
                }, {});

                console.log('----------->', this.testSteps);
                
                let keys = Object.keys(this.testSteps);
                for (let l = 0; l < keys.length; l++) {
                  this.TestCase = keys[l];

                  let testSteps = [];
                  for (
                    let m = 0;
                    m < this.testSteps[this.TestCase].length;
                    m++
                  ) {
                    testSteps.push({
                      element: this.testSteps[this.TestCase][m].element,
                      keyword: this.testSteps[this.TestCase][m].keyword,
                      module: this.testSteps[this.TestCase][m].module,
                      testStepDesc: this.testSteps[this.TestCase][m]
                        .testStepDesc,
                      iterations: this.testSteps[this.TestCase][m].iterations
                    });
                  }
                }
                console.log(this.CaseJson);
                console.log('this.data-->', this.data);
                $('#importTable').show();
                this.dataservice.customFilter['Element'] = '';
                this.errorMessage = '';
              } else {
                this.importFile.nativeElement.value = '';
                this.toastr.errorToastr(
                  'Please fill mandatory fields into excel, that you are selected... '
                );
              }
            }
          }

          // bind the parse excel file data to Grid
        }.bind(this),
        this
      );
    };

    reader.onerror = function(ex) {
      console.log(ex);
    };

    if (file != undefined && file != null) {
      reader.readAsBinaryString(file);
    }
  }
  public onSuccess(args: any): void {
    this.data = [];
    let files = args.target.files; // FileList object
    this.parseExcel(files[0]);
  }

  radioCheck(id) {
    this.isRadioChecked = id;
    this.submitted = false;
    this.txtSearch = '';
    this.isTextChanged = false;
    this.platformId;
    this.Platformchange;
    this.XL_row_object = [];
    this.oldBrand = 0;
    this.strOldName = '';
    this.Platformchange = 0;
    this.elementForm.reset();
    this.elementFormExport.reset();
    this.elementFormImport.reset();
    this.data = [];
    this.selectedFile = null;

    this.successMessage = '';
    this.errorMessage = '';

    if (id == 1) {
      this.dataservice.customFilter['elementName'] = '';
    } else if (id == 2) {
      this.dataservice.customFilter['Element'] = '';
    }

    this.elementForm = this.formBuilder.group({
      brandId: ['', [Validators.required]],
      regionId: ['', [Validators.required]],
      elementName: ['', [Validators.required]],
      elementDesc: [''],
      elementPath: ['', [Validators.required]],
      module: ['', [Validators.required]],
      screen: ['', [Validators.required]],
      platformId: ['', [Validators.required]],
      projectTypeId: ['', [Validators.required]]
    });

    this.elementFormExport = this.formBuilder.group({
      brandId: ['', [Validators.required]],
      regionId: ['', [Validators.required]],
      platformId: ['', [Validators.required]],
      projectTypeId: ['', [Validators.required]]
    });

    this.elementFormImport = this.formBuilder.group({
      brandId: ['', [Validators.required]],
      regionId: ['', [Validators.required]],
      platformId: ['', [Validators.required]],
      projectTypeId: ['', [Validators.required]]
    });
  }

  filter() {
    
    let key = '';
    if (this.isRadioChecked == 1) {
      key = 'elementName';
    } else if (this.isRadioChecked == 2) {
      key = 'Element';
    }

    this.dataservice.filter(key, this.txtSearch);
  }

  exportElementFile() {
    this.submitted = true;
    this.form = this.elementFormExport;
    let platformId = 0;
    if (this.form.value.projectTypeId > 0 && this.platformList != null) {
      platformId = this.form.value.platformId;
      if (platformId == 0 || platformId == null || platformId == undefined) {
        this.isPlatformNotSelected = true;
      } else {
        this.isPlatformNotSelected = false;
      }
    } else {
      this.isPlatformNotSelected = false;
    }

    if (this.form.valid && !this.isPlatformNotSelected) {
      this.dataservice
        .exportElementFile(
          this.form.value.brandId,
          platformId,
          this.form.value.projectTypeId
        )
        .subscribe(res => {
          const blob = new Blob([res], { type: 'application/octet-stream' });
          var a = document.createElement('a');
          a.href = URL.createObjectURL(blob);
          a.download = 'Objects_WP' + '.xls';
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        });
    }
  }

  async checkDuplication() {
    

    if (
      this.elementForm.value.platformId != 0 &&
      this.elementForm.value.platformId != '' &&
      this.elementForm.value.platformId != undefined
    ) {
      const ElementDetails = this.elementForm.value;
      ElementDetails.resource = this.elementForm.value.elementName
        .toString()
        .trim();
      ElementDetails.type = Type.Element;
      let platformValue = parseInt(this.elementForm.value.platformId);
      ElementDetails.id = this.elementForm.value.brandId;
      ElementDetails.platformId = platformValue;
      ElementDetails.optionalId =
        this.platformList != null || this.platformList.length > 0
          ? platformValue
          : 0;

      await this.dataservice
        .checkDuplicationwithOptionalId(ElementDetails)
        .toPromise()
        .then((response: Boolean) => {
          // Success
          this.inValid = response;
          if (this.inValid) {
            this.toastr.errorToastr('Element name "'+ElementDetails.resource+'" is already exists');          } else {
            this.errorMessage = '';
          }
        });
    }
  }

  changeData(e) {
    
    this.changeElementName = e.target.value
      .toString()
      .trim()
      .toUpperCase();
    if (
      this.strOldName
        .toString()
        .trim()
        .toUpperCase() != this.changeElementName &&
      this.strOldName.toUpperCase() != ''
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}