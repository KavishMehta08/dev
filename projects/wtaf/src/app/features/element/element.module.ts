import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { ElementComponent } from './element/element.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { ElementRoutingModule } from './element-routing.module';
@NgModule({
  declarations: [ElementComponent],
  imports: [CommonModule, SharedModule,ElementRoutingModule,Ng2SearchPipeModule]
})
export class ElementModule {}
