import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { SearchTestCaseComponent } from './search-test-case/search-test-case.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SearchTestCaseRoutingModule } from './search_test_case-routing.module';
@NgModule({
  declarations: [SearchTestCaseComponent],
  imports: [CommonModule, SharedModule,SearchTestCaseRoutingModule,Ng2SearchPipeModule]
})
export class SearchTestCaseModule {}
