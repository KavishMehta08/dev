import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Role } from '../../../models/role';
import { DataService } from '../../../shared/services/data.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { Type } from '../../models';
declare var $: any;
@Component({
  selector: 'wtaf-search-test-case',
  templateUrl: './search-test-case.component.html',
  styleUrls: ['./search-test-case.component.css']
})
export class SearchTestCaseComponent implements OnInit {
  successMessage: String = '';
  errorMessage: String = '';
  regionId: Number = 0;
  brandId: Number = 0;
  testSuiteId: Number = 0;
  testCaseId: Number = 0;
  testCaseList: any = [];
  sharedTestCaseList: any = [];
  regionList: any = [];
  brandList: any = [];
  testSuiteList: any = [];
  projectTypeId: Number = 0;
  projectNameList: any = [];
  projectName: string = '';
  projectTypeList: any = [];
  isFromSuite = false;
  txtSearch = '';
  sharedtxtSearch = '';
  role: string = "";
  Admin:string = "";
  Test_lead:string = "";
  Test_Mgr:string = "";
  // soerting tables required variables
  sequenceNum: any;
  sortedStepId: any;
  sortedSteps: any = [];
  isDraggable = false;
  isSort = false;
  dataOrder: any = [];
  stepOrderJson: [];
  updatedStepOrderNum: any;
  selectedSharedTestcases:any = [];
  addSharedcaseFlag:boolean = false;
  importSharedcaseFlag:boolean = false;
  testcaseReorderFlag:boolean = false;
  userId:number;
  destroy$: Subject<boolean> = new Subject<boolean>();
  constructor(
    public toastr: ToastrManager,
    private dataservice: DataService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public loaderService: LoaderService

  ) {}

  async ngOnInit() {
    
    this.role=localStorage.getItem('userRole');
    this.userId=+localStorage.getItem('userId');
    this.Admin = Role.Admin;
    this.Test_lead = Role.Test_Lead;
    this.Test_Mgr = Role.Test_Manager;
    let flag=Boolean(
      this.activatedRoute.snapshot.paramMap.get('isFromCreateCase')
    );
    
    let isfromTestSuite=Boolean(
      this.activatedRoute.snapshot.paramMap.get('isFromTestSuiteToTestCase')
    );

    if(!flag && !isfromTestSuite){
     this.cleareAllLocalStorage(flag);
    }

    let QuerySuiteID = parseInt(
      sessionStorage.getItem('SearchTestCaseTestSuiteId')
    ); //// Commented by Mohini --2020-06-20 Used Session storage instead of Querystring  //this.activatedRoute.snapshot.paramMap.get('testSuiteId');
    if (QuerySuiteID != null) {
      this.testSuiteId = QuerySuiteID;
    }
    //drag and drop functionality added by shafa

    await this.getAllProjectType();
    
    if (this.testSuiteId > 0) {
      this.isFromSuite = true;

      this.projectTypeId = parseInt(
        sessionStorage.getItem('SearchTestCaseProjectTypeId')
      ); //// Commented by Mohini --2020-06-20 Used Session storage instead of Querystring  //  parseInt(this.activatedRoute.snapshot.paramMap.get('projectTypeId'));

      //Get All Regions by Project type
      await this.getAllRegion_byProjectTypeId(this.projectTypeId);

      //GET ALL BRANDS
      this.regionId = parseInt(
        sessionStorage.getItem('SearchTestCaseRegionId')
      ); //// Commented by Mohini --2020-06-20 Used Session storage instead of Querystring // parseInt( this.activatedRoute.snapshot.paramMap.get('regionId')  );
      await this.getBrandsByRegionId(this.regionId);

      //GET ALL PROJECT TYPE
      this.brandId = parseInt(sessionStorage.getItem('SearchTestCaseBandId')); //// Commented by Mohini --2020-06-20 Used Session storage instead of Querystring   // parseInt(  this.activatedRoute.snapshot.paramMap.get('brandId') );

      //GET ALL PROJECT NAME

      await this.getAllProjectNameByTypeId(
        this.brandId,
        this.projectTypeId,
        this.regionId
      );

      //GET ALL TEST SUITE
      this.projectName = sessionStorage.getItem('SearchTestCaseProjectName'); //// Commented by Mohini --2020-06-20 Used Session storage instead of Querystring
      await this.getAllTestSuite(
        this.regionId,
        this.brandId,
        this.projectTypeId,
        this.projectName
      );

      this.testCaseList = [];
      await this.getAllTestCasebyProjects(
        this.regionId,
        this.brandId,
        this.projectTypeId,
        this.projectName,
        this.testSuiteId
      );
    }
    else {
      // this.getAllTestCasebyProjects(0, 0, 0, '', 0);
     }
    function fixWidthHelper(e, ui) {
      ui.children().each(function() {
        $(this).width($(this).width());
      });
      return ui;
    }
    let jQueryInstance = this;
    $('#tblTestcasetable').sortable({
      //   connectWith: 'table',
      items: 'tbody tr',
      //  items: 'tr',
      cursor: 'pointer',
      axis: 'y',
      opacity: 0.35,
      dropOnEmpty: true,
      helper: fixWidthHelper,
      containment: '#tblTestcasetable',

      start: function(e, ui) {
        jQueryInstance.isSort = true;
        console.log('UI----', ui);
        jQueryInstance.sortedSteps = [];
        jQueryInstance.dataOrder = [];
        $('.testcasetr').remove();
        ui.item.addClass('selected');
        jQueryInstance.testcaseReorderFlag = true;
      },
      stop: function(e, ui) {
        jQueryInstance.isSort = true;
        ui.item.removeClass('selected');
        var listValues = [];
        $(this)
          .find('tr')
          .each(function(index) {
            if (index > 0) {
              var rowDetails = $(this)
                .find('td')
                .eq(0)
                .html(index);
              jQueryInstance.updatedStepOrderNum = index;
              var sortstepId = $(this)
                .find('td')
                .eq(1)
                .html();
              console.log(sortstepId);
              jQueryInstance.sortedStepId = sortstepId;
              console.log('New sequence==', jQueryInstance.updatedStepOrderNum);
              console.log('New sequence==', jQueryInstance.sortedStepId);
              listValues.push(jQueryInstance.updatedStepOrderNum);
              jQueryInstance.sortedSteps.push(jQueryInstance.sortedStepId);

              jQueryInstance.dataOrder = listValues;
              console.log('steps', jQueryInstance.dataOrder);
              console.log('ID', jQueryInstance.sortedSteps);
              var sortedIDs = $(this).sortable('toArray');
              console.log('sortedIds----', sortedIDs);
              jQueryInstance.testcaseReorderFlag = true;
            }
            var widget = $('.selector').sortable('widget');            
          });
          
          
      }
    });
    if(isfromTestSuite && this.testSuiteId > 0){
this.importSharedcaseFlag = true;
    }
    if(flag && this.testSuiteId > 0){
      this.importSharedcaseFlag = true;
    }else if(flag){
      this.projectTypeId = 0;
      this.testSuiteId =0;
      this.testCaseId = 0;
      this.regionId = 0;
      this.brandId =0;
      this.projectName =''; 
    }
  }

  //  Added by Akash - Sorting Table Asc Desc
  orderedArray = [{
    testCaseName: "ASC",
    total_Number_Of_TestSteps: "ASC",
    testCaseDesc: "ASC",
    modifiedOn: "ASC",
    modifiedBy: "ASC",
  }]
  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
    this.testCaseList = this.sort_by_key(this.testCaseList, key, order);
    console.log(this.testCaseList);
    this.loaderService.hide();
  }
  sort_by_key(array, key, order) {
    if (order === "ASC") {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];

        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
    else {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
  }
  /* change region and get brands*/
  async changeRegion(e) {

    this.testSuiteList = [];
    this.testSuiteId = 0;
    this.testCaseList =[];
    this.testCaseId = 0;
    this.projectNameList = [];
    this.projectName = '';
    this.regionId = parseInt(e.target.value);

  //Temporary changes after pagination implementeation this condition if, else condition will be removed
    if(this.addSharedcaseFlag)
    {
    let testCaseId = Type.TestCaseId;
    let desc = Type.descending;
    this.dataservice
    .getAllSharedTestCasebyProjects(
      testCaseId,
      desc,
      this.regionId,
      0,
      this.projectTypeId,
      this.projectName,
      this.addSharedcaseFlag,
      0
    )
    .toPromise()
    .then((response: {}) => {
      this.testCaseList = response;
      if(this.testCaseList != null)
      {
      this.testCaseList.forEach(element => {
        element.IsSelected = false;
        let modifiedOn_LocalTime = moment.utc(element.modifiedOn).toDate();
        element.convert_modifiedTime_format = moment(modifiedOn_LocalTime).format('YYYY-MM-DD HH:mm:ss');
      });
    }
      this.dataservice.customFilter['testCaseName'] = '';
      this.getBrandsByRegionId(this.regionId);
    });
  }
  else{
    this.getBrandsByRegionId(this.regionId);
  }
  }

  async changeBrand(e) {

    this.testSuiteList = [];
    this.testSuiteId = 0;
    this.testCaseList =[];
    this.testCaseId = 0;
    this.projectNameList = [];
    this.projectName = '';
    this.brandId = parseInt(e.target.value);
    if(this.addSharedcaseFlag){
     await this.getAllTestCasebyProjects(this.regionId,this.brandId,this.projectTypeId,'', 0);
    }
    await this.getAllProjectNameByTypeId(
      this.brandId,
      this.projectTypeId,
      this.regionId
    );
  
      
  }

  changeProjectType(e) {
    this.regionList = [];
    this.brandList = [];
    this.projectNameList = [];
    this.projectName = '';
    this.regionId = 0;
    this.brandId = 0;
    this.testSuiteList = [];
    this.testCaseList = [];
    this.projectTypeId = parseInt(e.target.value);
    this.getAllRegion_byProjectTypeId(this.projectTypeId);
  }

  async changeProjectName(e) {
    this.testSuiteList = [];
    this.testSuiteId = 0;
    this.testCaseList=[];
    this.testCaseId = 0;
    if(this.addSharedcaseFlag){
    await this.getAllTestCasebyProjects(this.regionId,this.brandId,this.projectTypeId,this.projectName,0);
    }
    await this.getAllTestSuite(
      this.regionId,
      this.brandId,
      this.projectTypeId,
      e.target.value
    );
  
   
  }

  async changeTestSuite(e) {
    if ((this.projectTypeId != 0 || this.regionId != 0 || this.brandId != 0
      || this.projectNameList != 0 || this.testSuiteList != 0)) {
          this.importSharedcaseFlag =true;
      }
    await this.getAllTestCasebyProjects(
      this.regionId,
      this.brandId,
      this.projectTypeId,
      this.projectName,
      parseInt(e.target.value)

    );
  }

  async getAllTestSuite(regionId, brandId, projectTypeId, projectName) {
    
    let TestSuiteName = Type.TestSuiteName;
    let asc = Type.ascending;
    await this.dataservice
      .getAllTestSuite(
        TestSuiteName,
        asc,
        regionId,
        brandId,
        projectTypeId,
        projectName,0,0
      )
      .toPromise()
      .then(async (response: any) => {
        this.testSuiteList = response;
        this.dataservice.customFilter['testSuiteName'] = '';
        if(response.length == 1)
        {
          this.testSuiteId = this.testSuiteList[0].testSuiteId;
        }
        if ((this.projectTypeId != 0 || this.regionId != 0 || this.brandId != 0
          || this.projectNameList != 0 || this.testSuiteList != 0)) {
              this.importSharedcaseFlag =true;
          }
          //Temporary changes after pagination implementeation this condition if condition will be removed
          if(this.testSuiteId !=0)
          {
            await this.getAllTestCasebyProjects(this.regionId,this.brandId,this.projectTypeId,this.projectName,this.testSuiteId);
          }
      });
  }

  /* get all regions  */
  async getAllRegion() {
    await this.dataservice
      .getAllRegion()
      .toPromise()
      .then((response: {}) => {
        this.regionList = response;
        if (response != null) {
          if (this.regionList.length == 1) {
            this.regionId = this.regionList[0].regionId;
          } else {
            this.regionId = 0;
          }
        }
        if (this.regionId > 0) {
          this.testSuiteList = [];
          this.testSuiteId = 0;
          this.getBrandsByRegionId(this.regionId);
        }
      });
  }

  /* get all brands by region Id */
  async getBrandsByRegionId(regionId) {
    await this.dataservice
      .getBrandsByRegionId(regionId)
      .toPromise()
      .then((response: {}) => {
        this.brandList = response;
        if (response != null) {
          if (this.brandList.length == 1) {
            this.brandId = this.brandList[0].brandId;
          } else {
            this.brandId = 0;
          }
        } else {
          this.brandId = 0;
        }
        if (this.brandId > 0) {
          this.getAllProjectNameByTypeId(
            this.brandId,
            this.projectTypeId,
            this.regionId
          );
        }
      });
  }

  /* get all project type by brand Id */
  async getAllProjectTypeByBrandId(brandId) {
    await this.dataservice
      .getProjectTypeByBrandId(brandId)
      .toPromise()
      .then((response: {}) => {
        this.projectTypeList = response;
        console.log('response', response);
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.projectTypeId = this.projectTypeList[0].projectTypeId;
          } else {
            this.projectTypeId = 0;
          }
        } else {
          this.projectTypeId = 0;
        }
        if (this.projectTypeId > 0) {
          this.getAllProjectNameByTypeId(
            brandId,
            this.projectTypeId,
            this.regionId
          );
        }
      });
  }

  async getAllProjectType() {
    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        // Success
        this.projectTypeList = response;
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.projectTypeId = this.projectTypeList[0].projectTypeId;

            this.getAllRegion_byProjectTypeId(this.projectTypeId);
          } else {
            this.projectTypeId = 0;
          }
        }
      });
  }

  async getAllRegion_byProjectTypeId(typeId) {
    await this.dataservice
      .getAllRegionByProjectType(typeId)
      .toPromise()
      .then(async (response: {}) => {
        this.regionList = response;
        //to place NAR region at first position in frop down
        this.regionList.unshift(this.regionList.splice(this.regionList.findIndex(item => item.regionName === 'NAR'), 1)[0])
        console.log(this.regionList);
        if (response != null) {
          if (this.regionList.length == 1) {
            this.regionId = this.regionList[0].regionId;
          } else {
            this.regionId = 0;
          }
        }
        if (this.regionId > 0) {
          this.testSuiteList = [];
          this.testSuiteId = 0;
          this.getBrandsByRegionId(this.regionId);
        }
      });
  }

  /* get all project name by type Id */
  async getAllProjectNameByTypeId(brandId, projectTypeId, regionId) {
    await this.dataservice
      .getProjectNameByTypeId(brandId, projectTypeId, regionId)
      .toPromise()
      .then((response: {}) => {
        this.projectNameList = response;
        console.log('response-product name list', response);
        if (response != null) {
          if (this.projectNameList.length == 1) {
            this.projectName = this.projectNameList[0].projectTypeId;
          } else {
            this.projectName = '';
          }
        } else {
          this.projectName = '';
        }
        if (this.projectName != '') {
          this.getAllTestSuite(
            this.regionId,
            this.brandId,
            this.projectTypeId,
            this.projectName
          );
        }
      });
  }

/* get all test case  */
async getAllTestCasebyProjects(
  regionId,
  brandId,
  projectTypeId,
  projectName,
  testSuiteId
) {
  this.testCaseList = [];
  let testCaseId = Type.TestCaseId;
  let desc = Type.descending;
  projectTypeId= isNaN(+projectTypeId)?0:projectTypeId;
  testSuiteId= isNaN(+testSuiteId)?0:testSuiteId;
  
  if(this.addSharedcaseFlag){
      this.dataservice
    .getAllSharedTestCasebyProjects(
      testCaseId,
      desc,
      regionId,
      brandId,
      projectTypeId,
      projectName,
      this.addSharedcaseFlag,
      0
    )
    .toPromise()
    .then((response: {}) => {
      this.testCaseList = response;
      // this.selectedSharedTestcases = response;
      console.log('  this.testCaseList', this.testCaseList);
      if(this.testCaseList != null)
      {
      this.testCaseList.forEach(element => {
        element.IsSelected = false;
        let modifiedOn_LocalTime = moment.utc(element.modifiedOn).toDate();
        element.convert_modifiedTime_format = moment(modifiedOn_LocalTime).format('YYYY-MM-DD HH:mm:ss');
      });
    }
      this.dataservice.customFilter['testCaseName'] = '';
    });
  }else{
  this.dataservice
    .getAllSharedTestCasebyProjects(
      testCaseId,
      desc,
      regionId,
      brandId,
      projectTypeId,
      projectName,
      this.addSharedcaseFlag,
      testSuiteId
    )
    .toPromise()
    .then((response: {}) => {
      this.testCaseList = response;
      // this.selectedSharedTestcases = response;
      console.log('  this.testCaseList', this.testCaseList);
      if(this.testCaseList != null)
      {
      this.testCaseList.forEach(element => {
        element.IsSelected = false;
        let modifiedOn_LocalTime = moment.utc(element.modifiedOn).toDate();
        element.convert_modifiedTime_format = moment(modifiedOn_LocalTime).format('YYYY-MM-DD HH:mm:ss');
      });
    }
      this.dataservice.customFilter['testCaseName'] = '';
    });
  }
  this.cleareAllLocalStorage(false);
}

  //get test case by project name
  async getTestSuiteNameByProjectName(projectName) {
    let prj = '';

    if (projectName != '' && projectName != undefined) {
      prj = projectName.toString().trim();
    }
    await this.dataservice
      .getTestSuiteNameByProjectName(prj)
      .toPromise()
      .then(async (response: {}) => {
        this.testSuiteList = response;
        console.log('this.testSuiteList', this.testSuiteList);

        if (response != null) {
          if (this.testSuiteList.length == 1) {
            this.testSuiteId = this.testSuiteList[0].testSuiteId;
          } else {
            this.testSuiteId = 0;
          }
        } else {
          this.testSuiteId = 0;
        }

        if (this.testSuiteId > 0 && this.isFromSuite == false) {
          await this.getAllTestCasebyProjects(
            this.regionId,
            this.brandId,
            this.projectTypeId,
            this.projectName,
            this.testSuiteId
          );
        }
      });
  }
  /* this method clear all messages and variables */
  clearData(val) {

    this.txtSearch = " ";
    this.sharedtxtSearch = '';
    var r = false;
    if ((this.projectTypeId != 0 || this.regionId != 0 || this.brandId != 0
      || this.projectNameList != 0 || this.testSuiteList != 0) && val == 0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.addSharedcaseFlag = false;
      this.importSharedcaseFlag = false;
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      this.testSuiteId = 0;
      this.errorMessage = '';
      this.regionId = 0;
      this.brandId = 0;
      this.projectTypeId = 0;
      this.regionList=[];
      this.projectNameList = [];
      this.projectTypeList = [];
      this.projectName = '';
      this.testCaseId = 0;
      this.brandList = [];
      this.testSuiteList = [];
      this.testCaseList = [];
      this.projectTypeId = 0;
      this.cleareAllLocalStorage(false);
      this.dataservice.customFilter['testCaseName'] = '';
      this.getAllProjectType();
      $('#shared_testCase').prop('checked', false);
    }
  }

  /* delete test case by test case Id */
  deleteTestCase(testCaseId, testCaseName, projectTypeId,isShare, testSuiteId) {
   
    console.log(testSuiteId);
    
    var isDelete = confirm(
      "Test case '" +
      testCaseName +
      "' and all its steps will be deleted. Click OK to confirm or CANCEL to go back."
    );
    if (isDelete) {
      if (
        this.regionId != null &&
        this.regionId != undefined &&
        this.regionId != 0
      ) {
        sessionStorage.setItem(
          'SearchTestCaseRegionId',
          this.regionId.toString()
        );
      }
      if (
        this.brandId != null &&
        this.brandId != undefined &&
        this.brandId != 0
      ) {
        sessionStorage.setItem('SearchTestCaseBandId', this.brandId.toString());
      }
      if (
        this.projectTypeId != null &&
        this.projectTypeId != undefined &&
        this.projectTypeId != 0
      ) {
        sessionStorage.setItem(
          'SearchTestCaseProjectTypeId',
          this.projectTypeId.toString()
        );
      }
      if (
        this.projectName != null &&
        this.projectName != undefined &&
        this.projectName != ''
      ) {
        sessionStorage.setItem(
          'SearchTestCaseProjectName',
          this.projectName.toString()
        );
      }
      if (
        this.testSuiteId != null &&
        this.testSuiteId != undefined &&
        this.testSuiteId != 0
      ) {
        sessionStorage.setItem(
          'SearchTestCaseTestSuiteId',
          this.testSuiteId.toString()
        );
      }
      if(this.addSharedcaseFlag){
        this.dataservice.deleteShareTestCase(testCaseId ,testSuiteId).subscribe(
          response => {
            let data:any = response;
           if(data.deletedStatus){
            this.toastr.successToastr('Shared Test case deleted successfully');
            this.getAllSharedTestCases(this.regionId, this.brandId, this.projectTypeId, this.projectName,true,0);
           }else{
            this.toastr.errorToastr('Shared Test case is in use');
           }
            
          },
          error => {
            this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
            setTimeout(() => {
              this.errorMessage = '';
              this.dataservice.errorWhenInUse = '';
            }, 2000);
          }
        );
      }else{
  
      this.dataservice.deleteShareTestCase(testCaseId, testSuiteId).subscribe(
        response => {
          let data:any = response;
           if(data.deletedStatus){
            this.toastr.successToastr('Test case deleted successfully');
            this.testSuiteId = isNaN(+this.testSuiteId)?0:this.testSuiteId;
            this.getAllSharedTestCases(this.regionId, this.brandId, this.projectTypeId, this.projectName, false,this.testSuiteId)
           }else{
            this.toastr.errorToastr('Test case is in use');
           }
          
        },
        error => {
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
          setTimeout(() => {
            this.errorMessage = '';
            this.dataservice.errorWhenInUse = '';
          }, 2000);
        }
      );
    }
  }
}
  /* navigate to add test steps */
  searchTeststeps(regionId, brandId, testSuiteId, testCaseId, projectTypeId,isShared) {
    sessionStorage.setItem('SearchTestStepRegionId', regionId.toString());
    sessionStorage.setItem('SearchTestStepBandId', brandId.toString());
    sessionStorage.setItem(
      'SearchTestStepProjectTypeId',
      projectTypeId.toString()
    );
    sessionStorage.setItem('SearchTestStepTestSuiteId', testSuiteId.toString());
    sessionStorage.setItem('SearchTestStepTestCaseId', testCaseId.toString());
    sessionStorage.setItem('isShared', isShared);

    this.router.navigate(['test_step/search_test_step', { regionId, brandId, testSuiteId, testCaseId, projectTypeId,isShared }]);

    ////Commented by Mohini - 2020-06-20 Used session storage instead of query string.
    //, { regionId, brandId, testSuiteId, testCaseId, projectTypeId }
  }

  /* navigate to add test case */
  getTestCaseById(testCaseId, projectTypeid,isShared) {
    console.log('projectType Id----------------', projectTypeid);

    if (
      this.regionId != null &&
      this.regionId != undefined &&
      this.regionId != 0
    ) {
      sessionStorage.setItem(
        'SearchTestCaseRegionId',
        this.regionId.toString()
      );
    }

    if (
      this.brandId != null &&
      this.brandId != undefined &&
      this.brandId != 0
    ) {
      sessionStorage.setItem('SearchTestCaseBandId', this.brandId.toString());
    }

    if (
      this.projectTypeId != null &&
      this.projectTypeId != undefined &&
      this.projectTypeId != 0
    ) {
      sessionStorage.setItem(
        'SearchTestCaseProjectTypeId',
        this.projectTypeId.toString()
      );
    }

    if (
      this.projectName != null &&
      this.projectName != undefined &&
      this.projectName != ''
    ) {
      sessionStorage.setItem(
        'SearchTestCaseProjectName',
        this.projectName.toString()
      );
    }

    if (
      this.testSuiteId != null &&
      this.testSuiteId != undefined &&
      this.testSuiteId != 0
    ) {
      sessionStorage.setItem(
        'SearchTestCaseTestSuiteId',
        this.testSuiteId.toString()
      );
    }

    this.router.navigate(['test_case/create_test_case', { id: testCaseId,isShared:isShared }]);
  }

  /* navigate to Copy test case */
  copyTestCase(testCaseId, isCopying, projectTypeid,isShared) {
    if (
      this.regionId != null &&
      this.regionId != undefined &&
      this.regionId != 0
    ) {
      sessionStorage.setItem(
        'SearchTestCaseRegionId',
        this.regionId.toString()
      );
    }

    if (
      this.brandId != null &&
      this.brandId != undefined &&
      this.brandId != 0
    ) {
      sessionStorage.setItem('SearchTestCaseBandId', this.brandId.toString());
    }

    if (
      this.projectTypeId != null &&
      this.projectTypeId != undefined &&
      this.projectTypeId != 0
    ) {
      sessionStorage.setItem(
        'SearchTestCaseProjectTypeId',
        this.projectTypeId.toString()
      );
    }

    if (
      this.projectName != null &&
      this.projectName != undefined &&
      this.projectName != ''
    ) {
      sessionStorage.setItem(
        'SearchTestCaseProjectName',
        this.projectName.toString()
      );
    }

    if (
      this.testSuiteId != null &&
      this.testSuiteId != undefined &&
      this.testSuiteId != 0
    ) {
      sessionStorage.setItem(
        'SearchTestCaseTestSuiteId',
        this.testSuiteId.toString()
      );
    }

    this.router.navigate([
      'test_case/create_test_case',
      { id: testCaseId, flag: isCopying,isShared:isShared }
    ]);
  }

  /* navigate to add test step */
  addTestStep(testCaseId, projectTypeid,isShared) {
    this.router.navigate([
      'test_step/create_test_step',
      { testCaseId: testCaseId, projectTypeid,isShared}
    ]);
  }
  addTestData(testCaseId, projectTypeid,isShared) {
    this.router.navigate([
      '/test_data',
      { testCaseId: testCaseId, projectTypeid,isShared }
    ]);
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }
  sharedfilter(key){
    this.dataservice.filter(key, this.sharedtxtSearch);
  }
  submitReorder() {
    
    let reorderDetails = [];
    // console.log('listValues------', this.testStepList);
    // console.log('lenth of the list', this.testStepList.length);
    for (let m = 0; m < this.sortedSteps.length; m++) {
      // console.log('testStepId', this.testStepList[m].testStepId);
      console.log('After testStepOrderNumber', this.dataOrder[m]);
      console.log('After testcases', this.testCaseList);
      
      let index = this.testCaseList.findIndex(
        x => x.testCaseId === +this.sortedSteps[m]
      );
      console.log(index);
      
      reorderDetails.push({testCaseNumber:m+1,testCaseId:this.testCaseList[index].testCaseId,testSuiteId:this.testSuiteId});
    }
    console.log('Updated Details---', reorderDetails);
    if (reorderDetails.length != 0) {
      this.dataservice.addReorderTestcases(reorderDetails).subscribe(res => {
        this.toastr.successToastr('Test Case  Reordered  successfully');
    
      });
    } else {
      this.toastr.warningToastr(' Please reorder the test case. ');
    }
  }
  cancelReorder() {

    var r = false;
    if (this.isSort != false) {
      r = confirm('Are you sure you want to cancel data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.testCaseList = this.testCaseList;

    }
    this.isSort = false;

  }
  getAllSharedTestCases(regionId,
    brandId,
    projectTypeId,
    projectName,isShared,suiteId){
      let testCaseId = Type.TestCaseId;
      let desc = Type.descending;
      projectTypeId= isNaN(+projectTypeId)?0:projectTypeId;
      suiteId= isNaN(+suiteId)?0:suiteId;
    this.dataservice.getAllSharedTestCasebyProjects(testCaseId,
      desc,
      regionId,
      brandId,
      projectTypeId,
      projectName,
      this.addSharedcaseFlag,suiteId).pipe(takeUntil(this.destroy$)).subscribe((response: {}) => {
        this.testCaseList = response;
        if(this.testCaseList != null)
        {
        this.testCaseList.forEach(element => {
          element.IsSelected = false;
          let modifiedOn_LocalTime = moment.utc(element.modifiedOn).toDate();
          element.convert_modifiedTime_format = moment(modifiedOn_LocalTime).format('YYYY-MM-DD HH:mm:ss');
          console.log("modifiedOn_LocalTime..", element.convert_modifiedTime_format);
        });
      }
    });
  }

  cleareAllLocalStorage(flag) {
    flag = false;
    sessionStorage.setItem('SearchTestCaseRegionId', '');
    sessionStorage.setItem('SearchTestCaseBandId', '');
    sessionStorage.setItem('SearchTestCaseProjectTypeId', '');
    sessionStorage.setItem('SearchTestCaseProjectName', '');
    sessionStorage.setItem('SearchTestCaseTestSuiteId', '');
  }
  async checksharedtestcase(event) {
   this.resetFormData();
    this.testCaseId =0;
      this.testCaseList =[];
    if (event.target.checked) {
      this.addSharedcaseFlag = true;
      // await this.getAllTestCasebyProjects(0, 0, 0, '', 0);
      $('#tblTestcasetable').sortable("disable");
      
    }else{
      this.addSharedcaseFlag = false;
     
      // await this.getAllTestCasebyProjects(0, 0, 0, '', 0);
      $( "#tblTestcasetable" ).sortable();
      $( "#tblTestcasetable" ).sortable( "option", "disabled", false );
      $( "#tblTestcasetable" ).disableSelection();
    }
  }
  resetFormData(){
    this.importSharedcaseFlag =false;
    this.projectTypeId = 0;
    this.regionId = 0;
    this.brandId = 0;
    this.testSuiteId = 0;
    this.projectName = '';

    //changed by mustaid
    this.regionList=[];
    this.brandList=[];
  }
  opensharedteacModal(){
    this.sharedtxtSearch = '';
    let testCaseId = Type.TestCaseId;
    let desc = Type.descending;
  this.dataservice.getAllSharedTestCasebyProjects(testCaseId,
    desc,
    this.regionId,
    this.brandId,
    this.projectTypeId,
    this.projectName,
    true,0).pipe(takeUntil(this.destroy$)).subscribe((response: {}) => {
      this.sharedTestCaseList = response;
      // this.selectedSharedTestcases = response;
      console.log('  this.sharedcaselist', this.sharedTestCaseList);
      if(this.sharedTestCaseList != null)
      {
      this.sharedTestCaseList.forEach(element => {
        element.IsSelected = false;
        let modifiedOn_LocalTime = moment.utc(element.modifiedOn).toDate();
        element.convert_modifiedTime_format = moment(modifiedOn_LocalTime).format('YYYY-MM-DD HH:mm:ss');
      });
    }
  });
  this.selectedSharedTestcases = [];
    $('#testcaseTemplateModal').modal('show');
  }
  
  hideModel() {
    $('#testcaseTemplateModal .close').click();
    this.sharedtxtSearch = '';
  }
  AddSharedTestCasestoSuite(){
    debugger
    this.selectedSharedTestcases.map((elem)=>{
      elem.testSuiteId = this.testSuiteId;
      elem.userId = this.userId;
    });
    this.dataservice.addSharedTestcaseToSuite(this.selectedSharedTestcases).pipe(takeUntil(this.destroy$)).subscribe(async (response) => {
         if(response){
          this.toastr.successToastr('Test Case added to suite sucessfully');
            await this.getAllTestCasebyProjects(
            this.regionId,
            this.brandId,
            this.projectTypeId,
            this.projectName,
            this.testSuiteId
          );
         }
    });
   
    $('#testcaseTemplateModal .close').click(); 

  }
  selectTestCase(event,testCase){
    if (event.target.checked) {
      let existflag;
      if(this.testCaseList != null){
        existflag =  this.testCaseList.some((obj)=>{
          return obj.testCaseId === testCase.testCaseId
          });
      }
  
      if(existflag){
        this.toastr.warningToastr('This shared case already exist in current suite ');
      }else{
      if(this.selectedSharedTestcases.length !=0){
        const resultArr = this.sharedTestCaseList.filter(
          arrObj => arrObj.testCaseId === testCase.testCaseId
        );
        this.selectedSharedTestcases.push(resultArr[0]);
      }else{
        this.selectedSharedTestcases.push(testCase);
      }
      }
   
    } else {
      const resultArr = this.selectedSharedTestcases.filter(
        arrObj => arrObj.testCaseId !== testCase.testCaseId
      );
      this.selectedSharedTestcases = resultArr;
    }
    console.log(this.selectedSharedTestcases);
  }
  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
