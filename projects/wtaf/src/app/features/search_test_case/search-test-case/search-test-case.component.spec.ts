import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTestCaseComponent } from './search-test-case.component';

describe('SearchTestCaseComponent', () => {
  let component: SearchTestCaseComponent;
  let fixture: ComponentFixture<SearchTestCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTestCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTestCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
