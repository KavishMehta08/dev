import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchTestCaseComponent } from './search-test-case/search-test-case.component';

const routes: Routes = [
  {
    path: '',
    component: SearchTestCaseComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchTestCaseRoutingModule {}
