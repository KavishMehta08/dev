import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScheduleComponent } from './schedule/schedule.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgPipesModule} from 'ngx-pipes';
const routes: Routes = [
  {
    path: '',
    component: ScheduleComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),Ng2SearchPipeModule ,NgPipesModule],
  exports: [RouterModule]
})
export class ScheduleRoutingModule {}
