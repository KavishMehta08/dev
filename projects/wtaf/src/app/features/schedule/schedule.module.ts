import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ScheduleComponent } from './schedule/schedule.component';
import { ScheduleRoutingModule } from './schedule-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgPipesModule} from 'ngx-pipes';

@NgModule({
  declarations: [ScheduleComponent],
  imports: [CommonModule, SharedModule,ScheduleRoutingModule,Ng2SearchPipeModule,NgPipesModule]
})
export class ScheduleModule {}
