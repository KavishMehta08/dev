import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { TestCaseComponent } from './test-case/test-case.component';

import { TestCaseRoutingModule } from './test_case-routing.module';
@NgModule({
  declarations: [TestCaseComponent],
  imports: [CommonModule, SharedModule,TestCaseRoutingModule]
})
export class TestCaseModule {}
