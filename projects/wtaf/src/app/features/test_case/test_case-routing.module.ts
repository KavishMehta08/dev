import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestCaseComponent } from './test-case/test-case.component';

const routes: Routes = [
  {
    path: '',
    component: TestCaseComponent,
    data: { title: '' },
    children:[
      {
        path: '',
        redirectTo: 'search_test_case',
        pathMatch: 'full'
      },
      {
        path: 'search_test_case',
        loadChildren: () =>
        import('../search_test_case/search_test_case.module').then(
          m => m.SearchTestCaseModule
        ),
      }, 
       {
        path: 'create_test_case',
        loadChildren: () =>
        import('../create_test_case/create_test_case.module').then(
          m => m.CreateTestSuiteModule
        ),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestCaseRoutingModule {}
