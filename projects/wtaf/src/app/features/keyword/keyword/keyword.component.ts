import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder , FormControl } from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { Type } from '../../models';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from '../../../shared/services/loader.service';

@Component({
  selector: 'wtaf-keyword',
  templateUrl: './keyword.component.html',
  styleUrls: ['./keyword.component.css']
})
export class KeywordComponent implements OnInit {
  keywordId: Number = 0;
  submitted = false;
  successMessage: String;
  errorMessage: String = '';
  keywordList: any = [];
  inValid: Boolean = false;
  isTextChanged: Boolean = false;
  strOldName: String = '';
  txtSearch = '';
  changeKeywordName: string = '';
  projectTypeList: any = [];
  projectTypechange: number = 0;
  oldProjTypeId: number = 0;
  userId: number;
  isKeywordNameExist: boolean = false;
  keywordForm = this.formBuilder.group({
    keywordName: ['', [Validators.required , noWhitespaceValidator]],
    keywordDesc: [''],
    dataRequired: [false],
    elementRequired: [false],
    projectTypeId: ['', [Validators.required]]
  });
  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    public loaderService: LoaderService

  ) { }

  ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log("local usr id", this.userId);
    this.getAllKeyword();
    this.getAllProjectType();
  }

  get f() {
    return this.keywordForm.controls;
  }

  //  Added by Akash - Sorting Table Asc Desc
  orderedArray = [{
    projectTypeName: "ASC",
    keywordName: "ASC",
  }]
  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
    this.keywordList = this.sort_by_key(this.keywordList, key, order);
    console.log(this.keywordList);
    this.loaderService.hide();
  }
  sort_by_key(array, key, order) {
    if (order === "ASC") {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];

        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
    else {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
  }
  /* for duplication check */
  async checkDuplication() {

    const keywordDetails = this.keywordForm.value;
    if (
      this.keywordForm.value.projectTypeId != null &&
      this.keywordForm.value.projectTypeId != 0 &&
      this.keywordForm.value.projectTypeId != undefined
    ) {
      keywordDetails.resource = this.keywordForm.value.keywordName
        .toString()
        .trim();
      keywordDetails.type = Type.Keyword;
      keywordDetails.id = parseInt(this.keywordForm.value.projectTypeId);
      await this.dataservice
        .checkDuplication(keywordDetails)
        .toPromise()
        .then((response: Boolean) => {
          // Success

          this.inValid = response;
          if (this.inValid) {
            this.toastr.errorToastr('Keyword name "' + keywordDetails.resource + '" is already exists');
          } else {

          }
        });
    }
  }

  /* save and update method for keyword */
  async saveKeyword() {
    
    this.submitted = true;
    this.inValid = false;
    this.isKeywordNameExist = false;
    if (
      (this.changeKeywordName != null &&
        this.changeKeywordName != undefined &&
        this.changeKeywordName != '' &&
        this.strOldName
          .toString()
          .trim()
          .toUpperCase() != this.changeKeywordName &&
        this.strOldName.toString() != '') ||
      (this.projectTypechange != 0 &&
        this.oldProjTypeId != 0 &&
        this.oldProjTypeId.toString() != this.projectTypechange.toString())
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }

    if (this.keywordForm.valid && (this.keywordId == 0 || this.isTextChanged)) {
      // this.keywordForm.value.keywordName
      // .toString()
      // .trim();
      await this.checkDuplication();
    }
    if (this.keywordForm.valid && !this.inValid) {
      if (this.keywordForm.value.keywordName
        .toString()
        .trim() != "") {
        this.errorMessage = '';
        // this.keywordForm.value.keywordName
        //   .toString()
        //   .trim();
        const keywordDetails = this.keywordForm.value;
        keywordDetails.userId = this.userId;
        keywordDetails.keywordId = this.keywordId;
        this.dataservice.saveKeyword(keywordDetails).subscribe(response => {

          if (this.keywordId != 0) {
            this.toastr.successToastr('Keyword updated successfully');
          } else {
            this.toastr.successToastr('Keyword created successfully');
          }
          this.clearData(1);
          this.getAllKeyword();
        });
      }
      else {
        this.isKeywordNameExist = true;
        console.log('this.f.keywordName.errors....', this.f.keywordName.errors);
        // this.toastr.errorToastr("Please enter Keyword Name");
      }
    }
  }

  /* get all keywords */
  getAllKeyword() {
    this.dataservice.getAllKeyword().then(response => {
      this.keywordList = response;
      this.dataservice.customFilter['keywordName'] = '';
    });
  }

  /* get keyword by keyword Id*/
  getKeywordById(keywordId) {

    this.clearData(1);
    this.keywordId = keywordId;
    this.dataservice.getKeywordById(keywordId).subscribe(response => {

      this.strOldName = response['keywordName'];
      console.log('keyword---->response', response);
      this.oldProjTypeId = response['projectTypeId'];
      console.log("response['projectTypeId']", response['projectTypeId']);
      this.keywordForm.patchValue(response);
      window.scroll(0, 0);
    });
  }

  /* delete keyword by keyword Id*/
  deleteKeyword(keywordId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this Keyword?'
    );
    if (isDelete) {
      this.dataservice.deleteKeyword(keywordId).subscribe(
        response => {
          this.toastr.successToastr('Keyword deleted successfully');
          this.getAllKeyword();
          this.clearData(1);
        },
        error => {
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');

        }
      );
    }
  }

  /* clear all variables and reset the keyword form */
  clearData(val) {
    var r = false;
    if ((this.keywordForm.value.projectTypeId != 0 || this.keywordForm.value.keywordName != 0 || this.keywordForm.value.keywordDesc != 0
      || this.keywordForm.value.dataRequired != false || this.keywordForm.value.elementRequired != false) && val == 0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.submitted = false;
      this.keywordForm.reset();
      this.keywordId = 0;
      this.isKeywordNameExist = false;
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      this.keywordForm = this.formBuilder.group({
        keywordName: ['', [Validators.required ,noWhitespaceValidator]],
        keywordDesc: [''],
        dataRequired: [false],
        elementRequired: [false],
        projectTypeId: ['', [Validators.required]]
      });
      this.errorMessage = '';
      this.inValid = false;
      this.strOldName = '';
      this.oldProjTypeId = 0;
      this.changeKeywordName = '';
      this.projectTypechange = 0;
      this.isTextChanged = false;
      this.keywordForm.controls['projectTypeId'].setValue('');
    }
  }
  changeData(e) {

    this.changeKeywordName = e.target.value
      .toString()
      .trim()
      .toUpperCase();
    if (
      this.strOldName
        .toString()
        .trim()
        .toUpperCase() != this.changeKeywordName &&
      this.strOldName.toUpperCase() != ''
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  onProjTypeChange(event) {

    this.projectTypechange = parseInt(event.target.value);
    if (this.projectTypechange != 0) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }

  async getAllProjectType() {

    await this.dataservice
      .getAllProjectType()
      .toPromise()
      .then(response => {
        // Success
        this.projectTypeList = response;
        if (response != null) {
          if (this.projectTypeList.length == 1) {
            this.keywordForm.controls['projectTypeId'].setValue(
              this.projectTypeList[0].projectTypeId
            );
          } else {
            this.keywordForm.controls['projectTypeId'].setValue('');
          }
        }
      });
  }

  oninput_keyword(e)
  {
   console.log('eeeeeeeeeeeeee' , e);
   if (e.data == null)
   {
    this.isKeywordNameExist = false;
   }
   
  }
}

export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}
