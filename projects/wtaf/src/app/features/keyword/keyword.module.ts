import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { KeywordComponent } from './keyword/keyword.component';
import { KeywordRoutingModule } from './keyword-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [KeywordComponent],
  imports: [CommonModule, SharedModule,KeywordRoutingModule,Ng2SearchPipeModule]
})
export class KeywordModule {}
