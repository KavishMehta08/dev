import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KeywordComponent } from './keyword/keyword.component';

const routes: Routes = [
  {
    path: '',
    component: KeywordComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KeywordRoutingModule {}
