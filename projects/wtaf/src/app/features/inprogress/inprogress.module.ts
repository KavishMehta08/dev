import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { InprogressComponent } from './inprogress/inprogress.component';
import { InprogressRoutingModule } from './inprogress-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgPipesModule} from 'ngx-pipes';

@NgModule({
  declarations: [InprogressComponent],
  imports: [CommonModule, SharedModule,InprogressRoutingModule,Ng2SearchPipeModule,NgPipesModule]
})
export class InprogressModule {}
