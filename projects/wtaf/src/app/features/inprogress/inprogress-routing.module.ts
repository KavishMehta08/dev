import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InprogressComponent } from './inprogress/inprogress.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgPipesModule} from 'ngx-pipes';
const routes: Routes = [
  {
    path: '',
    component: InprogressComponent,
    data: { title: '' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),Ng2SearchPipeModule ,NgPipesModule],
  exports: [RouterModule]
})
export class InprogressRoutingModule {}
