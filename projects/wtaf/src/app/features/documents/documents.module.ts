import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { DocumentsComponent } from './documents/documents.component';

import { DocumentsRoutingModule } from './documents-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgPipesModule } from 'ngx-pipes';
@NgModule({
  declarations: [DocumentsComponent],
  imports: [
    CommonModule,
    SharedModule,
    DocumentsRoutingModule,
    Ng2SearchPipeModule,
    NgPipesModule
  ]
})
export class DocumentsModule {}
