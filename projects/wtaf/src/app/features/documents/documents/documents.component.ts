import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoaderService } from '../../../shared/services/loader.service';
import { Chart } from 'chart.js';
import { DataService } from '../../../shared/services/data.service';
import { Router, RouterLink } from '@angular/router';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { Type } from '../../models';
import { Role } from '../../../models/role';
import { HttpResponse } from '@angular/common/http';

declare var $: any;
@Component({
  selector: 'wtaf-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {
  submitted = false;
  successMessage: String = '';
  documentList: any = [];
  documentId: Number = 0;
  errorMessage: String = '';
  inValid: Boolean = false;
  isTextChanged: Boolean = false;
  strOldName: String = '';
  changeDocumentName: String = '';
  txtSearch = '';
  userId: number;
  role: string = '';
  Admin: string = '';
  Test_lead: string = '';
  Test_Mgr: string = '';
  documentForm = this.formBuilder.group({
    documentName: ['', [Validators.required, noWhitespaceValidator]],
    documentLink: ['', [Validators.required]]
  });
  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    public dataservice: DataService,
    public loaderService: LoaderService
  ) {}

  ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log('local usr id', this.userId);
    this.role = localStorage.getItem('userRole');
    this.Admin = Role.Admin;
    this.Test_lead = Role.Test_Lead;
    this.Test_Mgr = Role.Test_Manager;
    this.getAllDocuments();
  }

  openDocumentLink(documentLink: string): void {
    if (documentLink != '') {
      window.open(documentLink, '_blank');
    }
  }

  downloadGoogleDriveDocument(
    documentLink: string,
    documentName: string
  ): void {
    var fileId = documentLink.replace(/.*\/d\//, '').replace(/\/.*/, '');
    console.log('fileId', fileId);
    this.dataservice
      .downloadGoggleDriveDocument(fileId)
      .subscribe((res: any) => {
        var contentDisposition = res.headers.get('content-disposition');
        var filename = contentDisposition
          .split(';')[1]
          .split('filename')[1]
          .split('=')[1]
          .trim();
        console.log(filename);
        const blob = new Blob([res.body], { type: 'application/octet-stream' });
        var a = document.createElement('a');
        a.href = URL.createObjectURL(blob);
        console.log(res.headers);
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        console.log('download finished');
        document.body.removeChild(a);
      });
  }

  downloadConfluenceDocument(documentLink: string, documentName: string): void {
    var title = documentLink.split('/').pop();
    console.log('title', title);
    this.dataservice.downloadConfluenceDocument(title).subscribe((res: any) => {
      var contentDisposition = res.headers.get('content-disposition');
      const blob = new Blob([res.body], { type: 'application/octet-stream' });
      var a = document.createElement('a');
      a.href = URL.createObjectURL(blob);
      console.log(res.headers);
      a.download = title + '.pdf';
      document.body.appendChild(a);
      a.click();
      console.log('download finished');
      document.body.removeChild(a);
    });
  }

  downloadDocument(documentLink: string, documentName: string): void {
    if (documentLink.indexOf('google') > -1) {
      this.downloadGoogleDriveDocument(documentLink, documentName);
    } else {
      this.downloadConfluenceDocument(documentLink, documentName);
    }
  }

  get f() {
    return this.documentForm.controls;
  }

  orderedArray = [
    {
      documentId: 'ASC',
      documentName: 'ASC'
    }
  ];

  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    console.log('in sort table ', this.orderedArray[0][key]);
    this.orderedArray[0][key] = order == 'ASC' ? 'ASC' : 'DESC';
    this.documentList = this.sort_by_key(this.documentList, key, order);
    console.log(this.documentList);
    this.loaderService.hide();
  }

  sort_by_key(array, key, order) {
    if (order === 'ASC') {
      return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];

        if (
          (x != null && x != undefined && x != '') ||
          (y != null && y != undefined && y != '')
        ) {
          return String(x).toLowerCase() < String(y).toLowerCase()
            ? -1
            : String(x).toLowerCase() > String(y).toLowerCase()
            ? 1
            : 0;
        }
      });
    } else {
      return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];
        if (
          (x != null && x != undefined && x != '') ||
          (y != null && y != undefined && y != '')
        ) {
          return String(x).toLowerCase() > String(y).toLowerCase()
            ? -1
            : String(x).toLowerCase() < String(y).toLowerCase()
            ? 1
            : 0;
        }
      });
    }
  }
  /* for duplication check */
  async checkDuplication() {
    const documentDetails = this.documentForm.value;
    if (
      documentDetails['documentName'] != null &&
      documentDetails['documentName'] != 0 &&
      documentDetails['documentName'] != undefined
    ) {
      documentDetails.resource = this.documentForm.value.documentName
        .toString()
        .trim();
    }
    documentDetails.type = Type.User;
    documentDetails.id = 0;
    await this.dataservice
      .checkDuplication(documentDetails)
      .toPromise()
      .then((response: Boolean) => {
        // Success
        this.inValid = response;
        if (this.inValid) {
          this.toastr.errorToastr(
            'Document "' + documentDetails.resource + '" already exists'
          );
        } else {
          this.errorMessage = '';
        }
      });
  }

  /* add and update method for document */
  async saveDocument() {
    this.submitted = true;
    this.inValid = false;

    // if (this.documentId == 0 || this.isTextChanged) {
    //   if(this.documentForm.value.userId != null)
    //   {
    //     await this.checkDuplication();
    //   }
    // }
    if (this.documentId == 0) {
      //|| this.isTextChanged
      await this.checkDuplication();
    }
    console.log('this.userId' + this.userId);
    console.log('is this.documentForm.valid ' + this.documentForm.valid);
    console.log('is this.inValid ' + this.inValid);
    if (this.documentForm.valid && !this.inValid) {
      console.log('is valid');
      const documentDetails = this.documentForm.value;
      documentDetails.userId = this.userId;
      documentDetails.documentId = this.documentId;
      this.dataservice.saveDocument(documentDetails).subscribe(response => {
        if (this.documentId != 0) {
          this.toastr.successToastr('Document updated successfully');
        } else {
          this.toastr.successToastr('Document created successfully');
        }
        this.clearData(1);
        this.getAllDocuments();
      });
    }
  }

  /* get document by DocumentId */
  async getDocumentById(documentId) {
    this.clearData(1);
    this.documentId = documentId;
    await this.dataservice.getDocumentById(documentId).subscribe(response => {
      console.log('data', response);
      this.strOldName = response['documentName'];
      this.documentForm.controls['documentName'].setValue(
        response['documentName']
      );
      this.documentForm.controls['documentLink'].setValue(
        response['documentLink']
      );
      this.documentForm.patchValue(response);
      window.scroll(0, 0);
    });
  }

  getAllDocuments() {
    this.dataservice.getAllDocument().then(response => {
      this.documentList = response;
      this.dataservice.customFilter['documentName'] = '';
    });
  }

  /* this method clear all messages and document form also */
  clearData(val) {
    // this.txtSearch = " ";
    var r = false;
    if (
      (this.documentForm.value.documentId != 0 ||
        this.documentForm.value.documentName != 0 ||
        this.documentForm.value.documentLink != 0) &&
      val == 0
    ) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.submitted = false;
      this.documentForm.reset();
      setTimeout(() => {
        this.successMessage = '';
      }, 1000);
      this.documentForm = this.formBuilder.group({
        documentName: ['', [Validators.required, noWhitespaceValidator]],
        documentLink: ['', [Validators.required]]
      });
      this.documentId = 0;
      this.errorMessage = '';
      this.inValid = false;
      this.strOldName = '';
    }
  }

  /* delete document by documentId*/
  deleteDocument(documentId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this document?'
    );
    if (isDelete) {
      this.dataservice.deleteDocument(documentId).subscribe(
        response => {
          this.toastr.successToastr('Document deleted successfully');
          this.getAllDocuments();
          this.clearData(1);
        },
        error => {
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
          setTimeout(() => {
            this.errorMessage = '';
            this.dataservice.errorWhenInUse = '';
          }, 1000);
        }
      );
    }
  }

  changeData(e) {
    this.changeDocumentName = e.target.value
      .toString()
      .trim()
      .toUpperCase();
    if (
      this.strOldName
        .toString()
        .trim()
        .toUpperCase() != this.changeDocumentName &&
      this.strOldName.toUpperCase() != ''
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }

  changeRole() {
    this.isTextChanged = true;
  }

  onlyNumberValidation(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 12 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}

export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? { whitespace: true } : null;
}
