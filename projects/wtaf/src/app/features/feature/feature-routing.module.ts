import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../core/core.module';
import { FeatureComponent } from './feature/feature.component';
const routes: Routes = [
  {
    path: '',
    component: FeatureComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        loadChildren: () =>
          import('../dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'configuration',
        loadChildren: () =>
          import('../configuration/configuration.module').then(
            m => m.ConfigurationModule
          )
      },
      {
        path: 'test_suite',
        loadChildren: () =>
          import('../test_suite/test_suite.module').then(m => m.TestSuiteModule)
      },
      {
        path: 'test_case',
        loadChildren: () =>
          import('../test_case/test_case.module').then(m => m.TestCaseModule)
      },
      {
        path: 'test_data',
        loadChildren: () =>
          import('../test_data/test_data.module').then(m => m.TestDataModule)
      },
      {
        path: 'test_step',
        loadChildren: () =>
          import('../test_step/test_step.module').then(m => m.TestStepModule)
      },
      {
        path: 'execution',
        loadChildren: () =>
          import('../execution/execution.module').then(m => m.ExecutionModule)
      },
      {
        path: 'report',
        loadChildren: () =>
          import('../report/report.module').then(m => m.ReportModule)
      },
      {
        path: 'report_details',
        loadChildren: () =>
          import('../reports_details/reports-details.module').then(
            m => m.ReportsDetailsModule
          )
      },
      {
        path: 'documents',
        loadChildren: () =>
          import('../documents/documents.module').then(m => m.DocumentsModule)
      }
    ]
  },
  {
    path: 'error',
    loadChildren: () =>
      import('../../layout/error/error.module').then(m => m.ErrorModule)
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeatureRoutingModule {}
