import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { FeatureComponent } from './feature/feature.component';
// import { HighchartsChartComponent } from 'highcharts-angular';

import{HeaderComponent} from '../../layout/header/header/header.component'

import { FeatureRoutingModule } from './feature-routing.module';

@NgModule({
  declarations: [FeatureComponent,HeaderComponent],
  imports: [CommonModule, SharedModule,FeatureRoutingModule]
})
export class FeatureModule {}
