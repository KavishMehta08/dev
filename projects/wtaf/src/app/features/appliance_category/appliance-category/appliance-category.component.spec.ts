import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplianceCategoryComponent } from './appliance-category.component';

describe('ApplianceCategoryComponent', () => {
  let component: ApplianceCategoryComponent;
  let fixture: ComponentFixture<ApplianceCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplianceCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplianceCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
