import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { Type } from '../../models';
import { ToastrManager } from 'ng6-toastr-notifications';
import { type } from 'os';
import { LoaderService } from '../../../shared/services/loader.service';

@Component({
  selector: 'wtaf-appliance-category',
  templateUrl: './appliance-category.component.html',
  styleUrls: ['./appliance-category.component.css']
})
export class ApplianceCategoryComponent implements OnInit {
  submitted = false;
  successMessage: String = '';
  errorMessage: String = '';
  applianceCategoryList: any = [];
  applianceCategoryId: Number = 0;
  inValid: Boolean = false;
  isTextChanged: Boolean = false;
  strOldName: String = '';
  changeApplicaneCategory: string = '';
  txtSearch = '';
  userId: number;
  applianceCategoryForm = this.formBuilder.group({
    applianceCategoryName: ['', [Validators.required,noWhitespaceValidator]],
    applianceCategoryCode: ['', [Validators.required,noWhitespaceValidator]],
    applianceCategoryDesc: ['']
  });
  constructor(
    public toastr: ToastrManager,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    public loaderService: LoaderService

  ) { }

  ngOnInit() {
    this.userId = parseInt(localStorage.getItem('userId'));
    console.log("local usr id", this.userId);
    this.getAllapplianceCategory();
  }

  get f() {
    return this.applianceCategoryForm.controls;
  }

  //  Added by Akash - Sorting Table Asc Desc
  orderedArray = [{
    applianceCategoryCode: "ASC",
    applianceCategoryName: "ASC",
  }]

  sortTable(key) {
    this.loaderService.show();
    let order = this.orderedArray[0][key];
    this.orderedArray[0][key] = order == "ASC" ? "DESC" : "ASC";
    this.applianceCategoryList = this.sort_by_key(this.applianceCategoryList, key, order);
    console.log(this.applianceCategoryList);
    this.loaderService.hide();
  }
  sort_by_key(array, key, order) {
    if (order === "ASC") {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];

        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() < String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() > String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
    else {
      return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        if (x != null && x != undefined && x != '' || y != null && y != undefined && y != '') {
          return ((String(x).toLowerCase() > String(y).toLowerCase()) ? -1 : ((String(x).toLowerCase() < String(y).toLowerCase()) ? 1 : 0));
        }
      });
    }
  }

  /* for duplication check */
  async checkDuplication() {
    const applianceCategoryDetails = this.applianceCategoryForm.value;
    if (
      applianceCategoryDetails.applianceCategoryName != null &&
      applianceCategoryDetails.applianceCategoryName.trim() != '' &&
      applianceCategoryDetails.applianceCategoryName != undefined
    ) {
      applianceCategoryDetails.resource = this.applianceCategoryForm.value.applianceCategoryCode
        .toString()
        .trim();
      applianceCategoryDetails.type = Type.ApplicanceCategory;
      applianceCategoryDetails.id = 0;
      await this.dataservice
        .checkDuplication(applianceCategoryDetails)
        .toPromise()
        .then((response: Boolean) => {
          // Success
          this.inValid = response;
          if (this.inValid) {
            this.toastr.errorToastr('Appliance category code "' + applianceCategoryDetails.resource + '" is already exists');
          } else {
            this.errorMessage = '';
          }
        });
    }
  }

  /* add and update method for appliance category */
  async saveApplianceCategory() {
    this.inValid = false;
    this.submitted = true;

    if (
      this.changeApplicaneCategory != null &&
      this.changeApplicaneCategory != undefined &&
      this.changeApplicaneCategory.trim() != '' &&
      this.strOldName
        .toString()
        .trim()
        .toUpperCase() != this.changeApplicaneCategory &&
      this.strOldName.toString() != ''
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
    if (this.applianceCategoryId == 0 || this.isTextChanged) {
      await this.checkDuplication();
    }
    if (this.applianceCategoryForm.valid && !this.inValid) {
      const applianceCategoryDetails = this.applianceCategoryForm.value;
      applianceCategoryDetails.userId = this.userId;
      applianceCategoryDetails.applianceCategoryId = this.applianceCategoryId;
      this.dataservice
        .saveApplianceCategory(applianceCategoryDetails)
        .subscribe(response => {
          if (this.applianceCategoryId != 0) {
            this.toastr.successToastr(
              'Appliance category updated successfully'
            );
          } else {
            this.toastr.successToastr(
              'Appliance category created successfully'
            );
          }
          this.clearData(1);
          this.getAllapplianceCategory();
        });
    }
  }

  /* get appliance category by applianceCategoryId */
  getApplianceCategoryById(applianceCategoryId) {
    this.clearData(1);
    this.applianceCategoryId = applianceCategoryId;
    this.dataservice
      .getApplianceCategoryById(applianceCategoryId)
      .subscribe(response => {
        this.strOldName = response['applianceCategoryCode'];
        this.applianceCategoryForm.patchValue(response);
        window.scroll(0, 0);
      });
  }

  /* get all applianceCategories  */
  getAllapplianceCategory() {
    let ApplicanceCategoryId = Type.ApplicanceCategoryId;
    let desc = Type.descending;
    this.dataservice.getAllApplianceCategory(ApplicanceCategoryId, desc).subscribe((response: {}) => {
      this.applianceCategoryList = response;
      this.dataservice.customFilter['applianceCategoryName'] = '';
    });
  }

  /* this method clear all messages and applianceCategoryForm also */
  clearData(val) {

    // this.txtSearch = " ";
    var r = false;
    if ((this.applianceCategoryForm.value.applianceCategoryDesc != 0 || this.applianceCategoryForm.value.applianceCategoryName != 0 || this.applianceCategoryForm.value.applianceCategoryCode != 0) && val == 0) {
      r = confirm('Are you sure you want to reset data?');
    } else {
      r = true;
    }
    if (r == true) {
      this.submitted = false;
      this.applianceCategoryForm.reset();
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      this.applianceCategoryId = 0;
      this.errorMessage = '';
      this.inValid = false;
      this.strOldName = '';
      this.isTextChanged = false;
      this.changeApplicaneCategory = '';
      this.applianceCategoryForm = this.formBuilder.group({
        applianceCategoryName: ['', [Validators.required,noWhitespaceValidator]],
        applianceCategoryCode: ['', [Validators.required,noWhitespaceValidator]],
        applianceCategoryDesc: ['']
      });
    }
  // }
}

  /* delete appliance category by appliance category Id */
  deleteApplianceCategory(applianceCategoryId) {
    var isDelete = confirm(
      'Are you sure that you want to permanently delete this Appliance category?'
    );
    if (isDelete) {
      this.dataservice.deleteapplianceCategory(applianceCategoryId).subscribe(
        response => {
          this.toastr.successToastr(
            'Application category deleted successfully'
          );
          this.getAllapplianceCategory();
          this.clearData(1);
        },
        error => {
          this.toastr.errorToastr(' ' + this.dataservice.errorWhenInUse + ' ');
          setTimeout(() => {
            this.errorMessage = '';
            this.dataservice.errorWhenInUse = '';
          }, 2000);
        }
      );
    }
  }

  changeData(e) {
    this.changeApplicaneCategory = e.target.value
      .toString()
      .trim()
      .toUpperCase();
    if (
      this.strOldName
        .toString()
        .trim()
        .toUpperCase() != this.changeApplicaneCategory &&
      this.strOldName.toUpperCase() != ''
    ) {
      this.isTextChanged = true;
    } else {
      this.isTextChanged = false;
    }
  }

  filter(key) {
    this.dataservice.filter(key, this.txtSearch);
  }
}
export function noWhitespaceValidator(control: FormControl) {
  const isSpace = (control.value || '').trim().length === 0;
  return isSpace ? {'whitespace': true} : null;
}