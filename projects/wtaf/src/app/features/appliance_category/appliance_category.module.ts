import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { ApplianceCategoryComponent } from './appliance-category/appliance-category.component';
import { ApplianceCategoryRoutingModule } from './appliance_category-routing.module';
@NgModule({
  declarations: [ApplianceCategoryComponent],
  imports: [CommonModule, SharedModule,ApplianceCategoryRoutingModule,Ng2SearchPipeModule]
})
export class ApplianceCategoryModule {}
