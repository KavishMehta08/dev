import { Injectable, Injector, ErrorHandler } from '@angular/core';
import { Router, RouterLink } from '@angular/router';

import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

/** Passes HttpErrorResponse to application-wide error handler */
@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private injector: Injector,
              private router:Router) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    
    return next.handle(request).pipe(
      tap({
        error: (err: any) => {
          if (err instanceof HttpErrorResponse) {
            
            if (err.status == 401) {
              // setTimeout (()=>{
              //   console.log("err.status----------------401",err.status);
              //   this.router.navigate(['/']);
              //   return err; 
              // },900)
             
             new Promise<void>(resolve => setTimeout(() => resolve(), 900)).then(() => {
              console.log("err.status----------------401",err.status);
              this.router.navigate(['/']);
              return err; 
              });
            }
            const appErrorHandler = this.injector.get(ErrorHandler);
            appErrorHandler.handleError(err);
          }
        }
      })
    );
  }
}
