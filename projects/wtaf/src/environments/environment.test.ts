const packageJson = require('../../../../package.json');
const configJson = require('../assets/config/config.json');
export const environment = {
  appName: 'Prodigio',
  envName: 'TEST',
  production: false,
  test: true,
  i18nPrefix: '',
  versions: {
    app: packageJson.version,
    angular: packageJson.dependencies['@angular/core'],
    ngrx: packageJson.dependencies['@ngrx/store'],
    material: packageJson.dependencies['@angular/material'],
    bootstrap: packageJson.dependencies.bootstrap,
    rxjs: packageJson.dependencies.rxjs,
    ngxtranslate: packageJson.dependencies['@ngx-translate/core'],
    fontAwesome:
      packageJson.dependencies['@fortawesome/fontawesome-free-webfonts'],
    angularCli: packageJson.devDependencies['@angular/cli'],
    typescript: packageJson.devDependencies['typescript'],
    cypress: packageJson.devDependencies['cypress']
  },
  urlConfig: {
    baseUrl: configJson.baseUrl,
    jenkinsUrl: configJson.jenkinsUrl,
    headspinBaseUrl :configJson.headspinBaseUrl,
    JenkinsWorkspace_Url:configJson.JenkinsWorkspace_Url ,
    envValue :configJson.envValue ,
    headspinAPIToken : configJson.headspinAPIToken


  },
  AwsConfig: {
    ProjectArn: configJson.ProjectArn,
    IdentityPoolId: configJson.IdentityPoolId,
    accessKeyId: configJson.accessKeyId,
    secretAccessKey: configJson.secretAccessKey,
    DF_region: configJson.DF_region,
    testSpecArnAndroid: configJson.testSpecArnAndroid,
    testSpecArnIOS:configJson.testSpecArnIOS  }
};
